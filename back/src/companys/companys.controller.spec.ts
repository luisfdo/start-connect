import { Test, TestingModule } from '@nestjs/testing';
import { CompanysController } from './companys.controller';

describe('Companys Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [CompanysController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: CompanysController = module.get<CompanysController>(CompanysController);
    expect(controller).toBeDefined();
  });
});
