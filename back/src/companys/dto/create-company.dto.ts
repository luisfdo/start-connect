export class CreateCompanyDTO {
    readonly name: string;
    readonly nit: number;
    readonly description: string;
}