import { Controller, Get, Param, UseGuards, Post, Body, Delete, Query, Res, HttpStatus, NotFoundException, Put } from '@nestjs/common';
import { CompanysService } from './companys.service';
import { CreateCompanyDTO } from './dto/create-company.dto';
import { ValidateObjectId } from './../shared/pipes/validate-object-id.pipe';
import { AuthGuard } from '../shared/auth.gaurd';
// import { RolsService } from '../rols/rols.service';

@Controller('companys')
@UseGuards(new AuthGuard())
export class CompanysController {

    constructor(
            private companySvc: CompanysService,
            // private rol: RolsService
            ) {}

    @Get()
    async getCompanys(@Res() res) {

        const companys = await this.companySvc.getCompanys();
        return res.status(HttpStatus.OK).json(companys);

    }

    @Get(':companyID')
    async getCompany(@Res() res, @Param('companyID', new ValidateObjectId()) companyID) {

        const fetchedCompany = await this.companySvc.getCompany(companyID);
        if (!fetchedCompany) {
            throw new NotFoundException('Company does not exist!');
        }
        return res.status(HttpStatus.OK).json(fetchedCompany);
    }

    @Post()
    async addCompany(@Res() res, @Body() createCompanyDTO: CreateCompanyDTO) {
        const addedCompany = await this.companySvc.addCompany(createCompanyDTO);
        return res.status(HttpStatus.OK).json({
            message: 'Company has been successfully added!',
            company: addedCompany,
        });
    }

    @Put()
    async updateCompany(
        @Res() res,
        @Query('companyID', new ValidateObjectId()) companyID,
        @Body() createCompanyDTO: CreateCompanyDTO) {
            const updatedCompany = await this.companySvc.updateCompany(companyID, createCompanyDTO);
            if (!updatedCompany) {
                throw new NotFoundException('Company does not exist!');
            }
            return res.status(HttpStatus.OK).json({
                message: 'Company has been successfully updated!',
                company: updatedCompany,
            });
    }

    @Delete()
    async deleteCompany(@Res() res, @Query('companyID', new ValidateObjectId()) companyID) {
        const deletedCompany = await this.companySvc.deleteCompany(companyID);
        if (!deletedCompany) {
            throw new NotFoundException('Company Does not exist!');
        }


        // const deletedRols = await this.rolSvc.getRolCompany(companyID);

        // console.log('eeeeeeeeeeeeeeeeee');
        // console.log(deletedRols);


        return res.status(HttpStatus.OK).json({
            message: 'Company has been successfully deleted!',
            // company: deletedCompany,
            company: 'deletedCompany',

        });
    }
}
