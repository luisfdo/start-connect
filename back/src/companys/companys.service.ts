import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Company } from './interfaces/company.interface';
import { CreateCompanyDTO } from './dto/create-company.dto';

@Injectable()
export class CompanysService {
    constructor(@InjectModel('Company') private readonly companyModel: Model<Company>){}

    async getCompanys(): Promise<Company[]> {
        const companys = await this.companyModel.find().exec();
        return companys;
    }

    async getCompany(companyID): Promise<Company> {
        const fetchedCompany = await this.companyModel
            .findById(companyID)
            .exec();
        return fetchedCompany;
    }

    async addCompany(createCompanyDTO: CreateCompanyDTO): Promise<Company> {
       const addedCompany = await this.companyModel(createCompanyDTO);
       return addedCompany.save();
    }

    async updateCompany(companyID, createCompanyDTO: CreateCompanyDTO): Promise<Company> {
        const updatedCompany = await this.companyModel
            .findByIdAndUpdate(companyID, createCompanyDTO, {new: true});
        return updatedCompany;
    }

    async deleteCompany(companyID): Promise<Company> {
        const deletedCompany = await this.companyModel
            .findByIdAndRemove(companyID);
        return deletedCompany;
    }
}
