import { Module } from '@nestjs/common';
import { CompanysController } from './companys.controller';
import { CompanysService } from './companys.service';
// import { RolsModule } from '../rols/rols.module';
import { MongooseModule } from '@nestjs/mongoose';
import { CompanySchema } from './schemas/company.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{name: 'Company', schema: CompanySchema}]),
    // RolsModule
  ],
  controllers: [CompanysController],
  providers: [CompanysService],
  exports:[CompanysService]
})
export class CompanysModule {}
