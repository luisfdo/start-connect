import * as mongoose from 'mongoose';

export const CustomerSchema = new mongoose.Schema({
     name: String,
     identification: String,
     country: String,
     state: String,
     city: String,
     address: String,
     phone: String,
     email: String,
     company: mongoose.Schema.Types.ObjectId
});