import { Document } from 'mongoose';

export interface Customer extends Document {
  
    readonly name: string;
    readonly identification: string;
    readonly country: string;
    readonly state: string;
    readonly city: string;
    readonly address: string;
    readonly phone: string;
    readonly email: string;
    readonly company: string;
}