import { Controller, Get, Param, UseGuards, Post, Body, Delete, Query, Res, HttpStatus, NotFoundException, Put } from '@nestjs/common';
import { CustomersService } from './customers.service';
import { CreateCustomerDTO } from './dto/create-customer.dto';
import { ValidateObjectId } from './../shared/pipes/validate-object-id.pipe';
import { AuthGuard } from '../shared/auth.gaurd';
import { QuotesService } from '../quotes/quotes.service';
import { BillingsService } from '../billings/billings.service';



@Controller('customers')
@UseGuards(new AuthGuard())
export class CustomersController {

    constructor(
        private customerSvc: CustomersService,
        private quoSvc: QuotesService,
        private billSvc: BillingsService,
        
        ) {}

    @Get()
    async getCustomers(@Res() res) {

        const customers = await this.customerSvc.getCustomers();
        return res.status(HttpStatus.OK).json(customers);
    }

    @Get(':customerID')
    async getCustomer(@Res() res, @Param('customerID', new ValidateObjectId()) customerID) {

        var status:Boolean = false;
        var response;
        response = await this.customerSvc.getCustomer(customerID);
        
        if(!response){
            status = true;
        }

        if(status){
            response = await this.customerSvc.getCustomerCompany(customerID);
        }

        if(!response){
            throw new NotFoundException('Customer does not exist!');
        }
   
        return res.status(HttpStatus.OK).json(response);
        
    }


    @Post()
    async addCustomer(@Res() res, @Body() createCustomerDTO: CreateCustomerDTO) {

        const searchCustomer = await this.customerSvc.getCustomerForIdentification(createCustomerDTO.identification);
      
        if(!searchCustomer[0]){
            const addedCustomer = await this.customerSvc.addCustomer(createCustomerDTO);
            return res.status(HttpStatus.OK).json({
                message: 200,
                customer: addedCustomer,
            });
        }else{
            return res.status(HttpStatus.OK).json({
                message: 200,
                customer:{exist:true},
            });
        }
        
    }

    @Put()
    async updateCustomer(
        @Res() res,
        @Query('customerID', new ValidateObjectId()) customerID,
        @Body() createCustomerDTO: CreateCustomerDTO) {
            const updatedCustomer = await this.customerSvc.updateCustomer(customerID, createCustomerDTO);
            if (!updatedCustomer) {
                throw new NotFoundException('Customer does not exist!');
            }
            return res.status(HttpStatus.OK).json({
                message: 'Customer has been successfully updated!',
                customer: updatedCustomer,
            });
    }

    @Delete()
    async deleteCustomer(@Res() res, @Query('customerID', new ValidateObjectId()) customerID) {
        var resp : any;
        var respQuote:any;
        var respBilling:any;

        const deletedCustomer = await this.customerSvc.deleteCustomer(customerID);
        resp = deletedCustomer

        if(!deletedCustomer){
            throw new NotFoundException('Customer Does not exist!');
        }

        const searchQuote = await this.quoSvc.searchQuoteCustomer(resp._id);

        if(searchQuote[0]){
            respQuote = searchQuote;
            const deletedQuote = await this.quoSvc.deleteQuote(respQuote[0]._id);
            if (!deletedQuote) {
                throw new NotFoundException('Quote Does not exist!');
            }
        }

        const searchBilling = await this.billSvc.searchBillingCustomer(resp._id);

        if(searchBilling[0]){
            respBilling = searchBilling;
            const deletedBilling = await this.billSvc.deleteBilling(respBilling[0]._id);
            if (!deletedBilling) {
                throw new NotFoundException('Billing Does not exist!');
            }
        }


        return res.status(HttpStatus.OK).json({
            message: 'Customer has been successfully deleted!',
            customer: deletedCustomer,
        });
    }
}
