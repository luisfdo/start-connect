import { Module } from '@nestjs/common';
import { CustomersController } from './customers.controller';
import { CustomersService } from './customers.service';
import { MongooseModule } from '@nestjs/mongoose';
import { CustomerSchema } from './schemas/customer.schema';
import { QuotesModule } from '../quotes/quotes.module';
import { BillingsModule } from '../billings/billings.module';

@Module({
    imports: [
        MongooseModule.forFeature([{name: 'Customer', schema: CustomerSchema}]),
        QuotesModule,
        BillingsModule
      ],
      controllers: [CustomersController],
      providers: [CustomersService],
      exports: [CustomersService]

})
export class CustomersModule {}
