import * as mongoose from 'mongoose';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Customer } from './interfaces/customer.interface';
import { CreateCustomerDTO } from './dto/create-customer.dto';

@Injectable()
export class CustomersService {
    constructor(@InjectModel('Customer') private readonly customerModel: Model<Customer>){}

    async getCustomers(): Promise<Customer[]> {
        const customers = await this.customerModel.find().exec();
        return customers;
    }

    async getCustomer(customerID): Promise<Customer> {
        const fetchedCustomer = await this.customerModel
            .findById(customerID)
            .exec();
        return fetchedCustomer;
    }

    async getCustomerCompany(companyID): Promise<Customer> {
        let _query = {company: new mongoose.Types.ObjectId(companyID)};
        const fetchedCustomer = await this.customerModel
            .find(_query)
            .exec();
        return fetchedCustomer;
    }

    async getCustomerForIdentification(identification): Promise<Customer> {
        let _query = {identification: identification};
        const fetchedCustomer = await this.customerModel
            .find(_query)
            .exec();
        return fetchedCustomer;
    }

    async addCustomer(createCustomerDTO: CreateCustomerDTO): Promise<Customer> {
       const addedCustomer = await this.customerModel(createCustomerDTO);
       return addedCustomer.save();
    }

    async updateCustomer(customerID, createCustomerDTO: CreateCustomerDTO): Promise<Customer> {
        const updatedCustomer = await this.customerModel
            .findByIdAndUpdate(customerID, createCustomerDTO, {new: true});
        return updatedCustomer;
    }

    async deleteCustomer(customerID): Promise<Customer> {
        const deletedCustomer = await this.customerModel
            .findByIdAndRemove(customerID);
        return deletedCustomer;
    }

    
}
