import { Document } from 'mongoose';

export interface Payment extends Document {
    readonly subscription_id: string;
    readonly status: string;
    readonly user: string;
}