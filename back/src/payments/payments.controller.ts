import { Controller, Get, Param, UseGuards, Post, Body, Delete, Query, Res, HttpStatus, NotFoundException, Put } from '@nestjs/common';
import { PaymentsService } from './payments.service';
import { CreatePaymentDTO } from './dto/create-payment.dto';
import { ValidateObjectId } from './../shared/pipes/validate-object-id.pipe';
import { AuthGuard } from '../shared/auth.gaurd';
import { CompanysService } from '../companys/companys.service';

@Controller('payments')
@UseGuards(new AuthGuard())
export class PaymentsController {

    constructor(
        private paymentSvc: PaymentsService,
        private comSvc: CompanysService
        ) {}

    @Get()
    async getPayments(@Res() res) {

        const payments = await this.paymentSvc.getPayments();
        return res.status(HttpStatus.OK).json(payments);
    }

    @Get(':paymentID')
    async getPayment(@Res() res, @Param('paymentID', new ValidateObjectId()) paymentID) {

        var status:Boolean = false;
        var response;
        response = await this.paymentSvc.getPayment(paymentID);
        
        if(!response){
            status = true;
        }

        if(!response){
            throw new NotFoundException('Payment does not exist!');
        }
   
        return res.status(HttpStatus.OK).json(response);
        
    }


   

   

    
}
