import { Module } from '@nestjs/common';
import { PaymentsController } from './payments.controller';
import { PaymentsService } from './payments.service';
import { MongooseModule } from '@nestjs/mongoose';
import { PaymentSchema } from './schemas/payment.schema';
import { CompanysModule } from '../companys/companys.module';

@Module({
    imports: [
        MongooseModule.forFeature([{name: 'Payment', schema: PaymentSchema}]),
        CompanysModule
      ],
      controllers: [PaymentsController],
      providers: [PaymentsService],
      exports:[PaymentsService]
})
export class PaymentsModule {}
