import * as mongoose from 'mongoose';

export const PaymentSchema = new mongoose.Schema({
     subscription_id: String,
     status: String,
     user: mongoose.Schema.Types.ObjectId
});