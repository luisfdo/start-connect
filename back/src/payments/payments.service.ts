import * as mongoose from 'mongoose';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Payment } from './interfaces/payment.interface';
import { CreatePaymentDTO } from './dto/create-payment.dto';

@Injectable()
export class PaymentsService {
    constructor(@InjectModel('Payment') private readonly paymentModel: Model<Payment>){}

    async getPayments(): Promise<Payment[]> {
        const payments = await this.paymentModel.find().exec();
        return payments;
    }

    async getPayment(paymentID): Promise<Payment> {
        const fetchedPayment = await this.paymentModel
            .findById(paymentID)
            .exec();
        return fetchedPayment;
    }


    async addPaymentSubscription(createPaymentDTO: CreatePaymentDTO): Promise<Payment> {

       const addedPayment = await this.paymentModel(createPaymentDTO);
       return addedPayment.save();
    }

   

    
}
