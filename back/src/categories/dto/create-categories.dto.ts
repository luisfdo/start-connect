export class CreateCategoryDTO {
    readonly title: string;
    readonly description: string;
    readonly company: string;
}
