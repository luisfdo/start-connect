import { Module } from '@nestjs/common';
import { CategoriesController } from './categories.controller';
import { CategoriesService } from './categories.service';
import { MongooseModule } from '@nestjs/mongoose';
import { CategorySchema } from './schemas/categories.schema';
import { CompanysModule } from '../companys/companys.module';

@Module({
    imports: [
        MongooseModule.forFeature([{name: 'Category', schema: CategorySchema}]),
        CompanysModule
      ],
      controllers: [CategoriesController],
      providers: [CategoriesService],
    //   exports:[RolsService]
})
export class CategoriesModule {}
