import * as mongoose from 'mongoose';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Category } from './interfaces/categories.interface';
import { CreateCategoryDTO } from './dto/create-categories.dto';

@Injectable()
export class CategoriesService {
    constructor(@InjectModel('Category') private readonly categoryModel: Model<Category>){}

    async getCategories(): Promise<Category[]> {
        const categories = await this.categoryModel.find().exec();
        return categories;
    }

    async getCategory(categoryID): Promise<Category> {
        const fetchedCategory = await this.categoryModel
            .findById(categoryID)
            .exec();
        return fetchedCategory;
    }

    async getCategoryCompany(companyID): Promise<Category> {
        let _query = {company: new mongoose.Types.ObjectId(companyID)};
        const fetchedCategory = await this.categoryModel
            .find(_query)
            .exec();
        return fetchedCategory;
    }

    async addCategory(createCategoryDTO: CreateCategoryDTO): Promise<Category> {
       const addedCategory = await this.categoryModel(createCategoryDTO);
       return addedCategory.save();
    }

    async updateCategory(categoryID, createCategoryDTO: CreateCategoryDTO): Promise<Category> {
        const updatedCategory = await this.categoryModel
            .findByIdAndUpdate(categoryID, createCategoryDTO, {new: true});
        return updatedCategory;
    }

    async deleteCategory(categoryID): Promise<Category> {
        const deletedCategory = await this.categoryModel
            .findByIdAndRemove(categoryID);
        return deletedCategory;
    }

    
}
