import { Controller, Get, Param, UseGuards, Post, Body, Delete, Query, Res, HttpStatus, NotFoundException, Put } from '@nestjs/common';
import { CategoriesService } from './categories.service';
import { CreateCategoryDTO } from './dto/create-categories.dto';
import { ValidateObjectId } from './../shared/pipes/validate-object-id.pipe';
import { AuthGuard } from '../shared/auth.gaurd';
import { CompanysService } from '../companys/companys.service';

@Controller('categories')
@UseGuards(new AuthGuard())
export class CategoriesController {

    constructor(
        private categorySvc: CategoriesService,
        private comSvc: CompanysService
        ) {}

    @Get()
    async getCategories(@Res() res) {

        const categories = await this.categorySvc.getCategories();
        return res.status(HttpStatus.OK).json(categories);
    }

    @Get(':categoryID')
    async getCategory(@Res() res, @Param('categoryID', new ValidateObjectId()) categoryID) {

        var status:Boolean = false;
        var response;
        response = await this.categorySvc.getCategory(categoryID);
        
        if(!response){
            status = true;
        }

        if(status){
            response = await this.categorySvc.getCategoryCompany(categoryID);
        }

        if(!response){
            throw new NotFoundException('Category does not exist!');
        }
   
        return res.status(HttpStatus.OK).json(response);
        
    }


    @Post()
    async addCategory(@Res() res, @Body() createCategoryDTO: CreateCategoryDTO) {
        const addedCategory = await this.categorySvc.addCategory(createCategoryDTO);
        return res.status(HttpStatus.OK).json({
            message: 'Category has been successfully added!',
            rol: addedCategory,
        });
    }

    @Put()
    async updateCategory(
        @Res() res,
        @Query('categoryID', new ValidateObjectId()) categoryID,
        @Body() createCategoryDTO: CreateCategoryDTO) {
            const updatedCategory = await this.categorySvc.updateCategory(categoryID, createCategoryDTO);
            if (!updatedCategory) {
                throw new NotFoundException('Category does not exist!');
            }
            return res.status(HttpStatus.OK).json({
                message: 'Category has been successfully updated!',
                rol: updatedCategory,
            });
    }

    @Delete()
    async deleteCategory(@Res() res, @Query('categoryID', new ValidateObjectId()) categoryID) {
        const deletedCategory = await this.categorySvc.deleteCategory(categoryID);
        if (!deletedCategory) {
            throw new NotFoundException('Category Does not exist!');
        }
        return res.status(HttpStatus.OK).json({
            message: 'Category has been successfully deleted!',
            rol: deletedCategory,
        });
    }
}
