import * as mongoose from 'mongoose';

export const BankSchema = new mongoose.Schema({
     typeAccount: String,
     nameAccount: String,
     numberAccount: Number,
     beginningBalance: Number,
     date: String,
     description: String,
     company: mongoose.Schema.Types.ObjectId
});