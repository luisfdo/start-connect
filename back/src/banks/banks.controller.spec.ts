import { Test, TestingModule } from '@nestjs/testing';
import { BanksController } from './banks.controller';

describe('Banks Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [BanksController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: BanksController = module.get<BanksController>(BanksController);
    expect(controller).toBeDefined();
  });
});
