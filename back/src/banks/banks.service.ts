import * as mongoose from 'mongoose';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Bank } from './interfaces/bank.interface';
import { CreateBankDTO } from './dto/create-bank.dto';

@Injectable()
export class BanksService {
    constructor(@InjectModel('Bank') private readonly bankModel: Model<Bank>){}

    async getBanks(): Promise<Bank[]> {
        const banks = await this.bankModel.find().exec();
        return banks;
    }

    async getBank(bankID): Promise<Bank> {
        const fetchedBank = await this.bankModel
            .findById(bankID)
            .exec();
        return fetchedBank;
    }

    async getBankCompany(companyID): Promise<Bank> {
        let _query = {company: new mongoose.Types.ObjectId(companyID)};
        const fetchedBank = await this.bankModel
            .find(_query)
            .exec();
        return fetchedBank;
    }

    async addBank(createBankDTO: CreateBankDTO): Promise<Bank> {
       const addedBank = await this.bankModel(createBankDTO);
       return addedBank.save();
    }

    async updateBank(bankID, createBankDTO: CreateBankDTO): Promise<Bank> {
        const updatedBank = await this.bankModel
            .findByIdAndUpdate(bankID, createBankDTO, {new: true});
        return updatedBank;
    }

    async deleteBank(bankID): Promise<Bank> {
        const deletedBank = await this.bankModel
            .findByIdAndRemove(bankID);
        return deletedBank;
    }

    
}
