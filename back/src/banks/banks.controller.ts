import { Controller, Get, Param, UseGuards, Post, Body, Delete, Query, Res, HttpStatus, NotFoundException, Put } from '@nestjs/common';
import { BanksService } from './banks.service';
import { CreateBankDTO } from './dto/create-bank.dto';
import { ValidateObjectId } from './../shared/pipes/validate-object-id.pipe';
import { AuthGuard } from '../shared/auth.gaurd';
import { CompanysService } from '../companys/companys.service';

@Controller('banks')
@UseGuards(new AuthGuard())
export class BanksController {

    constructor(
        private bankSvc: BanksService,
        private comSvc: CompanysService
        ) {}

    @Get()
    async getBanks(@Res() res) {

        const banks = await this.bankSvc.getBanks();
        return res.status(HttpStatus.OK).json(banks);
    }

    @Get(':bankID')
    async getBank(@Res() res, @Param('bankID', new ValidateObjectId()) bankID) {

        var status:Boolean = false;
        var response;
        response = await this.bankSvc.getBank(bankID);
        
        if(!response){
            status = true;
        }

        if(status){
            response = await this.bankSvc.getBankCompany(bankID);
        }

        if(!response){
            throw new NotFoundException('Bank does not exist!');
        }
   
        return res.status(HttpStatus.OK).json(response);
        
    }


    @Post()
    async addBank(@Res() res, @Body() createBankDTO: CreateBankDTO) {
        const addedBank = await this.bankSvc.addBank(createBankDTO);
        return res.status(HttpStatus.OK).json({
            message: 'Bank has been successfully added!',
            bank: addedBank,
        });
    }

    @Put()
    async updateBank(
        @Res() res,
        @Query('bankID', new ValidateObjectId()) bankID,
        @Body() createBankDTO: CreateBankDTO) {
            const updatedBank = await this.bankSvc.updateBank(bankID, createBankDTO);
            if (!updatedBank) {
                throw new NotFoundException('Bank does not exist!');
            }
            return res.status(HttpStatus.OK).json({
                message: 'Bank has been successfully updated!',
                bank: updatedBank,
            });
    }

    @Delete()
    async deleteBank(@Res() res, @Query('bankID', new ValidateObjectId()) bankID) {
        const deletedBank = await this.bankSvc.deleteBank(bankID);
        if (!deletedBank) {
            throw new NotFoundException('Bank Does not exist!');
        }
        return res.status(HttpStatus.OK).json({
            message: 'Bank has been successfully deleted!',
            bank: deletedBank,
        });
    }
}
