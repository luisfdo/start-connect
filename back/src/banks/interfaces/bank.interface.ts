import { Document } from 'mongoose';

export interface Bank extends Document {
    readonly typeAccount: string;
    readonly nameAccount: string;
    readonly numberAccount: number;
    readonly beginningBalance: number;
    readonly date: string;
    readonly description: string;
    readonly company: string;
}