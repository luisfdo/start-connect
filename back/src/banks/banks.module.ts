import { Module } from '@nestjs/common';
import { BanksController } from './banks.controller';
import { BanksService } from './banks.service';
import { MongooseModule } from '@nestjs/mongoose';
import { BankSchema } from './schemas/bank.schema';
import { CompanysModule } from '../companys/companys.module';

@Module({
    imports: [
        MongooseModule.forFeature([{name: 'Bank', schema: BankSchema}]),
        CompanysModule
      ],
      controllers: [BanksController],
      providers: [BanksService],
    //   exports:[RolsService]
})
export class BanksModule {}
