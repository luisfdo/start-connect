import * as mongoose from 'mongoose';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Card } from './interfaces/card.interface';
import { CreateCardDTO } from './dto/create-card.dto';

@Injectable()
export class CardsService {
    constructor(@InjectModel('Card') private readonly cardModel: Model<Card>){}

    async getCards(): Promise<Card[]> {
        const cards = await this.cardModel.find().exec();
        return cards;
    }

    async getCard(cardID): Promise<Card> {
        const fetchedCard = await this.cardModel
            .findById(cardID)
            .exec();
        return fetchedCard;
    }

    async getCardUser(cardIdUser): Promise<Card> {
        let _query = {user: new mongoose.Types.ObjectId(cardIdUser)};
        const fetchedCard = await this.cardModel
            .find(_query)
            .exec();
        return fetchedCard;
    }

   
    async addCard(createCardDTO: CreateCardDTO): Promise<Card> {

        let _query = {number: createCardDTO.number};
        const fetchedCard = await this.cardModel
            .find(_query)
            .exec();        
        
        if(fetchedCard[0]){
            const updatedCard = await this.cardModel
            .findByIdAndUpdate(fetchedCard[0]._id, createCardDTO, {new: true});
            return updatedCard;
        }

        const addedCard = await this.cardModel(createCardDTO);
        return addedCard.save();
       
    }

    async updateCard(cardID, createCardDTO: CreateCardDTO): Promise<Card> {
        const updatedCard = await this.cardModel
            .findByIdAndUpdate(cardID, createCardDTO, {new: true});
        return updatedCard;
    }

    async deleteCard(cardID): Promise<Card> {
        const deletedCard = await this.cardModel
            .findByIdAndRemove(cardID);
        return deletedCard;
    }

    
}
