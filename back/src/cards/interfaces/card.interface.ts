import { Document } from 'mongoose';

export interface Card extends Document {
    readonly number: string;
    readonly exp_year: string;
    readonly exp_month: string;
    readonly cvc: string;
    readonly dateCreation: Date;
    readonly user: string;
}