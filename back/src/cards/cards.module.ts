import { Module } from '@nestjs/common';
import { CardsController } from './cards.controller';
import { CardsService } from './cards.service';
import { MongooseModule } from '@nestjs/mongoose';
import { CardSchema } from './schemas/card.schema';
import { PaymentsModule } from '../payments/payments.module';
import { UsersModule } from '../users/users.module';


@Module({
    imports: [
        MongooseModule.forFeature([{name: 'Card', schema: CardSchema}]),
        PaymentsModule,
        UsersModule
      ],
      controllers: [CardsController],
      providers: [CardsService],
    //   exports:[RolsService]
})
export class CardsModule {}
