import * as mongoose from 'mongoose';

export const CardSchema = new mongoose.Schema({
     number: String,
     exp_year: String,
     exp_month: String,
     cvc: String,
     dateCreation: {type: Date, default: Date.now},
     user: mongoose.Schema.Types.ObjectId

});