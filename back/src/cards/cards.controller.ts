import { Controller, Get, Param, UseGuards, Post, Body, Delete, Query, Res, HttpStatus, NotFoundException, Put } from '@nestjs/common';
import { CardsService } from './cards.service';
import { CreateCardDTO } from './dto/create-card.dto';
import { ValidateObjectId } from './../shared/pipes/validate-object-id.pipe';
import { AuthGuard } from '../shared/auth.gaurd';
import { PaymentsService } from '../payments/payments.service';
import { UsersService } from '../users/users.service';


@Controller('cards')
@UseGuards(new AuthGuard())
export class CardsController {

    constructor(
        private cardSvc: CardsService,
        private paySvc: PaymentsService,
        private userSvc: UsersService,
        ) {}

    @Get()
    async getCards(@Res() res) {

        const cards = await this.cardSvc.getCards();
        return res.status(HttpStatus.OK).json(cards);
    }

    @Get(':cardID')
    async getCard(@Res() res, @Param('cardID', new ValidateObjectId()) cardID) {

        var status:Boolean = false;
        var response;
        response = await this.cardSvc.getCard(cardID);
        
        if(!response){
            status = true;
        }


        if(!response){
            throw new NotFoundException('Card does not exist!');
        }
   
        return res.status(HttpStatus.OK).json(response);
        
    }

    @Get(':cardIdUser')
    async getCardUser(@Res() res, @Param('cardIdUser', new ValidateObjectId()) cardIdUser) {

        var status:Boolean = false;
        var response;
        response = await this.cardSvc.getCardUser(cardIdUser);

        if(!response){
            throw new NotFoundException('Card does not exist!');
        }
   
        return res.status(HttpStatus.OK).json(response);
        
    }


    @Post()
    async addCard(@Res() res, @Body() createCardDTO: CreateCardDTO) {

        var epayco = require('epayco-node')({
            apiKey: '1fe88116fae7a54f691646cbea559942',
            privateKey: 'b88929621200d1b6a581c31cc5edfb86',
            lang: 'ES',
            test: true
        })

        var credit_info = {
            "card[number]": createCardDTO.number,
            "card[exp_year]": createCardDTO.exp_year,
            "card[exp_month]": createCardDTO.exp_month,
            "card[cvc]": createCardDTO.cvc
        }

       
        epayco.token.create(credit_info)
        .then(token => {
                if(token.status){
                    const saveCardData = {
                        number: String(createCardDTO.number), 
                        exp_year:String(createCardDTO.exp_year),
                        exp_month:String(createCardDTO.exp_month),
                        cvc:String(createCardDTO.cvc),
                        token:String(token.id),
                        user:String(createCardDTO.user),
                     }
                    this.saveCard(res, saveCardData)
                }else{
                    throw new NotFoundException();
                }
        })
        .catch(error => {
            console.log('----CARD----');
            console.log(error)
            console.log('----CARD----');
            return res.status(HttpStatus.OK).json({
                message: error,
            });
        });
            
        
    }

    async saveCard(@Res() res, @Body() createCardDTO) {


        const addedCard = await this.cardSvc.addCard(createCardDTO);

        if(!addedCard){
            throw new NotFoundException('Problems!');
        }

        const userData = await this.userSvc.getUser(createCardDTO.user);

        const customer = {
            token_card: createCardDTO.token,
            name: userData.name,
            email: userData.email,
            default: true,
            city: userData.city,
            address: userData.address,
            phone: userData.phone,
            cell_phone: userData.phone
        }

        this.createCustomerEpayco(res, customer, userData.identification, createCardDTO.user);

    }

    async createCustomerEpayco(@Res() res, customer, identification, _id) {
        
        var epayco = require('epayco-node')({
            apiKey: '1fe88116fae7a54f691646cbea559942',
            privateKey: 'b88929621200d1b6a581c31cc5edfb86',
            lang: 'ES',
            test: true
        })

        epayco.customers.create(customer)
            .then(cust => {
                this.createSubscription(res, identification, cust.data.customerId, customer.token_card, 'suscripcin_anual_start_connect', _id);
            })
            .catch(function(err) {
                console.log("err: " + err);
            });

    }

    async createSubscription(@Res() res, identification, customer, token_card, id_plan, _id) {

        var epayco = require('epayco-node')({
            apiKey: '1fe88116fae7a54f691646cbea559942',
            privateKey: 'b88929621200d1b6a581c31cc5edfb86',
            lang: 'ES',
            test: true
        })

        var subscription_info = {
            id_plan: id_plan,
            customer: customer,
            token_card: token_card,
            doc_type: "CC",
            doc_number: String(identification)
        }
        epayco.subscriptions.create(subscription_info)
            .then(subscription => {
                const dataPaySub = {'subscription_id': subscription.id, 'user': _id, 'status': null};
                this.createPayment(res, dataPaySub, subscription_info);
            })
            .catch(function(err) {
                console.log("err: " + err);
            });
    }

    async createPayment(@Res() res, dataPaySub, subscription_info) {


        var epayco = require('epayco-node')({
            apiKey: '1fe88116fae7a54f691646cbea559942',
            privateKey: 'b88929621200d1b6a581c31cc5edfb86',
            lang: 'ES',
            test: true
        })


        /*epayco.subscriptions.list()
        .then(function(subscriptions) {
            epayco.subscriptions.cancel(subscriptions.data[0]._id)
            .then(function(subscription) {
                console.log(subscription);
            })
            .catch(function(err) {
                console.log("err: " + err);
            });
        })
        .catch(function(err) {
            console.log("err: " + err);
        });*/
        
        epayco.subscriptions.charge(subscription_info)
            .then(subscription => {
                
                console.log(subscription);

                if(subscription.subscription.status == 'active'){
                    dataPaySub.status = subscription.subscription.status;
                    this.savePayment(res, dataPaySub);
                    
                }else if(subscription.subscription.status == 'pending'){
                    dataPaySub.status = subscription.subscription.status;
                    this.savePayment(res, dataPaySub);
                }
                
                
                else{
                    console.log('eeeeee');
                    throw new NotFoundException();
                }
                
            })
            .catch(error => {
                return res.status(HttpStatus.OK).json({
                    message: error,
                });
            });

        
    }
    
 

    async savePayment(@Res() res, dataPaySub) {

        const addedSubscription = await this.paySvc.addPaymentSubscription(dataPaySub);
        return res.status(HttpStatus.OK).json({
            message: 'Subscription has been successfully added!',
            subscription: dataPaySub,
        });

    }

}
