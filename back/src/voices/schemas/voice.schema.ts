import * as mongoose from 'mongoose';

export const VoiceSchema = new mongoose.Schema({
     callId: String,
     from: String,
     to: String,
     body: String,
     cost: String,
     createdAt: String,
     bells:mongoose.Schema.Types.ObjectId,
     company: mongoose.Schema.Types.ObjectId
});