export class CreateVoiceDTO {
   
    readonly callId: string;
    readonly from: string;
    readonly to: string;
    readonly body: string;
    readonly cost: string;
    readonly createdAt: string;
    readonly bells:string;
    readonly company: string;



}
