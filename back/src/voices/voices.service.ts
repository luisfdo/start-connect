import * as mongoose from 'mongoose';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Voice } from './interfaces/voice.interface';
import { CreateVoiceDTO } from './dto/create-voice.dto';

@Injectable()
export class VoicesService {
    constructor(@InjectModel('Voice') private readonly voiceModel: Model<Voice>){}

    async getVoices(): Promise<Voice[]> {
        const voices = await this.voiceModel.find().exec();
        return voices;
    }

    async getVoice(voiceID): Promise<Voice> {
        const fetchedVoice = await this.voiceModel
            .findById(voiceID)
            .exec();
        return fetchedVoice;
    }

    async getVoiceCompany(companyID): Promise<Voice> {
        let _query = {company: new mongoose.Types.ObjectId(companyID)};
        const fetchedVoice = await this.voiceModel
            .find(_query)
            .exec();
        return fetchedVoice;
    }

    async addVoice(createVoiceDTO: CreateVoiceDTO): Promise<Voice> {

       const addedVoice = await this.voiceModel(createVoiceDTO);
       return addedVoice.save();
    }

    async updateVoice(voiceID, createVoiceDTO: CreateVoiceDTO): Promise<Voice> {
        const updatedVoice = await this.voiceModel
            .findByIdAndUpdate(voiceID, createVoiceDTO, {new: true});
        return updatedVoice;
    }

    async deleteVoice(voiceID): Promise<Voice> {
        const deletedVoice = await this.voiceModel
            .findByIdAndRemove(voiceID);
        return deletedVoice;
    }

    
}
