import { Controller, Get, Param, UseGuards, Post, Body, Delete, Query, Res, HttpStatus, NotFoundException, Put } from '@nestjs/common';
import { VoicesService } from './voices.service';
import { CreateVoiceDTO } from './dto/create-voice.dto';
import { ValidateObjectId } from './../shared/pipes/validate-object-id.pipe';
import { AuthGuard } from '../shared/auth.gaurd';
// import { CompanysService } from '../companys/companys.service';

@Controller('voices')
@UseGuards(new AuthGuard())
export class VoicesController {

    constructor(
        private voiceSvc: VoicesService,
        // private comSvc: CompanysService
        ) {}

    @Get()
    async getVoices(@Res() res) {

        const voices = await this.voiceSvc.getVoices();
        return res.status(HttpStatus.OK).json(voices);
    }

    @Get(':voiceID')
    async getVoice(@Res() res, @Param('voiceID', new ValidateObjectId()) voiceID) {

        var status:Boolean = false;
        var response;
        response = await this.voiceSvc.getVoice(voiceID);
        
        if(!response){
            status = true;
        }

        if(status){
            response = await this.voiceSvc.getVoiceCompany(voiceID);
        }

        if(!response){
            throw new NotFoundException('Voice does not exist!');
        }
        return res.status(HttpStatus.OK).json(response);
    }


    @Post()
    async addVoice(@Res() res, @Body() createVoiceDTO: CreateVoiceDTO) {
      
      const accountSid = 'AC26894181f91e6fb2e75ef0291f0c6eb9'; 
      const authToken = '6f62c6ca7c244004864428612902c172'; 
      const client = require('twilio')(accountSid, authToken); 
 
        client.calls
        .create({ 
            url: 'https://www.somosstart.com.co/voice.xml',
            from: createVoiceDTO.from,       
            to: createVoiceDTO.to
        }) 
        .then(call => {
        console.log(call); 
        const saveVoice = {
            'callId': call._solution.sid,
            'from' : call.from,
            'to' : call.to,
            'body' : call.body,
            'cost' : call.price,
            'createdAt' : call.dateCreated,
            'bells' : createVoiceDTO.bells,
            'company': createVoiceDTO.company
        }
        this.addVoiceSave(res, saveVoice)
        })
        .catch(error => {
            console.log(error);
        });
     
     

    }

    @Post()
    async addVoiceSave(@Res() res, @Body() createVoiceDTO: CreateVoiceDTO) {
        
        const addedVoice = await this.voiceSvc.addVoice(createVoiceDTO);
        return res.status(HttpStatus.OK).json({
            message: 'Voice has been successfully added!',
            voice: addedVoice,
        });
    }

    @Put()
    async updateVoice(
        @Res() res,
        @Query('voiceID', new ValidateObjectId()) voiceID,
        @Body() createVoiceDTO: CreateVoiceDTO) {
            const updatedVoice = await this.voiceSvc.updateVoice(voiceID, createVoiceDTO);
            if (!updatedVoice) {
                throw new NotFoundException('Voice does not exist!');
            }
            return res.status(HttpStatus.OK).json({
                message: 'Voice has been successfully updated!',
                voice: updatedVoice,
            });
    }

    @Delete()
    async deleteVoice(@Res() res, @Query('voiceID', new ValidateObjectId()) voiceID) {
        const deletedVoice = await this.voiceSvc.deleteVoice(voiceID);
        if (!deletedVoice) {
            throw new NotFoundException('Voice Does not exist!');
        }
        return res.status(HttpStatus.OK).json({
            message: 'Voice has been successfully deleted!',
            voice: deletedVoice,
        });
    }
}
