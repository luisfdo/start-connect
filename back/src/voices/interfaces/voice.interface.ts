import { Document } from 'mongoose';

export interface Voice extends Document {
    
    readonly callId: string;
    readonly from: string;
    readonly to: string;
    readonly body: string;
    readonly cost: string;
    readonly createdAt: string;
    readonly bells:string;
    readonly company: string;
}