import { Test, TestingModule } from '@nestjs/testing';
import { VoicesController } from './voices.controller';

describe('Voices Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [VoicesController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: VoicesController = module.get<VoicesController>(VoicesController);
    expect(controller).toBeDefined();
  });
});
