import { Module } from '@nestjs/common';
import { VoicesController } from './voices.controller';
import { VoicesService } from './voices.service';
import { MongooseModule } from '@nestjs/mongoose';
import { VoiceSchema } from './schemas/voice.schema';
import { CompanysModule } from '../companys/companys.module';

@Module({
    imports: [
        MongooseModule.forFeature([{name: 'Voice', schema: VoiceSchema}]),
        CompanysModule
      ],
      controllers: [VoicesController],
      providers: [VoicesService,]
})
export class VoicesModule {}
