import { Document } from 'mongoose';

export interface Plan extends Document {
    readonly name: string;
    readonly maxRise: string;
    readonly uploadUnit: string;
    readonly maxDesc: string;
    readonly descentUnit: string;
    readonly frequency: string;
    readonly company: string;
}