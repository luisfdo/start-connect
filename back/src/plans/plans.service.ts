import * as mongoose from 'mongoose';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Plan } from './interfaces/plan.interface';
import { CreatePlanDTO } from './dto/create-plan.dto';

@Injectable()
export class PlansService {
    constructor(@InjectModel('Plan') private readonly planModel: Model<Plan>){}

    async getPlans(): Promise<Plan[]> {
        const plans = await this.planModel.find().exec();
        return plans;
    }

    async getPlan(planID): Promise<Plan> {
        const fetchedPlan = await this.planModel
            .findById(planID)
            .exec();
        return fetchedPlan;
    }

    async getPlanCompany(companyID): Promise<Plan> {
        let _query = {company: new mongoose.Types.ObjectId(companyID)};
        const fetchedPlan = await this.planModel
            .find(_query)
            .exec();
        return fetchedPlan;
    }

    async addPlan(createPlanDTO: CreatePlanDTO): Promise<Plan> {
       const addedPlan = await this.planModel(createPlanDTO);
       return addedPlan.save();
    }

    async updatePlan(planID, createPlanDTO: CreatePlanDTO): Promise<Plan> {
        const updatedPlan = await this.planModel
            .findByIdAndUpdate(planID, createPlanDTO, {new: true});
        return updatedPlan;
    }

    async deletePlan(planID): Promise<Plan> {
        const deletedPlan = await this.planModel
            .findByIdAndRemove(planID);
        return deletedPlan;
    }

    
}
