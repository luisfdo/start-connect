import { Module } from '@nestjs/common';
import { PlansController } from './plans.controller';
import { PlansService } from './plans.service';
import { MongooseModule } from '@nestjs/mongoose';
import { PlanSchema } from './schemas/plan.schema';
import { CompanysModule } from '../companys/companys.module';

@Module({
    imports: [
        MongooseModule.forFeature([{name: 'Plan', schema: PlanSchema}]),
        CompanysModule
      ],
      controllers: [PlansController],
      providers: [PlansService],
      exports:[PlansService]
})
export class PlansModule {}
