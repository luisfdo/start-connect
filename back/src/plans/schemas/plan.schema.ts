import * as mongoose from 'mongoose';

export const PlanSchema = new mongoose.Schema({
     name: String,
     maxRise: String,
     uploadUnit: String,
     maxDesc: String,
     descentUnit: String,
     frequency: String,
     company: mongoose.Schema.Types.ObjectId
});