import { Test, TestingModule } from '@nestjs/testing';
import { PlansController } from './plans.controller';

describe('Plans Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [PlansController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: PlansController = module.get<PlansController>(PlansController);
    expect(controller).toBeDefined();
  });
});
