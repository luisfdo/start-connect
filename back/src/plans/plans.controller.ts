import { Controller, Get, Param, UseGuards, Post, Body, Delete, Query, Res, HttpStatus, NotFoundException, Put } from '@nestjs/common';
import { PlansService } from './plans.service';
import { CreatePlanDTO } from './dto/create-plan.dto';
import { ValidateObjectId } from './../shared/pipes/validate-object-id.pipe';
import { AuthGuard } from '../shared/auth.gaurd';
import { CompanysService } from '../companys/companys.service';

@Controller('plans')
@UseGuards(new AuthGuard())
export class PlansController {

    constructor(
        private planSvc: PlansService,
        private comSvc: CompanysService
        ) {}

    @Get()
    async getPlans(@Res() res) {

        const plans = await this.planSvc.getPlans();
        return res.status(HttpStatus.OK).json(plans);
    }

    @Get(':planID')
    async getPlan(@Res() res, @Param('planID', new ValidateObjectId()) planID) {

        console.log('LLEGAMOS!!!!!');

        var status:Boolean = false;
        var response;
        response = await this.planSvc.getPlan(planID);
        
        if(!response){
            status = true;
        }

        if(status){
            response = await this.planSvc.getPlanCompany(planID);
        }

        if(!response){
            throw new NotFoundException('Plan does not exist!');
        }
   
        return res.status(HttpStatus.OK).json(response);
        
    }

    /*@Get(':planID')
    async getPlan(@Res() res, @Param('planID', new ValidateObjectId()) companyID) {

        const fetchedCompany = await this.planSvc.getPlan(companyID);
        if (!fetchedCompany) {
            throw new NotFoundException('Plan does not exist!');
        }
        return res.status(HttpStatus.OK).json(fetchedCompany);
    }*/


    @Post()
    async addPlan(@Res() res, @Body() createPlanDTO: CreatePlanDTO) {
        const addedPlan = await this.planSvc.addPlan(createPlanDTO);
        return res.status(HttpStatus.OK).json({
            message: 'Plan has been successfully added!',
            plan: addedPlan,
        });
    }

    @Put()
    async updatePlan(
        @Res() res,
        @Query('planID', new ValidateObjectId()) planID,
        @Body() createPlanDTO: CreatePlanDTO) {
            const updatedPlan = await this.planSvc.updatePlan(planID, createPlanDTO);
            if (!updatedPlan) {
                throw new NotFoundException('Plan does not exist!');
            }
            return res.status(HttpStatus.OK).json({
                message: 'Plan has been successfully updated!',
                bank: updatedPlan,
            });
    }

    @Delete()
    async deletePlan(@Res() res, @Query('planID', new ValidateObjectId()) planID) {
        const deletedPlan = await this.planSvc.deletePlan(planID);
        if (!deletedPlan) {
            throw new NotFoundException('Plan Does not exist!');
        }
        return res.status(HttpStatus.OK).json({
            message: 'Plan has been successfully deleted!',
            bank: deletedPlan,
        });
    }
}
