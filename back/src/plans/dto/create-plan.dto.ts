export class CreatePlanDTO {
    readonly name: string;
    readonly maxRise: string;
    readonly uploadUnit: string;
    readonly maxDesc: string;
    readonly descentUnit: string;
    readonly frequency: string;
    readonly company: string;
}
