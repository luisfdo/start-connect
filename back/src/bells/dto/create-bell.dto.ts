export class CreateBellDTO {
    readonly name: string;
    readonly date_start: string;
    readonly message:string;
    readonly country: string;
    readonly company: string;
    readonly status: number;
}
