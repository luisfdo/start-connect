import * as mongoose from 'mongoose';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Bell } from './interfaces/bell.interface';
import { CreateBellDTO } from './dto/create-bell.dto';

@Injectable()
export class BellsService {
    constructor(@InjectModel('Bell') private readonly bellModel: Model<Bell>){}

    async getBells(): Promise<Bell[]> {
        const bells = await this.bellModel.find().exec();
        return bells;
    }

    async getBell(bellID): Promise<Bell> {
        const fetchedBell = await this.bellModel
            .findById(bellID)
            .exec();
        return fetchedBell;
    }

    async getBellCompany(companyID): Promise<Bell> {
        let _query = {company: new mongoose.Types.ObjectId(companyID)};
        const fetchedBell = await this.bellModel
            .find(_query)
            .exec();
        return fetchedBell;
    }

    async addBell(createBellDTO: CreateBellDTO): Promise<Bell> {
       const addedBell = await this.bellModel(createBellDTO);
       return addedBell.save();
    }

    async updateBell(bellID, createBellDTO: CreateBellDTO): Promise<Bell> {
        const updatedBell = await this.bellModel
            .findByIdAndUpdate(bellID, createBellDTO, {new: true});
        return updatedBell;
    }

    async deleteBell(bellID): Promise<Bell> {
        const deletedBell = await this.bellModel
            .findByIdAndRemove(bellID);
        return deletedBell;
    }

    
}
