import { Controller, Get, Param, UseGuards, Post, Body, Delete, Query, Res, HttpStatus, NotFoundException, Put } from '@nestjs/common';
import { BellsService } from './bells.service';
import { CreateBellDTO } from './dto/create-bell.dto';
import { ValidateObjectId } from './../shared/pipes/validate-object-id.pipe';
import { AuthGuard } from '../shared/auth.gaurd';

@Controller('bells')
@UseGuards(new AuthGuard())
export class BellsController {

    constructor(
        private bellSvc: BellsService,
        ) {}

    @Get()
    async getBells(@Res() res) {

        const bells = await this.bellSvc.getBells();
        return res.status(HttpStatus.OK).json(bells);
    }

    @Get(':bellID')
    async getBell(@Res() res, @Param('bellID', new ValidateObjectId()) bellID) {

        var status:Boolean = false;
        var response;
        response = await this.bellSvc.getBell(bellID);
        
        if(!response){
            status = true;
        }

        if(status){
            response = await this.bellSvc.getBellCompany(bellID);
        }

        if(!response){
            throw new NotFoundException('Bell does not exist!');
        }
   
        return res.status(HttpStatus.OK).json(response);
        
    }


    @Post()
    async addBell(@Res() res, @Body() createBellDTO: CreateBellDTO) {
        const addedBell = await this.bellSvc.addBell(createBellDTO);
        return res.status(HttpStatus.OK).json({
            message: 'Bell has been successfully added!',
            rol: addedBell,
        });
    }

    @Put()
    async updateBell(
        @Res() res,
        @Query('bellID', new ValidateObjectId()) bellID,
        @Body() createBellDTO: CreateBellDTO) {
            const updatedBell = await this.bellSvc.updateBell(bellID, createBellDTO);
            if (!updatedBell) {
                throw new NotFoundException('Bell does not exist!');
            }
            return res.status(HttpStatus.OK).json({
                message: 'Bell has been successfully updated!',
                rol: updatedBell,
            });
    }

    @Delete()
    async deleteBell(@Res() res, @Query('bellID', new ValidateObjectId()) bellID) {
        const deletedBell = await this.bellSvc.deleteBell(bellID);
        if (!deletedBell) {
            throw new NotFoundException('Bell Does not exist!');
        }
        return res.status(HttpStatus.OK).json({
            message: 'Bell has been successfully deleted!',
            rol: deletedBell,
        });
    }
}
