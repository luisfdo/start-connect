import { Module } from '@nestjs/common';
import { BellsController } from './bells.controller';
import { BellsService } from './bells.service';
import { MongooseModule } from '@nestjs/mongoose';
import { BellSchema } from './schemas/bell.schema';
import { CompanysModule } from '../companys/companys.module';

@Module({
    imports: [
        MongooseModule.forFeature([{name: 'Bell', schema: BellSchema}]),
        CompanysModule
      ],
      controllers: [BellsController],
      providers: [BellsService],
    //   exports:[RolsService]
})
export class BellsModule {}
