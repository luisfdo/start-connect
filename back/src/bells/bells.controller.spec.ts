import { Test, TestingModule } from '@nestjs/testing';
import { BellsController } from './bells.controller';

describe('Rols Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [BellsController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: BellsController = module.get<BellsController>(BellsController);
    expect(controller).toBeDefined();
  });
});
