import * as mongoose from 'mongoose';

export const BellSchema = new mongoose.Schema({

    name: String,
    date_start: String,
    message:String,
    country: String,
    company: mongoose.Schema.Types.ObjectId,
    status: Number,

});