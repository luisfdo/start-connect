import * as mongoose from 'mongoose';

export const SmsSchema = new mongoose.Schema({
     messageId: String,
     from: String,
     to: String,
     body: String,
     cost: String,
     createdAt: String,
     bells: mongoose.Schema.Types.ObjectId,
     company: mongoose.Schema.Types.ObjectId,
});