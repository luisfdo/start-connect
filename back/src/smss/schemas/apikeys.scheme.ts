import * as mongoose from 'mongoose';

export const APIKeysSchema = new mongoose.Schema({
    apikeyId: { type: String, required: false },
    type: { type: String, required: true },
    name: { type: String, required: true },
    api: { type: String, required: true },
    user: { type: String, required: true },
    passw: { type: String, required: true },
    status: { type: Boolean, required: true },
});

// export const ApiKey = mongoose.model('ApiKeys', APIKeysSchema);
