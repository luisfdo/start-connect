import { Test, TestingModule } from '@nestjs/testing';
import { SmssController } from './smss.controller';

describe('Smss Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [SmssController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: SmssController = module.get<SmssController>(SmssController);
    expect(controller).toBeDefined();
  });
});
