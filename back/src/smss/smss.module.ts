import { Module, MiddlewareConsumer, RequestMethod, HttpModule } from '@nestjs/common';
import { SmssController } from './smss.controller';
import { SmssService } from './smss.service';
import { MongooseModule } from '@nestjs/mongoose';
import { SmsSchema } from './schemas/sms.schema';
import { APIKeysSchema } from './schemas/apikeys.scheme';
import { CompanysModule } from '../companys/companys.module';
import { SmssMiddleware } from './middlewares/smss.middleware';

@Module({
    imports: [
        HttpModule,
        MongooseModule.forFeature([{name: 'Sms', schema: SmsSchema}, {name: 'ApiKeys', schema: APIKeysSchema}]),
        CompanysModule,
      ],
      controllers: [SmssController],
      providers: [SmssService],
    //   exports:[RolsService]
})
export class SmssModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
          .apply(SmssMiddleware)
          .forRoutes({ path: 'smss/addSms', method: RequestMethod.POST });
    }
}
