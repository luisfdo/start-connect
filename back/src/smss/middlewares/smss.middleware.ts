import { Injectable, MiddlewareFunction, NestMiddleware } from '@nestjs/common';
import { Model } from 'mongoose';
import { ApiKeys } from '../interfaces/apiKeys.interface';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class SmssMiddleware implements NestMiddleware {
  constructor(@InjectModel('ApiKeys') private readonly apikeyModel: Model<ApiKeys>){}

  resolve(...args: any[]): MiddlewareFunction {
    return (req, res, next) => {
      try{
        this.apikeyModel.findOne({type: 'sms', status: true}).exec()
        .then((apiKeys) => {
          req.apiKey = apiKeys;
          next();
        });
      } catch (err){
        // tslint:disable-next-line:no-console
        console.log(err);
      }
    };
  }
}