import { Document } from 'mongoose';

export interface ApiKeys extends Document{
    readonly apikeyId: string;
    readonly type: string;
    readonly name: string;
    readonly api: string;
    readonly user: string;
    readonly passw: string;
    readonly status: boolean;
}