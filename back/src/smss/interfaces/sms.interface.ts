import { Document } from 'mongoose';

export interface Sms extends Document {
    readonly messageId: string;
    readonly from: string;
    readonly to: string;
    readonly body: string;
    readonly cost: string;
    readonly createdAt: string;
    readonly bells: string;
    readonly company: string;
}