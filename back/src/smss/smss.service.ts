import * as mongoose from 'mongoose';
import { Injectable, HttpService } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Sms } from './interfaces/sms.interface';
import { CreateSmsDTO } from './dto/create-sms.dto';
import { SMSAldeamoReq } from './models/smsAldeamoReq.model';
import { map, catchError } from 'rxjs/operators';
import { AxiosRequestConfig } from 'axios';

@Injectable()
export class SmssService {
    constructor(@InjectModel('Sms') private readonly smsModel: Model<Sms>,
                private readonly http: HttpService){}

    async getSmss(): Promise<Sms[]> {
        const smss = await this.smsModel.find().exec();
        return smss;
    }

    async getSms(smsID): Promise<Sms> {
        const fetchedSms = await this.smsModel
            .findById(smsID)
            .exec();
        return fetchedSms;
    }

    async getSmsCompany(companyID): Promise<Sms> {
        const _query = {company: new mongoose.Types.ObjectId(companyID)};
        const fetchedSms = await this.smsModel
            .find(_query)
            .exec();
        return fetchedSms;
    }

    async addSms(createSmsDTO: CreateSmsDTO): Promise<Sms> {

       const addedSms = await this.smsModel(createSmsDTO);
       return addedSms.save();
    }

    async updateSms(smsID, createSmsDTO: CreateSmsDTO): Promise<Sms> {
        const updatedSms = await this.smsModel
            .findByIdAndUpdate(smsID, createSmsDTO, {new: true});
        return updatedSms;
    }

    async deleteSms(smsID): Promise<Sms> {
        const deletedSms = await this.smsModel
            .findByIdAndRemove(smsID);
        return deletedSms;
    }

    /**
     * Envio de solicitud de sms a Aldeamo
     * @param aldeamoReq
     */
    sendSms_aldeamo(aldeamoReq: SMSAldeamoReq) {
        const headers = {headers: aldeamoReq.getHeaders()};
        const config = headers as AxiosRequestConfig;

        const params = aldeamoReq.objectService();

        return this.http.post(
                    aldeamoReq.apiKeys.api,
                    params,
                    config,
                )
                .pipe(
                    map((response) => {
                        return response.data;
                    }),
                    catchError((error) => {
                        // tslint:disable-next-line:no-console
                        console.log('ERROR SERVICIO***********************');
                        // tslint:disable-next-line:no-console
                        console.log(error);
                        return error.response;
                    }),
                );
    }

    async sendSms_twillio(): Promise<any> {
        return null;
    }
}
