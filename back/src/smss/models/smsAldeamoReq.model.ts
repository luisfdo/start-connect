import { ApiKeys } from '../interfaces/apiKeys.interface';
import { Base64 } from 'js-base64';

export class SMSAldeamoReq {
    country: number = 57;
    dateToSend: string = '';
    message: string;
    encoding: string;
    messageFormat: number = 1;
    addresseeList: Array<object> = new Array();

    constructor( public apiKeys: ApiKeys) {}

    private getHashAuth(): string {
        const basic = `${this.apiKeys.user}:${this.apiKeys.passw}`;
        return Base64.encode(basic);
    }

    getHeaders() {
        return {
            'Content-Type': 'application/json',
            'Authorization': `Basic ${this.getHashAuth()}`,
        };
    }

    validateCod(mobile: string){
        if (mobile[0] === '+'){
            return mobile.substring(3);
        }
        return mobile;
    }

    addMobile(mobil: string, correlationLabe = '', urls = ''): void {
        const sms = {
            mobile: this.validateCod(mobil),
            correlationLabel: correlationLabe,
            url: urls,
        };
        this.addresseeList.push(sms);
    }

    objectService() {
        const obj = {
            country: this.country,
            dateToSend: this.dateToSend,
            message: this.message,
            encoding: this.encoding,
            messageFormat: this.messageFormat,
            addresseeList: this.addresseeList,
        };
        return obj;
    }
}