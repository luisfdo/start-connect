import { Controller, Get, Param, UseGuards, Post, Body, Delete, Query, Res, HttpStatus, NotFoundException, Put, Req } from '@nestjs/common';
import { SmssService } from './smss.service';
import { CreateSmsDTO } from './dto/create-sms.dto';
import { ValidateObjectId } from './../shared/pipes/validate-object-id.pipe';
import { AuthGuard } from '../shared/auth.gaurd';
import { CompanysService } from '../companys/companys.service';
import { ApiKeys } from './interfaces/apiKeys.interface';
import { SMSAldeamoReq } from './models/smsAldeamoReq.model';

@Controller('smss')
@UseGuards(new AuthGuard())
export class SmssController {

    constructor(
        private smsSvc: SmssService,
        private comSvc: CompanysService,
    ) {}

    @Get()
    async getSmss(@Res() res) {
        const smss = await this.smsSvc.getSmss();
        return res.status(HttpStatus.OK).json(smss);
    }

    @Get(':smsID')
    async getSms(@Res() res, @Param('smsID', new ValidateObjectId()) smsID) {

        let status: boolean = false;
        let response;
        response = await this.smsSvc.getSms(smsID);

        if (!response){
            status = true;
        }

        if (status){
            response = await this.smsSvc.getSmsCompany(smsID);
        }

        if (!response){
            throw new NotFoundException('Sms does not exist!');
        }
        return res.status(HttpStatus.OK).json(response);
    }

    @Post('addSms_old')
    async addSms_old(@Res() res, @Body() createSmsDTO: CreateSmsDTO) {

        const accountSid = 'AC26894181f91e6fb2e75ef0291f0c6eb9';
        const authToken = '6f62c6ca7c244004864428612902c172';
        const client = require('twilio')(accountSid, authToken);

        client.messages
            .create({
                body: createSmsDTO.body,
                from: createSmsDTO.from,
                to: createSmsDTO.to,
            })
            .then(message => {
                const saveSms = {
                    messageId: message._solution.sid,
                    from : message.from,
                    to : message.to,
                    body : message.body,
                    cost : message.price,
                    createdAt : message.dateCreated,
                    bells : createSmsDTO.bells,
                    company: createSmsDTO.company,
                };
                this.addSmsSave(res, saveSms);
            })
            .catch(error => {
                // tslint:disable-next-line:no-console
                console.log(error);
            });
    }

    @Post()
    async addSmsSave(@Res() res, @Body() createSmsDTO: CreateSmsDTO) {
        const addedSms = await this.smsSvc.addSms(createSmsDTO);
        return res.status(HttpStatus.OK).json({
            message: 'Sms has been successfully added!',
            sms: addedSms,
        });
    }

    @Put()
    async updateSms(
        @Res() res,
        @Query('smsID', new ValidateObjectId()) smsID,
        @Body() createSmsDTO: CreateSmsDTO) {
            const updatedSms = await this.smsSvc.updateSms(smsID, createSmsDTO);
            if (!updatedSms) {
                throw new NotFoundException('Sms does not exist!');
            }
            return res.status(HttpStatus.OK).json({
                message: 'Sms has been successfully updated!',
                sms: updatedSms,
            });
    }

    @Delete()
    async deleteSms(@Res() res, @Query('smsID', new ValidateObjectId()) smsID) {
        const deletedSms = await this.smsSvc.deleteSms(smsID);
        if (!deletedSms) {
            throw new NotFoundException('Sms Does not exist!');
        }
        return res.status(HttpStatus.OK).json({
            message: 'Sms has been successfully deleted!',
            sms: deletedSms,
        });
    }

    /**
     * Método de entrada del envio de SMS
     */
    @Post('addSms')
    async addSms(@Res() res, @Body() createSmsDTO: CreateSmsDTO, @Req() req)
    {
        // tslint:disable-next-line:no-console
        console.log('SEND SMS**********************');
        if (req.apiKey != null){
            switch (req.apiKey.name) {
                case 'aldeamo':
                    return await this.processAldeamo(req.apiKey, createSmsDTO, res);
                    break;

                case 'twillio':
                    return await this.processTwillio(req.apiKey, createSmsDTO, res);
                    break;

                default:
                    break;
            }
        }else{
            // tslint:disable-next-line:no-console
            console.log('No active SMS service');
            return res.status(HttpStatus.OK).json({
                status: false,
                message: 'No active SMS service',
            });
        }
    }

    @Post()
    async processAldeamo(apiKey: ApiKeys, createSmsDTO: CreateSmsDTO, @Res() res) {
        const sms = new SMSAldeamoReq(apiKey);
        sms.addMobile(createSmsDTO.to);
        sms.message = createSmsDTO.body;
        return this.smsSvc.sendSms_aldeamo(sms)
            .subscribe((data) => {
                const response = data.result.receivedRequests[0];
                const saveSms = {
                    messageId: response.transactionId,
                    from : 'aldeamo',
                    to : response.mobile,
                    body : createSmsDTO.body,
                    cost : '50',
                    createdAt : data.result.dateToSend,
                    bells : createSmsDTO.bells,
                    company: createSmsDTO.company,
                };
                this.addSmsSave(res, saveSms);
            }, err => {
                // tslint:disable-next-line:no-console
                console.log(err);
                return res.status(HttpStatus.BAD_REQUEST).json({
                    status: false,
                    message: err,
                });
            });
    }

    @Post()
    async processTwillio(apiKey: ApiKeys, createSmsDTO: CreateSmsDTO, @Res() res) {

        const client = require('twilio')(apiKey.user, apiKey.passw);
        client.messages
            .create({
                body: createSmsDTO.body,
                from: createSmsDTO.from,
                to: createSmsDTO.to,
            })
            .then(message => {
                const saveSms = {
                    messageId: message._solution.sid,
                    from : message.from,
                    to : message.to,
                    body : message.body,
                    cost : message.price,
                    createdAt : message.dateCreated,
                    bells : createSmsDTO.bells,
                    company: createSmsDTO.company,
                };
                this.addSmsSave(res, saveSms);
            })
            .catch(error => {
                // tslint:disable-next-line:no-console
                console.log(error);
                return res.status(HttpStatus.BAD_REQUEST).json({
                    status: false,
                    message: error.message,
                });
            });
    }
}
