import * as mongoose from 'mongoose';

export const BillingSchema = new mongoose.Schema({
     value: String,
     customer: mongoose.Schema.Types.ObjectId,
     company: mongoose.Schema.Types.ObjectId
});