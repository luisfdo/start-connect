import { Controller, Get, Param, UseGuards, Post, Body, Delete, Query, Res, HttpStatus, NotFoundException, Put } from '@nestjs/common';
import { BillingsService } from './billings.service';
import { CreateBillingDTO } from './dto/create-billing.dto';
import { ValidateObjectId } from './../shared/pipes/validate-object-id.pipe';
import { AuthGuard } from '../shared/auth.gaurd';
import { CompanysService } from '../companys/companys.service';

@Controller('billings')
@UseGuards(new AuthGuard())
export class BillingsController {

    constructor(
        private billingSvc: BillingsService,
        private comSvc: CompanysService
        ) {}

    @Get()
    async getBillings(@Res() res) {

        const billings = await this.billingSvc.getBillings();
        return res.status(HttpStatus.OK).json(billings);
    }

    @Get(':billingID')
    async getBilling(@Res() res, @Param('billingID', new ValidateObjectId()) billingID) {

        var status:Boolean = false;
        var response;
        response = await this.billingSvc.getBilling(billingID);
        
        if(!response){
            status = true;
        }

        if(status){
            response = await this.billingSvc.getBillingCompany(billingID);
        }

        if(!response){
            throw new NotFoundException('Billing does not exist!');
        }
   
        return res.status(HttpStatus.OK).json(response);
        
    }


    @Post()
    async addBilling(@Res() res, @Body() createBillingDTO: CreateBillingDTO) {
        const addedBilling = await this.billingSvc.addBilling(createBillingDTO);
        return res.status(HttpStatus.OK).json({
            message: 'Billing has been successfully added!',
            quote: addedBilling,
        });
    }

    @Put()
    async updateBilling(
        @Res() res,
        @Query('billingID', new ValidateObjectId()) billingID,
        @Body() createBillingDTO: CreateBillingDTO) {
            const updatedBilling = await this.billingSvc.updateBilling(billingID, createBillingDTO);
            if (!updatedBilling) {
                throw new NotFoundException('Billing does not exist!');
            }
            return res.status(HttpStatus.OK).json({
                message: 'Billing has been successfully updated!',
                quote: updatedBilling,
            });
    }

    @Delete()
    async deleteBilling(@Res() res, @Query('billingID', new ValidateObjectId()) billingID) {
        const deletedBilling = await this.billingSvc.deleteBilling(billingID);
        if (!deletedBilling) {
            throw new NotFoundException('Billing Does not exist!');
        }
        return res.status(HttpStatus.OK).json({
            message: 'Billing has been successfully deleted!',
            billing: deletedBilling,
        });
    }
}
