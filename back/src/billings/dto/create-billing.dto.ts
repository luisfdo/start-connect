export class CreateBillingDTO {
    readonly value: string;
    readonly customer: string;
    readonly company: string;
}
