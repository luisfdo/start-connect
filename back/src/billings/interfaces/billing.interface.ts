import { Document } from 'mongoose';

export interface Billing extends Document {
    readonly value: string;
    readonly customer: string;
    readonly company: string;
}