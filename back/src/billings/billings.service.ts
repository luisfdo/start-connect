import * as mongoose from 'mongoose';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Billing } from './interfaces/billing.interface';
import { CreateBillingDTO } from './dto/create-billing.dto';

@Injectable()
export class BillingsService {
    constructor(@InjectModel('Billing') private readonly billingModel: Model<Billing>){}

    async getBillings(): Promise<Billing[]> {
        const billings = await this.billingModel.find().exec();
        return billings;
    }

    async getBilling(billingID): Promise<Billing> {
        const fetchedBilling = await this.billingModel
            .findById(billingID)
            .exec();
        return fetchedBilling;
    }

    async getBillingCompany(companyID): Promise<Billing> {
        let _query = {company: new mongoose.Types.ObjectId(companyID)};
        const fetchedBilling = await this.billingModel
            .find(_query)
            .exec();
        return fetchedBilling;
    }

    async addBilling(createBillingDTO: CreateBillingDTO): Promise<Billing> {
       const addedBilling = await this.billingModel(createBillingDTO);
       return addedBilling.save();
    }

    async updateBilling(billingID, createBillingDTO: CreateBillingDTO): Promise<Billing> {
        const updatedBilling = await this.billingModel
            .findByIdAndUpdate(billingID, createBillingDTO, {new: true});
        return updatedBilling;
    }

    async deleteBilling(billingID): Promise<Billing> {
        const deletedBilling = await this.billingModel
            .findByIdAndRemove(billingID);
        return deletedBilling;
    }

    async searchBillingCustomer(customerID): Promise<Billing> {
        let _query = {customer: new mongoose.Types.ObjectId(customerID)};
        const fetchedBilling = await this.billingModel
            .find(_query)
            .exec();
        return fetchedBilling;
    }

    
}
