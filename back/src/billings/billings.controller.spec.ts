import { Test, TestingModule } from '@nestjs/testing';
import { BillingsController } from './billings.controller';

describe('Billings Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [BillingsController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: BillingsController = module.get<BillingsController>(BillingsController);
    expect(controller).toBeDefined();
  });
});
