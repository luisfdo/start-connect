import { Module } from '@nestjs/common';
import { BillingsController } from './billings.controller';
import { BillingsService } from './billings.service';
import { MongooseModule } from '@nestjs/mongoose';
import { BillingSchema } from './schemas/billing.schema';
import { CompanysModule } from '../companys/companys.module';

@Module({
    imports: [
        MongooseModule.forFeature([{name: 'Billing', schema: BillingSchema}]),
        CompanysModule
      ],
      controllers: [BillingsController],
      providers: [BillingsService],
      exports:[BillingsService]
})
export class BillingsModule {}
