import { Document } from 'mongoose';

export interface Quote extends Document {
    readonly date_start: string;
    readonly hour: string;
    readonly customer: string;
    readonly company: string;
}