import * as mongoose from 'mongoose';

export const QuoteSchema = new mongoose.Schema({
    
     date_start: String,
     hour: String,
     customer: mongoose.Schema.Types.ObjectId,
     company: mongoose.Schema.Types.ObjectId
});