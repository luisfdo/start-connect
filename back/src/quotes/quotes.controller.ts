import { Controller, Get, Param, UseGuards, Post, Body, Delete, Query, Res, HttpStatus, NotFoundException, Put } from '@nestjs/common';
import { QuotesService } from './quotes.service';
import { CreateQuoteDTO } from './dto/create-quote.dto';
import { ValidateObjectId } from './../shared/pipes/validate-object-id.pipe';
import { AuthGuard } from '../shared/auth.gaurd';
import { CompanysService } from '../companys/companys.service';

@Controller('quotes')
@UseGuards(new AuthGuard())
export class QuotesController {

    constructor(
        private quoteSvc: QuotesService,
        private comSvc: CompanysService
        ) {}

    @Get()
    async getQuotes(@Res() res) {

        const quotes = await this.quoteSvc.getQuotes();
        return res.status(HttpStatus.OK).json(quotes);
    }

    @Get(':quoteID')
    async getQuote(@Res() res, @Param('quoteID', new ValidateObjectId()) quoteID) {

        var status:Boolean = false;
        var response;
        response = await this.quoteSvc.getQuote(quoteID);
        
        if(!response){
            status = true;
        }

        if(status){
            response = await this.quoteSvc.getQuoteCompany(quoteID);
        }

        if(!response){
            throw new NotFoundException('Quote does not exist!');
        }
   
        return res.status(HttpStatus.OK).json(response);
        
    }


    @Post()
    async addQuote(@Res() res, @Body() createQuoteDTO: CreateQuoteDTO) {

        const addedQuote = await this.quoteSvc.addQuote(createQuoteDTO);
        return res.status(HttpStatus.OK).json({
            message: 'Quote has been successfully added!',
            quote: addedQuote,
        });
    }

    @Put()
    async updateQuote(
        @Res() res,
        @Query('quoteID', new ValidateObjectId()) quoteID,
        @Body() createQuoteDTO: CreateQuoteDTO) {
            const updatedQuote = await this.quoteSvc.updateQuote(quoteID, createQuoteDTO);
            if (!updatedQuote) {
                throw new NotFoundException('Quote does not exist!');
            }
            return res.status(HttpStatus.OK).json({
                message: 'Quote has been successfully updated!',
                quote: updatedQuote,
            });
    }

    @Delete()
    async deleteQuote(@Res() res, @Query('quoteID', new ValidateObjectId()) quoteID) {
        const deletedQuote = await this.quoteSvc.deleteQuote(quoteID);
        if (!deletedQuote) {
            throw new NotFoundException('Quote Does not exist!');
        }
        return res.status(HttpStatus.OK).json({
            message: 'Quote has been successfully deleted!',
            quote: deletedQuote,
        });
    }
}
