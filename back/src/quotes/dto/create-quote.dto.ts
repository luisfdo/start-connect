export class CreateQuoteDTO {
    readonly date_start: string;
    readonly hour: string;
    readonly customer: string;
    readonly company: string;
}
