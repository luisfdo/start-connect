import { Module } from '@nestjs/common';
import { QuotesController } from './quotes.controller';
import { QuotesService } from './quotes.service';
import { MongooseModule } from '@nestjs/mongoose';
import { QuoteSchema } from './schemas/quote.schema';
import { CompanysModule } from '../companys/companys.module';

@Module({
    imports: [
        MongooseModule.forFeature([{name: 'Quote', schema: QuoteSchema}]),
        CompanysModule
      ],
      controllers: [QuotesController],
      providers: [QuotesService],
      exports:[QuotesService]
})
export class QuotesModule {}
