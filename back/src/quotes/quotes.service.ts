import * as mongoose from 'mongoose';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Quote } from './interfaces/quote.interface';
import { CreateQuoteDTO } from './dto/create-quote.dto';

@Injectable()
export class QuotesService {
    constructor(@InjectModel('Quote') private readonly quoteModel: Model<Quote>){}

    async getQuotes(): Promise<Quote[]> {
        const quotes = await this.quoteModel.find().exec();
        return quotes;
    }

    async getQuote(quoteID): Promise<Quote> {
        const fetchedQuote = await this.quoteModel
            .findById(quoteID)
            .exec();
        return fetchedQuote;
    }

    async getQuoteCompany(companyID): Promise<Quote> {
        let _query = {company: new mongoose.Types.ObjectId(companyID)};
        const fetchedQuote = await this.quoteModel
            .find(_query)
            .exec();
        return fetchedQuote;
    }

    async addQuote(createQuoteDTO: CreateQuoteDTO): Promise<Quote> {
       const addedQuote = await this.quoteModel(createQuoteDTO);
       return addedQuote.save();
    }

    async updateQuote(quoteID, createQuoteDTO: CreateQuoteDTO): Promise<Quote> {
        const updatedQuote = await this.quoteModel
            .findByIdAndUpdate(quoteID, createQuoteDTO, {new: true});
        return updatedQuote;
    }

    async deleteQuote(quoteID): Promise<Quote> {
        const deletedQuote = await this.quoteModel
            .findByIdAndRemove(quoteID);
        return deletedQuote;
    }

    async searchQuoteCustomer(customerID): Promise<Quote> {
        let _query = {customer: new mongoose.Types.ObjectId(customerID)};
        const fetchedQuote = await this.quoteModel
            .find(_query)
            .exec();
        return fetchedQuote;
    }

    
}
