import { Test, TestingModule } from '@nestjs/testing';
import { QuotesController } from './quotes.controller';

describe('Quotes Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [QuotesController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: QuotesController = module.get<QuotesController>(QuotesController);
    expect(controller).toBeDefined();
  });
});
