import { Test, TestingModule } from '@nestjs/testing';
import { ContractsController } from './contracts.controller';

describe('Contracts Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [ContractsController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: ContractsController = module.get<ContractsController>(ContractsController);
    expect(controller).toBeDefined();
  });
});
