import { Controller, Get, Param, UseGuards, Post, Body, Delete, Query, Res, HttpStatus, NotFoundException, Put } from '@nestjs/common';
import { ContractsService } from './contracts.service';
import { CreateContractDTO } from './dto/create-contract.dto';
import { ValidateObjectId } from './../shared/pipes/validate-object-id.pipe';
import { AuthGuard } from '../shared/auth.gaurd';
import { MikrotikService } from '../mikrotik/mikrotik.service';
import { PlansService } from '../plans/plans.service';
import { CustomersService } from '../customers/customers.service';


@Controller('contracts')
@UseGuards(new AuthGuard())
export class ContractsController {

    constructor(
        private contractSvc: ContractsService,
        private mikSvc: MikrotikService,
        private planSvc:PlansService,
        private customerSvc:CustomersService
        ) {}

    @Get()
    async getContracts(@Res() res) {

        const contracts = await this.contractSvc.getContracts();
        return res.status(HttpStatus.OK).json(contracts);
    }


    @Get(':contractID')
    async getContract(@Res() res, @Param('contractID', new ValidateObjectId()) contractID){

        var status:Boolean = false;
        var response;
        response = await this.contractSvc.getContract(contractID); 
        
        if(!response){status = true;}
        if(status){response = await this.contractSvc.getContractCompany(contractID);}
        if(!response){throw new NotFoundException('Contract does not exist!');}
        
        return res.status(HttpStatus.OK).json(response);
        
    }


    @Post()
    async addContract(@Res() res, @Body() createContractDTO: CreateContractDTO) {

        const response = await this.mikSvc.searchServer(createContractDTO.mikrotik, createContractDTO.company);
        const response2 = await this.planSvc.getPlan(createContractDTO.plan);
        const response3 = await this.customerSvc.getCustomer(createContractDTO.customer);

        const queue = {
            name: response3.name,
            target: createContractDTO.addressIp,
            queue: "default-small",
            "max-limit": response2.maxRise+response2.uploadUnit.substr(0,1).toUpperCase()+'/'+response2.maxDesc+response2.descentUnit.substr(0,1).toUpperCase(),
            comment: "clent_12 contract_124",
            disabled: false
        }


        const response4:any = await this.mikSvc.addQueue(response, queue)

        if(!response4.target){
            throw new NotFoundException('Contract no se pudo crear');
        }

        createContractDTO.addressIp = response4.target

        const addedContract = await this.contractSvc.addContract(createContractDTO);
            return res.status(HttpStatus.OK).json({
                    message: 'Contract has been successfully added!',
                    plan: addedContract,
        });
        
    }

    @Put()
    async updateContract(
        @Res() res,
        @Query('contractID', new ValidateObjectId()) contractID,
        @Body() createContractDTO: CreateContractDTO) {


            const response = await this.mikSvc.searchServer(createContractDTO.mikrotik, createContractDTO.company);
            const response2 = await this.planSvc.getPlan(createContractDTO.plan);
            const response3 = await this.customerSvc.getCustomer(createContractDTO.customer);
            
            let queue = {}
            if(createContractDTO.state == 'habilitado'){
                 queue = {
                    targetId: createContractDTO.addressIpId,
                    name: response3.name,
                    target: createContractDTO.addressIp,
                    queue: "default-small",
                    maxlimit: response2.maxRise+response2.uploadUnit.substr(0,1).toUpperCase()+'/'+response2.maxDesc+response2.descentUnit.substr(0,1).toUpperCase(),
                    comment: "clent_12 contract_124",
                    disabled: false
                }
            }else{
                queue = {
                    targetId: createContractDTO.addressIpId,
                    name: response3.name,
                    target: createContractDTO.addressIp,
                    queue: "default-small",
                    maxlimit: response2.maxRise+response2.uploadUnit.substr(0,1).toUpperCase()+'/'+response2.maxDesc+response2.descentUnit.substr(0,1).toUpperCase(),
                    comment: "clent_12 contract_124",
                    disabled: true
                }
            }
            
            const response4:any = await this.mikSvc.updateQueue(response, queue)

            if(!response4.target){
                throw new NotFoundException('Contract no se pudo crear');
            }
            createContractDTO.addressIp = response4.target

            const updatedContract = await this.contractSvc.updateContract(contractID, createContractDTO);
            if (!updatedContract) {
                throw new NotFoundException('Contract does not exist!');
            }
            return res.status(HttpStatus.OK).json({
                message: 'Contract has been successfully updated!',
                bank: updatedContract,
            });
    }

    @Delete()
    async deleteContract(@Res() res, @Query('contractID', new ValidateObjectId()) contractID) {

        const response = await this.contractSvc.getContract(contractID);
        const response2 = await this.mikSvc.searchServer(response.mikrotik, response.company);

        let queue = {
            target: response.addressIp
        }
        
        const response3:any = await this.mikSvc.removeQueue(response2, queue);

        if(!response3.target){
            throw new NotFoundException('Contract no se pudo eliminar');
        }

        const deletedContract = await this.contractSvc.deleteContract(contractID);
        if(!deletedContract){
            throw new NotFoundException('Contract Does not exist!');
        }
        return res.status(HttpStatus.OK).json({
            message: 'Contract has been successfully deleted!',
            bank: deletedContract,
        });
    }
}
