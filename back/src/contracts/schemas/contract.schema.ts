import * as mongoose from 'mongoose';

export const ContractSchema = new mongoose.Schema({
     customer: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Customer' }],
     plan: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Plan' }],
     state: String,
     mikrotik: mongoose.Schema.Types.ObjectId,
     interface:String,
     addressIp: String,
     networkMask: String,
     addressMac: String,
     observations: String,
     dateStart: String,    
     price: String,    
     company: mongoose.Schema.Types.ObjectId
});