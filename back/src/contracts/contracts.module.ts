import { Module } from '@nestjs/common';
import { ContractsController } from './contracts.controller';
import { ContractsService } from './contracts.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ContractSchema } from './schemas/contract.schema';
import { CompanysModule } from '../companys/companys.module';
import { MikrotikModule } from '../mikrotik/mikrotik.module';
import { PlansModule } from '../plans/plans.module';
import { CustomersModule } from '../customers/customers.module';

@Module({
    imports: [
        MongooseModule.forFeature([{name: 'Contract', schema: ContractSchema}]),
        CompanysModule,
        MikrotikModule,
        PlansModule,
        CustomersModule
      ],
      controllers: [ContractsController],
      providers: [ContractsService],
      //exports:[ContractsService]
})
export class ContractsModule {}
