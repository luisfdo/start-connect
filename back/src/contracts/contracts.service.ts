import * as mongoose from 'mongoose';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Contract } from './interfaces/contract.interface';
import { CreateContractDTO } from './dto/create-contract.dto';

@Injectable()
export class ContractsService {
    constructor(@InjectModel('Contract') private readonly contractModel: Model<Contract>){}

    async getContracts(): Promise<Contract[]> {
        const contracts = await this.contractModel.find().exec();
        return contracts;
    }

    async getContract(contractID): Promise<Contract> {
        const fetchedContract = await this.contractModel
            .findById(contractID)
            .exec();
        return fetchedContract;
    }

    async getContractCompany(companyID): Promise<Contract> {
        let _query = {company: new mongoose.Types.ObjectId(companyID)};
        const fetchedContract = await this.contractModel
            .find(_query)
            .populate('customer')
            .populate('plan')
            .exec();
        return fetchedContract;
    }

    async addContract(createContractDTO: CreateContractDTO): Promise<Contract> {
       const addedContract = await this.contractModel(createContractDTO);
       return addedContract.save();
    }

    async updateContract(contractID, createContractDTO: CreateContractDTO): Promise<Contract> {
        const updatedContract = await this.contractModel
            .findByIdAndUpdate(contractID, createContractDTO, {new: true});
        return updatedContract;
    }

    async deleteContract(contractID): Promise<Contract> {
        const deletedContract = await this.contractModel
            .findByIdAndRemove(contractID);
        return deletedContract;
    }

    
}
