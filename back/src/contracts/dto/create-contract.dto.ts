export class CreateContractDTO {
    readonly customer: string;
    readonly plan: string;
    readonly state: string;
    readonly mikrotik: string;
    readonly interface:string;
    public   addressIp: string;
    public   addressIpId: string;
    readonly networkMask: string;
    readonly addressMac: string;
    readonly observations: string;
    readonly dateStart: string;    
    readonly price: string;    
    readonly company: string;
}
