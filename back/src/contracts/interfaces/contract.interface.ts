import { Document } from 'mongoose';

export interface Contract extends Document {
    readonly customer: string;
    readonly plan: string;
    readonly state: string;
    readonly mikrotik: string;
    readonly interface:string;
    readonly addressIp: string;
    readonly networkMask: string;
    readonly addressMac: string;
    readonly observations: string;
    readonly dateStart: string;    
    readonly price: string;    
    readonly company: string;
}