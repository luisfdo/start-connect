import {
    Injectable,
    CanActivate,
    ExecutionContext,
    HttpException,
    HttpStatus } from '@nestjs/common';

import { GqlExecutionContext } from '@nestjs/graphql';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class AuthGuard implements CanActivate {
  async canActivate( context: ExecutionContext, ): Promise<boolean> {

    const request = context.switchToHttp().getRequest();

    if(!request.headers.authorization){
      return false; 
    }


    request.user = await this.validateToken(request.headers.authorization);


    return true;
  }


  async validateToken(auth:string){

    if(auth !== 'Bearer fe28f5be-afbd-4a77-b5a6-1b784e224047'){ //bearer token
      throw new HttpException('Invalid token', HttpStatus.FORBIDDEN)
    }
    
    // const token = auth.split(' ')[1];
    // try {
    //   console.log('2222222222222222222222');
    //   console.log(token);

    //   const decoded = await jwt.verify(token, process.env.SECRET);  
    //   console.log('eeeeeee');
    //   console.log(decoded)

    //   return decoded;
    // } catch (err) {
    //   const message = 'Token error: ' + (err.message || err.name);
    //   throw new HttpException(message, HttpStatus.FORBIDDEN);
    // }
    
    
  }

}