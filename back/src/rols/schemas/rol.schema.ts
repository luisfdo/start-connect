import * as mongoose from 'mongoose';

export const RolSchema = new mongoose.Schema({
    name: String,
    description: String,
    company: mongoose.Schema.Types.ObjectId
});