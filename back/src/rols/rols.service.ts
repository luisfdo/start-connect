import * as mongoose from 'mongoose';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Rol } from './interfaces/rol.interface';
import { CreateRolDTO } from './dto/create-rol.dto';

@Injectable()
export class RolsService {
    constructor(@InjectModel('Rol') private readonly rolModel: Model<Rol>){}

    async getRols(): Promise<Rol[]> {
        const rols = await this.rolModel.find().exec();
        return rols;
    }

    async getRol(rolID): Promise<Rol> {
        const fetchedRol = await this.rolModel
            .findById(rolID)
            .exec();
        return fetchedRol;
    }

    async getRolCompany(companyID): Promise<Rol> {
        let _query = {company: new mongoose.Types.ObjectId(companyID)};
        const fetchedRol = await this.rolModel
            .find(_query)
            .exec();
        return fetchedRol;
    }

    async addRol(createRolDTO: CreateRolDTO): Promise<Rol> {
       const addedRol = await this.rolModel(createRolDTO);
       return addedRol.save();
    }

    async updateRol(rolID, createRolDTO: CreateRolDTO): Promise<Rol> {
        const updatedRol = await this.rolModel
            .findByIdAndUpdate(rolID, createRolDTO, {new: true});
        return updatedRol;
    }

    async deleteRol(rolID): Promise<Rol> {
        const deletedRol = await this.rolModel
            .findByIdAndRemove(rolID);
        return deletedRol;
    }

    
}
