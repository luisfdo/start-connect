import { Test, TestingModule } from '@nestjs/testing';
import { RolsController } from './rols.controller';

describe('Rols Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [RolsController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: RolsController = module.get<RolsController>(RolsController);
    expect(controller).toBeDefined();
  });
});
