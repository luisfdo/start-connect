import { Module } from '@nestjs/common';
import { RolsController } from './rols.controller';
import { RolsService } from './rols.service';
import { MongooseModule } from '@nestjs/mongoose';
import { RolSchema } from './schemas/rol.schema';
import { CompanysModule } from '../companys/companys.module';

@Module({
    imports: [
        MongooseModule.forFeature([{name: 'Rol', schema: RolSchema}]),
        CompanysModule
      ],
      controllers: [RolsController],
      providers: [RolsService],
    //   exports:[RolsService]
})
export class RolsModule {}
