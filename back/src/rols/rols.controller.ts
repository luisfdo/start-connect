import { Controller, Get, Param, UseGuards, Post, Body, Delete, Query, Res, HttpStatus, NotFoundException, Put } from '@nestjs/common';
import { RolsService } from './rols.service';
import { CreateRolDTO } from './dto/create-rol.dto';
import { ValidateObjectId } from './../shared/pipes/validate-object-id.pipe';
import { AuthGuard } from '../shared/auth.gaurd';
import { CompanysService } from '../companys/companys.service';

@Controller('rols')
@UseGuards(new AuthGuard())
export class RolsController {

    constructor(
        private rolSvc: RolsService,
        private comSvc: CompanysService
        ) {}

    @Get()
    async getRols(@Res() res) {

        const rols = await this.rolSvc.getRols();
        return res.status(HttpStatus.OK).json(rols);
    }

    @Get(':rolID')
    async getRol(@Res() res, @Param('rolID', new ValidateObjectId()) rolID) {

        var status:Boolean = false;
        var response;
        response = await this.rolSvc.getRol(rolID);
        
        if(!response){
            status = true;
        }

        if(status){
            response = await this.rolSvc.getRolCompany(rolID);
        }

        if(!response){
            throw new NotFoundException('Rol does not exist!');
        }
   
        return res.status(HttpStatus.OK).json(response);
        
    }


    @Post()
    async addRol(@Res() res, @Body() createRolDTO: CreateRolDTO) {
        const addedRol = await this.rolSvc.addRol(createRolDTO);
        return res.status(HttpStatus.OK).json({
            message: 'Rol has been successfully added!',
            rol: addedRol,
        });
    }

    @Put()
    async updateRol(
        @Res() res,
        @Query('rolID', new ValidateObjectId()) rolID,
        @Body() createRolDTO: CreateRolDTO) {
            const updatedRol = await this.rolSvc.updateRol(rolID, createRolDTO);
            if (!updatedRol) {
                throw new NotFoundException('Rol does not exist!');
            }
            return res.status(HttpStatus.OK).json({
                message: 'Rol has been successfully updated!',
                rol: updatedRol,
            });
    }

    @Delete()
    async deleteRol(@Res() res, @Query('rolID', new ValidateObjectId()) rolID) {
        const deletedRol = await this.rolSvc.deleteRol(rolID);
        if (!deletedRol) {
            throw new NotFoundException('Rol Does not exist!');
        }
        return res.status(HttpStatus.OK).json({
            message: 'Rol has been successfully deleted!',
            rol: deletedRol,
        });
    }
}
