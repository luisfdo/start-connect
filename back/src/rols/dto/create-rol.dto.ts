export class CreateRolDTO {
    readonly name: string;
    readonly description: string;
    readonly company: string;
}
