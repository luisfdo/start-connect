import { Document } from 'mongoose';

export interface Rol extends Document {
  
    readonly name: string;
    readonly description: string;
    readonly company: string;
}