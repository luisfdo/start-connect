import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { CompanysModule } from './companys/companys.module';
import { UsersModule } from './users/users.module';
import { RolsModule } from './rols/rols.module';
import { BellsModule } from './bells/bells.module';
import { CustomersModule } from './customers/customers.module';
import { CategoriesModule } from './categories/categories.module';
import { BanksModule } from './banks/banks.module';
import { SmssModule } from './smss/smss.module';
import { VoicesModule } from './voices/voices.module';
import { MikrotikModule } from './mikrotik/mikrotik.module';
import { PlansModule } from './plans/plans.module';
import { ContractsModule } from './contracts/contracts.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/start-connect'),
    CompanysModule,
    UsersModule,
    RolsModule,
    BellsModule,
    CustomersModule,
    CategoriesModule,
    BanksModule,
    SmssModule,
    VoicesModule,
    MikrotikModule,
    PlansModule,
    ContractsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
