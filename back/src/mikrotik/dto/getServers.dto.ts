export class GetServersDto{
    readonly company: string;
    readonly status: boolean;
}