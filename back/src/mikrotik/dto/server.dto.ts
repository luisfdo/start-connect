export class ServerDto{
    readonly name: string;
    readonly host: string;
    readonly user: string;
    readonly password: string;
    readonly port: number;
    readonly status: boolean;
    readonly company: string;
}