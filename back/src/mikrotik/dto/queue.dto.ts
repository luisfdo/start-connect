export class QueueDTO{
    readonly id?: string;
    readonly name: string;
    readonly target: string;
    readonly queue: string;
    readonly 'max-limit': string;
    readonly comment?: string;
    readonly disabled?: boolean;
}