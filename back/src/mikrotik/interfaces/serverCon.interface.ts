import { Document } from 'mongoose';

export interface ServerCon extends Document{
    readonly name: string;
    readonly host: string;
    readonly user: string;
    readonly password: string;
    readonly port: string;
    readonly status: string;
    readonly company: string;
}