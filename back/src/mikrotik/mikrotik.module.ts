import { Module, RequestMethod, MiddlewareConsumer } from '@nestjs/common';
import { MikrotikService } from './mikrotik.service';
import { MikrotikController } from './mikrotik.controller';

import { MongooseModule } from '@nestjs/mongoose';
import { SeverMikrotikSchema } from './schemes/serverMikrotik.scheme';
import { MikrotikMiddleware } from './middlewares/mikrotik.middleware';

@Module({
  imports: [
    MongooseModule.forFeature([{name: 'ServerMikrotik', schema: SeverMikrotikSchema}]),
  ],
  providers: [MikrotikService],
  controllers: [MikrotikController],
  exports:[MikrotikService]
})
export class MikrotikModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(MikrotikMiddleware)
      .forRoutes(
        { path: 'mikrotik/interfaces', method: RequestMethod.POST },
        { path: 'mikrotik/getAllQueues', method: RequestMethod.POST },
        { path: 'mikrotik/addQueue', method: RequestMethod.POST },
        { path: 'mikrotik/updateQueue', method: RequestMethod.PUT },
        { path: 'mikrotik/getQueue', method: RequestMethod.POST },
        { path: 'mikrotik/removeQueue', method: RequestMethod.DELETE },
      );
}
}
