import { Injectable, MiddlewareFunction, NestMiddleware } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { ServerCon } from '../interfaces/serverCon.interface';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class MikrotikMiddleware implements NestMiddleware {
  constructor(@InjectModel('ServerMikrotik') private readonly serverModel: Model<ServerCon>){}

  resolve(...args: any[]): MiddlewareFunction {
    return (req, res, next) => {
      try{
        const _query = {
          _id: new mongoose.Types.ObjectId(req.body.servermikrotik),
          company: new mongoose.Types.ObjectId(req.body.company),
          status: true,
        };

        this.serverModel.findOne(_query).exec()
        .then((server) => {
          req.server = server;
          next();
        });
      } catch (err){
        // tslint:disable-next-line:no-console
        console.log(err);
      }
    };
  }
}