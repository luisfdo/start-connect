import { Controller, UseGuards, Post, Delete, Query, Body, Req, Res, Get, Put, HttpCode, HttpStatus, NotFoundException } from '@nestjs/common';
import { MikrotikService } from './mikrotik.service';
import { AuthGuard } from '../shared/auth.gaurd';
import { ServerDto } from './dto/server.dto';
import { GetServersDto } from './dto/getServers.dto';
import { ValidateObjectId } from './../shared/pipes/validate-object-id.pipe';

@Controller('mikrotik')
@UseGuards(new AuthGuard())
export class MikrotikController {
    response: any;

    constructor(private readonly mikrotikSvc: MikrotikService){}

    /**
     * Crea un servidor asociado a una empresa
     */
    @Post('createServer')
    async createServer(@Body() serverDto: ServerDto) {
        return this.mikrotikSvc.addServer(serverDto);
    }

    /**
     * Consulta los servidores que posee una empresa
     */
    @Post('servers')
    async servers(@Res() res, @Body() getServersDto: GetServersDto) {

        try{
            const resp = await this.mikrotikSvc.getServersByCompany(getServersDto.company);
            return res.status(HttpStatus.OK).json(resp);
            
        }catch (err){
            return {status: false, error: err.message};
        }
    }


    @Put('updateServer')
    async updateServer(
        @Res() res,
        @Query('serverID', new ValidateObjectId()) serverID,
        @Body() serverDto: ServerDto) {
            const updatedServer = await this.mikrotikSvc.updateServer(serverID, serverDto);
            if (!updatedServer) {
                throw new NotFoundException('Bank does not exist!');
            }
            return res.status(HttpStatus.OK).json({
                message: 'Server has been successfully updated!',
                bank: updatedServer,
            });
    }
    
    /** 
     * Elimina los servidores que posee una empresa
    */
   @Delete()
    async deleteServer(@Res() res, @Query('serverID', new ValidateObjectId()) serverID) {
        var resp : any;
        var respQuote:any;
        var respBilling:any;

        const deletedServer = await this.mikrotikSvc.deleteServer(serverID);
        resp = deletedServer

        if(!deletedServer){
            throw new NotFoundException('Server Does not exist!');
        }

        return res.status(HttpStatus.OK).json({
            message: 'Server has been successfully deleted!',
            customer: deletedServer,
        });
    }

    /**
     * Consulta las interfaces que posee un servidor
     */
    @Post('interfaces')
    async interfaces(@Req() req, @Res() res) {
        // Si hay datos de un servidor activo
        if (req.server != null)
        {
            await this.mikrotikSvc.getAllInterfaces( req.server as ServerDto )
            .then(result => {
                this.response = {status: true, data: result};
            })
            .catch(err => {
                this.response = {status: false, error: err.message};
            });
        }else{
            this.response = {status: false, error: 'No active Server'};
        }
        return  res.status(HttpStatus.OK).json(this.response);
    }

    /**
     * Consulta todas las colas que posee un servidor
     * @param serverDto
     */
    @Post('getAllQueues')
    async getAllQueues(@Req() req, @Res() res) {
        if (req.server != null)
        {
            await this.mikrotikSvc.getAllQueues( req.server as ServerDto )
            .then(result => {
                this.response = {status: true, data: result};
            })
            .catch(err => {
                this.response = {status: false, error: err.message};
            });
        }else{
            this.response = {status: false, error: 'No active Server'};
        }
        return  res.status(HttpStatus.OK).json(this.response);
    }

    /**
     * Agrega una nueva cola o cliente al servidor
     * @param req
     * @param res
     */
    @Post('addQueue')
    @HttpCode(201)
    async addQueue(@Req() req, @Res() res) {
        if (req.server != null)
        {
            await this.mikrotikSvc.addQueue( req.server as ServerDto, req.body.queue )
            .then(result => {
                this.response = {status: true, data: result};
            })
            .catch(err => {
                this.response = {status: false, error: err.message};
            });
        }else{
            this.response = {status: false, error: 'No active Server'};
        }
        return  res.status(HttpStatus.OK).json(this.response);
    }

    /**
     * Actualiza una cola dado su id
     * @param req
     * @param res
     */
    @Put('updateQueue')
    async updateQueue(@Req() req, @Res() res) {
        if (req.server != null)
        {
            await this.mikrotikSvc.updateQueue( req.server as ServerDto, req.body.queue )
            .then(result => {
                this.response = {status: true, data: result};
            })
            .catch(err => {
                this.response = {status: false, error: err.message};
            });
        }else{
            this.response = {status: false, error: 'No active Server'};
        }
        return  res.status(HttpStatus.OK).json(this.response);
    }

    /**
     * Consulta una cola que posee un servidor
     */
    @Post('getQueue')
    async getQueue(@Req() req, @Res() res) {
        if (req.server != null)
        {
            await this.mikrotikSvc.getQueue( req.server as ServerDto, req.body.queue )
            .then(result => {
                this.response = {status: true, data: result};
            })
            .catch(err => {
                this.response = {status: false, error: err.message};
            });
        }else{
            this.response = {status: false, error: 'No active Server'};
        }
        return  res.status(HttpStatus.OK).json(this.response);
    }

    /**
     * Consulta una cola que posee un servidor
     */
    @Delete('removeQueue')
    async removeQueue(@Req() req, @Res() res) {
        if (req.server != null)
        {
            await this.mikrotikSvc.removeQueue( req.server as ServerDto, req.body.queue )
            .then(result => {
                this.response = {status: true, data: result};
            })
            .catch(err => {
                this.response = {status: false, error: err.message};
            });
        }else{
            this.response = {status: false, error: 'No active Server'};
        }
        return  res.status(HttpStatus.OK).json(this.response);
    }

}
