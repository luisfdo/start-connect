import * as mongoose from 'mongoose';
import { Injectable } from '@nestjs/common';
import { ServerCon } from './interfaces/serverCon.interface';
import { Model } from 'mongoose';
import { RouterOSClient } from 'routeros-client';
import { InjectModel } from '@nestjs/mongoose';
import { ServerDto } from './dto/server.dto';

@Injectable()
export class MikrotikService {
    constructor(@InjectModel('ServerMikrotik') private readonly serverModel: Model<ServerCon>){}

    async addServer(serverDto: ServerDto): Promise<ServerCon> {
        try{
            const addedServer = await this.serverModel(serverDto);
            return addedServer.save();
        }catch (err){
            // tslint:disable-next-line:no-console
            console.log(err);
        }
     }

     async updateServer(serverID, serverDto: ServerDto): Promise<ServerDto> {
        const updatedServer = await this.serverModel
            .findByIdAndUpdate(serverID, serverDto, {new: true});
        return updatedServer;
    }

    /**
     * Consulta los servidores que posee una empresa
     * @param companyID
     */
    async getServersByCompany(companyID: string): Promise<ServerCon[]> {
        const _query = {company: new mongoose.Types.ObjectId(companyID)};
        const fetchedServers = await this.serverModel
            .find(_query)
            .exec();
        return fetchedServers;
    }

    /**
     * Elimina los serividores que posee una empresa
     * @param _id 
     */
    async deleteServer(serverID): Promise<ServerCon> {
        const deletedServer = await this.serverModel
            .findByIdAndRemove(serverID);
        return deletedServer;
    }

    /**
     * Obtiene las interfaces del Router
     * @param options
     */
    async getAllInterfaces(options): Promise<any[]> {
        const api = new RouterOSClient(options);
        try{
            const client = await api.connect();
            try{
                const resp = await client.menu('/interface print').get();
                api.close();
                return resp;
            }catch (err){
                api.close();
                return err.message;
            }
        }catch (err){
           return err.message;
        }
    }

    /**
     * Obtiene todas las colas de un servidor
     * @param options
     */
    async getAllQueues(options): Promise<any[]> {
        const api = new RouterOSClient(options);
        try{
            const client = await api.connect();
            try{
                const resp = await client.menu('/queue simple print').get();
                api.close();
                return resp;
            }catch (err){
                api.close();
                return err.message;
            }
        }catch (err){
           return err.message;
        }
    }

    /**
     * Agrega una cola a un servidor
     * @param options
     * @param queue
     */
    async addQueue(options, queue): Promise<any[]> {
        const api = new RouterOSClient(options);
        try{
            const client = await api.connect();
            try{
                const resp = await client.menu('/queue simple')
                .add(queue);
                api.close();
                return resp;
            }catch (err){
                api.close();
                return err.message;
            }
        }catch (err){
           return err.message;
        }
    }

    async searchServer(id, company): Promise<any[]>{
        try{
            const _query = {
              _id: new mongoose.Types.ObjectId(id),
              company: new mongoose.Types.ObjectId(company),
              status: true,
            };
    
           return this.serverModel.findOne(_query).exec()
            
          } catch (err){
            // tslint:disable-next-line:no-console
            console.log(err);
          }
    }

    /**
     * Actualiza el estado de una cola
     * @param options
     * @param queue
     */
    async updateQueue(options, queue): Promise<any[]> {
        const api = new RouterOSClient(options);
        try{
            const client = await api.connect();
            try{
                const resp = await client.menu('/queue simple')
                .where('target', queue.targetId).update({
                    disabled: queue.disabled,
                    name: queue.name,
                    target: queue.target,
                    queue: queue.queue,
                    "max-limit": queue.maxlimit,
                    comment: queue.comment
                });
                api.close();
                return resp;
            }catch (err){
                api.close();
                return err.message;
            }
        }catch (err){
            return err.message;
        }
    }

    /**
     * Obtiene todas las colas de un servidor
     * @param options
     */
    async getQueue(options, queue): Promise<any[]> {
        // tslint:disable-next-line:no-console
        console.log('ip: ' + queue.target);
        const api = new RouterOSClient(options);
        try{
            const client = await api.connect();
            try{
                const resp = await client.menu('/queue simple')
                .where('target', queue.target).get();
                api.close();
                return resp;
            }catch (err){
                api.close();
                return err.message;
            }
        }catch (err){
           return err.message;
        }
    }

    /**
     * Elimina una cola por su id
     * @param options
     * @param queue
     */
    async removeQueue(options, queue): Promise<any[]> {
        const api = new RouterOSClient(options);
        try{
            const client = await api.connect();
            try{
                const resp = await client.menu('/queue simple')
                .where('target', queue.target).remove();
                api.close();
                return resp;
            }catch (err){
                api.close();
                return err.message;
            }
        }catch (err){
            return err.message;
        }
    }
}
