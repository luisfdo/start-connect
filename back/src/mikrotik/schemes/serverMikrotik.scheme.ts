import * as mongoose from 'mongoose';

export const SeverMikrotikSchema = new mongoose.Schema({
    name: {type: String, required: true },
    host: { type: String, required: true },
    user: { type: String, required: true },
    password: { type: String, required: false },
    port: { type: Number, required: false, default: 8728 },
    status: { type: Boolean, required: true },
    company: { type: mongoose.Schema.Types.ObjectId, ref: 'Company', required: false},
});

// export const ServerMikrotik = mongoose.model('ServerMikrotik', SeverMikrotikSchema);