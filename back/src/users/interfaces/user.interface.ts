import { Document } from 'mongoose';

export interface User extends Document {

    readonly name: string;
    readonly photo: string;
    readonly identification: number;
    readonly email: string;
    readonly city: string;
    readonly address: string;
    readonly phone: string;
    readonly rol: string;
    readonly company: string;
    readonly state: number;
    readonly password: string;
    readonly imei: string;
    readonly paw: string;
    readonly session: number;
    readonly super:number;
    readonly codeVerify:string;
    readonly dateCreation:Date;
    readonly balance: string;
    readonly id_customer: string;

}