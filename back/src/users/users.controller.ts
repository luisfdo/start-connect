import { 
    Controller,
    Get, 
    Param, 
    Post, 
    Body, 
    Delete, 
    UsePipes, 
    UseGuards, 
    Query, 
    Res, 
    HttpStatus, 
    NotFoundException, 
    Put 
} from '@nestjs/common';

import { UsersService } from './users.service';
import { CreateUserDTO } from './dto/create-user.dto';
import { ValidateObjectId } from './../shared/pipes/validate-object-id.pipe';
import { ValidationPipe } from './../shared/pipes/validation.pipe';

import { AuthGuard } from '../shared/auth.gaurd';

@Controller('users')
@UseGuards(new AuthGuard())
export class UsersController {

    constructor(private userSvc: UsersService) {}

    @Get()
    async getUsers(@Res() res) {
        const users = await this.userSvc.getUsers();
        return res.status(HttpStatus.OK).json(users);
    }

    @Get(':userID')
    async getUser(@Res() res, @Param('userID', new ValidateObjectId()) userID) {
        const fetchedUser = await this.userSvc.getUser(userID);
        if (!fetchedUser) {
            throw new NotFoundException('User does not exist!');
        }
        return res.status(HttpStatus.OK).json(fetchedUser);
    }

    @Post()
    async addUser(@Res() res, @Body() createUserDTO: CreateUserDTO) {

        

        const addedUser = await this.userSvc.addUser(createUserDTO);
        return res.status(HttpStatus.OK).json({
            message: 'User has been successfully added!',
            user: addedUser,
        });
    }


    @Post(':userPay')
    async addUserPay(@Res() res, @Body() createUserDTO) {

     
        var epayco = require('epayco-node')({
            apiKey: '1fe88116fae7a54f691646cbea559942',
            privateKey: 'b88929621200d1b6a581c31cc5edfb86',
            lang: 'ES',
            test: true
        })

       
        const dataSavePay = {
            token_card:createUserDTO.token_card, 
            name:createUserDTO.name, 
            email:createUserDTO.email, 
            default:createUserDTO.default, 
            city:createUserDTO.city, 
            address:createUserDTO.address, 
            phone:createUserDTO.phone, 
            cell_phone:createUserDTO.cell_phone
        }

        epayco.customers.create(dataSavePay)
        .then(customer => {
            
            console.log('--CUSTOMER--');
            console.log(customer);
            console.log('--CUSTOMER--');

            if(customer.status){
                const updateUserPay = {
                    id_customer:customer.data.customerId
                }
                this.updateUserPay(res, createUserDTO._id, updateUserPay)
            }else{
                return res.status(HttpStatus.NOT_FOUND).json({
                    message: 'Customer error!',
                    customer: false,
                });
            }
            
        })
        .catch(error => {
            console.log(error);
        });

    
    }

    @Put()
    async updateUser(
        @Res() res,
        @Query('userID', new ValidateObjectId()) userID,
        @Body() createUserDTO: CreateUserDTO) {

            const updatedUser = await this.userSvc.updateUser(userID, createUserDTO);
            if (!updatedUser) {
                throw new NotFoundException('User does not exist!');
            }
            return res.status(HttpStatus.OK).json({
                message: 'User has been successfully updated!',
                user: updatedUser,
            });
    }

    @Put()
    async updateUserPay(
        @Res() res,
        @Query('userID', new ValidateObjectId()) userID,
        @Body() createUserDTO) {

            const updatedUser = await this.userSvc.updateUser(userID, createUserDTO);
            if (!updatedUser) {
                throw new NotFoundException('User does not exist!');
            }
            return res.status(HttpStatus.OK).json({
                message: 'User has been successfully updated!',
                user: updatedUser,
            });
    }

    @Delete()
    async deleteUser(@Res() res, @Query('userID', new ValidateObjectId()) userID) {
        const deletedUser = await this.userSvc.deleteUser(userID);
        if (!deletedUser) {
            throw new NotFoundException('User Does not exist!');
        }
        return res.status(HttpStatus.OK).json({
            message: 'User has been successfully deleted!',
            user: deletedUser,
        });
    }


    @Post('auth/login')
    @UsePipes(new ValidationPipe())
    login(@Body() data: CreateUserDTO) {
        return this.userSvc.login(data);
    }



}
