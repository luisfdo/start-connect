import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './interfaces/user.interface';
import { CreateUserDTO } from './dto/create-user.dto';
import * as bcrypt from 'bcryptjs';


@Injectable()
export class UsersService {

    constructor(@InjectModel('User') private readonly userModel: Model<User>){}

    async getUsers(): Promise<User[]> {
        const users = await this.userModel.find().exec();
        return users;
    }

    async getUser(userID): Promise<User> {
        const fetchedUser = await this.userModel
            .findById(userID)
            .exec();
        return fetchedUser;
    }

    async addUser(createUserDTO: CreateUserDTO): Promise<User> {

        const userSave = {
            name: createUserDTO.name,
            photo: createUserDTO.photo,
            identification: createUserDTO.identification,
            email: createUserDTO.email,
            city: createUserDTO.city,
            address: createUserDTO.address,
            phone: createUserDTO.phone,
            rol: createUserDTO.rol,
            company: createUserDTO.company,
            state: createUserDTO.state,
            password: await bcrypt.hash(createUserDTO.password, 10),
            imei: createUserDTO.imei,
            paw: createUserDTO.paw,
            session: createUserDTO.session,
            codeVerify: createUserDTO.codeVerify
        }

        var nodemailer = require('nodemailer');

        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                   user: 'startmedia718@gmail.com',
                   pass: 'Start2022.'
               }
           });
        
           const mailOptions = {
            from: 'startmedia718@gmail.com', 
            to: createUserDTO.email, 
            subject: 'Activación de cuenta start connect', 
            html: '<h1>El codigo para activar su cuenta es <b style="color:blue;">'+createUserDTO.codeVerify+'</b></h1></br><img style="width:12%" src="http://startconnect.co/assets/img/logo-default.png">'
          };

          transporter.sendMail(mailOptions, function (err, info) {
            if(err){
                console.log(err)
            }else{
                console.log('--------');
                console.log(info);
                console.log('--------');
            }
         
              
         });
    
       const addedUser = await this.userModel(userSave);
       return addedUser.save();
    }

    async updateUser(userID, createUserDTO: CreateUserDTO): Promise<User> {
        const updatedUser = await this.userModel
            .findByIdAndUpdate(userID, createUserDTO, {new: true});
        return updatedUser;
    }

    async deleteUser(userID): Promise<User> {
        const deletedUser = await this.userModel
            .findByIdAndRemove(userID);
        return deletedUser;
    }

    
    async login(createUserDTO: CreateUserDTO) {

        const user = await this.userModel.findOne({ email: createUserDTO.email  });
        
        if(!user || !(await bcrypt.compare(createUserDTO.password, user.password))){
            throw new HttpException(
                'Invalid email/password',
                HttpStatus.BAD_REQUEST,
            );
        }

        return user;
    }
}
