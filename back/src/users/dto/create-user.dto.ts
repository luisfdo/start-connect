import { ArrayUnique, IsEmail, IsEnum, IsOptional, IsString } from 'class-validator';


export class CreateUserDTO {
    
    readonly name: string;
    readonly photo: string;
    readonly identification: number;

    @IsEmail()
    readonly email: string;
    readonly city: string;
    readonly address: string;
    readonly phone: string;


    readonly rol: string;
    readonly company: string;
    readonly state: number;

    @IsString()
    readonly password: string;
    readonly imei: string;
    readonly paw: string;
    readonly session: number;

    readonly super:number;
    readonly codeVerify:string;
    readonly dateCreation:Date;
    readonly balance: string;
    readonly id_customer: string;

}