import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({

    name: String,
    photo: String,
    identification: Number,
    email: String,
    city: String,
    address: String,
    phone: String,
    rol: mongoose.Schema.Types.ObjectId,
    company: mongoose.Schema.Types.ObjectId,
    state: Number,
    password: { type: String, required: true },
    imei: String,
    paw: String,
    session: Number,
    super:Number,
    codeVerify:String,
    createdAt: {type: Date, default: Date.now},
    balance: String,
    id_customer: String

});
