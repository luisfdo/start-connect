<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'start');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '0228');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'AOJF`(zaeS~s.{?mb4N4J0iS/bmRBb!^<)%Mfo,AauH14?wxyUG4.3G^!FAz5{(Q');
define('SECURE_AUTH_KEY',  ' UeV`es9?PQ1$( )Wv!4h6N`|`FqCv<&7[iX+HYdt!KAv2+/,$@eXbi~q1WV#zxN');
define('LOGGED_IN_KEY',    't:GxnAzQziyyyr{AJ1gCeK9}^mQN!}&oiw@QIKts5!wRs#^d~[R1n1K~C-;Bv 1Q');
define('NONCE_KEY',        ':ED>~4Fh8BJ`0cBw8O$8i W;z.}KY`{D?B*9e4N b~vA+Wxz;dX%r|mMk(_`C%X{');
define('AUTH_SALT',        'zV0uv=IEjd4?%Sd$,8o^Er{vAXHm7iR!Igh=0Qp!pYQ+y+dB7$h^y GK<W^zc8>h');
define('SECURE_AUTH_SALT', 'm9MwM9#e8#7%-xnaCfHLEK]G,@!TFD`ykIp7J9bodLr?B%Sa{[Z9cF.[R*UE*!aO');
define('LOGGED_IN_SALT',   'i%dTDgR<>tGocWbz-SxArW?_=sXO2_Gc_V*BX#[V#7h&k$Uq=t,Hw@s1W2])<>a8');
define('NONCE_SALT',       ' M4,?Yej4To:%$hM$s{!yYN{51Z&F :4/Y1W0A[Y;:i,%uef& plRr[U`m @J)=G');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
