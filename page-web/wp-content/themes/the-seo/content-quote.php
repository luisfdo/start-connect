<?php
/**
 * The template for displaying posts in the Quote post format
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<span class="icon-quote-left"></span>
	<div class="entry-content">
		<?php
			/* translators: %s: Name of current post */
			the_content();

			wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'the-seo' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-meta">
		<?php theseo_entry_meta(); ?>

		<?php edit_post_link( esc_html__( 'Edit', 'the-seo' ), '<i class="icon icon-pencil-square-o"></i><span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post -->
