
<!-- HEADER 1 START -->
<link rel="stylesheet" type="text/css" href="https://www.somosstart.com.co/wp-content/themes/the-seo/css/start.css">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133197024-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-133197024-1');
</script>
<header class="transparentmenu default">
	<div class="container-fluid">
		<div class="row">
            <div class="container">
				<div class="row">
                <?php
				
				theseo_set_header1_content();

			    echo wp_nav_menu( array( "theme_location" => "header1and5" ) );
				theseo_titleifnologo();
				?>
				</div>
			</div>
        </div>
    </div>
</header>

<?php

theseo_set_customized_slider();

?>

<!-- HEADER 1 END -->