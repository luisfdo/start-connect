<?php
/**
 * The template for displaying posts in the Link post format
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<span class="icon-link22"></span>
	<header class="entry-header">
		<h3 class="entry-title">
			<a href="<?php echo esc_url( theseo_get_link_url() ); ?>"><?php the_title(); ?></a>
		</h3>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				esc_html__( 'Continue reading ', 'the-seo' ), '<span class="meta-nav">&rarr;</span>',
				the_title( '<span class="screen-reader-text">', '</span>', false )
			) );

			wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'the-seo' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) );
		?>
	</div><!-- .entry-content -->


	<div class="entry-meta">
		<?php theseo_entry_meta(); ?>
		<?php if ( get_the_author_meta( 'description' ) && is_multi_author() ) : ?>
			<?php get_template_part( 'author-bio' ); ?>
		<?php endif; ?>
	</div><!-- .entry-meta -->

</article><!-- #post -->
