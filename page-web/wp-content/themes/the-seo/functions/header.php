<?php
/**
 * Functions to display footer section
 */

if ( ! function_exists( 'theseo_titleifnologo' ) ) {
    function theseo_titleifnologo() {
        global $secretlab;
        if (!class_exists('Redux')) {
            echo '<img src="'.get_template_directory_uri() .'/images/logodark.png" alt="';
            bloginfo( 'name' );
            echo '" class="center-block"><h1>';
            bloginfo( 'name' );
            echo '</h1>';
        }

    }
}