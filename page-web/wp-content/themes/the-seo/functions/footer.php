<?php
/**
 * Functions to display footer section
 */

if ( ! function_exists( 'theseo_footer_cta' ) ) {
    function theseo_footer_cta() {
        global $secretlab;
        $sl_footer_cta3 = isset($secretlab['footer-cta3']) ? $secretlab['footer-cta3'] : 1;
        if ($sl_footer_cta3 == 1) {
            echo '<section class="calltoactionblock3">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">';
            if (isset ($secretlab['footer-cta3-title'])) {
                $sl_fcta3t = $secretlab['footer-cta3-title'];
                if (!empty($sl_fcta3t)) {
                    echo '<h3>' . esc_attr($sl_fcta3t) . '</h3>';
                }
            }
            if (isset ($secretlab['footer-cta3-description'])) {
                $sl_fcta3d = $secretlab['footer-cta3-description'];
                if (!empty($sl_fcta3d)) {
                    echo '<p>' . esc_attr($sl_fcta3d) . '</p>';
                }
            }
            echo '</div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">';
            if (isset ($secretlab['footer-cta3-url'])) {
                $sl_fcta3url = $secretlab['footer-cta3-url'];
                if (!empty($sl_fcta3url)) {
                    echo '<a href="' . esc_url($sl_fcta3url) . '" ';
                }
            }
            if (isset ($secretlab['footer-cta3-btnclass'])) {
                $sl_fcta3btnclass = $secretlab['footer-cta3-btnclass'];
                if (!empty($sl_fcta3btnclass)) {
                    echo ' class="btn ' . esc_attr($sl_fcta3btnclass) . ' btn-lg">';
                }
            }
            if (isset ($secretlab['footer-cta3-buttontext'])) {
                $sl_fcta3bt = $secretlab['footer-cta3-buttontext'];
                if (!empty($sl_fcta3bt)) {
                    echo esc_attr($sl_fcta3bt) . '</a>';
                }
            }
            echo '</div>
                        </div>
                    </div>
				</section>';
        }
    }
}


if ( ! function_exists( 'theseo_footer_seolightn_dark' ) ) {
    function theseo_footer_seolightn_dark() {
        global $secretlab;
        $sell_foot_choose = isset($secretlab['footer-type-layout']) ? $secretlab['footer-type-layout'] : 1;
        if ($sell_foot_choose == 1) {
            $sl_design_css = isset($secretlab['design-css']) ? $secretlab['design-css'] : 1;
            if ($sl_design_css == 1 or $sl_design_css == 2 or $sl_design_css == 5) {
                echo '<section class="footer">';

                if (isset ($secretlab['display-footer'])) {
                    $sl_show_f = $secretlab['display-footer'];
                    if ($sl_show_f == 1) {

                        echo '<footer class="contactus">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 gcontact">';
                        if (isset ($secretlab['show-gmap'])) {
                            $sl_show_gmap = $secretlab['show-gmap'];
                            if ($sl_show_gmap == 1) {
                                echo '<div id="footermap"></div>';
                            }
                        }

                        echo '<div class="map-info animated bounceInLeft">
                                        <div class="cusinfopos">
                                            <h2>' . theseo_footer_contactus() . '</h2>';
                        if (isset ($secretlab['phone'])) {
                            $sl_phone = is_array( $secretlab['phone'] ) ? $secretlab['phone'][0] : $secretlab['phone'];
                            if (!empty($sl_phone)) {
                                echo '<strong><a href = "tel:' . esc_attr($sl_phone) . '" > ' . esc_attr($sl_phone) . '</strong></a>';
                            }
                        }
                        theseo_footer_address();
                        if (isset ($secretlab['email'])) {
                            $sl_email = is_array( $secretlab['email'] ) ? $secretlab['email'][0] : $secretlab['email'];
                            if (!empty($sl_email)) {
                                echo '<br><i class="icon-envelope"></i>  <a href="mailto:' . esc_attr($sl_email) . '">' . esc_attr($sl_email) . '</a>';
                            }
                        }
                        $sl_skype = $secretlab['skype'];
                        if (!empty($sl_skype)) {
                            echo '<span><i class="icon-skype"></i> ' . esc_attr($sl_skype) .'</span>';
                        }
                        echo '<br>';
                        $sl_fburl = $secretlab['button-footer-url'];
                        $sl_fbtext = $secretlab['button-footer-text'];
                        if (!empty($sl_fburl)) {
                            echo '<a href="' . esc_url($sl_fburl) . '" class="btn btn-info btn-lg mt40">' . $sl_fbtext . '</a><br>';
                        }

                        echo '<span class="see">' . theseo_footer_seeonmap() . ' <i class="icon-long-arrow-right"></i></span>
                                        </div>
                                    </div>
                                    <span id="dnone" class="showcont btn btn-green animated"><i class="icon-long-arrow-left"></i> ' . theseo_footer_contback() . '</span>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 socialbottom">';
                                theseo_footer_socials();
                                echo '
                                </div>
                            </div>
                        </div>
                    </footer>';
                    }
                }


                echo '<footer class="footerline clearfix">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 subscribebottom">';
                                dynamic_sidebar('_default_bottom_sidebar');
                                echo '</div>
                                                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 text-right">';
                                if (isset ($secretlab['copyr-text'])) {
                                    $sl_copyright = $secretlab['copyr-text'];
                                    if (!empty($sl_copyright)) {
                                        echo ' ' . esc_attr($sl_copyright);
                                    }
                                }
                                echo '.
                                </div>
                            </div>
                        </div>
                    </footer>
                </section>';
                
            }
        }
    }
}


if ( ! function_exists( 'theseo_footer_digital' ) ) {
    function theseo_footer_digital() {
        global $secretlab;
        $sell_foot_choose = isset($secretlab['footer-type-layout']) ? $secretlab['footer-type-layout'] : 1;
        if ($sell_foot_choose == 1) {
            $sl_design_css = isset($secretlab['design-css']) ? $secretlab['design-css'] : 1;
            if ($sl_design_css == 3 or $sl_design_css == 4) {
                if ($sl_design_css == 3 or $sl_design_css == 4) {
                    echo '<section class="footer">';
                    if (isset ($secretlab['display-footer'])) {
                        $sl_show_f = $secretlab['display-footer'];
                        if ($sl_show_f == 1) {

                            echo '<footer class="contactus">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dcontact">';
                            if (isset ($secretlab['show-gmap'])) {
                                $sl_show_gmap = $secretlab['show-gmap'];
                                if ($sl_show_gmap == 1) {
                                    echo '<div id="footermap"></div>';
                                }
                            }
                            echo '<div class="map-info">
                                                            <div class="container">
                                                                <div class="cusinfopos">
                                                                    <h2>' . theseo_footer_contactus() . '</h2>';
                            if (isset ($secretlab['phone'])) {
                                $sl_phone = is_array( $secretlab['phone'] ) ? $secretlab['phone'][0] : $secretlab['phone'];
                                if (!empty($sl_phone)) {
                                    echo '<i class="icon-phone"></i> <a href="tel:' . esc_attr($sl_phone) . '">' . esc_attr($sl_phone) . '</a><br>';
                                }
                            }
                            if (isset ($secretlab['email'])) {
                                $sl_email = is_array( $secretlab['email'] ) ? $secretlab['email'][0] : $secretlab['email'];
                                if (!empty($sl_email)) {
                                    echo '<i class="icon-envelope"></i>  <a href="mailto:'.$sl_email.'">' . esc_attr($sl_email) . '</a><br>';
                                }
                            }
                            theseo_footer_address_digital();
                            echo '
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </footer>';

                        }
                    }

                    if (isset ($secretlab['display-footer-contact'])) {
                        if ($secretlab['display-footer-contact'] == 1) {
                            echo '<footer class="messagesect">
                                <div class="bgrshadowviolet"></div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">';
                            $allowed_html = array(
                                'a' => array(
                                    'href' => array(),
                                    'title' => array()
                                ),
                                'br' => array(),
                                'em' => array(),
                                'strong' => array(),
                                'h1' => array(),
                                'h2' => array(),
                                'h3' => array(),
                                'h4' => array(),
                                'h5' => array(),
                                'h6' => array(),
                                'p' => array(
                                    'style' => array(),
                                ),
                                'b' => array(),
                                'i' => array(),
                                'u' => array(),
                                'ol' => array(),
                                'ul' => array(),
                                'li' => array(),
                                'code' => array(),
                                'del' => array()
                            );
                            if (isset ($secretlab['digital-footer-cta'])) {
                                $sl_digictatext = $secretlab['digital-footer-cta'];
                                echo wp_kses($sl_digictatext, $allowed_html);
                            }
                            echo '</div>
                                        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 mainform">';
                            dynamic_sidebar('_default_bottom_sidebar');
                            echo '
                                        </div>
                                    </div>
                                </div>
                            </footer>';
                        }
                    }

                    echo '<footer class="footerline clearfix">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">';
                    if (isset ($secretlab['scroll-to-top'])) {
                        $sl_totop = $secretlab['scroll-to-top'];
                        if (isset ($secretlab['design-css'])) {
                            if ($sl_design_css == 3 or $sl_design_css == 4) {
                                if ($sl_totop == 1) {
                                    echo '<a href="#" id="scroller"></a>';
                                }
                            }
                        }
                    }

                    echo '&copy; ';
                    echo date('Y') . ' ';
                    if (isset ($secretlab['copyr-text'])) {
                        $sl_copyright = $secretlab['copyr-text'];
                        if (!empty($sl_copyright)) {
                            echo ' ' . esc_attr($sl_copyright);
                        }
                    }
                    echo '
                                </div>
                            </div>
                        </div>
                    </footer>
                </section>';
                }
            }
        }
    }
}


if ( ! function_exists( 'theseo_footer_socials' ) ) {
    function theseo_footer_socials() {
        global $secretlab;
        $sl_social_footer = isset($secretlab['social-footer']) ? $secretlab['social-footer'] : 1;
        if ($sl_social_footer == 1) {
            if (isset ($secretlab['social_link_facebook'])) {
                $sl_fb = $secretlab['social_link_facebook'];
                if (!empty($sl_fb)) {
                    echo '<a href="' . esc_url($sl_fb) . '" target="_blank"><i class="icon-facebook"></i></a>';
                }
            }
            if (isset ($secretlab['social_link_twitter'])) {
                $sl_tw = $secretlab['social_link_twitter'];
                if (!empty($sl_tw)) {
                    echo '<a href="' . esc_url($sl_tw) . '" target="_blank"><i class="icon-twitter"></i></a>';
                }
            }
            if (isset ($secretlab['social_link_myspace'])) {
                $sl_myspace = $secretlab['social_link_myspace'];
                if (!empty($sl_myspace)) {
                    echo '<a href="' . esc_url($sl_myspace) . '" target="_blank"><i class="icon-users"></i></a>';
                }
            }
            if (isset ($secretlab['social_link_linkedin'])) {
                $sl_linkedin = $secretlab['social_link_linkedin'];
                if (!empty($sl_linkedin)) {
                    echo '<a href="' . esc_url($sl_linkedin) . '" target="_blank"><i class="icon-linkedin"></i></a>';
                }
            }
            if (isset ($secretlab['social_link_google'])) {
                $sl_google = $secretlab['social_link_google'];
                if (!empty($sl_google)) {
                    echo '<a href="' . esc_url($sl_google) . '" target="_blank"><i class="icon-google-plus"></i></a>';
                }
            }
            if (isset ($secretlab['social_link_tumblr'])) {
                $sl_tumblr = $secretlab['social_link_tumblr'];
                if (!empty($sl_tumblr)) {
                    echo '<a href="' . esc_url($sl_tumblr) . '" target="_blank"><i class="icon-tumblr"></i></a>';
                }
            }
            if (isset ($secretlab['social_link_pinterest'])) {
                $sl_pinterest = $secretlab['social_link_pinterest'];
                if (!empty($sl_pinterest)) {
                    echo '<a href="' . esc_url($sl_pinterest) . '" target="_blank"><i class="icon-pinterest-p"></i></a>';
                }
            }
            if (isset ($secretlab['social_link_youtube'])) {
                $sl_youtube = $secretlab['social_link_youtube'];
                if (!empty($sl_youtube)) {
                    echo '<a href="' . esc_url($sl_youtube) . '" target="_blank"><i class="icon-youtube-play"></i></a>';
                }
            }
            if (isset ($secretlab['social_link_instagram'])) {
                $sl_instagram = $secretlab['social_link_instagram'];
                if (!empty($sl_instagram)) {
                    echo '<a href="' . esc_url($sl_instagram) . '" target="_blank"><i class="icon-instagram"></i></a>';
                }
            }
            if (isset ($secretlab['social_link_vkcom'])) {
                $sl_vkcom = $secretlab['social_link_vkcom'];
                if (!empty($sl_vkcom)) {
                    echo '<a href="' . esc_url($sl_vkcom) . '" target="_blank"><i class="icon-vk"></i></a>';
                }
            }
            if (isset ($secretlab['social_link_reddit'])) {
                $sl_reddit = $secretlab['social_link_reddit'];
                if (!empty($sl_reddit)) {
                    echo '<a href="' . esc_url($sl_reddit) . '" target="_blank"><i class="icon-reddit"></i></a>';
                }
            }
            if (isset ($secretlab['social_link_blogger'])) {
                $sl_blogger = $secretlab['social_link_blogger'];
                if (!empty($sl_blogger)) {
                    echo '<a href="' . esc_url($sl_blogger) . '" target="_blank"><span class="icon icon-blogger"></span></a>';
                }
            }
            if (isset ($secretlab['social_link_wordpress'])) {
                $sl_wordpress = $secretlab['social_link_wordpress'];
                if (!empty($sl_wordpress)) {
                    echo '<a href="' . esc_url($sl_wordpress) . '" target="_blank"><i class="icon-wordpress"></i></a>';
                }
            }
            if (isset ($secretlab['social_link_behance'])) {
                $sl_behance = $secretlab['social_link_behance'];
                if (!empty($sl_behance)) {
                    echo '<a href="' . esc_url($sl_behance) . '" target="_blank"><i class="icon-behance"></i></a>';
                }
            }
        }
    }
}


