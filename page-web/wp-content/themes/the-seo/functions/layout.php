<?php
/**
 * Functions for layout and texts
 */

function sell_404_title() {
    global $secretlab;
    if (isset ($secretlab['404_title'])) {
        echo esc_html($secretlab['404_title']);
    }
}

function sell_404_descr() {
    global $secretlab;
    if (isset ($secretlab['404_descr'])) {
        echo esc_html($secretlab['404_descr']);
    }
}

// Page H1 heading
if ( ! function_exists( 'sell_entry_page_header' ) ) {
    function sell_entry_page_header() {
        if (class_exists( 'Redux' )) {
            if (isset($secretlab['single-header'])) {
                if ($secretlab['single-header'] == 1) {
                    echo '<h1 class="archive-title">' . get_the_title() . '</h1>';
                }
            }
        } else {
            echo '<h1 class="archive-title">' . get_the_title() . '</h1>';
        }
        if ( post_password_required() ) {
            echo '<h1 class="archive-title">' . get_the_title() . '</h1>';
        }
    }
}


// Page preloader
add_action('wp_head', 'sell_preloader_styles');
if( ! function_exists('sell_preloader_styles')) {
function sell_preloader_styles() {
    global $secretlab;
    if (isset ($secretlab['pageloader'])) {
        if ($secretlab['pageloader'] == 1) {
            echo '<style>
    /* Pageloader */
        #page-preloader {position: absolute;  left: 0; top:0; right: 0; bottom: 0; height: 100%; width: 100%; cursor: default;  pointer-events: none; text-align: center; vertical-align: middle; z-index: 99999999999;}
        #page-preloader .bo {position: absolute; top: calc(50% - 37px); left: calc(50% - 24px); height: 74px; width: 42px; }
        #page-preloader .l1, #page-preloader .l2, #page-preloader .l3 {width:10px; border-radius: 1px; margin: auto 2px 0 2px; float: left; position: absolute;
            bottom: 0;}
        #page-preloader .l1 {animation-name: grow1; left:0; animation-duration: 3s; animation-iteration-count: infinite; animation-timing-function: linear; background-color: #dd445a}
        #page-preloader .l2 {animation-name: grow2; left:15px; animation-duration: 3s; animation-iteration-count: infinite; animation-timing-function: linear;background-color: #26b7e7;}
        #page-preloader .l3 {animation-name: grow3; left:30px; animation-duration: 3s; animation-iteration-count: infinite; animation-timing-function: linear;background-color: #44dd61;}
        @-ms-keyframes grow1 {0% {height:0}	33%,100% {height:30px}}
        @-moz-keyframes grow1 {0% {height:0}	33%,100% {height:30px}}
        @-webkit-keyframes grow1 {0% {height:0}	33%,100% {height:30px}}
        @keyframes grow1 {0% {height:0}	33%,100% {height:30px}}
        @-ms-keyframes grow2 {0%,34% {height:0}	66%,100% {height:45px}}
        @-moz-keyframes grow2 {0%,34% {height:0}	66%,100% {height:45px}}
        @-webkit-keyframes grow2 {0%,34% {height:0}	66%,100% {height:45px}}
        @keyframes grow2 {0%,34% {height:0}	66%,100% {height:45px}}
        @-ms-keyframes grow3 {0%,67% {height:0}	100% {height:65px}}
        @-moz-keyframes grow3 {0%,67% {height:0}	100% {height:65px}}
        @-webkit-keyframes grow3 {0%,67% {height:0}	100% {height:65px}}
        @keyframes grow3 {0%,67% {height:0}	100% {height:65px}}
      </style>';
            echo '<script type="text/javascript">
                    jQuery(document).ready(function ($) {
                        setTimeout(function() {
                              var $preloader = $("#page-preloader");                                
                                $preloader.delay(300).fadeOut("slow");
                        }, 500 );
                    });
                </script>';
        }
    }
}
}

if( ! function_exists('theseo_custom_css_admin')) {
    add_action('admin_head', 'theseo_custom_css_admin');

    function theseo_custom_css_admin()     {
        echo '<style>
    div.revolution-basic-templates {
    height: 100% !important;
}
  </style>';
    }
}