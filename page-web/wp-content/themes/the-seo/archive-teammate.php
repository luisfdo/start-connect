<?php
/**
 * The template for displaying Archive pages
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content catteam" role="main">
			<div id="team-index">
		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<h1 class="archive-title"><?php theseo_team_title(); ?></h1>
			</header><!-- .archive-header -->
			<?php theseo_team_desc(); ?>

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php
				$postofmember = types_render_field("post-of-member", array("output"=>"normal"));
				$facebookprofile = types_render_field("facebook-profile", array("output"=>"normal"));
				$youtubeprofile = types_render_field("youtube-profile", array("output"=>"normal"));
				$twitterprofile = types_render_field("twitter-profile", array("output"=>"normal"));
				$behanceprofile = types_render_field("behance-profile", array("output"=>"normal"));
				$linkedinprofile = types_render_field("linkedin-profile", array("output"=>"normal"));
				$photoofmember = types_render_field( "photo-of-member", array("url" => "true"));
				?>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="item">
					<?php echo '<img src="'.esc_url($photoofmember).'" alt="" class="text-left" />'; ?>
					<div class="overmember"><a href="<?php the_permalink(); ?>"><i class="icon-link"></i></a></div>
					<strong><?php the_title(); ?></strong>
					<span class="desrdivider"><?php echo esc_attr($postofmember); ?></span>
					<?php the_excerpt(); ?>
					<div class="socialprofiles"><?php
						if (!empty($facebookprofile)) {
							echo '<a href="'.esc_url($facebookprofile).'" target="_blank"><i class="icon-facebook"></i></a>';
						}
						if (!empty($youtubeprofile)) {
							echo '<a href="'.esc_url($youtubeprofile).'" target="_blank"><i class="icon-youtube-play"></i></a>';
						}
						if (!empty($twitterprofile)) {
							echo '<a href="'.esc_url($twitterprofile).'" target="_blank"><i class="icon-twitter"></i></a>';
						}
						if (!empty($behanceprofile)) {
							echo '<a href="'.esc_url($behanceprofile).'" target="_blank"><i class="icon-behance"></i></a>';
						}
						if (!empty($linkedinprofile)) {
							echo '<a href="'.esc_url($linkedinprofile).'" target="_blank"><i class="icon-linkedin"></i></a>';
						}
						?></div>
					</div>
				</div>


			<?php endwhile; ?>

			<?php theseo_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>
			</div>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
