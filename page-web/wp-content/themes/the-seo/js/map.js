
	
	jQuery(document).ready(function($){
	    var map = new GMaps({
		    el: '#footermap',
		    lat: 4.5327335,
			lng: -75.6738339,17,
			zoom: 17,
			scrollwheel : false,
			zoomControl : true,
			    zoomControlOpt: {
				    style : 'SMALL',
					position: 'TOP_LEFT'
				},
			panControl : false,
            streetViewControl : false,
            mapTypeControl: false,
            overviewMapControl: false
        });	

        map.addMarker({
            lat: 4.5327335,
            lng: -75.6738339,17,
			icon: 'http://www.somosstart.com.co/wp-content/uploads/2019/01/Screenshot_2.png'
		});
		var styles = [
		    {
			    stylers: [
				    { hue: "#95a5a6" },
					{ saturation: -100 }
				]
			}, {
					featureType: "road",
					elementType: "geometry",
					stylers: [
						{ lightness: 100 },
						{ visibility: "simplified" }
					]
			}, {
					featureType: "road",
					elementType: "labels",
					stylers: [
						{ visibility: "off" }
					]
				}
            ];

			map.addStyle({
				styledMapName:"Styled Map",
				styles: styles,
				mapTypeId: "map_style"
			});

			map.setStyle("map_style");

		});