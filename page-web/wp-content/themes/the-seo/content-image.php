<?php
/**
 * The template for displaying posts in the Image post format
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<span class="icon icon-image3"></span>
	<header class="entry-header">
		<?php if ( is_single() ) : ?>
		<h1 class="entry-title"><?php the_title(); ?></h1>
		<?php else : ?>
		<h3 class="entry-title">
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
		</h3>
		<?php endif; // is_single() ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				esc_html__( 'Continue reading %s ', 'the-seo' ), '<span class="meta-nav">&rarr;</span>',
				the_title( '<span class="screen-reader-text">', '</span>', false )
			) );

			wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'the-seo' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) );
		?>
		<div class="clearfix"></div>
	</div><!-- .entry-content -->

	<div class="entry-meta">
		<?php theseo_entry_meta(); ?>

		<?php edit_post_link( esc_html__( 'Edit', 'the-seo' ), '<i class="icon icon-pencil-square-o"></i><span class="edit-link">', '</span>' ); ?>

		<?php if ( is_single() && get_the_author_meta( 'description' ) && is_multi_author() ) : ?>
			<?php get_template_part( 'author-bio' ); ?>
		<?php endif; ?>
	</div><!-- .entry-meta -->
</article><!-- #post -->
