<?php
/*
Teammate template page - spesial for teammmate custom post type
*/

get_header(); ?>


                <?php /* The loop */ ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                        <?php
                        $postofmember = types_render_field("post-of-member", array("output"=>"normal"));
                        $facebookprofile = types_render_field("facebook-profile", array("output"=>"normal"));
                        $youtubeprofile = types_render_field("youtube-profile", array("output"=>"normal"));
                        $twitterprofile = types_render_field("twitter-profile", array("output"=>"normal"));
                        $behanceprofile = types_render_field("behance-profile", array("output"=>"normal"));
                        $linkedinprofile = types_render_field("linkedin-profile", array("output"=>"normal"));
                        $photoofmember = types_render_field( "photo-of-member", array("url" => "true"));
                        echo '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 teammate paddingright mb40 tar"><img src="'.esc_url($photoofmember).'" alt="" ></div>';
                        ?>

                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 mb40 teammate" id="teammate">
                            <h1><?php the_title(); ?></h1>
                            <div class="headinginfo"><?php echo esc_attr($postofmember); ?></div>
                            <?php
                                /* translators: %s: Name of current post */
                                the_content();
                                ?>
                            <div class="socialprofiles">
                            <?php
                            if (!empty($facebookprofile)) {
                                echo '<a href="'.esc_url($facebookprofile).'" target="_blank"><i class="icon-facebook"></i></a>';
                            }
                            if (!empty($youtubeprofile)) {
                                echo '<a href="'.esc_url($youtubeprofile).'" target="_blank"><i class="icon-youtube-play"></i></a>';
                            }
                            if (!empty($twitterprofile)) {
                                echo '<a href="'.esc_url($twitterprofile).'" target="_blank"><i class="icon-twitter"></i></a>';
                            }
                            if (!empty($behanceprofile)) {
                                echo '<a href="'.esc_url($behanceprofile).'" target="_blank"><i class="icon-behance"></i></a>';
                            }
                            if (!empty($linkedinprofile)) {
                                echo '<a href="'.esc_url($linkedinprofile).'" target="_blank"><i class="icon-linkedin"></i></a>';
                            }
?>
                            </div>

                        </div>


                    </article><!-- #post -->

                    <?php theseo_post_nav(); ?>


                <?php endwhile; ?>

<?php get_footer(); ?>