
<!-- HEADER 2 START -->


<header class="headermenuwhitebox">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <div class="topbartransparent">
                <?php theseo_set_header2_content(); ?>
                </div>
                <div id="navbarwhitebox">
                        <?php echo wp_nav_menu( array( "theme_location" => "header2" ) ); ?>
                </div><!-- /.container-fluid -->
            </div>
        </div>
    </div>
</header>


<?php

theseo_set_customized_slider();

?>

<!-- HEADER 2 END -->