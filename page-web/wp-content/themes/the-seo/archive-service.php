<?php
/**
 * The template for displaying Archive pages
 */

get_header(); ?>

	<div id="primary" class="content-area <?php theseo_set_custom_posttype_sidebar_layout();  ?> ">
		<header class="archive-header">
			<h1 class="archive-title"><?php theseo_services_title(); ?></h1>
		</header><!-- .archive-header -->
		<?php theseo_services_desc(); ?>

		<div id="content" class="site-content serviceslistmain" role="main">
		<?php if ( have_posts() ) : ?>
			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 slist">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p0">
						<?php
						$servico = types_render_field( "sicon", array("url" => "true"));
						echo '<img src="'.esc_url($servico).'"  alt="" class="center-block img-responsive margintop">';
						?>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
						<h3><?php the_title(); ?></h3>
						<?php the_excerpt(); ?>
						<a href="<?php the_permalink(); ?>" class="more"><?php theseo_services_read_more(); ?> <i class="fa fa-long-arrow-right"></i></a>
					</div>
				</div>
			<?php endwhile; ?>

			<?php theseo_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
