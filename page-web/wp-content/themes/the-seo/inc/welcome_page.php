<?php

	wp_register_script('theseo_import_attach_js', esc_url(get_template_directory_uri()) . '/js/import_attach.js', array('jquery', 'jquery-ui-tooltip', 'jquery-ui-progressbar'), 20140421, true );
	wp_enqueue_script( 'theseo_import_attach_js');

    wp_register_style( 'theseo_welcome', esc_url(get_template_directory_uri()) . '/inc/welcome.css', array(), '3.03' );
    wp_enqueue_style( 'theseo_welcome');
	
	wp_register_style( 'theseo_jquery_ui', esc_url(get_template_directory_uri()) . '/inc/jquery-ui.css', array(), '3.03' );
    wp_enqueue_style( 'theseo_jquery_ui');
	
	wp_register_script('theseo_welcome_js', esc_url(get_template_directory_uri()) . '/js/welcome.js', array('jquery'), '1.0', true );
	wp_enqueue_script( 'theseo_welcome_js');	
	
    wp_register_script('theseo_boot_js', esc_url(get_template_directory_uri()) . '/js/bootstrap.min.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'theseo_boot_js');		
	
	wp_localize_script( 'theseo_import_attach_js', 'aiL10n', array(
	    'import_start' => __( 'Start of attachments import - reading appropriate XML file', 'the-seo' ),
		'import_start_failed' => __( 'XML file reading error occurred - check its existence', 'the-seo' ),
	    'emptyInput' => __( 'Please select a file.', 'the-seo' ),
	    'noAttachments' => __( 'There were no attachment files found in the import file.', 'the-seo' ),
		'parsing' => __( 'Parsing the file.', 'the-seo' ),
		'importing' => __( 'Importing file ', 'the-seo' ),
		'progress' => __( 'Overall progress: ', 'the-seo' ),
		'retrying' => __( 'An error occurred. In 5 seconds, retrying file ', 'the-seo' ),
		'done' => __( 'All done!', 'the-seo' ),
		'ajaxFail' => __( 'There was an error connecting to the server.', 'the-seo' ),
		'pbAjaxFail' => __( 'The program could not run. Check the error log below or your JavaScript console for more information', 'the-seo' ),
		'fatalUpload' => __( 'There was a fatal error. Check the last entry in the error log below.', 'the-seo' )
	) );	

$plugins_to_activate = array ( 'wordpress-importer/wordpress-importer.php', 'js_composer/js_composer.php', 
				   'Ultimate_VC_Addons/Ultimate_VC_Addons.php', 'revslider/revslider.php', 
				   'envato-market/envato-market.php', 'contact-form-7/wp-contact-form-7.php',
				   'widget-importer-exporter/widget-importer-exporter.php', 'woocommerce/woocommerce.php',
				   'redux-framework/redux-framework.php',
					 'wysija-newsletters/index.php',
					'wordpress-seo/wp-seo.php', 'SectretLabVcWidget/composer-post-widget.php');
						   
$current = get_option( 'active_plugins' );
	
$opts = get_transient('seo_on_click_setup');
if (!is_array($opts)) $opts = array( 'theme_type' => 'light', 'install_plugins' => 1, 'activate_plugins' => 1, 'import_widgets' => 1, 
                                     'set_theme_options' => 1, 'set_suppamenu_skins' => 1, 'import_color_shemes' => 1, 
									 'set_sliders' => 1, 'technical_refresh' => 1,
									 'set_types' => 1, 'set_icons' => 1, 'set_post_and_menu_screens' => 1, 'import_sample_data' => 1, 'i_id' => 1,
                                     'install_theme' => 1, 'import_data' => 1, 'import_attachments' => 1);
if (isset($_COOKIE['seo_theme_type'])) $opts['theme_type'] = $_COOKIE['seo_theme_type'];
$theme_type = $opts['theme_type'];
$theme_names = array('light' => 'SEO Light version',
	'dark' => 'SEO Dark version',
	'digital' => 'Digital version',
	'digitaldark' => 'Digital Dark version',
	'flat' => 'Flat version');

$checked = array();

foreach ($opts as $name => $val) {
    if ($val == 1) $checked[$name] = ' checked="checked"'; else $checked[$name] = '';
}

echo '<div class="increase">';

echo '<div class="container">';

if (function_exists('welcome_notice')) {
    welcome_notice();
	global $wn;
    echo $wn['real_capabilities'];
	$install_log_class = "install_log_active";
}
else {
    $install_log_class = "install_log_passive";
}

function sell_inst_error_mess() {
	if (!function_exists('welcome_notice')) {
		echo '<div class="noinstaller">'.esc_html__('Install SecretLab Installer plugin to install the theme', 'the-seo').'</div>';
	}
}
function sell_hide() {
	if (!function_exists('welcome_notice')) {
		$add_class = ' style="display:none !important"';
	}
	else $add_class = '';
    return $add_class;
}



 echo '<h1>'.esc_html__('Welcome to The SEO Theme', 'the-seo').'</h1>
 		<p class="descr">'.esc_html__('Fully Customizable and Easy-to-use', 'the-seo').'</p>
  			<ul class="nav-tabs" role="tablist"'.sell_hide().'>
                        <li role="presentation" class="active"><a href="#firsttab" aria-controls="home" role="tab" data-toggle="tab">'.esc_html__('Auto Install', 'the-seo').'</a></li>
                        <li role="presentation"><a href="#secondtab" aria-controls="profile" role="tab" data-toggle="tab">'.esc_html__('Manual Install', 'the-seo').'</a></li>
                    </ul>

<div class="tab-content" >';
sell_inst_error_mess();
      echo '<div role="tabpanel" class="tab-pane fade in active" id="firsttab"'.sell_hide().'>

            
            <div class="row">
                <div class="col-md-6 col-sm-12">
                <h2>1. '.esc_html__('Automatic installation can take 2-8 min', 'the-seo').'</h2>
                <div  id="theme_setup_box">
                    
                    <div id="progressBar1" class="progressBar"><div class="progress_bar"></div><div class="progress_count"></div></div>
					
					<div id="theme_setup_summary" style="position:relative">
					
						
					
		                <div id="theme_setup_control" style="position:relative">
						 <div id="theme_type">
							 <span>'.esc_html__('Select Theme version', 'the-seo').'</span>
							 <select id="theme_type_select" name="theme_type">';
									foreach ($theme_names as $tn=>$title) {
										if ($tn == $opts['theme_type'])
											echo '<option selected="selected" value="' . $tn . '">' . $title. '</option>';
										else
											echo '<option value="' . $tn . '">' . $title.'</option>';
									}
									echo '
							 </select>
						 </div>
						 <div class="welcome_install">
						    <p><input id="install_theme_info" class="bulk_install_item" type="checkbox" name="install_theme"' . $checked['install_theme'] . ' /> <b>'.esc_html__('Install Theme', 'the-seo').'</b></p>
						</div>
						
							<div id="theme_setup_control" class="install_steps" style="position:relative">
								<div class="welcome_install">
									<p><input id="install_plugins_checkbox" type="checkbox" name="install_plugins"' . $checked['install_plugins'] . ' /> <b>'.esc_html__('Install plugins', 'the-seo').'</b></p>
								</div>
								<div class="welcome_install">
									<p><input id="activate_plugins_checkbox" type="checkbox" name="activate_plugins"' . $checked['activate_plugins'] . ' /> <b>'.esc_html__('Activate plugins', 'the-seo').'</b></p>
								</div>								
								<div class="welcome_install">
									<input id="import_widgets_checkbox" name="import_widgets" type=checkbox ' . $checked['import_widgets'] . ' /> <b>'.esc_html__('Import Widgets', 'the-seo').'</b></p>
								</div>							
								<div class="welcome_install">
									<input id="set_theme_options_checkbox" name="set_theme_options" type=checkbox ' . $checked['set_theme_options'] . ' /> <b>'.esc_html__('Import Theme Options', 'the-seo').'</b></p>
								</div>
								<div class="welcome_install">
									<input id="set_suppamenu_skins_checkbox" name="set_suppamenu_skins" type=checkbox ' . $checked['set_suppamenu_skins'] . ' /> <b>'.esc_html__('Import Suppa Menu Skins', 'the-seo').'</b></p>
								</div>								
								<div class="welcome_install">
									<input id="import_color_shemes_checkbox" name="import_color_shemes" type=checkbox ' . $checked['import_color_shemes'] . ' /> <b>'.esc_html__('Import Color Shemes', 'the-seo').'</b></p>
								</div>								
								<div class="welcome_install">
									<input id="set_sliders_checkbox" name="set_sliders" type=checkbox ' . $checked['set_sliders'] . ' /> <b>'.esc_html__('Import Demo Revolution Sliders', 'the-seo').'</b></p>
								</div>
								<div class="welcome_install">
									<input id="technical_refresh_checkbox" name="technical_refresh" type=checkbox ' . $checked['technical_refresh'] . ' /> <b>'.esc_html__('Do Page Technical Refresh', 'the-seo').'</b></p>
								</div>	
								<div class="welcome_install">
									<input id="set_types_checkbox" name="set_types" type=checkbox ' . $checked['set_types'] . ' /> <b>'.esc_html__('Import Types Plugin Settings', 'the-seo').'</b></p>
								</div>
								<div class="welcome_install">
									<input id="set_icons_checkbox" name="set_icons" type=checkbox ' . $checked['set_icons'] . ' /> <b>'.esc_html__('Import Icon Fonts', 'the-seo').'</b></p>
								</div>								
								<div class="welcome_install">
									<input id="set_post_and_menu_screens_checkbox" name="set_post_and_menu_screens" type=checkbox ' . $checked['set_post_and_menu_screens'] . ' /> <b>'.esc_html__('Set Admin Pages Settings', 'the-seo').'</b></p>
								</div>
								
							</div>						 
								
							</div>
							
						</div>	
							
						<div id="theme_import_summary" style="position:relative">
						
						    <div class="welcome_install">
								<p><input id="import_data_info" class="bulk_install_item" type="checkbox" name="import_data"' . $checked['import_data'] . ' /> <b>'.esc_html__('Import Data', 'the-seo').'</b></p>
							</div>							
						
						    <div id="theme_import_control" class="install_steps">
                                <div class="welcome_install">
							        <input id="import_sample_data_checkbox" name="import_sample_data" type=checkbox ' . $checked['import_sample_data'] . ' /> <b>'.esc_html__('Import Demo Data', 'the-seo').'</b></p>
							    </div>
                                <div class="welcome_install">
							        <input id="import_attachments_checkbox" name="import_attachments" type=checkbox ' . $checked['import_attachments'] . ' /> <b>'.esc_html__('Import attachments', 'the-seo').'</b></p>
							    </div>
                                <div id="import_attachment_data">
								    <!--<p><input type="file" name="file" id="attachments_file"/></p>-->
	                                <p>' . __( 'Attribute uploaded images to:', 'the-seo' ) . '<br/>
		                            <input type="radio" name="author" value=1 checked />&nbsp;' . __( 'Current User', 'the-seo' ) . '<br/>
		                            <input type="radio" name="author" value=2 />&nbsp;' . __( 'User in the import file', 'the-seo') . '<br/>
		                            <input type="radio" name="author" value=3 />&nbsp;' . __( 'Select User:', 'the-seo' ) . wp_dropdown_users(array('echo' => false)) .

	                                '<p><input type="checkbox" checked="checked" name="delay" />&nbsp;' . __( 'Delay file requests by at least five seconds.', 'the-seo' ) . '&nbsp;<a href="#" title="' . __( 'This delay can be useful to mitigate hosts that throttle traffic when too many requests are detected from an IP address and mistaken for a DDOS attack.', 'the-seo' ) . '" style="text-decoration:none;"><span class="dashicons dashicons-editor-help"></span></a></p> 									
                                </div>								
							</div>

                        </div>							

						<div><a id="theme_setup_submit" class="meta_btn" href="#">'.esc_html__('Start installation', 'the-seo').'</a></div>
						
					</div>
						<h2 class="second">'.esc_html__('Log of installation', 'the-seo').'</h2>
						<div class="'.$install_log_class.'">
							   <div id="theme_setup_result"></div>
						</div>
                </div>
                <div class="col-md-6 col-sm-12">
                	<h2>'.esc_html__('2. Setup your theme', 'the-seo').'</h2>
					<div class="helpful">
						<ol>
						<li><a href="admin.php?page=revslider">'.esc_html__('Edit Sliders', 'the-seo').'</a></li>
						<li><a href="admin.php?page=_options">'.esc_html__('Theme Options', 'the-seo').'</a> '.esc_html__('(choose colors, design, pageloader, type of footer and header)', 'the-seo').'</li>
						<li><a href="admin.php?page=_options&tab=11">'.esc_html__('Typography', 'the-seo').'</a></li>
						<li><a href="nav-menus.php">'.esc_html__('Menu', 'the-seo').'</a> '.esc_html__('(configure your menu items)', 'the-seo').'</li>
						<li><a href="admin.php?page=CTF_suppa_menu">'.esc_html__('Menu Skins and Logo', 'the-seo').'</a> '.esc_html__('(change design of menu)', 'the-seo').'</li>
						<li><a href="edit.php?post_type=composer_widget">'.esc_html__('Custom Footer', 'the-seo').'</a> '.esc_html__('(if you choose it at Theme Options -> Design -> Footer)', 'the-seo').'</li>
						<li><a href="widgets.php">'.esc_html__('Widgets', 'the-seo').'</a> '.esc_html__('(choose custom footer to display)', 'the-seo').'</li>
						<li><a href="admin.php?page=bsf-font-icon-manager">'.esc_html__('Icon Fonts', 'the-seo').'</a></li>
						<li><a href="admin.php?page=wpcf7">'.esc_html__('Contact Forms', 'the-seo').'</a> '.esc_html__('(setup your email data to receive requests)', 'the-seo').'</li>
						</ol>
					</div>
                </div>
            </div>
            

            <div class="row pt30">';
				if (function_exists('welcome_notice')) {
				    global $wn;
				    echo $wn['recommended_capabilities'];
				    echo $wn['fail_install'];
				}
				
				echo '
                <div class="col-md-4 col-sm-12">
                    <h2 class="second">'.esc_html__('Support', 'the-seo').'</h2><div class="inform">
                    
                    <a href="http://secretlab.pw/doc/seowp/" target="_blank">'.esc_html__('Online documentation', 'the-seo').'</a><br>
                    <a href="http://secretlab.pw/helpdesk/ticketsnewguest.php" target="_blank">'.esc_html__('Send a Ticket', 'the-seo').'</a>
                    <p>'.esc_html__('Response time is 24 hours', 'the-seo').'</p>
                    <p>'.esc_html__('Send your site, admin login and password please. It is increase time of solving your problem', 'the-seo').'</p>
                    </div>
                </div>
            </div>
            
            <div class="row pt30">
            	<div class="col-md-6 col-sm-12">
            		<div class="manager">
            			<img src="'.get_template_directory_uri().'/images/manager.jpg" alt="Rate the theme">
            			<strong>'.esc_html__('Manager of Customer Care', 'the-seo').'</strong>
            			<p class="name"">'.esc_html__('Oleg Malkov', 'the-seo').'</p>
            			<p>'.esc_html__('Send us feedback and criticism. Your feedback and experience is valuable for us! It will help to improve the theme.', 'the-seo').'</p>
            			<b>m@secretlab.pw</b>
            		</div>
            	</div>
            	<div class="col-md-6 col-sm-12">
            		<div class="rate">
            			<b>'.esc_html__('Don\'t Forget To Rate The Theme!', 'the-seo').'</b>
            			<a href="http://themeforest.net/downloads" target="_blank"><img src="'.get_template_directory_uri().'/images/rate.png" alt="Rate the theme"></a>
            			<a href="http://themeforest.net/downloads" target="_blank">http://themeforest.net/downloads</a>
            		</div>
            	</div>
            </div>
            <div class="row pt30">
            	<div class="col-md-12">
            		
            	</div>
            </div>
</div>

<div role="tabpanel" class="tab-pane fade" id="secondtab"'.sell_hide().'>
	<div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="manins">
                                <p>'.esc_html__('You can use it if you got any error and dont want to wait a solution from your hosting company.', 'the-seo').'</p>
                                <p>'.esc_html__('Details about manual installations process: ', 'the-seo').'<a href="http://secretlab.pw/doc/seowp/#line4" target="_blank">www.secretlab.pw/doc/seowp/#line15</a></p>
								<div id="progressBar2" class="progressBar"><div  class="progress_bar"></div><div class="progress_count"></div></div>';
								echo '
								 <div id="theme_type">
									 <span>'.esc_html__('Select Theme version', 'the-seo').'</span>
									 <select id="manual_theme_type_select" name="theme_type">';
											foreach ($theme_names as $tn=>$title) {
												if ($tn == $opts['theme_type'])
													echo '<option selected="selected" value="' . $tn . '">'. $title. '</option>';
												else
													echo '<option value="' . $tn . '">' . $title. '</option>';
											}
											echo '
									 </select>
								 </div>';
echo '<ol class="step">';
echo '<li><span>1</span> <a target="_blank" href="themes.php?page=install-required-plugins&plugin_status=install">'.esc_html__('Begin installing plugins', 'the-seo').'</a> <a id="manual_install_plugins" class="manual_install manual_btn" href="#">'.esc_html__('Do It', 'the-seo').'</a></li>';

echo '<li><span>2</span> <a target="_blank" href="themes.php?page=install-required-plugins&plugin_status=activate">'.esc_html__('Begin activating plugins', 'the-seo').'</a> <a id="manual_activate_plugins" class="manual_install manual_btn" href="#">'.esc_html__('Do It', 'the-seo').'</a></li>';

echo '<li><span>3</span> <a href="themes.php?page=secretlab-welcome">'.esc_html__('Refresh the page', 'the-seo').'</a></li>';

echo '<li><span>4</span> <a target="_blank" href="admin.php?page=toolset-export-import">'.esc_html__('Import Custom Post Types', 'the-seo').'</a> <a id="manual_set_types" class="manual_install manual_btn" href="#">'.esc_html__('Do It', 'the-seo').'</a></p></li>';

echo '<li><span>5</span> <a target="_blank" href="admin.php?page=revslider">'.esc_html__('Import Sliders', 'the-seo').'</a> <a id="manual_set_sliders" class="manual_install manual_btn" href="#">'.esc_html__('Do It', 'the-seo').'</a></li>';

echo '<li><span>6</span> <a target="_blank" href="admin.php?page=backup-options">'.esc_html__('Import Suppa Menu Skins', 'the-seo').'</a> <a id="manual_set_suppamenu_skins" class="manual_install manual_btn" href="#">'.esc_html__('Do It', 'the-seo').'</a></li>';

echo '<li><span>7</span> <a target="_blank" href="admin.php?page=_options&tab=16">'.esc_html__('Import Theme Option Set', 'the-seo').'</a> <a id="manual_set_theme_options" class="manual_install manual_btn" href="#">'.esc_html__('Do It', 'the-seo').'</a></li>';

echo '<li><span>8</span> <a target="_blank" href="admin.php?page=_options&tab=14">'.esc_html__('Import Color Scheme Sets', 'the-seo').'</a> <a id="manual_import_color_shemes" class="manual_install manual_btn" href="#">'.esc_html__('Do It', 'the-seo').'</a></li>';

echo '<li><span>9</span> <a target="_blank" href="admin.php?page=bsf-font-icon-manager">'.esc_html__('Import Icon Fonts', 'the-seo').'</a><a id="manual_set_icons" class="manual_install manual_btn" href="#">'.esc_html__('Do It', 'the-seo').'</a></li>';

echo '<li><span>10</span> <a target="_blank" href="nav-menus.php">'.esc_html__('Set Menu and Discussions Checkboxes', 'the-seo').'</a> <a id="manual_set_post_and_menu_screens" class="manual_install manual_btn" href="#">'.esc_html__('Do It', 'the-seo').'</a></li>';


echo '<hr>'.esc_html__('Theme Setup completed. You can import demo-data, if it necessary', 'the-seo').'<hr>';

echo '<li><span>11</span> <a target="_blank" href="admin.php?import=wordpress">'.esc_html__('Import Dummy Data (Skip it, if you dont need demo-data)', 'the-seo').'</a>'.esc_html__(' If it got error, you can run it again 2-6 times (at slow webhosting) until you get a success message', 'the-seo').' <a id="manual_import_sample_data" class="manual_install manual_btn" href="#">'.esc_html__('Do It', 'the-seo').'</a></li>';
echo '</p>';

echo '<li><span>12</span> <a target="_blank" href="admin.php?import=wordpress">'.esc_html__('Import Dummy Data Attachments, It can take 5-15 min (Skip it, if you dont need demo-data)', 'the-seo').'</a> <a id="manual_import_attachments" class="manual_install manual_btn" href="#">'.esc_html__('Do It', 'the-seo').'</a></li>';

echo '<li><span>13</span> <a target="_blank" href="tools.php?page=widget-importer-exporter">'.esc_html__('Import Widgets', 'the-seo').'</a> <a id="manual_import_widgets" class="manual_install manual_btn" href="#">'.esc_html__('Do It', 'the-seo').'</a></li>';


echo '</ol>';
echo '</div></div>';

echo '<div class="col-md-6 col-sm-12 ">
		<h2 class="second">'.esc_html__('Log of installation', 'the-seo').'</h2>
		<div class="' . esc_attr($install_log_class, 'the-seo') . '">
                       
                       <div id="manual_theme_install_result"></div>
                  </div>
             </div>';
echo '</div>';
echo '</div></div>
</div>
</div>';

echo '<div id="theme_type_dialog">
          <div id="theme_type_dialog_content">'.esc_html__('It seems you have started SEO Theme Install already, now you are trying to change Theme Type to another one. Are you sure?', 'the-seo').'</div>
		  <a id="theme_type_dialog_continue" class="theme_type_dialog_button manual_btn" href="#">'.esc_html__('Continue', 'the-seo').'</a>
		  <a id="theme_type_dialog_stop" class="theme_type_dialog_button manual_btn" href="#">'.esc_html__('Stop', 'the-seo').'</a>
	  </div>';

?>
