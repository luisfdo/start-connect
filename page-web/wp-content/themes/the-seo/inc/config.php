<?php

    /** Theme Options Config File */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }
	
	if (class_exists('ReduxFrameworkPlugin')) {
        include_once ABSPATH.'/wp-admin/includes/file.php';

        WP_Filesystem();
	    if (!file_exists(WP_PLUGIN_DIR . '/redux-framework/ReduxCore/inc/extensions/color_scheme/extension_color_scheme.php')) {
            $r = unzip_file( get_template_directory().'/lib/ext/color_scheme.zip',  WP_PLUGIN_DIR . '/redux-framework/ReduxCore/inc/extensions/');
	        if ($r !== true) { }
	    }
	    if (!file_exists(WP_PLUGIN_DIR . '/redux-framework/ReduxCore/inc/extensions/widget_areas/extension_widget_areas.php')) {
            $r = unzip_file( get_template_directory().'/lib/ext/widget_areas.zip',  WP_PLUGIN_DIR . '/redux-framework/ReduxCore/inc/extensions/');
	        if ($r !== true) { }
	    }	

	}


    // This is your option name where all the Redux data is stored.
    $opt_name = "secretlab";
    // Allowed HTML tags for escaping of translite texts
    $allowed_html = array(
        'a' => array(
            'href' => array(),
            'title' => array(),
            'target' => array(),
        ),
        'br' => array(),
        'em' => array(),
        'strong' => array(),
        'h1' => array(),
        'h2' => array(),
        'h3' => array(),
        'h4' => array(),
        'h5' => array(),
        'h6' => array(),
        'p' => array(
            'style' => array(),
        ),
        'b' => array(),
        'i' => array(),
        'u' => array(),
        'ol' => array(),
        'ul' => array(),
        'li' => array(),
        'code' => array(),
        'del' => array()
    );

    // Background Patterns Reader
    $sample_patterns_path = ReduxFramework::$_dir . '/patterns/';
    $sample_patterns_url  = ReduxFramework::$_url . '/patterns/';
    $sample_patterns      = array();

    if ( is_dir( $sample_patterns_path ) ) {

        if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
            $sample_patterns = array();

            while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                    $name              = explode( '.', $sample_patterns_file );
                    $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                    $sample_patterns[] = array(
                        'alt' => $name,
                        'img' => $sample_patterns_url . $sample_patterns_file
                    );
                }
            }
        }
    }

    function get_sidebars() {
        $sidebars = array();
	    foreach ($GLOBALS['wp_registered_sidebars'] as $sb) {
	        $sidebars[$sb['id']] = $sb['name']; 
	    }
        return $sidebars;		
	}

    function theseo_get_sliders_array() {

	    global $wpdb;
	
	    $arr = array( 0 => 'none');
	
	    if (class_exists('LS_Sliders')) {
	        $sliders = LS_Sliders::find();
            foreach ($sliders as $slider) {
	            $arr['lay_'.$slider['id']] = $slider['name'];
	        }
	    }

	    $RsExists = count($wpdb->get_results("SELECT * FROM information_schema.tables WHERE table_schema = '".$wpdb->dbname."' AND table_name = '".$wpdb->prefix."revslider_sliders' LIMIT 1", ARRAY_A));
	    if ($RsExists > 0) {
	        $revSliders = $wpdb->get_results("SELECT title, alias FROM ".$wpdb->prefix."revslider_sliders", ARRAY_A);
	        if (count($revSliders) > 0) {
	            foreach ($revSliders as $slider) {
		            $arr['rev_'.$slider['alias']] = $slider['title'];
		        }
	        }
	    }
	
	    if (count($arr) == 1) {
	        $arr = array( 0 => esc_attr__('The Theme Support Layer Slider and Slider Revolution, but couldn\'t find it. Install one of the plug-ins to choose the slider to display in the header.', 'the-seo') );
	    }
	
	    return $arr;
    }

    /*=== Dev mode disable ===*/
    function removeDemoModeLinkSell() { // Be sure to rename this function to something more unique
        if ( class_exists('ReduxFrameworkPlugin') ) {
            remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2 );
        }
        if ( class_exists('ReduxFrameworkPlugin') ) {
            remove_action('admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );
        }
    }
    add_action('init', 'removeDemoModeLinkSell');

    /*=== Adding custom CSS for Theme Options design ===*/
    function addPanelCSS() {
        wp_register_style(
            'redux-custom-css',
            esc_url(get_template_directory_uri()) . '/inc/redux-custom-css.css',
            array( 'redux-admin-css' ), // Be sure to include redux-admin-css so it's appended after the core css is applied
            time(),
            'all'
        );
        wp_enqueue_style('redux-custom-css');
    }
    // This example assumes your opt_name is set to redux_demo, replace with your opt_name value
    add_action( 'redux/page/secretlab/enqueue', 'addPanelCSS' );

     /* ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'SEO Theme' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version 1.4.8' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => esc_attr__( 'Theme Options', 'the-seo' ),
        'page_title'           => esc_attr__( 'The SEO Theme Options', 'the-seo' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => 'AIzaSyDipjsETF4ETmbL_Z-0HwJ610s15rHQSx8',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => false,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => 'secretlab',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => false,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '_options',
        // Page slug used to denote the panel
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        'footer_credit'     => esc_attr__('Made with love by SecretLab.PW', 'the-seo'),                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!

        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        'compiler'             => true,
		
		'disable_tracking'     => true,

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

// ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
$args['admin_bar_links'][] = array(
    'id'    => 'sell-docs',
    'href'  => 'http://secretlab.pw/documentation/',
    'title' => esc_attr__( 'Documentation', 'the-seo' ),
);

$args['admin_bar_links'][] = array('id'    => 'sell-support',
    'href'  => 'http://secretlab.pw/helpdesk/',
    'title' => esc_attr__( 'Support', 'the-seo' ),
);

$args['admin_bar_links'][] = array(
    'id'    => 'sell-mainsite',
    'href'  => 'http://secretlab.pw/',
    'title' => esc_attr__( 'SecretLab', 'the-seo' ),
);

    // Add content after the form.
    $args['footer_text'] = '<p>'.esc_attr__( 'If you like the theme you can rate it by 5 stars on Theme Forest', 'the-seo' ).' <a href="http://themeforest.net/downloads" target="_blank">http://themeforest.net/downloads</a> '.esc_attr__( 'We are really appreciate for your help!', 'the-seo' ).'</p>';

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */

    // -> START General Settings Tab with no subsections
Redux::setSection( $opt_name, array(
    'title'  => esc_attr__( 'General Setting', 'the-seo' ),
    'id'     => 'general',
    'icon'   => 'el el-home',
    'fields' => array(
        array(
            'id'       => 'rtloption',
            'type'     => 'switch',
            'title'    => esc_attr__( 'Turn RTL mode', 'the-seo' ),
            'subtitle' => esc_attr__( 'Press ON if you need to support Right-to-left language', 'the-seo' ),
            'default'  => false,
        ),
        array(
            'id'       => 'rtlcarousel',
            'type'     => 'switch',
            'title'    => esc_attr__( 'RTL mode for carousels', 'the-seo' ),
            'subtitle' => esc_attr__( 'Press ON if you need to support team|testimonials carousels', 'the-seo' ),
            'default'  => false,
        ),

        array(
            'id'       => 'comp-name',
            'type'     => 'text',
            'title'    => esc_attr__( 'Company Name', 'the-seo' ),
            'subtitle' => esc_attr__( 'For any section about company.', 'the-seo' ),
            'default'  => 'SEO Agency'
        ),
        array(
            'id'       => 'index-page',
            'type'     => 'select',
            'data'     => 'pages',
            'title'    => esc_attr__( 'Frontpage Setting', 'the-seo' ),
            'desc'     => esc_attr__( 'Select which page to display on your Frontpage. If left blank the Blog will be displayed', 'the-seo' ),
            'default'  => '0'
        ),
        array(
            'id'   => 'opt-divide-before-logo',
            'type' => 'divide'
        ),
        array(
            'id'    => 'opt-info-logo',
            'type'  => 'info',
            'style' => 'success',
            'icon'  => 'el el-info-circle',
            'title' => __( 'Logo Option: Where to Set Your Logo?', 'the-seo' ),
            'desc'  => __( 'Logo option is in <strong>Suppamenu Pro</strong> plugin. Load a skin and click <b>Layout & Logo</b> in menu.', 'the-seo' )
        ),
        array(
            'id'   => 'opt-divide-after-logo',
            'type' => 'divide'
        ),

        array(
            'id'       => 'apple-touch-icon',
            'type'     => 'media',
            'title'    => esc_attr__( 'Apple Touch Icon', 'the-seo' ),
            'subtitle' => esc_attr__( 'Recomended size - 144 x 144px', 'the-seo' ),
            'default'  => array(
                'url'=> esc_url(get_template_directory_uri()).'/images/apple-touch-icon.png'
            ),
        ),
        array(
            'id'       => 'favicon',
            'type'     => 'media',
            'title'    => esc_attr__( 'Favicon', 'the-seo' ),
            'subtitle' => esc_attr__( 'Upload your favicon', 'the-seo' ),
            'default'  => array(
                'url'=> esc_url(get_template_directory_uri()).'/images/favicon.ico'
            ),
        )
    )
) );

/*
 * <--- END SECTIONS
 */
// -> START Contacts Settings Tab with no subsections
Redux::setSection( $opt_name, array(
    'title'  => esc_attr__( 'Contacts &amp; Map', 'the-seo' ),
    'id'     => 'contacts',
    'icon'   => 'el el-envelope',
    'fields' => array(
        array(
            'id'       => 'phone',
            'type'     => 'text',
            'title'    => esc_attr__( 'Phone Number', 'the-seo' ),
            'subtitle' => esc_attr__( 'For any section about company.', 'the-seo' ),
            'default'  => '+1 29 895 25 25'
        ),
        array(
            'id'       => 'email',
            'type'     => 'text',
            'title'    => esc_attr__( 'E-mail', 'the-seo' ),
            'subtitle' => esc_attr__( 'For any section about company', 'the-seo' ),
            'validate' => 'email',
            'msg'      => 'Wrong e-mail address',
            'default'  => 'seoagency@example.com'
        ),
        array(
            'id'       => 'skype',
            'type'     => 'text',
            'title'    => esc_attr__( 'Skype Login', 'the-seo' ),
            'subtitle' => esc_attr__( 'For any section about company', 'the-seo' ),
            'default'  => 'seoagency_la'
        ),
        array(
            'id'       => 'address',
            'type'     => 'multi_text',
            'title'    => esc_attr__( 'Address', 'the-seo' ),
            'subtitle' => esc_attr__( 'For any section about company', 'the-seo' ),
            'default'  => array(
                '3775 Vermont Ave, Los Angeles, CA 90007',
                '2441 Michelle Dr, Tustin, CA 92780',
            ),
        ),

        array(
           'id' => 'map_section_start',
           'type' => 'section',
           'title' => esc_attr__('Google Maps Options', 'the-seo'),
           'indent' => true
        ),
        array(
            'id'       => 'show-gmap',
            'type'     => 'switch',
            'title'    => esc_attr__( 'Show Google Map in Footer Section?', 'the-seo' ),
            'default'  => true,
            'indent'   => true
        ),
        array(
            'id'       => 'map_api_key',
            'type'     => 'text',
            'title'    => esc_attr__( 'API Key', 'the-seo' ),
            'subtitle' => esc_attr__( 'You can get a new API key at https://developers.google.com/maps/documentation/javascript/get-api-key', 'the-seo' ),
            'default'  => 'AIzaSyBoQ1waGu5KCQvqG1Wty1-ZlTzUO_pFSx4',
        ),
        array(
            'id'       => 'map_settings_latitude',
            'type'     => 'text',
            'title'    => esc_attr__( 'latitude', 'the-seo' ),
            'subtitle' => esc_attr__( 'Here must be latitude for Google Maps', 'the-seo' ),
            'default'  => '34.017973',
            'required' => array( 'show-gmap', '=', true ),
        ),
        array(
            'id'       => 'map_settings_longtitude',
            'type'     => 'text',
            'title'    => esc_attr__( 'longtitude', 'the-seo' ),
            'subtitle' => esc_attr__( 'Here must be longtitude for Google Maps', 'the-seo' ),
            'default'  => '-118.292053',
            'required' => array( 'show-gmap', '=', true ),
        ),
        array(
            'id'       => 'zoomonoff',
            'type'     => 'switch',
            'title'    => esc_attr__( 'Mouse scroll zooming', 'the-seo' ),
            'default'  => false,
            'indent'   => false
        ),
        array(
            'id'       => 'map_settings_zoom',
            'type'     => 'text',
			'class'    => 'redux_map_zoom',
            'title'    => esc_attr__( 'Map Zoom', 'the-seo' ),
            'subtitle' => esc_attr__( 'Here must be zoom for Google Maps', 'the-seo' ),
			'default'  => 17,
            'required' => array( 'show-gmap', '=', true ),
        ),
        array(
            'id'       => 'map_settings_marker',
            'type'     => 'media',
            'title'    => esc_attr__( 'Image for Map Marker', 'the-seo' ),
            'default'  => array(
                'url'=> esc_url(get_template_directory_uri()).'/images/map-marker.png'
            ),
            'required' => array( 'show-gmap', '=', true ),
        ),
        array(
            'id'     => 'map_section_end',
            'type'   => 'section',
            'indent' => false,
        ),			

    )
) );

/*
 * <--- END SECTIONS
 */
// -> START Design Tab
Redux::setSection( $opt_name, array(
    'title'  => esc_attr__( 'Social', 'the-seo' ),
    'id'     => 'social',
    'icon'   => 'el el-adult',
    'fields' => array(
        array(
            'id'       => 'header-social-buttons',
            'type'     => 'switch',
            'title'    => esc_attr__( 'Display social buttons', 'the-seo' ),
            'default'  => true,
            'indent'   => true
        ),
        array(
            'id'       => 'header-social-buttons-section',
            'type'     => 'section',
            'indent'   => true
        ),
            array(
                'id'       => 'social_link_facebook',
                'type'     => 'text',
                'title'    => esc_attr__( 'Facebook', 'the-seo' ),
                'subtitle' => esc_attr__( 'type here link to your facebook profile or page', 'the-seo' ),
                'required' => array( 'header-social-buttons', '=', true ),
                'default'  => 'http://fb.com/',
            ),

            array(
                'id'       => 'social_link_twitter',
                'type'     => 'text',
                'title'    => esc_attr__( 'Twitter', 'the-seo' ),
                'subtitle' => esc_attr__( 'type here link to your twitter profile', 'the-seo' ),
                'required' => array( 'header-social-buttons', '=', true ),
                'default'  => 'http://twitter.com/',
            ),
            array(
                'id'       => 'social_link_myspace',
                'type'     => 'text',
                'title'    => esc_attr__( 'MySpace', 'the-seo' ),
                'subtitle' => esc_attr__( 'type here link to your MySpace profile', 'the-seo' ),
                'required' => array( 'header-social-buttons', '=', true ),
            ),
            array(
                'id'       => 'social_link_linkedin',
                'type'     => 'text',
                'title'    => esc_attr__( 'LinkedIn', 'the-seo' ),
                'subtitle' => esc_attr__( 'type here link to your LinkedIn profile', 'the-seo' ),
                'required' => array( 'header-social-buttons', '=', true ),
                'default'  => 'http://linkedin.com/',
            ),
            array(
                'id'       => 'social_link_google',
                'type'     => 'text',
                'title'    => esc_attr__( 'Google+', 'the-seo' ),
                'subtitle' => esc_attr__( 'type here link to your Google+ profile or page', 'the-seo' ),
                'required' => array( 'header-social-buttons', '=', true ),
                'default'  => 'http://plus.google.com/',
            ),
            array(
                'id'       => 'social_link_tumblr',
                'type'     => 'text',
                'title'    => esc_attr__( 'Tumblr', 'the-seo' ),
                'subtitle' => esc_attr__( 'type here link to your Tumblr', 'the-seo' ),
                'required' => array( 'header-social-buttons', '=', true ),
            ),
            array(
                'id'       => 'social_link_pinterest',
                'type'     => 'text',
                'title'    => esc_attr__( 'Pinterest', 'the-seo' ),
                'subtitle' => esc_attr__( 'type here link to your Pinterest', 'the-seo' ),
                'required' => array( 'header-social-buttons', '=', true ),
            ),
            array(
                'id'       => 'social_link_youtube',
                'type'     => 'text',
                'title'    => esc_attr__( 'YouTube', 'the-seo' ),
                'subtitle' => esc_attr__( 'type here link to your YouTube', 'the-seo' ),
                'required' => array( 'header-social-buttons', '=', true ),
            ),
            array(
                'id'       => 'social_link_instagram',
                'type'     => 'text',
                'title'    => esc_attr__( 'Instagram', 'the-seo' ),
                'subtitle' => esc_attr__( 'type here link to your Instagram', 'the-seo' ),
                'required' => array( 'header-social-buttons', '=', true ),
            ),
            array(
                'id'       => 'social_link_vkcom',
                'type'     => 'text',
                'title'    => esc_attr__( 'vk.com', 'the-seo' ),
                'subtitle' => esc_attr__( 'type here link to your vk.com', 'the-seo' ),
                'required' => array( 'header-social-buttons', '=', true ),
            ),
            array(
                'id'       => 'social_link_reddit',
                'type'     => 'text',
                'title'    => esc_attr__( 'Reddit', 'the-seo' ),
                'subtitle' => esc_attr__( 'type here link to your Reddit', 'the-seo' ),
                'required' => array( 'header-social-buttons', '=', true ),
            ),
            array(
                'id'       => 'social_link_blogger',
                'type'     => 'text',
                'title'    => esc_attr__( 'Blogger', 'the-seo' ),
                'subtitle' => esc_attr__( 'type here link to your Blogger', 'the-seo' ),
                'required' => array( 'header-social-buttons', '=', true ),
            ),
            array(
                'id'       => 'social_link_wordpress',
                'type'     => 'text',
                'title'    => esc_attr__( 'Wordpress', 'the-seo' ),
                'subtitle' => esc_attr__( 'type here link to your Wordpress', 'the-seo' ),
                'required' => array( 'header-social-buttons', '=', true ),
            ),
            array(
                'id'       => 'social_link_behance',
                'type'     => 'text',
                'title'    => esc_attr__( 'Behance', 'the-seo' ),
                'subtitle' => esc_attr__( 'type here link to your Behance', 'the-seo' ),
                'required' => array( 'header-social-buttons', '=', true ),
            ),
        array(
            'id'     => 'social_section_end',
            'type'   => 'section',
            'indent' => false,
        ),
    )
) );
/*
 * <--- END SECTIONS
 */
// -> START Design Tab
Redux::setSection( $opt_name, array(
    'title'  => esc_attr__( 'Design', 'the-seo' ),
    'id'     => 'design',
    'desc'   => esc_attr__( 'Design settings for customization.', 'the-seo' ),
    'icon'   => 'el el-brush',
    'fields' => array(

    )
) );

        // Layout Design Tab
    Redux::setSection( $opt_name, array(
        'title'      => esc_attr__( 'Layout', 'the-seo' ),
        'id'         => 'design-layout-subsection',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'            => 'transition',
                'type'          => 'slider',
                'title'         => esc_attr__( 'Transition time', 'the-seo' ),
                'subtitle'      => esc_attr__( 'Choose hover effects time in ms', 'the-seo' ),
                'desc'          => esc_attr__( 'Slider description. Min: 0, max: 1000, step: 5, default value: 600', 'the-seo' ),
                'default'       => 600,
                'min'           => 0,
                'step'          => 5,
                'max'           => 1000,
                'display_value' => 'text'
            ),
            array(
                'id'       => 'design-css',
                'type'     => 'image_select',
                'title'    => esc_attr__( 'Choose design', 'the-seo' ),
                'subtitle' => esc_attr__( 'Also you need to set up color scheme', 'the-seo' ),
                //Must provide key => value(array:title|img) pairs for radio options
                'options'  => array(
                    '1' => array(
                        'alt' => 'Design 1: Light Version',
                        'img' => get_template_directory_uri() . '/images/framework/seo1.jpg'
                    ),
                    '2' => array(
                        'alt' => 'Design 2: Dark version',
                        'img' => get_template_directory_uri() . '/images/framework/seo2.jpg'
                    ),
                    '3' => array(
                        'alt' => 'Design 3: Violet Light Version',
                        'img' => get_template_directory_uri() . '/images/framework/seo3.jpg'
                    ),
                    '4' => array(
                        'alt' => 'Design 4: Violet Dark Version',
                        'img' => get_template_directory_uri() . '/images/framework/seo4.jpg'
                    ),
                    '5' => array(
                        'alt' => 'SEO Flat',
                        'img' => get_template_directory_uri() . '/images/framework/seo5.jpg'
                    )
                ),
                'default'  => '1'
            ),
            array(
                'id'       => 'network-image',
                'type'     => 'switch',
                'title'    => esc_attr__( 'Display network image?', 'the-seo' ),
                'default'  => true,
                'required' => array('design-css', '=', array(1, 2)),
            ),

            array(
                'id'       => 'design-layout',
                'type'     => 'image_select',
                'title'    => esc_attr__( 'Choose page layout', 'the-seo' ),
                //Must provide key => value(array:title|img) pairs for radio options
                'options'  => array(
                   '1' => array(
                        'alt' => 'Full width layout',
                        'img' => get_template_directory_uri() . '/images/framework/full.gif'
                    ),
                    '2' => array(
                        'alt' => 'Boxed layout, maximum resolution - 1170 px',
                        'img' => get_template_directory_uri() . '/images/framework/boxed.gif'
                    )
                ),
                'default'  => '1'
            ),
                array(
                    'id'        => 'boxed-background-color',
                    'type'      => 'color_rgba',
                    'title'     => esc_attr__( 'Choose background color for box', 'the-seo' ),
                    'output'    => array('background-color' => '.mainbgr'),
                    'default'   => array(
                        'color'     => '#323232',
                        'alpha'     => 1
                    ),
                    'required'  => array('design-layout', '=', '2')
                ),
                array(
                    'id'       => 'boxed-background',
                    'type'     => 'media',
                    'title'    => esc_attr__( 'Box Background', 'the-seo' ),
                    'subtitle' => esc_attr__( 'Upload your background for box', 'the-seo' ),
                    'output'    => array('background-image' => '.mainbgr'),
                    'default'  => array(
                       'url'=> esc_url(get_template_directory_uri()).'/images/bgr.jpg'
                    ),
                    'required'  => array('design-layout', '=', '2')
                ),

            array(
                'id'       => 'sidebar-layout',
                'type'     => 'image_select',
                'title'    => esc_attr__( 'Choose sidebar option', 'the-seo' ),
                //Must provide key => value(array:title|img) pairs for radio options
                'options'  => array(
                    '1' => array(
                        'alt' => 'Without sidebar',
                        'img' => get_template_directory_uri() . '/images/framework/nosidebar.gif'
                    ),
                    '2' => array(
                        'alt' => '2 sidebars',
                        'img' => get_template_directory_uri() . '/images/framework/2sidebars.gif'
                    ),
                    '3' => array(
                        'alt' => 'Left sidebar',
                        'img' => get_template_directory_uri() . '/images/framework/leftsidebar.gif'
                    ),
                    '4' => array(
                        'alt' => 'Right sidebar',
                        'img' => get_template_directory_uri() . '/images/framework/rightsidebar.gif'
                    )
                ),
                'default'  => '1'
            ),
            
            array(
                'id'       => 'scroll-to-top',
                'type'     => 'switch',
                'title'    => esc_attr__( 'Show Scroll to Top Button?', 'the-seo' ),
                'default'  => true,
            ),
            array(
                'id'       => 'pageloader',
                'type'     => 'switch',
                'title'    => esc_attr__( 'Display Page Loader?', 'the-seo' ),
                'subtitle'    => esc_attr__( 'Do you want to show page loader, when website is loading?', 'the-seo' ),
                'default'  => true,
                'indent'   => true
            ),

            array(
                'id'        => 'pageloadercolor',
                'type'      => 'color_rgba',
                'title'     => esc_attr__('Choose Background Color for Page Loader', 'the-seo' ),
                'output'    => array('background-color' => '#page-preloader'),
                'default'   => array(
                    'color'     => '#f4f4f4',
                    'alpha'     => 1
                ),
                'required' => array( 'pageloader', '=', true ),
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_attr__( 'Header', 'the-seo' ),
        'id'         => 'design-header-subsection',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'        => 'header-layout_info_notice',
                'type'      => 'info',
                'notice'    => true,
                'style'     => 'info',
                'icon'      => 'el-icon-info-sign',
                'title'     => esc_attr__('Note', 'the-seo'),
                'desc'      => esc_attr__( 'Some header required dark version of logo. You can set logo in Suppamenu Pro plugin', 'the-seo'),
            ),
            array(
                'id'       => 'header-layout',
                'type'     => 'image_select',
                'title'    => esc_attr__( 'Choose header & menu type', 'the-seo' ),
                //Must provide key => value(array:title|img) pairs for radio options
                'options'  => array(
                    '1' => array(
                        'alt' => 'Menu with dark topbar',
                        'img' => get_template_directory_uri() . '/images/framework/h1.jpg'
                    ),
                    '2' => array(
                        'alt' => 'Minimal transparent menu',
                        'img' => get_template_directory_uri() . '/images/framework/h3.jpg'
                    ),
                    '3' => array(
                        'alt' => 'Transparent menu for dark background',
                        'img' => get_template_directory_uri() . '/images/framework/h4.jpg'
                    ),
                    '4' => array(
                        'alt' => 'Menu with light topbar',
                        'img' => get_template_directory_uri() . '/images/framework/h7.jpg'
                    ),

                ),
                'default'  => '1'
            ),
            /*======== Slider ==========*/
                 array(
                    'id'       => 'header14_slider',
                    'type'     => 'select',
                    'title'    => esc_attr__( 'Choose Slider', 'the-seo' ),
                    'subtitle' => esc_attr__( 'Choose slider for header section', 'the-seo' ),
                    //Must provide key => value(array:title|img) pairs for radio options
                    'options'  => theseo_get_sliders_array(),
                    'default'  => 'rev_header-static',

                ),

            array(
                'id'       => 'section-topbar',
                'type'     => 'section',
                'title'    => esc_attr__( 'Topbar Settings', 'the-seo' ),
                'indent'   => true, // Indent all options below until the next 'section' option is set.
            ),
            array(
                'id'       => 'header-topbar',
                'type'     => 'switch',
                'title'    => esc_attr__( 'Show header topbar?', 'the-seo' ),
                'subtitle' => esc_attr__( 'Topbar with phone number, email, social buttons and "Get report" button. * The composition is different in different header', 'the-seo' ),
                'default'  => true,
            ),

            array(
                'id'     => 'section-topbar-end',
                'type'   => 'section',
                'indent' => false, //
            ),

        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => esc_attr__( 'Footer', 'the-seo' ),
        'id'         => 'design-footer-subsection',
        'subsection' => true,
        'fields'     => array(

            array(
                'id'       => 'footer-type-layout',
                'type'     => 'image_select',
                'title'    => esc_attr__( 'Choose type of footer', 'the-seo' ),
                //Must provide key => value(array:title|img) pairs for radio options
                'options'  => array(
                    '1' => array(
                        'alt' => 'Theme Design Footer',
                        'img' => get_template_directory_uri() . '/images/framework/footer1.jpg'
                    ),
                    '2' => array(
                        'alt' => 'Customized footer',
                        'img' => get_template_directory_uri() . '/images/framework/footer2.jpg'
                    ),
                ),
                'default'  => '1'
            ),

            array(
                'id'       => 'social-footer',
                'type'     => 'switch',
                'title'    => esc_attr__( 'Display social buttons on the footer?', 'the-seo' ),
                'default'  => true,
                'required'  => array(
                    array('footer-type-layout', '=', 1),
                )
            ),


            array(
                'id'       => 'social-footer-background',
                'type'     => 'background',
                'title'    => esc_attr__( 'Background for block with Social links', 'the-seo' ),
                'desc'     => esc_attr__( 'Set image or color', 'the-seo' ),
                'output'    => array('background-image, background-color, background-repeat, background-attachment, background-size, background-position' => '.socialbottom, .messagesect'
                ),
                'compiler'  => true,
                'default'  => array(
                    'background-color' => '#000',
                    'background-repeat' => 'no-repeat',
                    'background-attachment' => 'scroll',
                    'background-size' => 'cover',
                    'background-position' => 'left top',
                    'background-image' => esc_url(get_template_directory_uri()).'/images/clipart6.jpg',
                ),
                'required'  => array(
                    array('footer-type-layout', '=', 1),
                )
            ),

            array(
                'id'       => 'display-footer',
                'type'     => 'switch',
                'title'    => esc_attr__( 'Display footer section with address, map and social links?', 'the-seo' ),
                'default'  => true,
                'required'  => array(
                    array('footer-type-layout', '=', 1),
                )
            ),
            array(
                'id'       => 'display-footer-contact',
                'type'     => 'switch',
                'title'    => esc_attr__( 'Display footer section with text and contact form?', 'the-seo' ),
                'default'  => true,
                'required'  => array(
                    array('design-css', '=', array(3, 4)),
                    array('footer-type-layout', '=', 1),
                )
            ),

            array(
                'id'      => 'copyr-text',
                'type'    => 'text',
                'title'   => esc_attr__( 'Footer Copyright Text', 'the-seo' ),
                'subtitle' => esc_attr__( 'For display at footer after "&copy; "', 'the-seo' ),
                'default' => 'All Rights Reserved. Seo Agency Wordpress Theme',
                'required'  => array(
                    array('footer-type-layout', '=', 1),
                )
            ),
            array(
                'id'       => 'digital-footer-cta',
                'type'     => 'editor',
                'title'    => esc_attr__( 'Footer Call-to-Action', 'the-seo' ),
                'subtitle' => esc_attr__( 'Text for Call-to-Action Section at the footer near contact form. (Only for Digital Agency version of design)', 'the-seo' ),
                'default'  => '<h2>Contact Us</h2><strong> and we will call you back</strong><p>Let us now details about your project and we will prepare strategy for you!</p>',
                'required'  => array(
                    array('footer-type-layout', '=', 1),
                )
            ),
            array(
                'id'       => 'contactus-footer-text',
                'type'     => 'text',
                'title'    => esc_attr__( 'Footer Heading for Contact US section', 'the-seo' ),
                'subtitle' => esc_attr__( 'Text for Contact US block over Google Map in footer section. ', 'the-seo' ),
                'default'  => 'Contact Us',
                'required'  => array(
                    array('footer-type-layout', '=', 1),
                )

            ),
            array(
                'id'       => 'button-footer-text',
                'type'     => 'text',
                'title'    => esc_attr__( 'Button text ', 'the-seo' ),
                'subtitle' => esc_attr__( 'Text for button over Google Map in footer section. ', 'the-seo' ),
                'default'  => 'Request a free quote!',
                'required'  => array(
                    array('footer-type-layout', '=', 1),
                )
            ),
            array(
                'id'       => 'button-footer-url',
                'type'     => 'text',
                'title'    => esc_attr__( 'Button URL ', 'the-seo' ),
                'subtitle' => esc_attr__( 'URL for button over Google Map in footer section. ', 'the-seo' ),
                'default'  => '/contacts/',
                'required'  => array(
                    array('footer-type-layout', '=', 1),
                )
            ),
            array(
                'id'       => 'seeonmap-footer-text',
                'type'     => 'text',
                'title'    => esc_attr__( 'Text for a link See on the map on Contact US section', 'the-seo' ),
                'subtitle' => esc_attr__( 'Text for Contact US block over Google Map in footer section. ', 'the-seo' ),
                'default'  => 'See on the map',
                'required'  => array(
                    array('footer-type-layout', '=', 1),
                )
            ),
            array(
                'id'       => 'contback-footer-text',
                'type'     => 'text',
                'title'    => esc_attr__( 'Text for a back link on Contact US section', 'the-seo' ),
                'subtitle' => esc_attr__( 'Text for Contact US block over Google Map in footer section. ', 'the-seo' ),
                'default'  => 'Contacts',
                'required'  => array(
                    array('footer-type-layout', '=', 1),
                )
            ),
            array(
                'id'       => 'section-footercta',
                'type'     => 'section',
                'title'    => esc_attr__( 'Footer CTA Block Settings', 'the-seo' ),
                'indent'   => true, // Indent all options below until the next 'section' option is set.
            ),
            array(
                'id'       => 'footer-cta3',
                'type'     => 'switch',
                'title'    => esc_attr__( 'Display Call-to-Action block on footer?', 'the-seo' ),
                'default'  => true,
            ),
                array(
                    'id'       => 'footer-cta3-title',
                    'type'     => 'text',
                    'title'    => esc_attr__( 'Title', 'the-seo' ),
                    'subtitle' => esc_attr__( 'Title for Call-to-Action block on footer', 'the-seo' ),
                    'default'  => 'Not enough for your seo needs?',
                    'required' => array( 'footer-cta3', '=', '1')
                ),
                array(
                    'id'        => 'footer-cta3-h3-color',
                    'type'      => 'color_rgba',
                    'title'     => esc_attr__( 'Choose color of the title', 'the-seo' ),
                    'output'    => array('color' => '.calltoactionblock3 h3'),
                    'default'   => array(
                        'color'     => '#252a2c',
                        'alpha'     => 1
                    ),
                    'required' => array( 'footer-cta3', '=', '1')
                ),
                array(
                    'id'       => 'footer-cta3-description',
                    'type'     => 'text',
                    'title'    => esc_attr__( 'Description', 'the-seo' ),
                    'subtitle' => esc_attr__( 'Description for Call-to-Action block on footer', 'the-seo' ),
                    'default'  => 'Have you specific requirements? Just send us details!',
                    'required' => array( 'footer-cta3', '=', '1')
                ),
                array(
                    'id'       => 'footer-cta3-url',
                    'type'     => 'text',
                    'title'    => esc_attr__( 'URL of Button', 'the-seo' ),
                    'subtitle' => esc_attr__( 'URL for Call-to-Action button', 'the-seo' ),
                    'default'  => '/seolight/get-report/',
                    'required' => array( 'footer-cta3', '=', '1')
                ),
                array(
                    'id'       => 'footer-cta3-buttontext',
                    'type'     => 'text',
                    'title'    => esc_attr__( 'Text of Button', 'the-seo' ),
                    'subtitle' => esc_attr__( 'Text for Call-to-Action button', 'the-seo' ),
                    'default'  => 'Get started now!',
                    'required' => array( 'footer-cta3', '=', '1')
                ),
                array(
                    'id'       => 'footer-cta3-background',
                    'type'     => 'background',
                    'title'    => esc_attr__( 'Background setting for short header', 'the-seo' ),
                    'output'    => array('background-image, background-color, background-repeat, background-attachment, background-size, background-position' => '.calltoactionblock3'
                    ),
                    'default'  => array(
                        'background-color' => '#f6f6f6',
                        'background-repeat' => 'no-repeat',
                        'background-attachment' => 'scroll',
                        'background-size' => 'contain',
                        'background-position' => 'center top',
                        'background-image' => esc_url(get_template_directory_uri()).'/images/graph.png',
                    ),
                    'required'  => array('footer-cta3', '=', '1')
                ),
                array(
                    'id'       => 'footer-cta3-btnclass',
                    'type'     => 'radio',
                    'title'    => esc_attr__( 'Choose button style', 'the-seo' ),
                    
                    //Must provide key => value pairs for radio options
                    'options'  => array(
                        'btn-info' => '<a class="btn btn-info">Button Text Here!</a>',
                        'btn-default' => '<a class="btn btn-default">Button Text Here!</a>',
                        'btn-white-white' => '<a class="btn btn-white-white">Button Text Here!</a>',
                        'btn-primary' => '<a class="btn btn-primary">Button Text Here!</a>',
                        'btn-green' => '<a class="btn btn-green">Button Text Here!</a>',
                        'btn-success' => '<a class="btn btn-success">Button Text Here!</a>',
                        'btn-link' => '<a class="btn btn-link">Button Text Here!</a>',
                        'btn-gray' => '<a class="btn btn-gray">Button Text Here!</a>',
                        'btn-warning' => '<a class="btn btn-warning">Button Text Here!</a>',
                        'btn-danger' => '<a class="btn btn-danger">Button Text Here!</a>'
                    ),
                    'default'  => 'btn-primary',
                    'required' => array( 'footer-cta3', '=', '1')
                ),
            array(
                'id'     => 'section-footercta-end',
                'type'   => 'section',
                'indent' => false, //
            ),
        )
    ) );
Redux::setSection( $opt_name, array(
    'title'      => esc_attr__( 'Menu', 'the-seo' ),
    'id'         => 'design-menu-subsection',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'first-submenu-background',
            'type'     => 'background',
            'title'    => esc_attr__( 'Background setting for first submenu', 'the-seo' ),
            'desc'     => wp_kses(__( 'Use class <code>subbgr1</code> at menu item', 'the-seo' ), $allowed_html ),
            'output'    => array('background-image, background-color, background-repeat, background-attachment, background-size, background-position' => '.suppaMenu_wrap .subbgr1.suppa_menu > .suppa_submenu,  body .suppaMenu_wrap .subbgr1 > .suppa_rwd_submenu'
            ),
            'default'  => array(
                'background-color' => '#FFFFFF',
                'background-repeat' => 'no-repeat',
                'background-attachment' => 'scroll',
                'background-size' => 'auto auto',
                'background-position' => 'right bottom',
                'background-image' => esc_url(get_template_directory_uri()).'/images/networkbg.png',
            ),
        ),
        array(
            'id'       => 'services-submenu-background',
            'type'     => 'background',
            'title'    => esc_attr__( 'Background setting for services submenu', 'the-seo' ),
            'desc'     => wp_kses(__( 'Use class <code>subbgr2</code> at menu item', 'the-seo' ), $allowed_html ),
            'output'    => array('background-image, background-color, background-repeat, background-attachment, background-size, background-position' => '.suppaMenu_wrap .subbgr2.suppa_menu > .suppa_submenu,  body .suppaMenu_wrap .subbgr2 > .suppa_rwd_submenu'
            ),
            'default'  => array(
                'background-color' => '#FFFFFF',
                'background-repeat' => 'no-repeat',
                'background-attachment' => 'scroll',
                'background-size' => 'auto auto',
                'background-position' => 'right bottom',
                'background-image' => esc_url(get_template_directory_uri()).'/images/smbgrs.png',
            ),

        ),
        /* === Sticky Setting  === */
        
        array(
            'id'        => 'header-background-sticky-menu',
            'type'      => 'color_rgba',
            'title'     => esc_attr__('Background color for sticky menu on Header 1', 'the-seo' ),
            'output'    => array('background-color' => '.header1and5.suppaMenu_wrap.suppa-sticky'),
            'compiler'  => true,
            'default'   => array(
                'color'     => '#000000',
                'alpha'     => 0.75,
                'important'  => true
            ),
        ),
        array(// its for header 3
            'id'        => 'header3-background-sticky-menu',
            'type'      => 'color_rgba',
            'title'     =>  esc_attr__('Background color for sticky menu on Header 2', 'the-seo' ),
            'output'    => array('background-color' => '.header2.suppaMenu_wrap.suppa-sticky, .header2.suppa-sticky .suppaMenu'),
            'compiler'  => true,
            'default'   => array(
                'color'     => '#FFFFFF',
                'alpha'     => 1,
                'important'  => true
            ),
        ),
        array(// its for header 3
            'id'        => 'header4-background-sticky-menu',
            'type'      => 'color_rgba',
            'title'     =>  esc_attr__('Background color for sticky menu on Header 3', 'the-seo' ),
            'output'    => array('background-color' => '.header3.suppaMenu_wrap.suppa-sticky'),
            'compiler'  => true,
            'default'   => array(
                'color'     => '#252a2c',
                'alpha'     => 1,
                'important'  => true
            ),
        ),
        array(// its for header digital
            'id'        => 'headerd-background-sticky-menu',
            'type'      => 'color_rgba',
            'title'     =>  esc_attr__('Background color for sticky menu on Header 4', 'the-seo' ),
            'compiler'  => true,
            'output'    => array('background-color' => 'header4.suppaMenu_wrap.suppa-sticky'),
            'default'   => array(
                'color'     => '#FFF',
                'alpha'     => 0.75,
                'important'  => true
            ),
        ),
        /* === Headers Sticky END === */
    )
));
/* Start Text Section */
Redux::setSection( $opt_name, array(
    'title'  => esc_attr__( 'Text', 'the-seo' ),
    'id'     => 'texts_settings',
    'desc'   => esc_attr__( 'Options for edit texts data.', 'the-seo' ),
    'icon'   => 'el el-fontsize',
    'fields' => array(
        array(
            'id'    => 'portfolio-info1',
            'type'  => 'info',
            'style' => 'info',
            'title' => esc_attr__( 'Portfolio post type options', 'the-seo' ),
        ),
        array(
            'id'       => 'portfolio_arch_title',
            'type'     => 'text',
            'title'    => esc_attr__( 'Portfolio Page H1 Heading', 'the-seo' ),
            'subtitle' => esc_attr__( 'Set H1 heading for Portfolio Archive page', 'the-seo' ),
            'default'  => 'Portfolio Grid',
        ),
        array(
            'id'       => 'portfolio_arch_desc',
            'type'     => 'editor',
            'title'    => esc_attr__( 'Description Text Under H1 Heading', 'the-seo' ),
            'subtitle' => esc_attr__( 'Allowed tags: a, img, br, em, strong, h1, h2, h3, h4, h5, h6, p, b, i, u, ol, ul, li, code, del', 'the-seo' ),
            'default'  => '',
        ),
        array(
            'id'    => 'services-info1',
            'type'  => 'info',
            'style' => 'info',
            'title' => esc_attr__( 'Services post type options', 'the-seo' ),
        ),
        array(
            'id'       => 'services_arch_title',
            'type'     => 'text',
            'title'    => esc_attr__( 'Services Page H1 Heading', 'the-seo' ),
            'subtitle' => esc_attr__( 'Set H1 heading for Services Archive page', 'the-seo' ),
            'default'  => 'Services List',
        ),
        array(
            'id'       => 'services_arch_desc',
            'type'     => 'editor',
            'title'    => esc_attr__( 'Description Text Under H1 Heading', 'the-seo' ),
            'subtitle' => esc_attr__( 'Allowed tags: a, img, br, em, strong, h1, h2, h3, h4, h5, h6, p, b, i, u, ol, ul, li, code, del', 'the-seo' ),
            'default'  => '',
        ),
        array(
            'id'       => 'services_read_more',
            'type'     => 'text',
            'title'    => esc_attr__( 'Services Page Read More Link', 'the-seo' ),
            'subtitle' => esc_attr__( 'Set Read More link text for Services Archive page', 'the-seo' ),
            'default'  => 'Read More',
        ),
        array(
            'id'    => 'team-info1',
            'type'  => 'info',
            'style' => 'info',
            'title' => esc_attr__( 'Team post type options', 'the-seo' ),
        ),
        array(
            'id'       => 'team_arch_title',
            'type'     => 'text',
            'title'    => esc_attr__( 'Team Page H1 Heading', 'the-seo' ),
            'subtitle' => esc_attr__( 'Set H1 heading for Team Archive page', 'the-seo' ),
            'default'  => 'Our Teammates',
        ),
        array(
            'id'       => 'team_arch_desc',
            'type'     => 'editor',
            'title'    => esc_attr__( 'Description Text Under H1 Heading', 'the-seo' ),
            'subtitle' => esc_attr__( 'Allowed tags: a, img, br, em, strong, h1, h2, h3, h4, h5, h6, p, b, i, u, ol, ul, li, code, del', 'the-seo' ),
            'default'  => '',
        ),
        array(
            'id'    => 'testi-info1',
            'type'  => 'info',
            'style' => 'info',
            'title' => esc_attr__( 'Testimonials post type options', 'the-seo' ),
        ),
        array(
            'id'       => 'testi_arch_title',
            'type'     => 'text',
            'title'    => esc_attr__( 'Testimonials Page H1 Heading', 'the-seo' ),
            'subtitle' => esc_attr__( 'Set H1 heading for Testimonials Archive page', 'the-seo' ),
            'default'  => 'Our Testimonials',
        ),
        array(
            'id'       => 'testi_arch_desc',
            'type'     => 'editor',
            'title'    => esc_attr__( 'Description Text Under H1 Heading', 'the-seo' ),
            'subtitle' => esc_attr__( 'Allowed tags: a, img, br, em, strong, h1, h2, h3, h4, h5, h6, p, b, i, u, ol, ul, li, code, del', 'the-seo' ),
            'default'  => '',
        ),
        array(
            'id'    => '404-info1',
            'type'  => 'info',
            'style' => 'info',
            'title' => esc_attr__( 'Text for 404 error page', 'the-seo' ),
        ),
        array(
            'id'       => '404_title',
            'type'     => 'text',
            'title'    => esc_attr__( '404 Page Heading', 'the-seo' ),
            'default'  => 'Not Found',
        ),
        array(
            'id'       => '404_descr',
            'type'     => 'text',
            'title'    => esc_attr__( '404 Page text', 'the-seo' ),
            'default'  => 'It looks like nothing was found at this location. Maybe try a search?',
        ),

    )
) );

/* END Text Section */
	/* Start Custom Section */
	
    Redux::setSection( $opt_name, array(
        'title'  => esc_attr__( 'Custom', 'the-seo' ),
        'id'     => 'custom_settings',
        'desc'   => esc_attr__( 'Code options for customization.', 'the-seo' ),
        'icon'   => 'el el-wrench',
        'fields' => array(


            array(
                'id'       => 'header-nested',
                'type'     => 'switch',
                'title'    => esc_attr__('Header Section JS, CSS editors', 'the-seo' ),
                'subtitle' => wp_kses(__('Click <code>On</code> to choice of editors to appear.', 'the-seo' ), $allowed_html ),
                'default'  => false
            ),
            array(
                'id'       => 'header-nested-buttonset',
                'type'     => 'button_set',
                'subtitle' => esc_attr__('This code will appear in the HEADER secrion of the site', 'the-seo' ),
                'options'  => array(
                    'button-js' => 'JS editor',
                    'button-css'   => 'CSS editor',
                ),
                'required' => array( 'header-nested', '=', true ),
                'default'  => 'button-js'
            ),
            array(
                'id'       => 'header-nested-ace-js',
                'type'     => 'ace_editor',
				'mode'     => 'javascript',
                'title'    => esc_attr__('JS Editor', 'the-seo' ),
				'default'  => '// function hello() { alert ("HELLO"); }',
                'required' => array( 'header-nested-buttonset', '=', 'button-js' )
            ),
            array(
                'id'       => 'header-nested-ace-css',
                'type'     => 'ace_editor',
				'mode'     => 'css',
                'title'    => esc_attr__('CSS Editor', 'the-seo' ),
				'default'  => 'body { margin : 0; padding : 0; }',
                'required' => array( 'header-nested-buttonset', '=', 'button-css' )
            ),
            array(
                'id'       => 'footer-nested',
                'type'     => 'switch',
                'title'    => esc_attr__('Footer Section JS, CSS editors', 'the-seo' ),
                'subtitle' => wp_kses(__('Click <code>On</code> to choice of editors to appear.', 'the-seo' ), $allowed_html ),
                'default'  => false
            ),
            array(
                'id'       => 'footer-nested-buttonset',
                'type'     => 'button_set',
                'subtitle' => esc_attr__('This code will appear in the FOOTER secrion of the site', 'the-seo' ),
                'options'  => array(
                    'button-js' => 'JS editor',
                    'button-css'   => 'CSS editor',
                ),
                'required' => array( 'footer-nested', '=', true ),
                'default'  => 'button-js'
            ),
            array(
                'id'       => 'footer-nested-ace-js',
                'type'     => 'ace_editor',
				'mode'     => 'javascript',
                'title'    => esc_attr__('JS Editor', 'the-seo' ),
				'default'  => 'function hello() { alert ("HELLO"); }',
                'required' => array( 'footer-nested-buttonset', '=', 'button-js' )
            ),	

            array(
                'id'       => 'footer-nested-ace-css',
                'type'     => 'ace_editor',
				'mode'     => 'css',
                'title'    => esc_attr__('CSS Editor', 'the-seo' ),
				'default'  => 'body { margin : 0; padding : 0; }',
                'required' => array( 'footer-nested-buttonset', '=', 'button-css' )
            ),
			
        )
    ) );	

    /*
     * <--- END SECTIONS
     */

// -> START Typography Settings Tab with no subsections
    Redux::setSection( $opt_name, array(
        'title'  => esc_attr__( 'Typography', 'the-seo' ),
        'id'     => 'typography',
        'icon'   => 'el el-fontsize',
        'fields' => array(
            array(
                'id'        => 'typography-_info_notice',
                'type'      => 'info',
                'notice'    => true,
                'style'     => 'info',
                'icon'      => 'el-icon-info-sign',
                'title'     => esc_attr__('Note', 'the-seo'),
                'desc'      => wp_kses(__('We recommend to use font pair: one font for body text and one for headings. You can find good font pair on <a href="http://fontpair.co/" target="_blank">http://fontpair.co/</a>', 'the-seo' ), $allowed_html ),
            ),
            array(
                'id'       => 'typography-body',
                'type'     => 'typography',
                'title'    => esc_attr__( 'Body Font', 'the-seo' ),
                'subtitle' => esc_attr__( 'Specify the body font properties.', 'the-seo' ),
                'font-backup' => false,
                'google'   => true,
                'subsets'   => true,
                'default'  => array(
                    'color'       => '#333333',
                    'font-size'   => '17px',
                    'font-family' => 'Raleway',
                    'font-weight'  => '400',
                    'font-backup' => 'sans-serif'
                ),
            ),
            array(
                'id'       => 'h1-typography',
                'type'     => 'typography',
                'title'    => esc_attr__( 'Heading H1 Font', 'the-seo' ),
                'subtitle' => esc_attr__( 'Specify the H1 font properties.', 'the-seo' ),
                'font-backup' => false,
                'google'   => true,
                'subsets'   => true,
                'text-transform' => true,
                'default'  => array(
                    'color'       => '#252a2c',
                    'font-weight'  => '700',
                    'font-family' => 'Montserrat',
                    'font-size'   => '40px',
                    'line-height' => '55px',
                    'text-transform' => 'uppercase',
                    'text-align' => 'center',
                ),
            ),
            
            array(
                'id'       => 'h2-typography',
                'type'     => 'typography',
                'title'    => esc_attr__( 'Heading H2 Font', 'the-seo' ),
                'subtitle' => esc_attr__( 'Specify the H2 font properties.', 'the-seo' ),
                'font-backup' => false,
                'google'   => true,
                'subsets'   => true,
                'text-transform' => true,
                'default'  => array(
                    'color'       => '#252a2c',
                    'font-size'   => '40px',
                    'line-height' => '55px',
                    'font-family' => 'Montserrat',
                    'font-weight'  => '400',
                    'text-transform' => 'uppercase',
                    'text-align' => 'center',
                ),
            ),
            array(
                'id'       => 'h3-typography',
                'type'     => 'typography',
                'title'    => esc_attr__( 'Heading H3 Font', 'the-seo' ),
                'subtitle' => esc_attr__( 'Specify the H3 font properties.', 'the-seo' ),
                'font-backup' => false,
                'google'   => true,
                'subsets'   => true,
                'text-transform' => true,
                'default'  => array(
                    'color'       => '#252a2c',
                    'font-size'   => '22px',
                    'line-height' => '34px',
                    'font-family' => 'Montserrat',
                    'font-weight'  => '400',
                    'text-transform' => 'uppercase',
                    'text-align' => 'center',
                ),
            ),
            array(
                'id'       => 'h4-typography',
                'type'     => 'typography',
                'title'    => esc_attr__( 'Heading H4 Font', 'the-seo' ),
                'subtitle' => esc_attr__( 'Specify the H4 font properties.', 'the-seo' ),
                'font-backup' => false,
                'google'   => true,
                'subsets'   => true,
                'text-transform' => true,
                'default'  => array(
                    'color'       => '#252a2c',
                    'font-size'   => '20px',
                    'line-height' => '30px',
                    'font-family' => 'Montserrat',
                    'font-weight'  => '400',
                    'text-transform' => 'uppercase',
                    'text-align' => 'center',
                ),
            ),
            array(
                'id'       => 'h5-typography',
                'type'     => 'typography',
                'title'    => esc_attr__( 'Heading H5 Font', 'the-seo' ),
                'subtitle' => esc_attr__( 'Specify the H5 font properties.', 'the-seo' ),
                'font-backup' => false,
                'google'   => true,
                'subsets'   => true,
                'text-transform' => true,
                'default'  => array(
                    'color'       => '#252a2c',
                    'font-size'   => '18px',
                    'line-height' => '28px',
                    'font-family' => 'Montserrat',
                    'font-weight'  => '400',
                    'text-transform' => 'uppercase',
                    'text-align' => 'center',
                ),
            ),
            array(
                'id'       => 'h6-typography',
                'type'     => 'typography',
                'title'    => esc_attr__( 'Heading H6 Font', 'the-seo' ),
                'subtitle' => esc_attr__( 'Specify the H6 font properties.', 'the-seo' ),
                'font-backup' => false,
                'google'   => true,
                'subsets'   => true,
                'text-transform' => true,
                'default'  => array(
                    'color'       => '#252a2c',
                    'font-size'   => '16px',
                    'line-height' => '26px',
                    'font-family' => 'Montserrat',
                    'font-weight'  => '400',
                    'text-transform' => 'uppercase',
                    'text-align' => 'center',
                ),
            ),
        )
    ) );

/*
 * <--- END SECTIONS
 */

// -> START Shop Settings Tab with no subsections

Redux::setSection( $opt_name, array(
    'title'  => esc_attr__( 'Shop', 'the-seo' ),
    'id'     => 'shop-setting',
    'icon'   => 'el el-shopping-cart',
    'fields' => array(
        array(
            'id'       => 'shop-sidebar-layout',
            'type'     => 'image_select',
            'title'    => esc_attr__( 'Choose sidebar option for shop', 'the-seo' ),
            
            
            //Must provide key => value(array:title|img) pairs for radio options
            'options'  => array(
                '1' => array(
                    'alt' => 'Without sidebar',
                    'img' => get_template_directory_uri() . '/images/framework/nosidebar.gif'
                ),
                '2' => array(
                    'alt' => '2 sidebars',
                    'img' => get_template_directory_uri() . '/images/framework/2sidebars.gif'
                ),
                '3' => array(
                    'alt' => 'Left sidebar',
                    'img' => get_template_directory_uri() . '/images/framework/leftsidebar.gif'
                ),
                '4' => array(
                    'alt' => 'Right sidebar',
                    'img' => get_template_directory_uri() . '/images/framework/rightsidebar.gif'
                )
            ),
            'default'  => '1'
        ),

        array(
            'id'       => 'shop-header14_slider',
            'type'     => 'select',
            'title'    => esc_attr__( 'Choose Slider for Shop', 'the-seo' ),
            'subtitle' => esc_attr__( 'Choose slider for header section', 'the-seo' ),
            
            'options'  => theseo_get_sliders_array(),
            'default'  => 'rev_header_short_1'
        ),			
    )
) );

// -> START BLOG SECTION

Redux::setSection( $opt_name, array(
    'title'  => esc_attr__( 'Blog', 'the-seo' ),
    'id'     => 'blog-setting',
    'icon'   => 'el el-blogger',
    'fields' => array(
        array(
            'id'    => 'blog-info1',
            'type'  => 'info',
            'style' => 'info',
            'title' => esc_attr__( 'These options working for slug /blog/ and it subpages/subcategories. So you need to make Blog category and put other categories like it subcategories', 'the-seo' ),
        ),
        array(
            'id'       => 'blog-layout',
            'type'     => 'image_select',
            'title'    => esc_attr__( 'Choose page layout', 'the-seo' ),
            
            
            //Must provide key => value(array:title|img) pairs for radio options
            'options'  => array(
                '1' => array(
                     'alt' => 'Full width layout',
                     'img' => get_template_directory_uri() . '/images/framework/full.gif'
                 ),
                '2' => array(
                    'alt' => 'Boxed layout, maximum resolution - 1170 px',
                    'img' => get_template_directory_uri() . '/images/framework/boxed.gif'
                )
            ),
            'default'  => '1'
        ),
        array(
            'id'        => 'blog-boxed-background-color',
            'type'      => 'color_rgba',
            'title'     => esc_attr__('Choose background for box', 'the-seo' ),
            'subtitle'  => esc_attr__('The option work for slug /blog/ and subpages/subcategories', 'the-seo' ),
            'output'    => array('background-color' => '.mainbgr'),
            'default'   => array(
                'color'     => '#323232',
                'alpha'     => 1
            ),
            'required'  => array('blog-layout', '=', '2')
        ),
        array(
            'id'       => 'blog-boxed-background',
            'type'     => 'media',
            'title'    => esc_attr__( 'Box Background', 'the-seo' ),
            'desc'     => esc_attr__( 'The option work for slug /blog/ and subpages/subcategories', 'the-seo' ),
            'subtitle' => esc_attr__( 'Upload your background for box', 'the-seo' ),
            'output'    => array('background-image' => '.mainbgr'),
            'default'  => array(
                'url'=> esc_url(get_template_directory_uri()).'/images/bgr.jpg'
            ),
            'required'  => array('blog-layout', '=', '2')
        ),
        array(
            'id'        => 'blog-content-background-color',
            'type'      => 'color_rgba',
            'title'     => esc_attr__( 'Choose background color for content section', 'the-seo' ),
            'subtitle'  => esc_attr__( 'Default - white. The option work for slug /blog/ and subpages/subcategories', 'the-seo' ),
            'output'    => array('background-color' => '.seo_blog'),
            'default'   => array(
                'color'     => '#FFF',
                'alpha'     => 1
            ),
            'required'  => array('blog-layout', '=', '2')
        ),
        array(
            'id'       => 'blog-sidebar-layout',
            'type'     => 'image_select',
            'title'    => esc_attr__( 'Choose sidebar option', 'the-seo' ),
            'subtitle' => esc_attr__( 'The option work for slug /blog/ and subpages/subcategories', 'the-seo' ),
            
            //Must provide key => value(array:title|img) pairs for radio options
            'options'  => array(
                '1' => array(
                    'alt' => 'Without sidebar',
                    'img' => get_template_directory_uri() . '/images/framework/nosidebar.gif'
                ),
                '2' => array(
                    'alt' => '2 sidebars',
                    'img' => get_template_directory_uri() . '/images/framework/2sidebars.gif'
                ),
                '3' => array(
                    'alt' => 'Left sidebar',
                    'img' => get_template_directory_uri() . '/images/framework/leftsidebar.gif'
                ),
                '4' => array(
                    'alt' => 'Right sidebar',
                    'img' => get_template_directory_uri() . '/images/framework/rightsidebar.gif'
                )
            ),
            'default'  => '1'
        ),

        array(
            'id'       => 'blog-columns',
            'type'     => 'button_set',
            'title'    => esc_attr__( 'Blog Columns Option', 'the-seo' ),
            'subtitle' => esc_attr__( 'Do you prefer 1 or 2 columns? The option work for slug /blog/ and subcategories', 'the-seo' ),
            'desc'     => esc_attr__( 'If you are turn on sidebar or sidebars, you cannot use 2 columns layout', 'the-seo' ),
            //Must provide key => value pairs for radio options
            'options'  => array(
                '1' => '1 Column',
                '2' => '2 Columns',
            ),
            'default'  => '2',
            'required' => array(
                array('blog-sidebar-layout', '=', array(1)),
            )
        ),

        array(
            'id'       => 'blog-header14_slider',
            'type'     => 'select',
            'title'    => esc_attr__( 'Choose Slider for Blog', 'the-seo' ),
            'subtitle' => esc_attr__( 'Choose slider for header section. The option work for slug /blog/ and subpages/subcategories', 'the-seo' ),
            
            //Must provide key => value(array:title|img) pairs for radio options
            'options'  => theseo_get_sliders_array(),
            'default'  => 'rev_header_short_1',

        ),

        array(
            'id'   => 'blog-opt-divide1',
            'type' => 'divide'
        ),

        /*======== Slider OR Image END ==========*/
        array(
            'id'       => 'rmorelink',
            'type'     => 'text',
            'title'    => esc_attr__( 'Read more link text', 'the-seo' ),
            'default'  => 'Continue reading'
        ),
            array(
                'id'       => 'is_related_posts',
                'type'     => 'switch',
                'title'    => esc_attr__( 'Related Posts for Single Post View', 'the-seo' ),
                'subtitle' => wp_kses(__('Press <code>On</code> to settings choice appear', 'the-seo' ), $allowed_html ),
                'default'  => true
            ),
                array(
                    'id'       => 'related_posts_title',
                    'type'     => 'text',
                    'title'    => esc_attr__( 'Related Posts Title', 'the-seo' ),
                    'subtitle' => esc_attr__( 'Set Title for Related Posts section', 'the-seo' ),
                    'required' => array( 'is_related_posts', '=', true ),
                    'default'  => 'Related Posts',
                ),
            array(
                'id'       => 'show_post_author',
                'type'     => 'switch',
                'title'    => esc_attr__( 'Show Post Author ', 'the-seo' ),
                'default'  => true,
            ),	
            array(
                'id'       => 'show_post_category',
                'type'     => 'switch',
                'title'    => esc_attr__( 'Show Post Category ', 'the-seo' ),
                'default'  => true,
            ),
            array(
                'id'       => 'show_post_tags',
                'type'     => 'switch',
                'title'    => esc_attr__( 'Show Post Tags ', 'the-seo' ),
                'default'  => true,
            ),
            array(
                'id'       => 'show_post_date',
                'type'     => 'switch',
                'title'    => esc_attr__( 'Show Post Date ', 'the-seo' ),
                'default'  => true,
            ),	
            array(
                'id'       => 'show_comments_count',
                'type'     => 'switch',
                'title'    => esc_attr__( 'Show Post Comments count ', 'the-seo' ),
                'default'  => true,
            ),			
		)
	)
);




Redux::setSection($opt_name, array(
    'title'     => esc_attr__('Color Schemes', 'the-seo'),
    'desc'      => esc_attr__('Color scheme of the current design. You can use preset color scheme or create your own.', 'the-seo'),
    'icon'      => 'el el-cog',

    'fields'    => array(
        array(
            'id'            => 'ocs',
            'type'          => 'color_scheme',
            'title'         => esc_attr__('Color Schemes', 'the-seo'),
            'subtitle'      => esc_attr__('Save and load color schemes', 'the-seo'),
            'compiler'  => true,
            'output'    => false,
            'simple'        => false,
            //'accordion_open' => true,
            'options'       => array(
                'show_input'                => true,
                'show_initial'              => true,
                'show_alpha'                => true,
                'show_palette'              => true,
                'show_palette_only'         => false,
                'show_selection_palette'    => true,
                'max_palette_size'          => 10,
                'allow_empty'               => true,
                'clickout_fires_change'     => false,
                'choose_text'               => 'Choose',
                'cancel_text'               => 'Cancel',
                'show_buttons'              => true,
                'use_extended_classes'      => false,
                'palette'                   => null,  // show default
            ),
            'groups'        => array(
                'Primary Colors'    => array(
                    'desc'              => esc_attr__('These colors making your design individual.', 'the-seo'),
                    'hidden'            => false,
                    'accordion_open'    => true
                ),
                'Auxiliary Colors'      => array(
                    'desc'              => esc_attr__('These colors making basic typography and backgrounds.', 'the-seo'),
                    'hidden'            => false,
                    'accordion_open'    => true
                ),
                'Additional Colors'    => array(
                    'desc'              => esc_attr__('Some colors', 'the-seo'),
                    'hidden'            => false,
                    'accordion_open'    => true
                ),
                'Topbar Colors'    => array(
                    'desc'              => esc_attr__('Topbar colors', 'the-seo'),
                    'hidden'            => false,
                    'accordion_open'    => true
                ),
            ),

            'default'       => array(
                // First Color ----------------
                array(
                    'id'        => 'gc1',
                    'title'     => 'Color 1',
                    'color'     => '#26b7e7',
                    'alpha'     => 1,
                    'group'     => 'Primary Colors'
                ),
                array(
                    'id'        => 'gc1l',
                    'title'     => 'Color 1 Light',
                    'color'     => '#45c6e9',
                    'alpha'     => 1,
                    'group'     => 'Primary Colors'
                ),
                array(
                    'id'        => 'gc1d',
                    'title'     => 'Color 1 Dark',
                    'color'     => '#0c98be',
                    'alpha'     => 1,
                    'group'     => 'Primary Colors'
                ),
                array(
                    'id'        => 'gc1t8',
                    'title'     => 'General Color 1 Semitransparent',
                    'desc'      => 'We use the color for semitransparent elements',
                    'color'     => '#26b7e7',
                    'alpha'     => 0.8,
                    'group'     => 'Primary Colors'
                ),
                // Second Color ----------------
                array(
                    'id'        => 'gc2',
                    'title'     => 'Color 2',
                    'color'     => '#44dd61',
                    'alpha'     => 1,
                    'group'     => 'Primary Colors'
                ),
                array(
                    'id'        => 'gc2l',
                    'title'     => 'Color 2 Light',
                    'color'     => '#59e28a',
                    'alpha'     => 1,
                    'group'     => 'Primary Colors'
                ),
                array(
                    'id'        => 'gc2d',
                    'title'     => 'Color 2 Dark',
                    'color'     => '#26c660',
                    'alpha'     => 1,
                    'group'     => 'Primary Colors'
                ),
                array(
                    'id'        => 'gc2t8',
                    'title'     => 'Color 2 Semitransparent',
                    'desc'      => 'We use the color for semitransparent elements',
                    'color'     => '#44dd61',
                    'alpha'     => 0.8,
                    'group'     => 'Primary Colors'
                ),
                //Auxiliary Colors
                array(
                    'id'        => 'bgc1',
                    'title'     => 'Background Color',
                    'color'     => '#ffffff',
                    'alpha'     => 1,
                    'group'     => 'Auxiliary Colors'
                ),
                array(
                    'id'        => 'bgc2',
                    'title'     => 'Content Background Color',
                    'color'     => '#fff',
                    'alpha'     => '0',
                    'group'     => 'Auxiliary Colors'
                ),

                array(
                    'id'        => 'ac1',
                    'title'     => 'Auxiliary Color',
                    'color'     => '#383f42',
                    'alpha'     => 1,
                    'group'     => 'Auxiliary Colors'
                ),
                array(
                    'id'        => 'ac2',
                    'title'     => 'Auxiliary Dark Color',
                    'color'     => '#252a2c',
                    'alpha'     => 1,
                    'group'     => 'Auxiliary Colors'
                ),
                array(
                    'id'        => 'ac3',
                    'title'     => 'Auxiliary Light Color',
                    'color'     => '#f8f8f8',
                    'alpha'     => 1,
                    'group'     => 'Auxiliary Colors'
                ),
                array(
                    'id'        => 'ac32',
                    'title'     => 'Auxiliary Light Color 2',
                    'color'     => '#f8f8f8',
                    'alpha'     => 1,
                    'group'     => 'Auxiliary Colors'
                ),
                array(
                    'id'        => 'ac4',
                    'title'     => 'Dark Font Color for Light BGRs',
                    'color'     => '#252a2c',
                    'alpha'     => 1,
                    'group'     => 'Auxiliary Colors'
                ),
                /// Additional Colors ------------>
                array(
                    'id'        => 'fbgr1',
                    'title'     => 'Form BGR Color',
                    'color'     => '#000000',
                    'alpha'     => 0.03,
                    'group'     => 'Additional Colors'
                ),
                array(
                    'id'        => 'fbgr1h',
                    'title'     => 'Form BGR Color on hover',
                    'color'     => '#000000',
                    'alpha'     => 0.06,
                    'group'     => 'Additional Colors'
                ),
                array(
                    'id'        => 'bbgr',
                    'title'     => 'Bottom Line BGR Color',
                    'color'     => '#44dd61',
                    'alpha'     => 1,
                    'group'     => 'Additional Colors'
                ),
                array(
                    'id'        => 'logosbgr',
                    'title'     => 'Logos Line BGR Color',
                    'color'     => '#FFFFFF',
                    'alpha'     => 1,
                    'group'     => 'Additional Colors',
                ),
                // Topbar Colors
                array(
                    'id'        => 'tbc1',
                    'title'     => 'Text color',
                    'color'     => '#FFFFFF',
                    'alpha'     => 1,
                    'group'     => 'Topbar Colors',
                ),
                array(
                    'id'        => 'tbc2',
                    'title'     => 'Icon Color',
                    'color'     => '#FFFFFF',
                    'alpha'     => 1,
                    'group'     => 'Topbar Colors',
                ),
                array(
                    'id'        => 'tbc3',
                    'title'     => 'Social Icon Color',
                    'color'     => '#FFFFFF',
                    'alpha'     => 1,
                    'group'     => 'Topbar Colors',
                ),
            )
        ),
    ),

) );

 
function compiler_action($options, $css, $changed_values) {
    global $wp_filesystem;

	$filename = get_stylesheet_uri() . '/style.css';
 
    if( empty( $wp_filesystem ) ) {
        require_once( ABSPATH .'/wp-admin/includes/file.php' );
        WP_Filesystem();
    }
 
    if( $wp_filesystem ) {
        $wp_filesystem->put_contents(
            $filename,
            $css,
            FS_CHMOD_FILE // predefined mode settings for WP files
        );
    }
}

add_filter('redux/options/secretlab/saved', 'set_index_page');

function set_index_page($var) {
    update_option( 'page_on_front', $var['index-page'] );
    update_option( 'show_on_front', 'page' );
}


add_filter('redux/options/secretlab/saved', 'save_map_script');

function save_map_script($var) {

    $lat = isset($var['map_settings_latitude']) ? $var['map_settings_latitude'] : 34.017973;
	$long = isset($var['map_settings_longtitude']) ? $var['map_settings_longtitude'] : -118.292053;
	$zoom = isset($var['map_settings_zoom']) ? $var['map_settings_zoom'] : 17;
    if (isset($var['zoomonoff'])) {
        if ($var['zoomonoff'] == true) {
            $zoomonoff = 'true';
        } else {
            $zoomonoff = 'false';
        }

    }
	$icon = isset($var['map_settings_marker']['url']) ? $var['map_settings_marker']['url'] : '';

    $out = "
	
	jQuery(document).ready(function($){
	    var map = new GMaps({
		    el: '#footermap',
		    lat: ".$lat.",
			lng: ".$long.",
			zoom: ".$zoom.",
			scrollwheel : ".$zoomonoff.",
			zoomControl : true,
			    zoomControlOpt: {
				    style : 'SMALL',
					position: 'TOP_LEFT'
				},
			panControl : false,
            streetViewControl : false,
            mapTypeControl: false,
            overviewMapControl: false
        });	

        map.addMarker({
            lat: ".$lat.",
            lng: ".$long.",
			icon: '".$icon."'
		});".
		'
		var styles = [
		    {
			    stylers: [
				    { hue: "#95a5a6" },
					{ saturation: -100 }
				]
			}, {
					featureType: "road",
					elementType: "geometry",
					stylers: [
						{ lightness: 100 },
						{ visibility: "simplified" }
					]
			}, {
					featureType: "road",
					elementType: "labels",
					stylers: [
						{ visibility: "off" }
					]
				}
            ];

			map.addStyle({
				styledMapName:"Styled Map",
				styles: styles,
				mapTypeId: "map_style"
			});

			map.setStyle("map_style");

		});';

        global $wp_filesystem;	

        if( empty( $wp_filesystem ) ) {
            require_once( ABSPATH .'/wp-admin/includes/file.php' );
            WP_Filesystem();
        }

            if( $wp_filesystem ) {
                $wp_filesystem->put_contents(get_stylesheet_directory().'/js/map.js', $out, FS_CHMOD_FILE);
            }

}

