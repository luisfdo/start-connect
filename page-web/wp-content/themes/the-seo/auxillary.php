<?php

function theseo_set_ajax_connector($hook) {

    wp_localize_script('jquery', 'localajax', 
	    array(
	        'url' => admin_url('admin-ajax.php')
    	)
	);	
}

add_action('admin_enqueue_scripts', 'theseo_set_ajax_connector');
add_action('wp_enqueue_scripts', 'theseo_set_ajax_connector');

add_action( 'wp_ajax_handle_post_layout', 'theseo_handle_post_layout' );
add_action( 'wp_ajax_nopriv_handle_post_layout', 'theseo_handle_post_layout' );

function theseo_handle_post_layout () {
    if (isset($_POST['post_id']) && is_numeric($_POST['post_id'])) {
        if (delete_post_meta($_POST['post_id'], 'layout_settings')) echo "data removed for id=".$_POST['post_id']; else echo "data not removed for id=".$_POST['post_id'];
	}
}

// Speed UP
add_filter('the_generator', '__return_empty_string'); // remove WordPress version
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

// Handle_IconSearch() - function handling AJAX queries from Icon Search Form (Suppamenu Plugin) */

add_action( 'wp_ajax_handle_iconsearch', 'theseo_handle_iconsearch' );
add_action( 'wp_ajax_nopriv_handle_iconsearch', 'theseo_handle_iconsearch' );
		
function theseo_handle_iconsearch() {
	global $fontAwesome, $alico;

	$icons = $alico;

	$results = array();
	if (isset($_POST['query'])) {
		$pattern = "/".$_POST['query'].".*/i";
		foreach ($icons as $icon) {
			if (preg_match($pattern, $icon)) {
				$results[] = $icon;
		    }
	    }
	}
	if (count($results) > 0) {
		echo json_encode(array('1', $results));
    }
    else {
		echo json_encode(array('0'));
    }			
}


/* 
    theseo_get_customized_slider() returns aliases of availeble sliders, dependinding
	of $theseo_page_type which can indicate WooComerce page (forms 'shop' prefix, 
	                                 Blog page (forms 'blog' prefix),
									 and regular page (forms '' prefix)
*/

function theseo_get_customized_slider() {
	
    global $secretlab, $theseo_layout;

	if ($secretlab['theseo_page_type'] == 'blog') {
	    $secretlab['theseo_pagetype_prefix'] = 'blog-';
	}
	else if ($secretlab['theseo_page_type'] == 'shop') {
	    $secretlab['theseo_pagetype_prefix'] = 'shop-';
	}
	else {
	    $secretlab['theseo_pagetype_prefix'] = '';
	}
	$param_name = $secretlab['theseo_pagetype_prefix'].'header14_slider';
	if (count($theseo_layout) == 0 || $theseo_layout[$param_name] == 'default') {
	    $params = $secretlab; 
	}
	else {
	    $params = $theseo_layout;
	}
    	
	if (!empty($params[$param_name]) && preg_match('/(rev_|lay_)(.+)/', $params[$param_name], $slider)) {
		$type = $slider[1];
		$slider = $slider[2];
        if( is_plugin_active( 'LayerSlider/layerslider.php' ) ) {
            if ($type == 'lay_') echo do_shortcode('[layerslider id="' . $slider . '"]');
        }
        if( is_plugin_active( 'revslider/revslider.php' ) ) {
            if ($type == 'rev_') echo do_shortcode('[rev_slider alias="' . $slider . '"]');
        }
	}
	else return;
}

add_action('get_header_scripts', 'theseo_get_header_scripts');

function theseo_array_insert($array, $var, $position) {
    $before = array_slice($array, 0, $position);
    $after = array_slice($array, $position);

    $return = array_merge($before, (array) $var);
    return array_merge($return, $after);
}

function theseo_get_header_scripts() {

    global $secretlab;

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
        wp_enqueue_script( 'comment-reply' );


	wp_enqueue_script('bootstrap',  esc_url(get_template_directory_uri()) . '/js/bootstrap.min.js', array('jquery'), null, false);

	wp_enqueue_script('theseo_mainjs',  esc_url(get_template_directory_uri()) . '/js/main.js', array('jquery', 'bootstrap'), null, false);
	wp_enqueue_script('slick_js', esc_url(get_template_directory_uri()) . '/js/lib/slick/slick.js', array('jquery'), false, true);

	
	$sell_foot_choose = isset($secretlab['footer-type-layout']) ? $secretlab['footer-type-layout'] : 1;
	if ($sell_foot_choose == 1)
	if (isset($secretlab['show-gmap']) && $secretlab['show-gmap'] == 1) {
		if (isset($secretlab['display-footer']) && $secretlab['display-footer'] == 1) {
			if (isset ($secretlab['map_api_key'])) {
				wp_enqueue_script('theseo_gmap', 'https://maps.google.com/maps/api/js?key=' . $secretlab['map_api_key'] . '', array(), null, false);
			}
			wp_enqueue_script('theseo_footer_map', esc_url(get_template_directory_uri()) . '/js/map.js', array('jquery'), null, false);
			wp_enqueue_script('theseo_jquerymaps', esc_url(get_template_directory_uri()) . '/js/jquery.maps.js', array('jquery', 'theseo_footer_map'), null, false);

		}

	}
	//wp_enqueue_style( 'slick_css', esc_url(get_template_directory_uri()) . '/js/lib/slick/slick.css', array(), false, 'all' );
	$plugins = get_option('active_plugins');
	if (!in_array('suppamenu/index.php', $plugins)) {
		//Suppamenu Plugin Contain Icons for the theme. So if its not active, we need to save base theme icons by Font Awesome
		wp_enqueue_style( 'suppa_frontend-alico',  esc_url(get_template_directory_uri()) . '/css/alico.css', array(), false);

	}
	wp_enqueue_style('theseo_bootstrap',  esc_url(get_template_directory_uri()) . '/css/bootstrap.min.css', array(), false, 'all');
	wp_enqueue_style('dynamic_css',  esc_url(get_template_directory_uri()) . '/css/dynamic.css', array(), false, 'all');
	wp_enqueue_style('theseo_ownstyles',  esc_url(get_template_directory_uri()) . '/style.css', array('dynamic_css'), false, 'all');


	/*if (isset($secretlab['show-rtl']) && $secretlab['show-rtl'] == 1) {
		wp_register_style('rtl_css',  esc_url(get_template_directory_uri()) . '/css/rtl.css', array('dynamic_css'), null, '');
		wp_enqueue_style('rtl_css');
	}*/

	if (isset ($secretlab['rtloption'])) {
		if ($secretlab['rtloption'] == 1) {

			$sl_design_css = isset($secretlab['design-css']) ? $secretlab['design-css'] : 1;
			if ($sl_design_css == 1 or $sl_design_css == 2) {
				wp_register_style('theseo_rtl', esc_url(get_template_directory_uri()) . '/css/rtl1.css', array('dynamic_css'), false, 'all');
				wp_enqueue_style('theseo_rtl');
			}
			if ($sl_design_css == 3 or $sl_design_css == 4) {
				wp_register_style('theseo_rtl', esc_url(get_template_directory_uri()) . '/css/rtl2.css', array('dynamic_css'), false, 'all');
				wp_enqueue_style('theseo_rtl');
			}
			if ($sl_design_css == 5) {
				wp_register_style('theseo_rtl', esc_url(get_template_directory_uri()) . '/css/rtl3.css', array('dynamic_css'), false, 'all');
				wp_enqueue_style('theseo_rtl');
			}
		}
	}


}

function theseo_post_is_in_descendant_category( $cats, $_post = null )
{
    foreach ( (array) $cats as $cat ) {
        // get_term_children() accepts integer ID only
        $descendants = get_term_children( (int) $cat, 'category');
        if ( $descendants && in_category( $descendants, $_post ) )
            return true;
    }
    return false;
}


/* 
    fuction passing params to Slick Carousel JS script 
*/

function theseo_add_slick_carousel() {

    global $secretlab;
	
    foreach ($secretlab['slick'] as $params) {
	
	if (!isset($params["992"])) {
	    $params['992'] = array( 'sp_row' => $params['sp_row'],
		                        'sp_scroll' => $params['sp_scroll'],
								'sp_show' => $params['sp_show'] );
	}

	echo '<script type="text/javascript">jQuery(document).ready(function() { jQuery(".'.$params['class'].'").not(\'.slick-initialized\').slick({
		  autoplay      : '.$params['autoplay'].
		 ',arrows        : '.$params['enable_nav'].
		 ',autoplaySpeed : '.$params['speed'].
		 ',dots : '.$params['dots'].
		 ',arrows : '.$params['arrows'].',';
		if (isset ($secretlab['rtlcarousel'])) {
			if ($secretlab['rtlcarousel'] == 1) {
				echo 'rtl: true,';
			}
		}
		 if (isset($params["rows"])) {
		 	echo 'rows : '.$params['rows'].',';
		 }
		echo 'slidesPerRow : '.$params['sp_row'].
		 ',slidesToScroll : '.$params['sp_scroll'].
		 ',slidesToShow : '.$params['sp_show'].
		  			 
		 ',responsive : [';
          if (isset($params["992"])) {
			  echo '{
		      breakpoint : 992,
			  settings : {
			      slidesPerRow : ' . $params["992"]["sp_row"] . ',
				  slidesToScroll : ' . $params["992"]["sp_scroll"] . ', 
				  slidesToShow : ' . $params["992"]["sp_show"] . '
			  }
		  },';
		  }
if (isset($params["769"])) {
	echo '{
		      breakpoint : 769,
			  settings : {
			      slidesPerRow : ' . $params["769"]["sp_row"] . ',
				  slidesToScroll : ' . $params["769"]["sp_scroll"] . ', 
				  slidesToShow : ' . $params["769"]["sp_show"] . '
			  }
		  },';
}
if (isset($params["601"])) {
	echo '{
		      breakpoint : 601,
			  settings : {
			      slidesPerRow : ' . $params["601"]["sp_row"] . ',
				  slidesToScroll : ' . $params["601"]["sp_scroll"] . ', 
				  slidesToShow : ' . $params["601"]["sp_show"] . '
			  }
		  },';
}
	if (isset($params["521"])) {
		echo '{
		      breakpoint : 521,
			  settings : {
			      slidesPerRow : ' . $params["521"]["sp_row"] . ',
				  slidesToScroll : ' . $params["521"]["sp_scroll"] . ', 
				  slidesToShow : ' . $params["521"]["sp_show"] . '
			  }
		  },';
	}
          
	echo ']
		 }); });</script>';
		 
	}
			
    }


if ( ! function_exists( 'theseo_get_entry_meta' ) ) :
	/**
	 * Print HTML with meta information for current post: categories, tags, permalink, author, and date.
	 *
	 * Create your own theseo_entry_meta() to override in a child theme.
	 *

	 */
	function theseo_get_entry_meta($meta_set = true, $item = null)
	{
		global $secretlab, $post;

		if (!$meta_set) {
		    $settings = $secretlab;
		}
		else {
		    $settings = array ( 'show_post_date' => true,
                                'show_post_category' => true,
								'show_post_author' => true,
								'show_comments_count' => true );
		}

		if (!$item) {
		    $p = $post;
		}
		else {
		    $p = $item;
		}

		$out = '';

		if (is_sticky($p->ID))
			$out .= '<span class="featured-post"><i class="fa fa-paperclip stickyicon"></i></span>';

		$sl_show_post_date = isset($settings['show_post_date']) ? $settings['show_post_date'] : 1;
		if ($sl_show_post_date == 1) {
			if (!has_post_format('link') && 'post' == get_post_type($p)) {
				$out .= theseo_entry_date(false, $p);
			}
		}
		// Translators: used between list items, there is a space after the comma.
		$categories_list = get_the_category_list(esc_html__(', ', 'the-seo'), 'single', $p->ID);
		$sl_show_post_category = isset($settings['show_post_category']) ? $settings['show_post_category'] : 1;
		if ($sl_show_post_category == 1) {
			if ($categories_list) {
				$out .= '<span class="icon icon-stack-empty"></span><span class="categories-links">' . $categories_list . '</span>';
			}
		}


		// Post author
		$sl_show_author = isset($settings['show_post_author']) ? $settings['show_post_author'] : 1;
		if ($sl_show_author == 1) {
			if ('post' == get_post_type($p->ID)) {
			    $author = get_user_by('id', $p->post_author);
			    $name = get_the_author_meta( 'display_name', $author->ID );
				$out .= '<span class="icon icon-user-tie"></span><span class="author vcard"><a class="url fn n" href="'.
				         esc_url(get_author_posts_url($p->post_author)).'"  title="'.esc_html__('View all posts by ', 'the-seo').''. $name .'" rel="author">'. $name .'</a></span>';
			}
		}


        // Comments counter
		$sl_show_comments_count = isset($settings['show_comments_count']) ? $settings['show_comments_count'] : 1;
		if ($sl_show_comments_count == 1) {
			//if (comments_open($p->ID)) comments_popup_link(esc_html__('Leave a comment', 'the-seo'), esc_html__('1 Comment', 'the-seo'), esc_html__('% Comments', 'the-seo'));
			if (comments_open($p->ID)) {
			    $comments = wp_count_comments( $p->ID );
				if ($comments->approved > 0) {
				    $out .= '<span class="comments-link"><span class="icon icon-comments-o"></span> ';
				    $out .= '<a href="'.get_permalink($p->ID).'#comments'.'">'.$comments->approved.' '.esc_html__('comments', 'the-seo').'</a>';
					$out .= '</span>';
				}
		        else {
		            $out .= '<span class="comments-link">
		                <span class="icon icon-comments-o"></span>
		                    <a href="'.get_permalink($p->ID).'#respond"> '.esc_html__( 'Leave a comment', 'the-seo' ).'</a>
				        </span>';
				}
			
            }			
		}
		
			
		return $out;
	}
endif;

function theseo_rmorelink() {
    global $secretlab;
    if (isset($secretlab['rmorelink'])) {
        $more_link_text = $secretlab['rmorelink'].' &raquo;';
    } else {
        $more_link_text = esc_html__('Continue reading', 'the-seo').' &raquo;';
    }
    return $more_link_text;
}

function theseo_get_excerpt($p, $excerpt, $length = 35) {

    global $post;

    $more_link_text =  theseo_rmorelink();
	$excerpt_more = apply_filters( 'the_content_more_link', ' <a href="' . get_permalink($p->ID) . "#more-{$p->ID}\" class=\"more-link\">$more_link_text</a>", $more_link_text );
    if( has_excerpt( $p ) ){
        $excerpt = get_the_excerpt($p);
    } else {
        $text = $excerpt;
        $text = apply_filters('the_content', $text);
        $text = str_replace(']]>', ']]&gt;', $text);
        $text = strip_tags($text);
        $excerpt_length = apply_filters('excerpt_length', $length);
        $words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
        if ( count($words) > $excerpt_length || count($words) <= 5) {
            array_pop($words);
            $text = implode(' ', array_slice( $words, 0, $length ) );
            $text = $text;
        } else {
            $text = implode(' ', $words);
        }
        $excerpt =  $text;

    }
    if ($excerpt) return $excerpt . $excerpt_more;


}

function theseo_get_excerpt_short($p, $excerpt, $length = 35) {

	global $post;

	if ($excerpt) return $excerpt;

	if (!$p) $p = $post;

	$text = strip_shortcodes( $p->post_content );

	$text = apply_filters('the_content', $text);
	$text = str_replace(']]>', ']]&gt;', $text);
	$text = strip_tags($text);
	$excerpt_length = apply_filters('excerpt_length', $length);
	//$excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
	//$more_link_text = esc_html__('Continue reading', 'the-seo').' &raquo;';
	//$excerpt_more = apply_filters( 'the_content_more_link', ' <a href="' . get_permalink($p->ID) . "#more-{$p->ID}\" class=\"more-link\">$more_link_text</a>", $more_link_text );
	$words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
	if ( count($words) > $excerpt_length || count($words) <= 5) {
		array_pop($words);
		$text = implode(' ', array_slice( $words, 0, $length ) );
		$text = $text.'...' ;
	} else {
		$text = implode(' ', $words);
	}
	$raw_excerpt = '';

	return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
}


function theseo_get_content ($p = null, $more_link_text = null, $strip_teaser = false) {
    global $post;
	if (!$p) $p = $post;

	if ( null === $more_link_text )
		$more_link_text = theseo_rmorelink();

	$output = '';
	$has_teaser = false;

	// If post password required and it doesn't match the cookie.
	if ( post_password_required( $p ) )
		return get_the_password_form( $p );

	$content = apply_filters( 'the_content', $p->post_content);
	if ( preg_match( '/<!--more(.*?)?-->/', $content, $matches ) ) {
		$content = explode( $matches[0], $content, 2 );
		if ( ! empty( $matches[1] ) && ! empty( $more_link_text ) )
			$more_link_text = strip_tags( wp_kses_no_null( trim( $matches[1] ) ) );

		$has_teaser = true;
	} else {
		$content = array( $content );
	}

	if ( false !== strpos( $p->post_content, '<!--noteaser-->' ) && ( ! $multipage ) )
		$strip_teaser = true;

	$teaser = $content[0];

	if ( $more_link_text && $strip_teaser && $has_teaser )
		$teaser = '';

	$output .= $teaser;

	if ( count( $content ) > 1 ) {
		if ( $more ) {
			$output .= '<span id="more-' . $post->ID . '"></span>' . $content[1];
		} else {
			if ( ! empty( $more_link_text ) )
                $more_link_text = preg_replace('/%s/', '', $more_link_text); // it is a hack !!
				/**
				 * Filter the Read More link text.
				 *
				 * @since 2.8.0
				 *
				 * @param string $more_link_element Read More link element.
				 * @param string $more_link_text    Read More text.
				 */
				$output .= apply_filters( 'the_content_more_link', ' <a href="' . get_permalink($p->ID) . "#more-{$p->ID}\" class=\"more-link\">$more_link_text</a>", $more_link_text );
			$output = force_balance_tags( $output );
		}
	}
	
	$preview = false;

	if ( $preview ) // Preview fix for JavaScript bug with foreign languages.
		$output =	preg_replace_callback( '/\%u([0-9A-F]{4})/', '_convert_urlencoded_to_entities', $output );

	return $output;	
}

// Excerpt function for search path
function theseo_get_excerpt_for_search() {
	$text = get_the_excerpt();
	if ($text) { echo '<p>' . implode(' ', array_slice(explode(' ', $text), 0 , 30)) . '</p>';
	} else {
		$text = get_the_content('');
		$text = apply_filters('the_content', $text);
        $text=preg_replace( '#<script(.*?)>(.*?)</script>#is', '', $text );
		$text = trim(strip_tags($text, '<h1>,<p>'));
		if(preg_match('#<p[^>]*>.*?</p>#is',$text, $matches)){
			$text = $matches[0];
		};
		$text = strip_tags($text);
		echo apply_filters('the_content',$text);
	}
}


?>
