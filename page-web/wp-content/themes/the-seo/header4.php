
<!-- HEADER 4 START -->
<link rel="stylesheet" type="text/css" href="https://www.somosstart.com.co/wp-content/themes/the-seo/css/start.css">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133197024-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-133197024-1');
</script>

<div class="head4">
    <?php theseo_set_header4_content(); ?>
    <div class="container-fluid">
        <div class="row head4m">
            <?php echo wp_nav_menu( array( "theme_location" => "header4" ) ); ?>
        </div>
    </div>
</div>




<?php

theseo_set_customized_slider();

?>

<!-- HEADER 4 END -->