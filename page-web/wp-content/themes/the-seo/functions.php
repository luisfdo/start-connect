<?php
/*
 *  Author: SecretLab
 *  URL: http://secretlab.pw
 *  Custom functions, support and more.
 */
 
update_option('ultimate_vc_addons_redirect', false);
$ultimate_constants = get_option('ultimate_constants');
$ultimate_constants['ULTIMATE_NO_PLUGIN_PAGE_NOTICE'] = 1;
update_option('ultimate_constants', $ultimate_constants);
update_option( 'wpb_js_composer_license_activation_notified', 'yes' );
remove_action('init', 'vc_page_welcome_redirect');
remove_action('vc_activation_hook', 'vc_page_welcome_set_redirect');
delete_transient('_redux_activation_redirect');
delete_transient('_wc_activation_redirect');
$GLOBALS['redux_notice_check'] = 0;
update_option('revslider-valid-notice', 'false');

add_action( 'after_setup_theme', 'remove_vc_reminder' );

function remove_vc_reminder() {
    SetCookie('vchideactivationmsg_vc11', '12.0');
}

function theseo_check_pagetype($theseo_pagetype_prefix) {
    global $theseo_layout, $secretlab;

	$props = array ('shop-' => array('boxed-background', 'boxed-background-color'),
	                'blog-' => array() );
	if (isset($secretlab['boxed-background']) || isset($secretlab['boxed-background-color'])) {
		foreach ($props[$theseo_pagetype_prefix] as $prop) {
			if (!isset($theseo_layout[$theseo_pagetype_prefix . $prop])) {
				$theseo_layout[$theseo_pagetype_prefix . $prop] = $secretlab[$prop];
			}
		}
	}
	if (isset($secretlab['blog-columns'])) {
		$theseo_layout['blog-columns'] = $secretlab['blog-columns'];
	}
}
	
// Welcome Page section */

add_action('admin_menu', 'theseo_welcome_screen_page');
function theseo_welcome_screen_page(){
    add_theme_page('Welcome', 'Welcome', 'read', 'secretlab-welcome', 'theseo_welcome_page');
}
function theseo_welcome_page(){

	require_once (get_template_directory() . '/inc/welcome_page.php');
	
}


// end of Welcome Page section	

require_once ( ABSPATH . '/wp-admin/includes/taxonomy.php');

add_action('after_switch_theme', 'theseo_secretlab_activate');

function theseo_secretlab_activate() {

    include_once ABSPATH.'/wp-admin/includes/file.php';
	WP_Filesystem();
	$default_mods = array();

	$blog_cat = get_category_by_slug('blog');
	if (!$blog_cat) {
		$blog_cat = wp_insert_category(array( 'cat_name' => 'Blog', 'category_nicename' => 'blog'));
	}
	else {
		$blog_cat = $blog_cat->term_id;
	}
	if ($blog_cat) {
		$current_cat = get_option('default_category');
		$default_mods['default_category'] = $current_cat;
		update_option('default_category', $blog_cat);
	}
	
	update_option('default_mods', json_encode($default_mods));

    add_action('admin_menu','welcome_redirect');
    function welcome_redirect($plugin) {
        wp_redirect(admin_url('themes.php?page=secretlab-welcome'));
        die();
    }	
	
}

add_action('switch_theme', 'theseo_secretlab_deactivate');

function theseo_secretlab_deactivate() {
    $default_mods = json_decode(get_option('default_mods'), true);
	$saved_cat = $default_mods['default_category'];
	if (isset($saved_cat)) update_option('default_category', $saved_cat);
}

if ( class_exists( 'Redux' ) && file_exists( get_template_directory() . '/inc/config.php' ) ) {
	require_once ( get_template_directory() . '/inc/config.php' );
}

$opt_name = 'secretlab';

/**
 * The SEO only works in WordPress 4.1 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.1-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}
/**
 *  The SEO setup.
 *
 * Sets up theme defaults and registers the various WordPress features that
 *  The SEO supports.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add Visual Editor stylesheets.
 * @uses add_theme_support() To add support for automatic feed links, post
 * formats, and post thumbnails.
 * @uses register_nav_menu() To add support for a navigation menu.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *

 */

 


 
function theseo_setup() {
	/*
	 * Makes The SEO available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on SecretLab, use a find and
	 * replace to change 'the-seo' to the name of your theme in all
	 * template files.
	 */
	load_theme_textdomain( 'the-seo', get_template_directory() . '/languages' );

	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css' ) );

	/*
	 * Switches default core markup for search form, comment form,
	 * and comments to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * This theme supports all available post formats by default.
	 * See https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'audio', 'gallery', 'image', 'link', 'quote', 'video'
	) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'header1and5', esc_html__( 'Header 1', 'the-seo' ) );
	register_nav_menu( 'header2', esc_html__( 'Header 2 Menu', 'the-seo' ) );
	register_nav_menu( 'header3', esc_html__( 'Header 3 Menu', 'the-seo' ) );
	register_nav_menu( 'header4', esc_html__( 'Header 4 Menu', 'the-seo' ) );

	/*
	 * This theme uses a custom image size for featured images, displayed on
	 * "standard" posts and pages.
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 700, 525, true );
	add_image_size('theseo_large', 1100, 800, true); // Large Thumbnail
	add_image_size('theseo_smalldigital', 250, 310, true); // Small Digital Thumbnail
	add_image_size('theseo_featured_preview', 55, 55, true);
	add_image_size( 'theseo_masonry', 300, 9999, true );
	add_image_size('theseo_last4', 90, 130, true); // 4 Last Posts - 3
	add_image_size('theseo_last41', 680, 300, true); // 4 Last Posts - 1
	add_image_size('theseo_portfolio', 300, 300, true); // 4 Last Posts - 1
	

	// This theme uses its own gallery styles.
	add_filter( 'use_default_gallery_style', '__return_false' );
}
add_action( 'after_setup_theme', 'theseo_setup' );


/* Shop Setting */
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
/* Product Page*/
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
add_action('woocommerce_before_single_product_summary', 'woocommerce_template_single_title', 5);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 41);

// Change columns count ot 4
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 4; // 4 products per row
	}
}
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

// Related products count
function woo_related_products_limit() {
	global $product;
	$args['posts_per_page'] = 3;
	return $args;
}


add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
function jk_related_products_args( $args ) {
	$args['posts_per_page'] = 3; // 4 related products
	$args['columns'] = 3; // arranged in 2 columns
	return $args;
}
/*------------------------------------*\
	Functions
\*------------------------------------*/

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function theseo_my_css_attributes_filter($var)
{
	return is_array($var) ? array() : '';
}

/**
 * Filter the page title.
 *
 * Creates a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *

 *
 * @param string $title Default title text for current view.
 * @param string $sep   Optional separator.
 * @return string The filtered title.
 */
function theseo_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
		$title = "$title $sep " . sprintf( esc_html__( 'Page %s', 'the-seo' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'theseo_wp_title', 10, 2 );



/**
 * Register two widget areas.
 *
 */
function theseo_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Left Sidebar', 'the-seo' ),
		'id'            => '_default_left_sidebar',
		'description'   => esc_html__( 'Appears in the left section of the site.', 'the-seo' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Bottom Sidebar', 'the-seo' ),
		'id'            => '_default_bottom_sidebar',
		'description'   => esc_html__( 'Appears on posts and pages in the footer.', 'the-seo' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Right Sidebar', 'the-seo' ),
		'id'            => '_default_right_sidebar',
		'description'   => esc_html__( 'Appears in the right section of the site.', 'the-seo' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Left Blog Sidebar', 'the-seo' ),
		'id'            => 'blog_default_left_sidebar',
		'description'   => esc_html__( 'Appears in the left blog section of the site.', 'the-seo' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Right Blog Sidebar', 'the-seo' ),
		'id'            => 'blog_default_right_sidebar',
		'description'   => esc_html__( 'Appears in the right blog section of the site.', 'the-seo' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Left Shop Sidebar', 'the-seo' ),
		'id'            => 'shop_default_left_sidebar',
		'description'   => esc_html__( 'Appears in the left shop section of the site.', 'the-seo' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Right Shop Sidebar', 'the-seo' ),
		'id'            => 'shop_default_right_sidebar',
		'description'   => esc_html__( 'Appears in the left shop section of the site.', 'the-seo' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Customized', 'the-seo' ),
		'id'            => 'footer_customized',
		'description'   => esc_html__( 'Sidebar for fully customized footer section (can be turn on at Theme options -> Design -> Footer)', 'the-seo' ),
		'before_widget' => '<div class="footer_section container">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'theseo_widgets_init' );

if ( ! function_exists( 'theseo_paging_nav' ) ) :
	/**
	 * Display navigation to next/previous set of posts when applicable.
	 *
	 */
	function theseo_paging_nav() {
		global $wp_query;

		// Don't print empty markup if there's only one page.
		if ( $wp_query->max_num_pages < 2 )
			return;
		?>
		<nav class="navigation paging-navigation" role="navigation">
			<div class="nav-links clearfix">

				<?php if ( get_next_posts_link() ) : ?>
					<div class="nav-previous alignleft"><?php next_posts_link( '<span class="meta-nav">&larr;</span> '.esc_html__( 'Older posts', 'the-seo' ) ); ?></div>
				<?php endif; ?>

				<?php if ( get_previous_posts_link() ) : ?>
					<div class="nav-next alignright"><?php previous_posts_link( esc_html__( 'Newer posts', 'the-seo' ).' <span class="meta-nav">&rarr;</span>' ); ?></div>
				<?php endif; ?>

			</div><!-- .nav-links -->
		</nav><!-- .navigation -->
		<?php
	}
endif;

if ( ! function_exists( 'theseo_post_nav' ) ) :
	/**
	 * Display navigation to next/previous post when applicable.
	 *
	 */
	function theseo_post_nav() {
		global $post;

		// Don't print empty markup if there's nowhere to navigate.
		$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous )
			return;
		?>
		<nav class="navigation post-navigation" role="navigation">
			<div class="nav-links clearfix">

				<?php previous_post_link( '%link', _x( '<span class="meta-nav">&larr;</span> %title', 'Previous post link', 'the-seo' ) ); ?>
				<?php next_post_link( '%link', _x( '%title <span class="meta-nav">&rarr;</span>', 'Next post link', 'the-seo' ) ); ?>

			</div><!-- .nav-links -->
		</nav><!-- .navigation -->
		<?php
	}
endif;

function theseo_add_class_next_post_link($html){
	$html = str_replace('<a','<a class="btn btn-default alignright"',$html);
	return $html;
}
add_filter('next_post_link','theseo_add_class_next_post_link',10,1);

function theseo_add_class_previous_post_link($html){
	$html = str_replace('<a','<a class="btn btn-default alignleft"',$html);
	return $html;
}
add_filter('previous_post_link','theseo_add_class_previous_post_link',10,1);

if ( ! function_exists( 'theseo_entry_meta' ) ) :
	/**
	 * Print HTML with meta information for current post: categories, tags, permalink, author, and date.
	 *
	 * Create your own theseo_entry_meta() to override in a child theme.
	 *

	 */
	function theseo_entry_meta()
	{
		global $secretlab;
		if (is_sticky() && is_home() && !is_paged())
			echo '<span class="featured-post"><span class="icon icon-paperclip"></span></span>';

		$sl_show_post_date = isset($secretlab['show_post_date']) ? $secretlab['show_post_date'] : 1;
		if ($sl_show_post_date == 1) {
			if ( 'post' == get_post_type())
				theseo_entry_date();
		}
		echo '<span class="updated"><i class="fa fa-calendar"></i> '. get_the_modified_time('F jS, Y h:i a').'</span>';

		// Translators: used between list items, there is a space after the comma.
		$categories_list = get_the_category_list(esc_html__(', ', 'the-seo'));
		$sl_show_post_category = isset($secretlab['show_post_category']) ? $secretlab['show_post_category'] : 1;
		if ($sl_show_post_category == 1) {
			if ($categories_list) {
				echo '<span class="categories-links"><span class="icon icon-stack-empty"></span> ' . $categories_list . '</span>';
			}
		}

		// Post author
		$sl_show_author = isset($secretlab['show_post_author']) ? $secretlab['show_post_author'] : 1;
		if ($sl_show_author == 1) {
			if ('post' == get_post_type()) {
				printf('<span class="author vcard"><span class="icon icon-user-tie"></span> <a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
					esc_url(get_author_posts_url(get_the_author_meta('ID'))),
					esc_attr(sprintf(esc_html__('View all posts by %s', 'the-seo'), get_the_author())),
					get_the_author()
				);
			}
		}


		// Comments counter
		$sl_show_comments_count = isset($secretlab['show_comments_count']) ? $secretlab['show_comments_count'] : 1;
		if ($sl_show_comments_count == 1) {
			if (comments_open(get_the_ID())) {
				echo '<span class="comments-link"><span class="icon icon-comments-o"></span> ';
				comments_popup_link(esc_html__('Leave a comment', 'the-seo'), esc_html__('1 Comment', 'the-seo'), esc_html__('% Comments', 'the-seo'));
				echo '</span>';
			}
		}

	}
endif;

if ( ! function_exists( 'theseo_entry_date' ) ) :
	/**
	 * Print HTML with date information for current post.
	 *
	 * Create your own theseo_entry_date() to override in a child theme.
	 *
	 *
	 * @param boolean $echo (optional) Whether to echo the date. Default true.
	 * @return string The HTML-formatted post date.
	 */
	function theseo_entry_date( $echo = true, $item = null ) {
	    global $post, $secretlab;

		$sl_show_post_date = isset($secretlab['show_post_date']) ? $secretlab['show_post_date'] : 1;
		if ($sl_show_post_date == 1) {

	    if (!$item) {
		    $p = $post;
		}
		else {
		    $p = $item;
		}
		if ( has_post_format( array( 'chat', 'status' ), $p->ID ) )
			$format_prefix = _x( '%1$s on %2$s', '1: post format name. 2: date', 'the-seo' );
		else
			$format_prefix = '%2$s';

		$date = sprintf( '<span class="date"><span class="icon icon-calendar2"></span> <a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span> <span class="updated"><i class="fa fa-calendar"></i> '. get_the_modified_time('F jS, Y h:i a').'</span>',
			esc_url( get_permalink($p->ID) ),
			esc_attr( sprintf( esc_html__( 'Permalink to %s', 'the-seo' ), the_title_attribute( 'echo=0' ) ) ),
			esc_attr( get_the_date( 'F j, Y', $p ) ),
			esc_html( sprintf( $format_prefix, get_post_format_string( get_post_format($p->ID) ), get_the_date('F j, Y', $p) ) )
		);

		if ( $echo )
			echo $date;

		return $date;
		}
	}
endif;

// Page meta
if ( ! function_exists( 'theseo_page_entry_meta' ) ) :
	function theseo_page_entry_meta()
	{
		echo '<div class="entry-title">' . get_the_title() . '</div>';
		echo '<span class="updated">'. get_the_modified_time('F jS, Y h:i a').'</span>';
		echo '<span class="author vcard"><span class="fn">'. get_the_author().'</span></span>';
		
	}
endif;

if ( ! function_exists( 'theseo_the_attached_image' ) ) :
	/**
	 * Print the attached image with a link to the next attached image.
	 *

	 */
	function theseo_the_attached_image() {
		/**
		 * Filter the image attachment size to use.
		 *
		 * @since Twenty thirteen 1.0
		 *
		 * @param array $size {
		 *     @type int The attachment height in pixels.
		 *     @type int The attachment width in pixels.
		 * }
		 */
		$attachment_size     = apply_filters( 'theseo_attachment_size', array( 724, 724 ) );
		$next_attachment_url = wp_get_attachment_url();
		$post                = get_post();

		/*
         * Grab the IDs of all the image attachments in a gallery so we can get the URL
         * of the next adjacent image in a gallery, or the first image (if we're
         * looking at the last image in a gallery), or, in a gallery of one, just the
         * link to that image file.
         */
		$attachment_ids = get_posts( array(
			'post_parent'    => $post->post_parent,
			'fields'         => 'ids',
			'numberposts'    => -1,
			'post_status'    => 'inherit',
			'post_type'      => 'attachment',
			'post_mime_type' => 'image',
			'order'          => 'ASC',
			'orderby'        => 'menu_order ID',
		) );

		// If there is more than 1 attachment in a gallery...
		if ( count( $attachment_ids ) > 1 ) {
			foreach ( $attachment_ids as $attachment_id ) {
				if ( $attachment_id == $post->ID ) {
					$next_id = current( $attachment_ids );
					break;
				}
			}

			// get the URL of the next image attachment...
			if ( $next_id )
				$next_attachment_url = get_attachment_link( $next_id );

			// or get the URL of the first image attachment.
			else
				$next_attachment_url = get_attachment_link( reset( $attachment_ids ) );
		}

		printf( '<a href="%1$s" title="%2$s" rel="attachment">%3$s</a>',
			esc_url( wp_get_attachment_url() ),
			the_title_attribute( array( 'echo' => false ) ),
			wp_get_attachment_image( $post->ID, $attachment_size )
		);
	}
endif;

/**
 * Return the post URL.
 *
 * @uses get_url_in_content() to get the URL in the post meta (if it exists) or
 * the first link found in the post content.
 *
 * Falls back to the post permalink if no URL is found in the post.
 *

 *
 * @return string The Link format URL.
 */
function theseo_get_link_url() {
	$content = get_the_content();
	$has_url = get_url_in_content( $content );

	return ( $has_url ) ? $has_url : apply_filters( 'the_permalink', get_permalink() );
}

if ( ! function_exists( 'theseo_excerpt_more' ) && ! is_admin() ) :
	function theseo_excerpt_more( $more ) {
		$link = '';
		return $link;
	}
	add_filter( 'excerpt_more', 'theseo_excerpt_more' );
endif;

/**
 * Filter whether comments are open for a given post type.
 *
 * @param string $status       Default status for the given post type,
 *                             either 'open' or 'closed'.
 * @param string $post_type    Post type. Default is `post`.
 * @param string $comment_type Type of comment. Default is `comment`.
 * @return string (Maybe) filtered default status for the given post type.
 */
function theseo_open_comments_for_pages( $status, $post_type, $comment_type ) {
	if ( 'page' !== $post_type ) {
		return $status;
	}

	// You could be more specific here for different comment types if desired
	return 'open';
}
add_filter( 'get_default_comment_status', 'theseo_open_comments_for_pages', 10, 3 );

/**
 * Extend the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Active widgets in the sidebar to change the layout and spacing.
 * 3. When avatars are disabled in discussion settings.
 *
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function theseo_body_class( $classes ) {
	if ( ! is_multi_author() )
		$classes[] = 'single-author';

	if ( is_active_sidebar( 'sidebar-2' ) && ! is_attachment() && ! is_404() )
		$classes[] = 'sidebar';

	if ( ! get_option( 'show_avatars' ) )
		$classes[] = 'no-avatars';

	return $classes;
}
add_filter( 'body_class', 'theseo_body_class' );

/**
 * Adjust content_width value for video post formats and attachment templates.
 *
 */
if ( ! isset( $content_width ) ) {
	$content_width = 1140;
}
function theseo_content_width() {
	global $content_width;

	if ( is_attachment() )
		$content_width = 724;
	elseif ( has_post_format( 'audio' ) )
		$content_width = 484;
}
add_action( 'template_redirect', 'theseo_content_width' );

// Create the Custom Excerpts callback
function theseo_wp_excerpt($length_callback = '', $more_callback = '')
{
	global $post;
	if (function_exists($length_callback)) {
		add_filter('excerpt_length', $length_callback);
	}
	if (function_exists($more_callback)) {
		remove_filter('excerpt_more', $more_callback);
	}
	$output = get_the_excerpt();
	$output = apply_filters('wptexturize', $output);
	$output = apply_filters('convert_chars', $output);
	$output = '<p>' . $output . '</p>';
	echo esc_attr( $output );
}


// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function theseo_remove_thumbnail_dimensions( $html )
{
	$html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
	return $html;
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function theseo_add_slug_to_body_class($classes)
{
	global $post;
	if (is_home()) {
		$key = array_search('blog', $classes);
		if ($key > -1) {
			unset($classes[$key]);
		}
	} elseif (is_page()) {
		$classes[] = sanitize_html_class($post->post_name);
	} elseif (is_singular()) {
		$classes[] = sanitize_html_class($post->post_name);
	}

	return $classes;
}

/* Color Schemes - Generate CSS */
function theseo_to_row($arr) {

	$keys = array();
	$values = array();
	foreach ($arr as $key=>$val) {
		if (is_array($val)) {
			foreach ($val as $k=>$v) {
				if (!is_array($v)) {
					$keys[] = '/\$'.$key.'_'.$k.'\$/';
					$values[] = $v;
				}
				else {
					foreach ($v as $k1=>$v1) {
						$keys[] = '/\$'.$key.'_'.$k.'_'.$k1.'\$/';
						$values[] = $v1;
					}
				}
			}
		}
		else {
			$keys[] = '/\$'.$key.'\$/';
			$values[] = $val;
		}
	}
	$result = array();
	$result['keys'] = $keys; ksort($result['keys']);
	$result['values'] = $values; ksort($result['values']);

	return $result;
}
add_action ('redux/options/' . $opt_name . '/settings/change', 'theseo_change_action', 10, 3);

/* Generate CSS by Template*/
function theseo_change_action($opts) {
	global $wp_filesystem, $secretlab;
	
	$sl_design_css = isset($secretlab['design-css']) ? $secretlab['design-css'] : 1;
	if ( $sl_design_css == 1 ) {
		$template_css = get_template_directory() . '/css/seoagency.css';
	}
	if ( $sl_design_css == 2 ) {
		$template_css = get_template_directory() . '/css/seoagency.css';
	}
	if ( $sl_design_css == 3 ) {
		$template_css = get_template_directory() . '/css/digital.css';
	}
	if ( $sl_design_css == 4 ) {
		$template_css = get_template_directory() . '/css/digitaldark.css';
	}
	if ( $sl_design_css == 5 ) {
		$template_css = get_template_directory() . '/css/flat.css';
	}
	
    if( empty( $wp_filesystem ) ) {
        require_once ( ABSPATH .'/wp-admin/includes/file.php' );
        WP_Filesystem();
    }
	
    if( $wp_filesystem ) {
	    $css = get_template_directory() . '/css/dynamic.css';
	    $content = $wp_filesystem->get_contents($template_css); 	
	    $opts = theseo_to_row($opts);
	    $content = preg_replace($opts['keys'], $opts['values'], $content);
	    $wp_filesystem->put_contents($css, $content, FS_CHMOD_FILE); 
	}
}


function theseo_remove_script_version( $src ){
	$parts = explode( '?ver', $src );
	return $parts[0];
}
add_filter( 'script_loader_src', 'theseo_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', 'theseo_remove_script_version', 15, 1 );
// Admin Panel Features
function theseo_get_featured_image($post_ID) {
	$post_thumbnail_id = get_post_thumbnail_id($post_ID);
	if ($post_thumbnail_id) {
		$post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'theseo_featured_preview');
		return $post_thumbnail_img[0];
	}
}
// ADD NEW COLUMN with featured image into posts list
function theseo_columns_head($defaults) {
	$defaults['featured_image'] = esc_html__('Featured Image', 'the-seo');
	return $defaults;
}

// SHOW THE FEATURED IMAGE in posts list
function theseo_columns_content($column_name, $post_ID) {
	if ($column_name == 'featured_image') {
		$post_featured_image = theseo_get_featured_image($post_ID);
		if ($post_featured_image) {
			echo '<img src="' . $post_featured_image . '" />';
		} else {
			echo '<img src="'.esc_url(get_template_directory_uri()).'/images/not55.jpg" alt="" />';
		}
	}
}
add_filter('manage_posts_columns', 'theseo_columns_head');
add_action('manage_posts_custom_column', 'theseo_columns_content', 10, 2);



function secretlab_option( $id, $fallback = false, $param = false ) {
	global $secretlab;
	if (!isset($secretlab)) {
	    $s = get_option('secretlab');
	}
	else $s = $secretlab;
	if ( $fallback == false ) $fallback = '';
	$output = ( isset($s[$id]) && $s[$id] !== '' ) ? $s[$id] : $fallback;
	if ( !empty($s[$id]) && $param ) {
		$output = ( isset($s[$id][$param]) && $s[$id][$param] !== '' ) ? $s[$id][$param] : $fallback;
	}
	return $output;
}

add_filter('wp_redirect', 'disable_redux_welcome');

function disable_redux_welcome($path) {
    if (preg_match('/redux.about|vc.welcome/', $path)) {
	    $path = admin_url('themes.php?page=secretlab-welcome' );
	}
	
	return $path;
}



/* Admin page for Export/Import of Suppamenu Skins */
class backup_restore_theme_options {

	function __construct() {
		add_action('admin_menu', array(&$this, 'admin_menu'));
	}
	function admin_menu() {
		// add_submenu_page($parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function);
		// $page = add_submenu_page('themes.php', 'Backup Options', 'Backup Options', 'manage_options', 'backup-options', array(&$this, 'options_page'));

		// add_theme_page($page_title, $menu_title, $capability, $menu_slug, $function);
		$page = add_theme_page('Suppamenu Skins', 'Suppamenu Skins', 'manage_options', 'backup-options', array(&$this, 'suppa_options_page'));

		add_action("load-{$page}", array(&$this, 'import_export'));
	}
	function import_export() {
	
		include_once ABSPATH.'/wp-admin/includes/file.php';
	    WP_Filesystem();
		
		global $wp_filesystem;	
	
		if (isset($_GET['action']) && ($_GET['action'] == 'download')) {
			header("Cache-Control: public, must-revalidate");
			header("Pragma: hack");
			header("Content-Type: text/plain");
			header('Content-Disposition: attachment; filename="suppamenu-skins'.date("dMy").'.dat"');
			echo serialize($this->_get_options());
			die();
		}
		if (isset($_POST['upload']) && check_admin_referer('shapeSpace_restoreOptions', 'shapeSpace_restoreOptions')) {
			if ($_FILES["file"]["error"] > 0) {
				// error
			} else {
				$options = unserialize($wp_filesystem->get_contents($_FILES["file"]["tmp_name"]));
				if ($options) {
					foreach ($options as $option) {
						update_option($option->option_name, unserialize($option->option_value));
					}
				}
			}
			/* import suppa css */
			if (preg_match('/light/', $_FILES["file"]["name"])) { $css_file = 'menu_css_seolight.zip'; $js_file = 'menu_js_seolight.zip'; }
			else if (preg_match('/dark/', $_FILES["file"]["name"])) { $css_file = 'menu_css_seodark.zip'; $js_file = 'menu_js_seodark.zip'; }
			else { $css_file = 'menu_css_digital.zip'; $js_file = 'menu_js_digital.zip'; }
			if (file_exists(get_template_directory() . '/import/menu_css/' . $css_file)) {
			    $upload_dir = wp_upload_dir();
				$upload_dir = preg_replace('/uploads\/[0-9]+\/[0-9]+$/', 'uploads', $upload_dir['path']);
			    if (is_dir($upload_dir . '/suppamenu2/css')) {
				    $r = unzip_file( get_template_directory() . '/import/menu_css/' . $css_file, $upload_dir . '/suppamenu2/css' );
					$r = unzip_file( get_template_directory() . '/import/menu_css/' . $js_file, $upload_dir . '/suppamenu2/js' );
				}
			}
			
			/* ------------------ */
			
			wp_redirect(admin_url('themes.php?page=backup-options'));
			exit;
		}
	}
	function suppa_options_page() { ?>

		<div class="wrap">
			<h2>Backup/Restore Suppamenu Options</h2>
			<form action="" method="POST" enctype="multipart/form-data">
				<style>#backup-options td { display: block; margin-bottom: 20px; }</style>
				<table id="backup-options">
					<tr>
						<td>
							<h3>Backup/Export</h3>
							<p>Here are the stored settings for Suppamenu's skins:</p>
							<p><textarea class="widefat code" rows="20" cols="100" onclick="this.select()"><?php echo serialize($this->_get_options()); ?></textarea></p>
							<p><a href="?page=backup-options&action=download" class="button-secondary">Download as file</a></p>
						</td>
						<td>
							<h3>Restore/Import</h3>
							<p><label class="description" for="upload">Restore a previous backup</label></p>
							<p><input type="file" name="file" /> <input type="submit" name="upload" id="upload" class="button-primary" value="Upload file" /></p>
							<?php if (function_exists('wp_nonce_field')) wp_nonce_field('shapeSpace_restoreOptions', 'shapeSpace_restoreOptions'); ?>
						</td>
					</tr>
				</table>
			</form>
		</div>

	<?php }
	function suppa_display_options() {
		$options = unserialize($this->_get_options());
	}
	function _get_options() {
		global $wpdb;
		
		return $wpdb->get_results("SELECT option_name, option_value FROM {$wpdb->options} WHERE option_name LIKE '%suppa%' OR option_name='theme_mods_the-seo'"); 
	}
}
new backup_restore_theme_options();

function theseo_set_menu_to_locations() {
    if ( isset ( $_POST['theme_type'] ) ) {
	    if ( $_POST['theme_type'] == 'light' ) $menu_slug = 'headermenu'; else $menu_slug = 'headermenu2';
	}
	else $menu_slug = 'headermenu';
    $native_menu = wp_get_nav_menu_object($menu_slug);
	$locations = array('header1and5', 'header2', 'header3', 'header4');
	$menu_set = array();
	foreach ($locations as $location) {
		    $menu_set[$location] = $native_menu->term_id;
	}
	if (count($menu_set) > 0) {
	    set_theme_mod('nav_menu_locations', $menu_set);
	}
	
	$homepage = get_page_by_title( 'Home Seo Agency' );
	if (($homepage && $homepage->ID)) {
	    update_option('show_on_front', 'page');
        if ($homepage && $homepage->ID) {
            update_option('page_on_front', $homepage->ID); // Front Page
        }		
	}	
}

add_action('import_end', 'theseo_set_menu_to_locations');

/* Setup Global Variables*/
function theseo_set_globals() {
    global $secretlab, $theseo_layout, $post;
	
$plugins = get_option('active_plugins');
if (!in_array('revslider/revslider.php', $plugins) && !in_array('LayerSlider/layerslider.php', $plugins)) $secretlab['is_active_slider_plugins'] = false; else $secretlab['is_active_slider_plugins'] = true;	

if (is_singular()) {
	$theseo_layout = json_decode(get_post_meta($post->ID, 'layout_settings', true), true);
	if (!$theseo_layout) $theseo_layout = $secretlab;
	if ( in_category( 'blog' ) || theseo_post_is_in_descendant_category(get_term_by( 'name', 'blog', 'category' ) )) {
		$secretlab['theseo_page_type'] = 'blog';
		$secretlab['theseo_design_layout'] = 'blog-layout';
		$secretlab['theseo_pagetype_prefix'] = 'blog-';
		theseo_check_pagetype($secretlab['theseo_pagetype_prefix']);
	}
	else if (function_exists('is_woocommerce') && is_woocommerce()) {
		$secretlab['theseo_page_type'] = 'shop';
		$secretlab['theseo_design_layout'] = 'design-layout';
		$secretlab['theseo_pagetype_prefix'] = 'shop-';
		theseo_check_pagetype($secretlab['theseo_pagetype_prefix']);
	}
	else {
		$secretlab['theseo_page_type'] = '';
		$secretlab['theseo_design_layout'] = 'design-layout';
		$secretlab['theseo_pagetype_prefix'] = '';
	}
}
else if (function_exists('is_woocommerce') && is_woocommerce()) {
	$theseo_layout = $secretlab;
	$secretlab['theseo_page_type'] = 'shop';
	$secretlab['theseo_design_layout'] = 'design-layout';
	$secretlab['theseo_pagetype_prefix'] = 'shop-';
	theseo_check_pagetype($secretlab['theseo_pagetype_prefix']);
}
else if (is_category()) {
	$theseo_layout = $secretlab;
	$secretlab['theseo_page_type'] = 'blog';
	$secretlab['theseo_design_layout'] = 'blog-layout';
	$secretlab['theseo_pagetype_prefix'] = 'blog-';
}
else {
	$theseo_layout = $secretlab;
	$secretlab['theseo_page_type'] = '';
	$secretlab['theseo_design_layout'] = 'design-layout';
	$secretlab['theseo_pagetype_prefix'] = '';
}

}
/* TOpbar Social network links */
if ( ! function_exists( 'theseo_get_active_social_links' ) ) {
    function theseo_get_active_social_links($args = array('container_class' => 'social_container', 'item_class' => 'social_class'))
    {
        global $secretlab;

        if (!isset($args['container_class'])) {
            $ul_class = '';
        } else {
            $ul_class = ' class="' . $args['container_class'] . '"';
        }
        if (!isset($args['item_class'])) {
            $li_class = '';
        } else {
            $li_class = ' class="' . $args['item_class'] . '"';
        }

        $sl_header_social_buttons = $secretlab['header-social-buttons'];
        if ($sl_header_social_buttons == true) {
            echo '<ul' . $ul_class . '>';
            if (isset ($secretlab['social_link_facebook'])) {
                $sl_fb = $secretlab['social_link_facebook'];
                if (!empty($sl_fb)) {
                    echo '<li><a href="' . esc_url($sl_fb) . '" target="_blank"><i class="icon-facebook"></i></a></li>';
                }
            }
            if (isset ($secretlab['social_link_twitter'])) {
                $sl_tw = $secretlab['social_link_twitter'];
                if (!empty($sl_tw)) {
                    echo '<li><a href="' . esc_url($sl_tw) . '" target="_blank"><i class="icon-twitter"></i></a></li>';
                }
            }
            if (isset ($secretlab['social_link_myspace'])) {
                $sl_myspace = $secretlab['social_link_myspace'];
                if (!empty($sl_myspace)) {
                    echo '<li><a href="' . esc_url($sl_myspace) . '" target="_blank"><i class="icon-users"></i></a></li>';
                }
            }
            if (isset ($secretlab['social_link_linkedin'])) {
                $sl_linkedin = $secretlab['social_link_linkedin'];
                if (!empty($sl_linkedin)) {
                    echo '<li><a href="' . esc_url($sl_linkedin) . '" target="_blank"><i class="icon-linkedin"></i></a></li>';
                }
            }
            if (isset ($secretlab['social_link_google'])) {
                $sl_google = $secretlab['social_link_google'];
                if (!empty($sl_google)) {
                    echo '<li><a href="' . esc_url($sl_google) . '" target="_blank"><i class="icon-google-plus"></i></a></li>';
                }
            }
            if (isset ($secretlab['social_link_tumblr'])) {
                $sl_tumblr = $secretlab['social_link_tumblr'];
                if (!empty($sl_tumblr)) {
                    echo '<li><a href="' . esc_url($sl_tumblr) . '" target="_blank"><i class="icon-tumblr"></i></a></li>';
                }
            }
            if (isset ($secretlab['social_link_pinterest'])) {
                $sl_pinterest = $secretlab['social_link_pinterest'];
                if (!empty($sl_pinterest)) {
                    echo '<li><a href="' . esc_url($sl_pinterest) . '" target="_blank"><i class="icon-pinterest-p"></i></a></li>';
                }
            }
            if (isset ($secretlab['social_link_youtube'])) {
                $sl_youtube = $secretlab['social_link_youtube'];
                if (!empty($sl_youtube)) {
                    echo '<li><a href="' . esc_url($sl_youtube) . '" target="_blank"><i class="icon-youtube-play"></i></a></li>';
                }
            }
            if (isset ($secretlab['social_link_instagram'])) {
                $sl_instagram = $secretlab['social_link_instagram'];
                if (!empty($sl_instagram)) {
                    echo '<li><a href="' . esc_url($sl_instagram) . '" target="_blank"><i class="icon-instagram"></i></a></li>';
                }
            }
            if (isset ($secretlab['social_link_vkcom'])) {
                $sl_vkcom = $secretlab['social_link_vkcom'];
                if (!empty($sl_vkcom)) {
                    echo '<li><a href="' . esc_url($sl_vkcom) . '" target="_blank"><i class="icon-vk"></i></a></li>';
                }
            }
            if (isset ($secretlab['social_link_reddit'])) {
                $sl_reddit = $secretlab['social_link_reddit'];
                if (!empty($sl_reddit)) {
                    echo '<li><a href="' . esc_url($sl_reddit) . '" target="_blank"><i class="icon-reddit"></i></a></li>';
                }
            }
            if (isset ($secretlab['social_link_blogger'])) {
                $sl_blogger = $secretlab['social_link_blogger'];
                if (!empty($sl_blogger)) {
                    echo '<li><a href="' . esc_url($sl_blogger) . '" target="_blank"><span class="icon icon-blogger"></span></a></li>';
                }
            }
            if (isset ($secretlab['social_link_wordpress'])) {
                $sl_wordpress = $secretlab['social_link_wordpress'];
                if (!empty($sl_wordpress)) {
                    echo '<li><a href="' . esc_url($sl_wordpress) . '" target="_blank"><i class="icon-wordpress"></i></a></li>';
                }
            }
            if (isset ($secretlab['social_link_behance'])) {
                $sl_behance = $secretlab['social_link_behance'];
                if (!empty($sl_behance)) {
                    echo '<li><a href="' . esc_url($sl_behance) . '" target="_blank"><i class="icon-behance"></i></a></li>';
                }
            }

            echo '</ul>';
        }
    }
}
/* Faicon and Apple touch icon load options */
function theseo_set_site_icons() {

    global $secretlab, $theseo_layout;
	if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
		if (isset ($secretlab['favicon'])) {
			$sl_favicon = $secretlab['favicon']['url'];
			if (!empty($sl_favicon)) {
				echo '<link href="' . esc_url($sl_favicon) . '" rel="shortcut icon">';
			}
		}
	}

	if (isset ($secretlab['apple-touch-icon'])) {
		$sl_ati = $secretlab['apple-touch-icon']['url'];
		if (!empty($sl_ati)) {
			echo '<link href="' . esc_url($sl_ati) . '" rel="apple-touch-icon">';
		}
	}
}

/*  Set website background */
function theseo_set_header_background() {
    
	global $secretlab, $theseo_layout;
	$sl_design_layout = isset($theseo_layout[$secretlab['theseo_design_layout']]) ? $theseo_layout[$secretlab['theseo_design_layout']] : 1;
	if ( $sl_design_layout == 2 ) {
        if (!empty($theseo_layout[$secretlab['theseo_pagetype_prefix'].'boxed-background-color'])) {
		    if (!is_array($theseo_layout[$secretlab['theseo_pagetype_prefix'].'boxed-background-color'])) {
		        $bg_color = $theseo_layout[$secretlab['theseo_pagetype_prefix'].'boxed-background-color'];
			}
			else if  (is_array($theseo_layout[$secretlab['theseo_pagetype_prefix'].'boxed-background-color'])) {
			    $bg_color = $theseo_layout[$secretlab['theseo_pagetype_prefix'].'boxed-background-color']['color'];
			} 
		}
		else {
		    $bg_color = 'transparent';
		}
		if (isset($theseo_layout[$secretlab['theseo_pagetype_prefix'].'boxed-background'])) {
		    if (! empty($theseo_layout[$secretlab['theseo_pagetype_prefix'].'boxed-background']) && !is_array($theseo_layout[$secretlab['theseo_pagetype_prefix'].'boxed-background'])) { $src = json_decode($theseo_layout[$secretlab['theseo_pagetype_prefix'].'boxed-background'], true); } 
			else { $src = $theseo_layout[$secretlab['theseo_pagetype_prefix'].'boxed-background']; }
			if ($src['url'] != 'none' && $src['url'] != '') {
			    $bg_image = 'background-image : url("'.$src['url'].'") !important; }';
			}
			else $bg_image = '';
		}
		else {
		    $bg_image = '';
		}
		echo '<style type="text/css">.mainbgr { 
                background-color : '.$bg_color.' !important;'.			           
			    $bg_image.
			  '</style>';
    }

	// Custom CSS and JS
	if (isset ($secretlab['header-nested'])) {
		if ($secretlab['header-nested'] == 1) {
			if (isset ($secretlab['header-nested-ace-js'])) {
				if (strlen($secretlab['header-nested-ace-js']) > 0 && $secretlab['header-nested-ace-js'] != 'function hello() { alert ("HELLO"); }') {
					echo '<script type="text/javascript">' . $secretlab['header-nested-ace-js'] . '</script>';
				}
			}
		}
	}
	if (isset ($secretlab['header-nested'])) {
		if ($secretlab['header-nested'] == 1) {
			if (isset ($secretlab['header-nested-ace-css'])) {
				if (strlen($secretlab['header-nested-ace-css']) > 0 && $secretlab['header-nested-ace-css'] != 'body { margin : 0; padding : 0; }') {
					echo '<style type="text/css">' . $secretlab['header-nested-ace-css'] . '</style>';
				}
			}
		}
	}
}

/* Designs changer */
if ( ! function_exists( 'theseo_set_css_layout' ) ) {
	function theseo_set_css_layout()
	{

		global $secretlab, $theseo_layout;
		if (isset ($theseo_layout[$secretlab['theseo_design_layout']])) {
			$sl_design_layout = $theseo_layout[$secretlab['theseo_design_layout']];
			if ($sl_design_layout == 2) {
				echo '<div class="mainbgr"><div class="mainbox">';
			}
		}

		$sl_header_layout = isset($secretlab['header-layout']) ? $secretlab['header-layout'] : 1;

		get_template_part('header' . $sl_header_layout);

		$cls = ' class="seo' . $secretlab['theseo_page_type'] . '"';

		echo '<main' . $cls . '>';

		$sl_design_layout = isset($theseo_layout[$secretlab['theseo_design_layout']]) ? $theseo_layout[$secretlab['theseo_design_layout']] : 1;
		if ($sl_design_layout != 2) {

			if (isset ($secretlab['design-css'])) {
				$sl_design_css = $secretlab['design-css'];
				if (isset ($secretlab['network-image'])) {
					if ($secretlab['network-image'] == 1) {
						if ($sl_design_css == 1) {
							echo '<img src="' . esc_url(get_template_directory_uri()) . '/images/network.png" alt="" id="network" />';
						}

						if ($sl_design_css == 2) {
							echo '<img src="' . esc_url(get_template_directory_uri()) . '/images/network-dark.png" alt="" id="network" />';
						}
					}
				}
			}
		}

	}
}
/* Custom background function for DEMO header 1-4 templates */
function theseo_set_demos_bgrs() {
	global $secretlab, $theseo_layout;
	if (isset ($theseo_layout[$secretlab['theseo_design_layout']])) {
		$sl_design_layout = $theseo_layout[$secretlab['theseo_design_layout']];
		if ($sl_design_layout == 2) {
			echo '<div class="mainbgr"><div class="mainbox">';
		}
	}
}
if ( ! function_exists( 'theseo_set_demos_layout' ) ) {
	function theseo_set_demos_layout()
	{

		global $secretlab, $theseo_layout;

		$cls = ' class="seo' . $secretlab['theseo_page_type'] . '"';

		echo '<main role="main"' . $cls . '>';

		$sl_design_layout = isset($theseo_layout[$secretlab['theseo_design_layout']]) ? $theseo_layout[$secretlab['theseo_design_layout']] : 1;
		if ($sl_design_layout != 2) {

			if (isset ($secretlab['design-css'])) {
				$sl_design_css = $secretlab['design-css'];
				if (isset ($secretlab['network-image'])) {
					if ($secretlab['network-image'] == 1) {
						if ($sl_design_css == 1) {
							echo '<img src="' . esc_url(get_template_directory_uri()) . '/images/network.png" alt="" id="network" />';
						}

						if ($sl_design_css == 2) {
							echo '<img src="' . esc_url(get_template_directory_uri()) . '/images/network-dark.png" alt="" id="network" />';
						}
					}
				}
			}

		}

	}
}

function theseo_set_header_sidebar_layout() {
	global $secretlab, $theseo_layout;
	$sl_sidebar_layout = isset($theseo_layout[$secretlab['theseo_pagetype_prefix'] . 'sidebar-layout']) ? $theseo_layout[$secretlab['theseo_pagetype_prefix'] . 'sidebar-layout'] : 1;
	if ($sl_sidebar_layout == 2 or $sl_sidebar_layout == 3) {
		echo '<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 widget-area">';
		if (isset($theseo_layout[$secretlab['theseo_page_type'] . '_' . 'left_sidebar_widgets'])) {
			dynamic_sidebar($theseo_layout[$secretlab['theseo_page_type'] . '_left_sidebar_widgets']);
		}
		else if (isset($theseo_layout['left_sidebar_widgets'])) {
			dynamic_sidebar($theseo_layout['left_sidebar_widgets']);
		}		
		else {
			dynamic_sidebar($secretlab['theseo_page_type'] . '_default_left_sidebar');
		}
		echo '</div>';
	}
	if ($sl_sidebar_layout == 1) {
		echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main">';
	}
	if ($sl_sidebar_layout == 2) {
		echo '<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 paddingright paddingleft main blogsidebarspage">';
	}
	if ($sl_sidebar_layout == 3) {
		echo '<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 paddingleft main">';
	}
	if ($sl_sidebar_layout == 4) {
		echo '<div class="col-lg-9 col-md-9 col-sm-6 col-xs-12 paddingright main">';
	}
}

function theseo_set_sticky_menu_id() {

global $secretlab, $theseo_layout;
	if (isset($secretlab['sticky-menu'])) {
		$sl_sticky_menu = $secretlab['sticky-menu'];
		if ($sl_sticky_menu == true) {
			echo 'id="stickymenu"';
		}
	}
}

function theseo_set_default_header_image() {

	global $secretlab;

	if (!$secretlab['is_active_slider_plugins']) {
		echo '<div class="theseo_saveheader">';
		if (isset ($secretlab['apple-touch-icon'])) {
			$sl_ati = $secretlab['apple-touch-icon']['url'];
			if (!empty($sl_ati)) {
				echo '<img src="' . esc_url($sl_ati) . '" alt="The SEO">';
			}
		}
		echo '</div>';
	}
}

if ( ! function_exists( 'theseo_set_customized_slider' ) ) {
	function theseo_set_customized_slider() {

		global $post, $secretlab, $theseo_layout;

		if ( is_singular() ) {
			$settings = json_decode(get_post_meta($post->ID, 'layout_settings', true), true);
			if ( isset( $settings[$secretlab['theseo_pagetype_prefix'] . 'header_type'] ) && $settings[$secretlab['theseo_pagetype_prefix'] . 'header_type'] == 2 ) {
				if ( $settings && isset( $settings[$secretlab['theseo_pagetype_prefix'] . 'header_image'] ) && strlen( $settings[$secretlab['theseo_pagetype_prefix'] . 'header_image'] ) > 4 ) {
					echo '<div class="header-transparent"><div class="header-image"><img src="' . $settings[$secretlab['theseo_pagetype_prefix'] . 'header_image'] . '"></div></div>';
					return;
				}
			}
			else {
				theseo_set_default_header_image();
				if (isset ($theseo_layout[$secretlab['theseo_pagetype_prefix'] . 'header14_slider'])) {
					$sl_headerslider = $theseo_layout[$secretlab['theseo_pagetype_prefix'] . 'header14_slider'];
					if (!empty($sl_headerslider)) {
						echo '<div class="header-transparent"><div class="container-fluid">';
						theseo_get_customized_slider();
						echo '</div></div>';
					}
				}
			}
		}
		else {
			theseo_set_default_header_image();
			if (isset ($theseo_layout[$secretlab['theseo_pagetype_prefix'] . 'header14_slider'])) {
				$sl_headerslider = $theseo_layout[$secretlab['theseo_pagetype_prefix'] . 'header14_slider'];
				if (!empty($sl_headerslider)) {
					echo '<div class="header-transparent"><div class="container-fluid">';
					theseo_get_customized_slider();
					echo '</div></div>';
				}
			}
		}
	}
}

if ( ! function_exists( 'theseo_set_header1_content' ) ) {
	function theseo_set_header1_content() {

		global $secretlab, $theseo_layout;
		if (isset($secretlab['header-topbar'])) {
			$sl_header_topbar = $secretlab['header-topbar'];
			if ($sl_header_topbar == 1) {
				echo '<div class="topbartransparent">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pl0">';
				$sl_header_social_buttons = $secretlab['header-social-buttons'];
				if ($sl_header_social_buttons == true) {
					echo theseo_get_active_social_links(array('container_class' => 'socialbartransparent'));
				}
				echo '</div>
						<div class="contacttb">';
				if (isset ($secretlab['email'])) {
					$sl_email = is_array( $secretlab['email'] ) ? $secretlab['email'][0] : $secretlab['email'];
					if (!empty($sl_email)) {
						echo '<div><i class="icon-envelope"></i> <a href="mailto:'.esc_attr($sl_email).'">' . esc_attr($sl_email).'</a></div>';
					}
				}
				echo '';
				if (isset ($secretlab['phone'])) {
					$sl_phone = is_array( $secretlab['phone'] ) ? $secretlab['phone'][0] : $secretlab['phone'];
					if (!empty($sl_phone)) {
						echo '<div><i class="icon-phone22"></i> <a href="tel:' . esc_attr($sl_phone) . '">' . esc_attr($sl_phone) . '</a></div>';
					}
				}
				echo '</div>
			
		</div>';
			}
		}
	}
}

if ( ! function_exists( 'theseo_set_header2_content' ) ) {
	function theseo_set_header2_content() {
		global $secretlab, $theseo_layout;

		$sl_header_topbar = $secretlab['header-topbar'];
		if ($sl_header_topbar == 1) {
			echo '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">';
			$sl_header_social_buttons = $secretlab['header-social-buttons'];
			if ($sl_header_social_buttons == true) {
				echo theseo_get_active_social_links(array('container_class' => 'socialbartransparent'));
			}
			echo '</div>
                <div class="contacttb">';
			if (isset ($secretlab['email'])) {
                $sl_email = is_array( $secretlab['email'] ) ? $secretlab['email'][0] : $secretlab['email'];
				if (!empty($sl_email)) {
					echo '<div><i class="icon-envelope"></i> <a href="mailto:'.esc_attr($sl_email).'">' . esc_attr($sl_email).'</a></div>';
				}
			}

			echo '';
			if (isset ($secretlab['phone'])) {
				$sl_phone = is_array( $secretlab['phone'] ) ? $secretlab['phone'][0] : $secretlab['phone'];
				if (!empty($sl_phone)) {
					echo '<div><i class="icon-phone22"></i> <a href="tel:' . esc_attr($sl_phone) . '">' . esc_attr($sl_phone) . '</a></div>';
				}
			}
			echo '</div>';
		}
	}
}

if ( ! function_exists( 'theseo_set_header3_content' ) ) {
	function theseo_set_header3_content()
	{

		global $secretlab, $theseo_layout;

		$sl_header_topbar = $secretlab['header-topbar'];
		if ($sl_header_topbar == 1) {
			echo '<div class="topbargreen">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">';
			$sl_header_social_buttons = $secretlab['header-social-buttons'];
			if ($sl_header_social_buttons == true) {
				echo theseo_get_active_social_links(array('container_class' => 'socialbartransparent'));
			}
			echo '</div>
                <div class="contacttb">';
			if (isset ($secretlab['email'])) {
                $sl_email = is_array( $secretlab['email'] ) ? $secretlab['email'][0] : $secretlab['email'];
				if (!empty($sl_email)) {
					echo '<div><i class="icon-envelope"></i> <a href="mailto:'.esc_attr($sl_email).'">' . esc_attr($sl_email).'</a></div>';
				}
			}

			echo '';
			if (isset ($secretlab['phone'])) {
				$sl_phone = is_array( $secretlab['phone'] ) ? $secretlab['phone'][0] : $secretlab['phone'];
				if (!empty($sl_phone)) {
					echo '<div><i class="icon-phone22"></i> <a href="tel:' . esc_attr($sl_phone) . '">' . esc_attr($sl_phone) . '</a></div>';
				}
			}
			echo '</div>
				</div>
            </div>
        </div>
    </div>
</div>';
		}
	}
}

if ( ! function_exists( 'theseo_set_header4_content' ) ) {
	function theseo_set_header4_content()
	{

		global $secretlab, $theseo_layout;

		$sl_header_topbar = $secretlab['header-topbar'];
		if ($sl_header_topbar == 1) {
			echo '<div class="topbargreen">
<div class="container-fluid">
        <div class="row">
            <div class="container"><div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">';
			$sl_header_social_buttons = $secretlab['header-social-buttons'];
			if ($sl_header_social_buttons == true) {
				echo theseo_get_active_social_links(array('container_class' => 'socialbartransparent'));
			}
			echo '</div>
                <div class="contacttb">';
			if (isset ($secretlab['email'])) {
                $sl_email = is_array( $secretlab['email'] ) ? $secretlab['email'][0] : $secretlab['email'];
				if (!empty($sl_email)) {
					echo '<div><span class="icon-envelope3"></span> <a href="mailto:'.esc_attr($sl_email).'">' . esc_attr($sl_email).'</a></div>';
				}
			}

			echo '';
			if (isset ($secretlab['phone'])) {
				$sl_phone = is_array( $secretlab['phone'] ) ? $secretlab['phone'][0] : $secretlab['phone'];
				if (!empty($sl_phone)) {
					echo '<div><span class="icon-phone5"></span> <a href="tel:' . esc_attr($sl_phone) . '">' . esc_attr($sl_phone) . '</a></div>';
				}
			}
			echo '</div>
            </div></div>
        </div>
        </div>
</div>';
		}
	}
}

function theseo_set_footer_sidebar_layout() {

	global $secretlab, $theseo_layout;

	$sl_sidebar_layout = isset($theseo_layout[$secretlab['theseo_pagetype_prefix'] . 'sidebar-layout']) ? $theseo_layout[$secretlab['theseo_pagetype_prefix'] . 'sidebar-layout'] : 1;
	if ($sl_sidebar_layout == 2 or $sl_sidebar_layout == 4) {
		echo '<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 widget-area">';
		if (isset($theseo_layout[$secretlab['theseo_page_type'] . '_' . 'right_sidebar_widgets'])) {
			dynamic_sidebar($theseo_layout[$secretlab['theseo_page_type'] . '_right_sidebar_widgets']);
		}
		else if (isset($theseo_layout['right_sidebar_widgets'])) {
		    dynamic_sidebar($theseo_layout['right_sidebar_widgets']);
		}
		else {
			dynamic_sidebar($secretlab['theseo_page_type'] . '_default_right_sidebar');
		}
		echo '</div>';
	}

}

// Dispay address
function theseo_footer_address() {
	global $secretlab;
	if (isset ($secretlab['address'])) {
		function theseo_get_owner_addr() {
			global $secretlab;
			$address = implode('<br><i class="icon-map-marker"></i> ', $secretlab['address']);
			return $address;
		}
		if (!empty($secretlab['address'])) {
			echo '<i class="icon-map-marker"></i> '.theseo_get_owner_addr();
		}
	}
}
// Dispay address for digital
function theseo_footer_address_digital() {
	global $secretlab;
	if (isset ($secretlab['address'])) {
		function theseo_get_owner_addr_d() {
			global $secretlab;
			$address = implode('<br><i class="icon-map"></i> ', $secretlab['address']);
			return $address;
		}
		if (!empty($secretlab['address'])) {
			echo '<i class="icon-map"></i> '.theseo_get_owner_addr_d();
		}
	}
}

if ( ! function_exists( 'theseo_set_footer_content' ) ) {
	function theseo_set_footer_content() {

		global $secretlab, $theseo_layout;
		// CTA Section
		theseo_footer_cta();
		
		// Choose Footer Section START
		// SEO and Digital design footer
		theseo_footer_seolightn_dark();
		theseo_footer_digital();
		//Customized footer
		$sell_foot_choose = isset($secretlab['footer-type-layout']) ? $secretlab['footer-type-layout'] : 1;
		if ($sell_foot_choose == 2) {
			dynamic_sidebar('footer_customized');
		}
		// Choose Footer Section END
		$sl_design_css = isset($secretlab['design-css']) ? $secretlab['design-css'] : 1;
		if ($sl_design_css == 1 or $sl_design_css == 2 or $sl_design_css == 5) {
			if (isset ($secretlab['scroll-to-top'])) {
				if ($secretlab['scroll-to-top'] == 1) {
					echo '<a href="#" id="scroller"><span class="icon icon-arrow-up3"></span></a>';
				}
			}
		}

		if (isset ($theseo_layout[$secretlab['theseo_design_layout']])) {
			$sl_design_layout = $theseo_layout[$secretlab['theseo_design_layout']];
			if ($sl_design_layout == 2) {
				echo '</div></div>';
			}
		}
		if (isset ($secretlab['footer-nested'])) {
			if ($secretlab['footer-nested'] == 1) {
				if (isset ($secretlab['footer-nested-ace-js'])) {
					if (strlen($secretlab['footer-nested-ace-js']) > 0 && $secretlab['footer-nested-ace-js'] != 'function hello() { alert ("HELLO"); }') {
						echo '<script type="text/javascript">' . $secretlab['footer-nested-ace-js'] . '</script>';
					}
				}
			}
		}
		if (isset ($secretlab['footer-nested'])) {
			if ($secretlab['footer-nested'] == 1) {
				if (isset ($secretlab['footer-nested-ace-css'])) {
					if (strlen($secretlab['footer-nested-ace-css']) > 0 && $secretlab['footer-nested-ace-css'] != 'body { margin : 0; padding : 0; }') {
						echo '<style type="text/css">' . $secretlab['footer-nested-ace-css'] . '</style>';
					}
				}
			}
		}

	}
}

/* set class for layout with one sidebar*/
function theseo_set_custom_posttype_sidebar_layout() {
    global $secretlab, $theseo_layout;
	if (isset($secretlab['sidebar-layout'])) {
		$sl_sidebar_layout = $secretlab['sidebar-layout'];
		if ($sl_sidebar_layout == 1) {
			echo ' onecolumnnsb';
		}
	}
}
/* Tags Display */
function theseo_show_tags() {
	global $secretlab;
	if (isset($secretlab['show_post_tags'])) {
		$sl_show_post_tags = $secretlab['show_post_tags'];
		if ($sl_show_post_tags == 1) {
			$tag_list = get_the_tag_list('', esc_html__(', ', 'the-seo'));
			if ($tag_list) {
				echo '<span class="tags-links"><b>' . esc_html__('Tags', 'the-seo') . ':</b> ' . $tag_list . '</span>';
			}
		}
	} else {
		$tag_list = get_the_tag_list('', ' ');
		if ($tag_list) {
			echo '<span class="tags-links"><b>' . esc_html__('Tags', 'the-seo') . ':</b> ' . $tag_list . '</span>';
		}
	}
}

function theseo_show_postmore() {
	global $secretlab;
	if (isset($secretlab['is_related_posts'])) {
		$sl_related_post = $secretlab['is_related_posts'];
		if ($sl_related_post == 1) {
			theseo_postmore_query();
		}
	}
}

function theseo_postmore_query() {
	global $post, $secretlab;
	$backup = $post;
	$tags = wp_get_post_tags($post->ID);
	if ($tags) {
		$tag_ids = array();
		foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;

		$args=array(
			'tag__in' => $tag_ids,
			'post__not_in' => array($post->ID),
			'showposts'=>3, // Number of related posts that will be shown.
		);
		$my_query = new wp_query($args);
		if( $my_query->have_posts() ) {

			$sl_related_title = $secretlab['related_posts_title'];
			if ( $sl_related_title == 1 ) {
				echo '<h3>'.$sl_related_title.'</h3>';
			}
			echo '<ul class="related">';
			while ($my_query->have_posts()) {
				$my_query->the_post();
				?>
				<li>
					<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
					<div class="thumb"><?php
						if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
							the_post_thumbnail( 'post-thumbnail' );
						}
						?></div>
					<div class="vanish">
						<a href="<?php the_permalink(); ?>" class="more-link"><i class="icon icon-eye"></i></a>
						<small><span class="icon icon-calendar2"></span> <a href="<?php the_permalink(); ?>" class="data-more-link"><?php the_time('F jS, Y') ?></a>
							<div><span class="icon-comments-o"></span> <?php comments_popup_link( esc_html__('No Comments', 'the-seo'), esc_html__('1 Comment', 'the-seo'), esc_html__('% Comment', 'the-seo')); ?></small></div>
					</div>

				</li>
				<?php
			}
			echo '</ul>';
		}
	}
	$post = $backup;
	wp_reset_postdata();
}

function theseo_set_archive_css_layout() {

	global $secretlab, $theseo_layout;

	if (isset($secretlab['blog-sidebar-layout'])) {
		$sl_ind_sidebars = $secretlab['sidebar-layout'];
		$sl_blog_sidebars = $secretlab['blog-sidebar-layout'];
		$sl_blog_columns = $secretlab['blog-columns'];

		if ( $sl_blog_columns == 2 and $sl_blog_sidebars == 1 and $sl_ind_sidebars == 1 ) {
			if ( ! is_single()) {
				echo ' blog2columnpage';
			}
		}
		if ( $sl_blog_sidebars == 3 or $sl_blog_sidebars == 4 ){
			echo ' onecolumnnsb';
		}
		if ( $sl_blog_columns == 1 ){
			echo ' onecolumn';
		}
		if ( $sl_ind_sidebars == 2 or $sl_ind_sidebars == 3 or $sl_ind_sidebars == 4 ){
			echo ' onecolumn';
		}
	} else {
		echo ' onecolumn';
	}
}

function theseo_set_archive_css_layout_index() {

	global $secretlab, $theseo_layout;

	if (isset($secretlab['blog-sidebar-layout'])) {
		$sl_ind_sidebars = $secretlab['sidebar-layout'];
		$sl_blog_columns = $secretlab['blog-columns'];

		if ( $sl_blog_columns == 2 and $sl_ind_sidebars == 1 ) {
			if ( ! is_single()) {
				echo ' blog2columnpage';
			}
		}
		if ( $sl_ind_sidebars == 3 or $sl_ind_sidebars == 4 ){
			echo ' onecolumnnsb';
		}
		if ( $sl_ind_sidebars == 1 ){
			echo ' onecolumn';
		}
	} else {
		echo ' onecolumn';
	}
}




// Title for Portfolio Study page
function theseo_portfolio_title() {
	global $secretlab;
	if (isset($secretlab['portfolio_arch_title'])) {
		if (!empty($secretlab['portfolio_arch_title'])) {
			echo esc_html($secretlab['portfolio_arch_title']);
		}
	}
}
function theseo_portfolio_desc() {
	global $secretlab;
	$allowed_html = array(
		'a' => array(
			'href' => array(),
			'title' => array()
		),
		'img' => array(
			'src' => array(),
			'title' => array(),
			'alt' => array(),
			'class' => array(),
		),
		'br' => array(),
		'em' => array(),
		'strong' => array(),
		'h1' => array(),
		'h2' => array(),
		'h3' => array(),
		'h4' => array(),
		'h5' => array(),
		'h6' => array(),
		'p' => array(
			'style' => array(),
		),
		'b' => array(),
		'i' => array(),
		'u' => array(),
		'ol' => array(),
		'ul' => array(),
		'li' => array(),
		'code' => array(),
		'del' => array()
	);
	
	if (isset ($secretlab['portfolio_arch_desc'])) {
		$sl_padesc = $secretlab['portfolio_arch_desc'];
		if ($sl_padesc != '') {
			echo '<div class="archdescr">'.wp_kses($sl_padesc, $allowed_html).'</div>';
		}
	}
}

// Title for services page
function theseo_services_title() {
	global $secretlab;
	if (isset($secretlab['services_arch_title'])) {
		if (!empty($secretlab['services_arch_title'])) {
			echo esc_html($secretlab['services_arch_title']);
		}
	}
}
function theseo_services_read_more() {
	global $secretlab;
	if (isset($secretlab['services_read_more'])) {
		if (!empty($secretlab['services_read_more'])) {
			echo esc_html($secretlab['services_read_more']);
		}
	}
}
function theseo_services_desc() {
	global $secretlab;
	$allowed_html = array(
		'a' => array(
			'href' => array(),
			'title' => array()
		),
		'img' => array(
			'src' => array(),
			'title' => array(),
			'alt' => array(),
			'class' => array(),
		),
		'br' => array(),
		'em' => array(),
		'strong' => array(),
		'h1' => array(),
		'h2' => array(),
		'h3' => array(),
		'h4' => array(),
		'h5' => array(),
		'h6' => array(),
		'p' => array(
			'style' => array(),
		),
		'b' => array(),
		'i' => array(),
		'u' => array(),
		'ol' => array(),
		'ul' => array(),
		'li' => array(),
		'code' => array(),
		'del' => array()
	);
	
	if (isset ($secretlab['services_arch_desc'])) {
		$sl_sadesc = $secretlab['services_arch_desc'];
		if ($sl_sadesc != '') {
			echo '<div class="archdescr">'.wp_kses($sl_sadesc, $allowed_html).'</div>';
		}
	}
}

// Title for team page
function theseo_team_title() {
	global $secretlab;
	if (isset($secretlab['team_arch_title'])) {
		if (!empty($secretlab['team_arch_title'])) {
			echo esc_html($secretlab['team_arch_title']);
		}
	}
}
function theseo_team_desc() {
	global $secretlab;
	$allowed_html = array(
		'a' => array(
			'href' => array(),
			'title' => array()
		),
		'img' => array(
			'src' => array(),
			'title' => array(),
			'alt' => array(),
			'class' => array(),
		),
		'br' => array(),
		'em' => array(),
		'strong' => array(),
		'h1' => array(),
		'h2' => array(),
		'h3' => array(),
		'h4' => array(),
		'h5' => array(),
		'h6' => array(),
		'p' => array(
			'style' => array(),
		),
		'b' => array(),
		'i' => array(),
		'u' => array(),
		'ol' => array(),
		'ul' => array(),
		'li' => array(),
		'code' => array(),
		'del' => array()
	);
	
	if (isset ($secretlab['team_arch_desc'])) {
		$sl_tadesc = $secretlab['team_arch_desc'];
		if ($sl_tadesc != '') {
			echo '<div class="archdescr">'.wp_kses($sl_tadesc, $allowed_html).'</div>';
		}
	}
}
// Title for Testimonials page
function theseo_testi_title() {
	global $secretlab;
	if (isset($secretlab['testi_arch_title'])) {
		if (!empty($secretlab['testi_arch_title'])) {
			echo esc_html($secretlab['testi_arch_title']);
		}
	}
}
function theseo_testi_desc() {
	global $secretlab;
	$allowed_html = array(
		'a' => array(
			'href' => array(),
			'title' => array()
		),
		'img' => array(
			'src' => array(),
			'title' => array(),
			'alt' => array(),
			'class' => array(),
		),
		'br' => array(),
		'em' => array(),
		'strong' => array(),
		'h1' => array(),
		'h2' => array(),
		'h3' => array(),
		'h4' => array(),
		'h5' => array(),
		'h6' => array(),
		'p' => array(
			'style' => array(),
		),
		'b' => array(),
		'i' => array(),
		'u' => array(),
		'ol' => array(),
		'ul' => array(),
		'li' => array(),
		'code' => array(),
		'del' => array()
	);
	if (isset ($secretlab['testi_arch_desc'])) {
		$sl_teadesc = $secretlab['testi_arch_desc'];
		if ($sl_teadesc != '') {
			echo '<div class="archdescr">'.wp_kses($sl_teadesc, $allowed_html).'</div>';
		}
	}
}
// Title for Contact US section
function theseo_footer_contactus() {
	global $secretlab;
	if (isset($secretlab['contactus-footer-text'])) {
		if (!empty($secretlab['contactus-footer-text'])) {
			$theseo_fcuso = esc_html($secretlab['contactus-footer-text']);
			return $theseo_fcuso;
		}
	}
}
// See on the map for Contact US section
function theseo_footer_seeonmap() {
	global $secretlab;
	if (isset($secretlab['seeonmap-footer-text'])) {
		if (!empty($secretlab['seeonmap-footer-text'])) {
			$theseo_fsee = esc_html($secretlab['seeonmap-footer-text']);
			return $theseo_fsee;
		}
	}
}
// Contacts back link for Contact US section
function theseo_footer_contback() {
	global $secretlab;
	if (isset($secretlab['contback-footer-text'])) {
		if (!empty($secretlab['contback-footer-text'])) {
			$theseo_fcoback = esc_html($secretlab['contback-footer-text']);
			return $theseo_fcoback;
		}
	}
}

// Pageloader
if( ! function_exists('theseo_pageloader')) {
	function theseo_pageloader() {
		global $secretlab;
		if (isset ($secretlab['pageloader'])) {
			if ($secretlab['pageloader'] == 1) {
				echo '<div id="page-preloader">
					<div class="bo"><div class="l1"></div><div class="l2"></div><div class="l3"></div></div>
					</div>';
			}
		}
	}
}

// One Click Installer
class seo_setup {

	public $theme_type;

	public $template_path;

	public $site_url;

	public function __construct() {

		set_time_limit(0);
		$this->template_path = get_template_directory() . '/';
		$this->site_url = site_url() . '/';
		WP_Filesystem();
		global $wp_filesystem;

	}

	public function bump_request_timeout() {
		return 60;
	}

	public function install_plugins() {
	
	    if ($_POST['i_id'] == 0) {
		    delete_transient('seo_on_click_setup');
		}
			
		$setup_opts = array( 'install_plugins' => $_POST['install_plugins'],
		                     'import_widgets' => $_POST['import_widgets'], 
							 'set_theme_options' => $_POST['set_theme_options'], 'set_suppamenu_skins' => $_POST['set_suppamenu_skins'],
                             'import_color_shemes' => $_POST['import_color_shemes'],							 
							 'set_sliders' => $_POST['set_sliders'], 'technical_refresh' => $_POST['technical_refresh'], 
							 'set_types' => $_POST['set_types'], 'set_icons' => $_POST['set_icons'], 
							 'set_post_and_menu_screens' => $_POST['set_post_and_menu_screens'],
							 'import_sample_data' => $_POST['import_sample_data'], 'i_id' => 0,
							 'install_theme' => $_POST['install_theme'], 'import_data' => $_POST['import_data'], 
							 'import_attachments' => $_POST['import_attachments']);
							 
		set_transient('seo_on_click_setup', $setup_opts, 60*10);
	
		$plugins = array(
		    array(
                'name' => 'SecretLab Installer',
                'slug' => 'SecretLabInstaller',
				'source' => 'https://secretlab.pw/plu/theseo/SecretLabInstaller.zip',
                'required' => true,
				'force_activation' => true,
				'force_deactivation' => true
			),
            array(
                'name' => 'Toolset',
                'slug' => 'types',
                'required' => true,
				'force_activation' => true,
				'force_deactivation' => true
            ),
            array(
                'name' => 'redux-framework',
				'slug' => 'redux-framework',
                'required' => true,
				'force_activation' => true,
            ),		
            array(
                'name' => 'Contact Form 7',
				'slug' => 'contact-form-7',
                'required' => true,
				'force_activation' => true,
				'force_deactivation' => true
            ),		
            array(
                'name' => 'WooCommerce',
                'slug' => 'woocommerce',
                'required' => true,
				'force_activation' => true,
				'force_deactivation' => true
            ),
            array(
                'name' => 'SecretLab Importer',
                'slug' => 'wordpress-importer',
                'source' => 'https://secretlab.pw/plu/theseo/wordpress-importer.zip',
                'external_url' => 'http://secretlab.pw',
                'required' => true,
				'force_activation' => true,
				'force_deactivation' => true
            ),

            array(
                'name' => 'MailPoet Newsletters',
                'slug' => 'wysija-newsletters',
                'required' => true,
				'force_activation' => true,
				'force_deactivation' => true
            ),		
            array(
                'name' => 'WPBakery Visual Composer',
				'slug' => 'js_composer',
                'source' => 'https://secretlab.pw/plu/theseo/js_composer.zip',
				'external_url' => 'http://wpbakery.com',
                'required' => true,
				'force_activation' => true,
				'force_deactivation' => true
            ),	
            array(
                'name' => 'Ultimate Addons for Visual Composer',
				'slug' => 'ultimate_vc_addons',
                'source' => 'https://secretlab.pw/plu/theseo/ultimate_vc_addons.zip',
				'external_url' => 'https://brainstormforce.com/demos/ultimate/',
                'required' => true,
				'force_activation' => true,
				'force_deactivation' => true
            ),
            array(
                'name' => 'Revolution Slider',
                'slug' => 'revslider',
                'source' => 'https://secretlab.pw/plu/theseo/revslider.zip',
                'external_url' => 'http://www.revolution.themepunch.com/',
                'required' => true,
				'force_activation' => true,
				'force_deactivation' => true
            ),

            array(
                'name' => 'SecretLab Metabox',
				'slug' => 'SecretLabMetabox',
                'source' => 'https://secretlab.pw/plu/theseo/SecretLabMetabox.zip',
				'external_url' => 'http://secretlab.pw',
                'required' => true,
				'force_activation' => true,
				'force_deactivation' => true
            ),
            array(
                'name' => 'SecretLab Shortcodes',
				'slug' => 'SecretLabShortcodes',
                'source' => 'https://secretlab.pw/plu/theseo/SecretLabShortcodes.zip',
				'external_url' => 'http://secretlab.pw',
                'required' => true,
				'force_activation' => true,
				'force_deactivation' => true
            ),
			array(
				'name' => 'Envato Market',
				'slug' => 'envato-market',
				'source' => 'https://secretlab.pw/plu/theseo/envato-market.zip',
				'external_url' => 'http://secretlab.pw',
				'force_activation' => true,
				'force_deactivation' => true
			),
            array(
                'name' => 'Yoast SEO',
                'slug' => 'wordpress-seo',
                'required' => true,
				'force_activation' => true,
				'force_deactivation' => true
            ),	
            array(
                'name' => 'Widget Importer & Exporter',
                'slug' => 'widget-importer-exporter',
                'required' => true,
                'force_activation' => true
            ),
            array(
                'name' => 'Suppa Menu',
				'slug' => 'suppamenu',
                'source' => 'https://secretlab.pw/plu/theseo/suppamenu.zip',
				'external_url' => 'http://codecanyon.net/item/suppamenu-all-purpose-wordpress-mega-menus/7265033??ref=secretlaboratory',
                'required' => true,
                'force_activation' => true,
                'force_deactivation' => true
            ),
			array(
				'name' => 'SecretLab Visual Composer Content Widgetizer',
				'slug' => 'SectretLabVcWidget',
				'source' => 'https://secretlab.pw/plu/theseo/SectretLabVcWidget.zip',
				'required' => true,
				'force_activation' => true,
				'force_deactivation' => true
			)
        );
		
		foreach ($plugins as $plugin) {
		    $_GET['plugin'] = $plugin['slug'];
		    $_POST['tgm_pass'] = 1;		
		    $tgma = new TGM_Plugin_Activation();
		    $tgma->register($plugin);
		    $tgma->is_automatic = false;
		    $tgma->do_plugin_install();
        }		
		
		echo '___<p><b>' . esc_attr__('Plugins Installed', 'the-seo') . '</b></p>___';
		
	}

	public function activate_plugins() {

		$plugins_to_activate = array ( 'SecretLabInstaller/SecretLabInstaller.php', 'SecretLabMetabox/setting.php',
		    'SecretLabShortcodes/sl_shortcodes.php', 'types/wpcf.php',
		    'js_composer/js_composer.php', 'wysija-newsletters/index.php', 'suppamenu/index.php',
			'Ultimate_VC_Addons/Ultimate_VC_Addons.php', 'revslider/revslider.php', 'wordpress-importer/wordpress-importer.php',
			'envato-wordpress-toolkit-master/index.php', 'redux-framework/redux-framework.php',
			'widget-importer-exporter/widget-importer-exporter.php', 'woocommerce/woocommerce.php',
			'contact-form-7/wp-contact-form-7.php', 'wordpress-seo/wp-seo.php', 'responsive-lightbox/responsive-lightbox.php',
			'regenerate-thumbnails/regenerate-thumbnails.php', 'SectretLabVcWidget/composer-post-widget.php' );

		$msg = '';
		ob_start();

		foreach ($plugins_to_activate as $plugin) {
			$current = get_option( 'active_plugins' );
			$plugin = plugin_basename( trim( $plugin ) );

			if ( !in_array( $plugin, $current ) ) {
				$current[] = $plugin;
				sort( $current );

				$activate = activate_plugin( $plugin );
				do_action( 'activate_' . trim( $plugin ) );
				do_action( 'activated_plugin', trim( $plugin) );

				if ( is_wp_error( $activate ) ) {
					$msg .= '___<div id="message" class="error"><p>' . wp_kses_post( $activate->get_error_message() ) . '</p></div>___';
				}
				else {

				}
			}

		}

		ob_end_clean();

		if ($msg != '') echo $msg;
		else echo '___<p><b>'.esc_attr__( 'Plugins activated', 'the-seo' ).'</b></p>___';

	}	
	
	public function run() {
	
		if (isset($_POST['theme_type'])) $this->theme_type = $_POST['theme_type']; else $this->theme_type = 'light';	
      	
		$method = $_POST['op'];
		if (method_exists($this, $method))
		    $this->$method();
		else {
		    $setup2 = new SL_Installer();
			if (method_exists($setup2, $method)) { $setup2->$method(); }
			else $this->abort();
		}

	}

	public function abort() {
		delete_transient('seo_on_click_setup');
	}	

}


function seo_theme_setup() {
	$setup = new seo_setup();
	$setup->run();
}
	
add_action( 'wp_ajax_setup_theme', 'seo_theme_setup' );
add_action( 'wp_ajax_nopriv_setup_theme', 'seo_theme_setup' );

add_action( 'vc_before_init', 'seo_setup_vc' );

function seo_setup_vc() {
	if(function_exists('vc_set_default_editor_post_types')) {
		$list = array(
			'page',
			'post',
			'cases',
			'service',
			'portfolio',
			'composer_widget'
		);
		vc_set_default_editor_post_types( $list );
	}
}


function seo_set_admin_metaboxes() {
	$post_types_fields = array('post', 'page');
	$user = wp_get_current_user();
	foreach ($post_types_fields as $screen_id) {
		$option = get_user_option('metaboxhidden_' . $screen_id);
		if (is_array($option))
			$option = array_diff($option, array('commentstatusdiv'));
		else
			$option = array('wpb_visual_composer', 'postexcerpt', 'trackbacksdiv', 'postcustom', 'slugdiv', 'authordiv');
		$r = update_user_option($user->ID, 'metaboxhidden_' . $screen_id, $option, true);
	}
	$screen_id = 'nav-menus';
	$option = get_user_option('metaboxhidden_' . $screen_id);
	if (is_array($option))
		$option = array_diff($option, array('add-post-type-testimonial', 'add-post-type-service', 'add-post-type-teammate', 'add-post-type-portfolio'));
	else
		$option = array('add-post_format', 'add-product_cat', 'add-product_tag', 'woocommerce_endpoints_nav_link');
	$r = update_user_option($user->ID, 'metaboxhidden_' . $screen_id, $option, true);


	$option = get_user_option('manage' . $screen_id . 'columnshidden');
	if (is_array($option))
		$option = array_diff($option, array('css-classes'));
	else
		$option = array('title-attribute', 'xfn');
	$r = update_user_option($user->ID, 'manage' . $screen_id . 'columnshidden', $option, true);
	if ($r || 1 == 1) echo '___<p><b>'.esc_attr__( 'Post and Menu Screen Settings saved', 'the-seo' ).'</b></p>___'; //else echo "Error occurred while Saving Screen Settings";
}


function activate_alico_icons() {
    $plugins = get_option('active_plugins');

    if (in_array('Ultimate_VC_Addons/Ultimate_VC_Addons.php', $plugins)) {
	    global $wp_filesystem;
		
        if( empty( $wp_filesystem ) ) {
            require_once( ABSPATH .'/wp-admin/includes/file.php' );
            WP_Filesystem();
        }

        $pathes = array ( '/import/icons/alico*.zip', '/import/icons/seo*.zip' );
		$ids = array();

		foreach ($pathes as $path) {
		
		$fp = glob( get_template_directory() . $path ); 
		
		if (!$fp) die('no available icons zip file in /import/icons directory!');
		else $fp = $fp[0];
		 
		preg_match('/\/([^\/]+)$/', $fp, $fn);
		$fn = $fn[1];
		
		$wp_upload_dir = wp_upload_dir();
	
		if (!file_exists($wp_upload_dir['path'] . '/' . $fn)) {	 
		    $result = wp_upload_bits( $fn, null, $wp_filesystem->get_contents($fp) );
		
		    $filename = $wp_upload_dir['path'] . '/' . $fn;
		    $filetype = wp_check_filetype( basename( $filename ), null );
		 
		    $attachment = array(
			    'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
			    'post_mime_type' => $filetype['type'],
			    'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
			    'post_content'   => '',
			    'post_status'    => 'inherit'
		    );

		    $attach_id = wp_insert_attachment( $attachment, $filename );

			    require_once( ABSPATH . 'wp-admin/includes/image.php' );
			    require_once( ABSPATH . 'wp-admin/includes/file.php' );
			    require_once( ABSPATH . 'wp-admin/includes/media.php' );

		    $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
		    wp_update_attachment_metadata( $attach_id, $attach_data );
		
		    $ids[] = $attach_id;
			
		}
		
	
		if (count($ids) > 0) {
		    update_option('theseo_icons_archive_ids', $ids);
		}
		
		}
		
        $ids = get_option('theseo_icons_archive_ids');

	    foreach ($ids as $id) {
	
	    if (($id && $id != 0) || !$id) {
		
            $icon_sets = get_option('smile_fonts'); 
			$p = get_post($id);
            
            if (!isset($icon_sets[$p->post_title])) {			
			
			    if (class_exists('AIO_Icon_Manager')) {
			
	                $a = new AIO_Icon_Manager();
 
                    $path 		= realpath(get_attached_file($id));
			        $unzipped 	= $a->zip_flatten( $path , array('\.eot','\.svg','\.ttf','\.woff','\.json','\.css'));
					
				    if($unzipped) {
				        $a->create_config();
						echo "___" . $p->post_title . " Icon Set added___";
			        } else {
					    echo "___Error occurred with " . $p->post_title . " icons file unzipping___";
					}
					
			    } else {
				    echo "___class AIO_Icon_Manager is not active now___";
				}
            }
			else {
			    echo "___" . $p->post_title . " Icons Set is integrated already, to reinstall remove it via admim area ___";
			}
	    } else {
		    echo "___Icon Set has been integrated already, remove DB field lawyer_alico_archive_id in options table___";
        }

        }		
		
	}
		
	else {
	    echo "___This operation requires Ultimate_VC_Addons Plugin to be activated___";
	}

}


add_action( 'wp_ajax_activate_alico_icons', 'activate_alico_icons' );
add_action( 'wp_ajax_nopriv_activate__alico_icons', 'activate_alico_icons' );


/* Prevent updates */
function theseo_dont_update_theme( $r, $url ) {
    if ( 0 !== strpos( $url, 'https://api.wordpress.org/themes/update-check/1.1/' ) )
        return $r; // Not a theme update request. Bail immediately.
    $themes = json_decode( $r['body']['themes'] );
    $child = get_option( 'stylesheet' );
    unset( $themes->themes->$child );
    $r['body']['themes'] = json_encode( $themes );
    return $r;
}
add_filter( 'http_request_args', 'theseo_dont_update_theme', 5, 2 );

/****************************************************
 * Included files
 */
require_once get_template_directory() . '/auxillary.php';
// Change plugins list if inctaller is active
if (function_exists( 'welcome_notice' )) {
	require_once ( get_template_directory() . '/inc/plugins-list.php');
} else {
	require_once ( get_template_directory() . '/inc/plugins-list_f.php');
}
require_once ( get_template_directory() . '/functions/layout.php');
require_once ( get_template_directory() . '/functions/footer.php');
require_once ( get_template_directory() . '/functions/header.php');


