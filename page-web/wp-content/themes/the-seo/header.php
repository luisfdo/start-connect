<?php
/**
 * The Header template for our theme
 */

theseo_set_globals();

?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">


	<link rel="stylesheet" type="text/css" href="https://www.somosstart.com.co/wp-content/themes/the-seo/css/start.css">

	<?php
	theseo_set_site_icons();
	do_action('get_header_scripts');
	?>
	<?php theseo_set_header_background(); ?>
	<?php wp_head(); ?>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133197024-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-133197024-1');
	</script>

	
</head>

<body <?php body_class(); ?>>
<?php theseo_pageloader(); ?>
<?php theseo_set_css_layout(); ?>

<div class="container">
	<div class="row">
		<?php
		/* Sidebars */

		theseo_set_header_sidebar_layout();

		?>


