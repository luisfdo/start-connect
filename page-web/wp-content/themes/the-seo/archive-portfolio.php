<?php
/**
 * The template for displaying Archive pages
 */

get_header(); ?>

	<div id="primary" class="content-area portfoliofeed">
		<div id="content" class="site-content" role="main">

		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<h1 class="archive-title"><?php theseo_portfolio_title(); ?></h1>
			</header><!-- .archive-header -->
			<?php theseo_portfolio_desc(); ?>

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php
				$port_url_client = types_render_field( "website-address", array("url" => "true"));
				?>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 plist">
					<a href="<?php the_permalink(); ?>" rel="bookmark"><div class="entry-thumbnail"><?php the_post_thumbnail(); ?></div>
					<h3 class="entry-title"><?php the_title(); ?></h3></a>
				</div>
			<?php endwhile; ?>

			<?php theseo_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
