<?php
/*
Single Testimonial Template for Testimonial Custom Post Type
*/

get_header(); ?>


                <?php /* The loop */ ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <h1 class="entry-title"><?php the_title(); ?></h1>
                        <div class="entry-content">
                            <div class="testimonialsingle clearfix">

                                <div class="item">
                                    <div class="bubbles">
                                        <div class="mention">
                                            <p><?php
                                                /* translators: %s: Name of current post */
                                                the_content();
                                                ?></p>
                                        </div>
                                        <div class="face">
    <?php
    $client_name = types_render_field("name-of-client", array("output"=>"normal"));
    $client_post = types_render_field("post-of-client", array("output"=>"normal"));
    $client_photo = types_render_field( "photo-of-client", array("url" => "true"));
    echo '<img src="'.esc_url($client_photo).'" alt=""><strong>'.esc_attr($client_name).'</strong><p>'.esc_attr($client_post).'</p>';
    ?>
                                            <div class="entry-meta">
                                                <?php the_time('j') ?> <?php the_time('M') ?> <?php the_time('Y') ?>
                                                <?php edit_post_link( esc_html__( 'Edit', 'the-seo' ), '<span class="edit-link">', '</span>' ); ?>
                                            </div><!-- .entry-meta -->
                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div><!-- .entry-content -->
                        <div class="entry-meta hidden">
                            <?php theseo_page_entry_meta(); ?>
                        </div><!-- .entry-meta -->



                    </article><!-- #post -->

                    <?php theseo_post_nav(); ?>


                <?php endwhile; ?>

<?php get_footer(); ?>