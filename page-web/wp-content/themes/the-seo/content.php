
<?php
/**
 * The default template for displaying content
 * Used for both single and index/archive/search.
 */
?>
<?php if ( is_single() ) { ?>
    <div class="postpage">
<?php } ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
        <?php if ( is_single() ) { ?>
            <h1 class="entry-title"><?php the_title(); ?></h1>
        <?php } ?>

        <?php if ( is_single() ) : ?>

        <?php else : ?>

            <?php if ( has_post_thumbnail() && ! is_attachment() ) {
                echo '<div class="entry-thumbnail">';
                the_post_thumbnail();
                echo '</div>';
            } ?>

            <h3 class="entry-title">
                <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
            </h3>
        <?php endif; // is_single() ?>



        <div class="entry-meta">
            <?php theseo_entry_meta(); ?>
            <?php edit_post_link( esc_html__( 'Edit', 'the-seo' ), '<span class="edit-link"> <i class="icon icon-pencil-square-o"></i>', '</span>' ); ?>
        </div><!-- .entry-meta -->

    </header><!-- .entry-header -->

    <?php if ( is_search() ) : // Only display Excerpts for Search ?>
        <div class="entry-summary">
            <?php theseo_get_excerpt_for_search(); ?> <a href="<?php the_permalink(); ?>" class="more-link"><?php echo theseo_rmorelink(); ?></a>
        </div><!-- .entry-summary -->
    <?php else : ?>
        <div class="entry-content">
            <?php if ( is_single()) : ?>
                <?php
                /* translators: %s: Name of current post */
                $rmoretext = theseo_rmorelink();
                the_content( sprintf(
                    $rmoretext, '<span class="meta-nav">&rarr;</span>',
                    the_title( '<span class="screen-reader-text">', '</span>', false )
                ) );

                wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'the-seo' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) );
                ?>
            <?php else : ?>
                <a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
                <a href="<?php the_permalink(); ?>" class="more-link"><?php echo theseo_rmorelink(); ?></a>
            <?php endif; // is_single() ?>

        </div><!-- .entry-content -->
    <?php endif; ?>

</article><!-- #post -->

<?php if ( is_single() ) { ?>
    </div>
<?php } ?>