<?php
/**
 * The template for displaying 404 pages (Not Found)
 */

get_header(); ?>

	<div id="primary" class="content-area mb80 e404i">
		<div id="content" class="site-content" role="main">

			<header class="page-header">
				<h1 class="page-title"><?php sell_404_title(); ?></h1>
			</header>

			<div class="page-wrapper">
				<div class="page-content text-center">
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/404.png" alt="<?php echo esc_html_e('Error 404: Page Not Found', 'the-seo'); ?>" class="img-responsive center-block mb50">
					<p><?php sell_404_descr(); ?></p>
					<?php get_search_form(); ?>

				</div><!-- .page-content -->
			</div><!-- .page-wrapper -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>