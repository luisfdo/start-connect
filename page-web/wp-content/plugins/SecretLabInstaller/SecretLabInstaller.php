<?php

/*
Plugin Name: SecretLab Installer
Plugin URI: http://www.secretlab.pw/
Description: Imports nessesary data and settings
Author: SecretLab
Version: 2.9.9.4
Author URI: http://www.secretlab.pw/
*/

class SL_Installer {

    public function __construct() {
	
	    include_once ABSPATH.'/wp-admin/includes/file.php';
	    WP_Filesystem();
		
		global $wp_filesystem;
		
		if (isset($_POST['theme_type'])) $this->theme_type = $_POST['theme_type']; else $this->theme_type = 'light';
		
	}
	
	public function set_sliders() {

	    global $wpdb;
	
	    $uploads_dir = wp_upload_dir(); 
		$sliders = $wpdb->get_col( "SELECT title, alias FROM ".$wpdb->prefix."revslider_sliders", 1 ); 
		$files = glob(get_template_directory() . '/'  . 'import/SEO ' . $this->theme_type . '/sliders/*.zip');
		$names = preg_replace('/(.+sliders\/)([^\.]+)(\.zip)/', "\$2", $files); 
		$msg = $result = array();
		//ob_start();
		$i = 0;
		foreach ($names as $name) {
			if (!in_array($name, $sliders)) { 
			    $_FILES["import_file"]["tmp_name"] = $files[$i];
			    $instance = new RevSliderSlider();
			    $instance->importSliderFromPost();
				$result[] = str_ireplace('_', ' ', $name);
			}
			else {
			    $msg[] = '<i>' . str_ireplace('_', ' ', $name) . '</i>';
			}
			$i++;
		}
		//ob_end_clean();
		if (count($result) > 0) $result = 'Revolution Sliders ' . implode(', ', $result) . ' are imported';
		else $result = '';
		if (count($msg) > 0) $msg = '<p>NOTE: sliders ' . implode(', ', $msg) . ' already exists</p>';
		else $msg = '';
		echo '___<p><b>'. $result .'</b>' . $msg . '</p>___';

	}

    public function set_icons () {
		
        activate_alico_icons();		
		echo '___<p><b>'.esc_attr__( 'Function ended', 'the-seo' ).'</b></p>___';
	}
	
	public function set_theme_options() {
	    global $wp_filesystem;
		    				
		$theme_options = $wp_filesystem->get_contents(get_template_directory() . '/'  . 'import/SEO ' . $this->theme_type . '/theme_options_seo' . $this->theme_type . '.json' );
		$theme_options = preg_replace('/http.....seo.secretlab.pw..(slpl|sdpl|dpl)/', site_url(), $theme_options);
		if (update_option('secretlab', json_decode($theme_options, true))) $msg = '___<p><b>'.esc_attr__( 'Theme Options updated', 'the-seo' ).'</b></p>___';
		else $msg = '___<p><b>'.esc_attr__( 'Error occured while Theme Options importing or you are trying to save the same data', 'the-seo' ).'</b></p>___';

		$options = get_option('secretlab');
		if (is_array( $options ) ) theseo_change_action($options);

		echo $msg;
	}
	
	public function set_suppamenu_skins() {
		global $wp_filesystem;

		$msg = '';

		$options = $wp_filesystem->get_contents(get_template_directory() . '/' . 'import/SEO ' . $this->theme_type . '/suppamenu-skins-seo' . $this->theme_type . '.dat' );
	
		$options = unserialize($options);
		$success = true;
		if ($options) {
			foreach ($options as $option) {
				delete_option($option->option_name);
				if (!update_option($option->option_name, unserialize($option->option_value))) $success = false;
			}
		}
		if ($success) $msg .= '___<p><b>'.esc_attr__( 'Suppamenu Skins updated', 'the-seo' ).'</b></p>___'; else '___<p><b>'.esc_attr__( 'Error occured while Suppamenu Skins importing', 'the-seo' ).'</b></p>___';

		if ($this->theme_type == 'light') { $css_file = 'menu_css_seolight.zip'; $js_file = 'menu_js_seolight.zip'; }
		else if ($this->theme_type == 'dark') { $css_file = 'menu_css_seodark.zip'; $js_file = 'menu_js_seodark.zip'; }
		else if ($this->theme_type == 'flat') { $css_file = 'menu_css_flat.zip'; $js_file = 'menu_js_flat.zip'; }
		else { $css_file = 'menu_css_digital.zip'; $js_file = 'menu_js_digital.zip'; }
		if (file_exists(get_template_directory() . '/import/menu_css/' . $css_file)) {
			$upload_dir = wp_upload_dir();
			$upload_dir = preg_replace('/uploads\/[0-9]+\/[0-9]+$/', 'uploads', $upload_dir['path']);
			$r = unzip_file( get_template_directory() . '/import/menu_css/' . $css_file, $upload_dir . '/suppamenu2/css' );
			$r = unzip_file( get_template_directory() . '/import/menu_css/' . $js_file, $upload_dir . '/suppamenu2/js' );
		}
		if ($r) $msg .= '___<p><b>'.esc_attr__( 'Suppamenu CSS updated', 'the-seo' ).'</b></p>___'; else $msg .= '___<b>'.esc_attr__( 'Error occurred while Suppamenu CSS importing', 'the-seo' ).'</b></p>___';

		echo $msg;
	
	}
	
	public function import_color_shemes() {
		sleep(5);
		$wp_upload_dir = wp_upload_dir();

		if (!is_dir($wp_upload_dir['basedir'] . '/redux/color-schemes')) {
			if(!mkdir($wp_upload_dir['basedir'] . '/redux/color-schemes', 0777, true)) die('___Could not create directory___');
		}
		if (!copy(get_template_directory() . '/import/3_color_kits.json', $wp_upload_dir['basedir'] . '/redux/color-schemes/secretlab_ocs.json'))
			die('___Could not copy file color schemes file___');
		else echo '___<p><b>'.esc_attr__( 'Color Schemes saved', 'the-seo' ).'</b></p>___';

	}	

	public function import_widgets($echo = true) {

		$file = glob( get_template_directory() . '/import/SEO ' .$this->theme_type. '/*-widgets.json' );
		$file = $file[0];
		$file_to_use = str_ireplace('.json', '_to_use.json', $file);
		copy( $file, $file_to_use );
		wie_process_import_file( $file_to_use );

		if ($echo)
			echo '___<p><b>'.esc_attr__( 'Widgets imported', 'the-seo' ).'</b></p>___';
	}

	public function set_post_and_menu_screens() {
		seo_set_admin_metaboxes();
		if ( ! isset( $_POST['data_import'] ) || $_POST['data_import'] == 0) { echo '___<p class="green"><b>'.esc_attr__( 'Theme setup completed', 'the-seo' ).'</b></p>___'; }
	}		

    public function set_types() {
	
	    global $wp_filesystem;
		
		sleep(5);
		
			$_POST['overwrite-settings'] = 1;
			$_POST['overwrite-groups'] = 1;
			$_POST['delete-groups'] = 1;
			$_POST['delete-fields'] = 1;
			$_POST['delete-types'] = 1;
			$_POST['delete-tax'] = 1;
			$_POST['overwrite-fields'] = 1;
			$_POST['overwrite-types'] = 1;
			$_POST['overwrite-tax'] = 1;
			$_POST['post_relationship'] = 1;
			$_POST['mode'] = 'file';
			$_POST['import-final'] = 1;
			$_POST['import'] = 'Import';
			//require_once WP_PLUGIN_DIR . '/types/embedded/admin.php';
			//require_once WP_PLUGIN_DIR . '/types/embedded/types.php';
			//require_once WP_PLUGIN_DIR . '/types/embedded/includes/fields.php';
			//require_once WP_PLUGIN_DIR . '/types/embedded/includes/import-export.php';
			$data = $wp_filesystem->get_contents( get_template_directory() . '/import/types_plugin_settings.xml' );
			wpcf_admin_import_data( $data, false, 'types-auto-import' );
			
			echo '___<p><b>'.esc_attr__( 'Types Plugin Settings imported', 'the-seo' ).'</b></p>___';
				
	}
	
	public function import_sample_data() {
		global $wpdb, $wp_import, $wp_filesystem;

		//$this->import_widgets();

		//$theme_options = $wp_filesystem->get_contents(get_template_directory() . '/' . 'import/SEO ' . $this->theme_type . '/theme_options_seo' . $this->theme_type . '.json' );
		//update_option('secretlab', json_decode($theme_options, true));

		if ( ( isset( $_POST['import_data'] ) && $_POST['import_data'] == 1 ) || $_POST['op'] == 'import_sample_data' ) {

			if ( !defined('WP_LOAD_IMPORTERS') ) define('WP_LOAD_IMPORTERS', true); // we are loading importers

			if ( ! class_exists( 'WP_Importer' ) ) { // if main importer class doesn't exist
				$wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
				include $wp_importer;
			}

			if ( ! class_exists('WP_Import') ) { // if WP importer doesn't exist
				$wp_import = WP_PLUGIN_DIR . '/wordpress-importer/secretlab-importer.php';
				include $wp_import;
			}

			if ( class_exists( 'WP_Importer' ) && class_exists( 'WP_Import' ) ) {

				$_POST['imported_authors'][0] = 'admin';
				$_POST['imported_authors'][1] = 'wooteam';
				$_POST['use_map'][0] = 0;
				$_POST['use_map'][1] = 0;
				$_POST['user_new'][0] = null;
				$_POST['user_new'][1] = null;

				$importer = new WP_Import();
				
				$files = glob( get_template_directory() . '/import/SEO ' .$this->theme_type. '/1 Part XML Import (For fast web hosts)/*.xml' );

				$past_files = glob( get_template_directory() . '/import/*inst.xml' );
				if (count($past_files) > 0) {
					foreach ($past_files as $pf) {
						unlink($pf);
					}
				}				
				
				// remove suppa menu
				wp_delete_nav_menu('headermenu2'); wp_delete_nav_menu('headermenu');
				$wpdb->query("DELETE FROM " . $wpdb->postmeta . " WHERE meta_key LIKE '_meta_item_suppa%';");
                //$wpdb->query("DELETE FROM " . $wpdb->options . " WHERE option_name LIKE 'suppa%' OR option_name LIKE 'CTF_suppa%';");
                //
				
				foreach ($files as $file) {
					$nfn = str_ireplace('.xml', '_inst.xml', $file);
					if (copy($file, $nfn)) {
						$object = array(
							'post_title' => $nfn ,
							'post_content' => $nfn,
							'post_mime_type' => '',
							'guid' => $nfn,
							'context' => 'import',
							'post_status' => 'private'
						);

						$id = wp_insert_attachment( $object, $nfn );
						wp_schedule_single_event( time() + DAY_IN_SECONDS, 'importer_scheduled_cleanup', array( $id ) );

						$_POST['import_id'] = $id;
						$importer->id = $id;

						ob_start();
						$importer->import($nfn);
						if (is_file($nfn)) unlink($nfn);
						ob_end_clean();
					}
				}
				echo '___<p class="green"><b>'.esc_attr__( 'Sample data imported', 'the-seo' ).'</b></p>___';
			}
			else {
				echo '___<p class="red"><b>'.esc_attr__( 'There are problems with WP_Import classes, check if "wordpress-importer" plugin is activated', 'the-seo' ).'</b></p>___';
			}
			// Connect Nav Menus to locations
			if ($this->theme_type == 'dark' || $this->theme_type == 'digital' || $this->theme_type == 'digital_dark') $menu_name = 'headermenu2'; else $menu_name = 'headermenu';
			$native_menu = wp_get_nav_menu_object($menu_name);
			if ( is_object ( $native_menu ) ) { 
			    $locations = array('header1and5', 'header2', 'header3', 'header4');
			    $menu_set = array();
			    foreach ($locations as $location) {
				    $menu_set[$location] = $native_menu->term_id;
			    }
			    if (count($menu_set) > 0) {
				    set_theme_mod('nav_menu_locations', $menu_set);
			    }
			}
			else echo '___<p class="red"><b>'.esc_attr__( 'It seems there are problem with Nav Menu Import', 'the-seo' ).'</b></p>___';
			// Set Front Page
			$homepage = get_post( 1975 );
			if (($homepage && $homepage->ID)) {
				update_option('show_on_front', 'page');
				update_option('page_on_front', $homepage->ID);
			}
			// change psrent URI to local URI in Post Content Visual Composer data
			$site_url = site_url();
			$posts = get_posts(array('posts_per_page' => -1, 'numberposts' => -1, 'post_type' => 'any'));
			foreach ($posts as $p) {
				$post_content = preg_replace('/url.http.3A.2F.2Fseo.secretlab.pw.2F(slpl|sdpl|dpl)/', 'url:' . urlencode($site_url), $p->post_content);
				if ( ! wp_is_post_revision( $p->ID ) ){

					$args = array('ID' => $p->ID, 
								  'post_content' => $post_content );
					$result = wp_update_post( $args );
				}
			}
            // end of VC data correction			
			// Finish Import
			delete_transient('seo_on_click_setup');
		}
	}
	
	
	public function get_xml_file() {
	    global $wp_filesystem;
		
		$file = glob(get_template_directory() . '/' . 'import/SEO ' . $this->theme_type . '/1 Part XML Import (For fast web hosts)/*.xml');
		$file = $file[0];
		$content = $wp_filesystem->get_contents($file);
		
		echo $content;
	}
	
	
	public function get_uploaded_attachments() {
	    $uploaded_attachments = get_option('seo_uploaded_attachments');
		if ( $uploaded_attachments && count( $uploaded_attachments > 0 ) ) {
		    echo json_encode(array('empty' => 'no', 'content' => array_values($uploaded_attachments)));
		}
		else {
		    echo json_encode(array('empty' => 'yes', 'content' => $uploaded_attachments));
		}
	}
	
	
	public function attachment_upload() {
	    //header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
		$parameters = array(
			'url' => $_POST['url'],
			'post_title' => $_POST['title'],
			'link' => $_POST['link'],
			'pubDate' => $_POST['pubDate'],
			'post_author' => $_POST['creator'],
			'guid' => $_POST['guid'],
			'import_id' => $_POST['post_id'],
			'post_date' => $_POST['post_date'],
			'post_date_gmt' => $_POST['post_date_gmt'],
			'comment_status' => $_POST['comment_status'],
			'ping_status' => $_POST['ping_status'],
			'post_name' => $_POST['post_name'],
			'post_status' => $_POST['status'],
			'post_parent' => $_POST['post_parent'],
			'menu_order' => $_POST['menu_order'],
			'post_type' => $_POST['post_type'],
			'post_password' => $_POST['post_password'],
			'is_sticky' => $_POST['is_sticky'],
			'attribute_author1' => $_POST['author1'],
			'attribute_author2' => $_POST['author2']
		);

		function process_attachment( $post, $url ) {
			
			$pre_process = pre_process_attachment( $post, $url );
			if( is_wp_error( $pre_process ) )
				return array(
					'fatal' => false,
					'type' => 'error',
					'code' => $pre_process->get_error_code(),
					'message' => $pre_process->get_error_message(),
					'text' => sprintf( __( '%1$s was not uploaded. (<strong>%2$s</strong>: %3$s)', 'attachment-importer' ), $post['post_title'], $pre_process->get_error_code(), $pre_process->get_error_message() )
				);

			// if the URL is absolute, but does not contain address, then upload it assuming base_site_url
			if ( preg_match( '|^/[\w\W]+$|', $url ) )
				$url = rtrim( $this->base_url, '/' ) . $url;

			$upload = fetch_remote_file( $url, $post );
			if ( is_wp_error( $upload ) )
				return array(
					'fatal' => ( $upload->get_error_code() == 'upload_dir_error' && $upload->get_error_message() != 'Invalid file type' ? true : false ),
					'type' => 'error',
					'code' => $upload->get_error_code(),
					'message' => $upload->get_error_message(),
					'text' => sprintf( __( '%1$s could not be uploaded because of an error. (<strong>%2$s</strong>: %3$s)', 'attachment-importer' ), $post['post_title'], $upload->get_error_code(), $upload->get_error_message() )
				);

			if ( $info = wp_check_filetype( $upload['file'] ) )
				$post['post_mime_type'] = $info['type'];
			else {
				$upload = new WP_Error( 'attachment_processing_error', __('Invalid file type', 'attachment-importer') );
				return array(
					'fatal' => false,
					'type' => 'error',
					'code' => $upload->get_error_code(),
					'message' => $upload->get_error_message(),
					'text' => sprintf( __( '%1$s could not be uploaded because of an error. (<strong>%2$s</strong>: %3$s)', 'attachment-importer' ), $post['post_title'], $upload->get_error_code(), $upload->get_error_message() )
				);
			}

			$post['guid'] = $upload['url'];

			// Set author per user options.
			switch( $post['attribute_author1'] ){

				case 1: // Attribute to current user.
					$post['post_author'] = (int) wp_get_current_user()->ID;
					break;

				case 2: // Attribute to user in import file.
					if( !username_exists( $post['post_author'] ) )
						wp_create_user( $post['post_author'], wp_generate_password() );
					$post['post_author'] = (int) username_exists( $post['post_author'] );
					break;

				case 3: // Attribute to selected user.
					$post['post_author'] = (int) $post['attribute_author2'];
					break;

			}

			// as per wp-admin/includes/upload.php
			$post_id = wp_insert_attachment( $post, $upload['file'] );
			wp_update_attachment_metadata( $post_id, wp_generate_attachment_metadata( $post_id, $upload['file'] ) );

			// remap image URL's
			backfill_attachment_urls( $url, $upload['url'] );

			return array(
				'fatal' => false,
				'type' => 'updated',
				'text' => sprintf( __( '%s was uploaded successfully', 'attachment-importer' ), $post['post_title'] )
			);
		}

		function pre_process_attachment( $post, $url ){
			global $wpdb;

			$imported = $wpdb->get_results(
				$wpdb->prepare(
					"
					SELECT ID, post_date_gmt, guid
					FROM $wpdb->posts
					WHERE post_type = 'attachment'
						AND post_title = %s
					",
					$post['post_title']
				)
			);

			if( $imported ){
				foreach( $imported as $attachment ){
					if( basename( $url ) == basename( $attachment->guid ) ){
						if( $post['post_date_gmt'] == $attachment->post_date_gmt ){
                            $remote_response = wp_safe_remote_get( $url );

                            $headers = wp_remote_retrieve_headers( $remote_response );
							if( filesize( get_attached_file( $attachment->ID ) ) == $headers['content-length'] ){
								return new WP_Error( 'duplicate_file_notice', __( 'File already exists', 'attachment-importer' ) );
							}
						}
					}
				}
			}

			return false;
		}
		
		function fetch_remote_file( $url, $post ) {
			// extract the file name and extension from the url
			$file_name = basename( $url );

			// get placeholder file in the upload dir with a unique, sanitized filename
			$upload = wp_upload_bits( $file_name, 0, '', $post['post_date'] );
			if ( $upload['error'] )
				return new WP_Error( 'upload_dir_error', $upload['error'] );

			// fetch the remote url and write it to the placeholder file
            $remote_response = wp_safe_remote_get( $url, array(
                'timeout' => 300,
                'stream' => true,
                'filename' => $upload['file'],
            ) );

            $headers = wp_remote_retrieve_headers( $remote_response );
			// request failed
			if ( ! $headers ) {
				@unlink( $upload['file'] );
				return new WP_Error( 'import_file_error', __('Remote server did not respond', 'attachment-importer') );
			}

            $remote_response_code = wp_remote_retrieve_response_code( $remote_response );

            // make sure the fetch was successful
            if ( $remote_response_code != '200' ) {
                @unlink( $upload['file'] );
                return new WP_Error( 'import_file_error', sprintf( __('Remote server returned error response %1$d %2$s', 'wordpress-importer'), esc_html($remote_response_code), get_status_header_desc($remote_response_code) ) );
            }

			$filesize = filesize( $upload['file'] );

			if ( isset( $headers['content-length'] ) && $filesize != $headers['content-length'] ) {
				@unlink( $upload['file'] );
				return new WP_Error( 'import_file_error', __('Remote file is incorrect size', 'attachment-importer') );
			}

			if ( 0 == $filesize ) {
				@unlink( $upload['file'] );
				return new WP_Error( 'import_file_error', __('Zero size file downloaded', 'attachment-importer') );
			}

			return $upload;
		}

		function backfill_attachment_urls( $from_url, $to_url ) {
			global $wpdb;
			// remap urls in post_content
			$wpdb->query(
				$wpdb->prepare(
					"
						UPDATE {$wpdb->posts}
						SET post_content = REPLACE(post_content, %s, %s)
					",
					$from_url, $to_url
				)
			);
			// remap enclosure urls
			$result = $wpdb->query(
				$wpdb->prepare(
					"
						UPDATE {$wpdb->postmeta}
						SET meta_value = REPLACE(meta_value, %s, %s) WHERE meta_key='enclosure'
					",
					$from_url, $to_url
				)
			);
		}

		$remote_url = ! empty($parameters['attachment_url']) ? $parameters['attachment_url'] : $parameters['guid'];
		
		echo json_encode( process_attachment( $parameters, $remote_url ) );
		
		$uploaded_attachments = get_option('seo_uploaded_attachments');
		if (!is_array($uploaded_attachments)) {
		    $uploaded_attachments[] = array();
		}
        $uploaded_attachments[] = $_POST['post_id'];
		update_option('seo_uploaded_attachments', array_unique($uploaded_attachments));
		
		die();
	}


}

function welcome_notice() {
    global $wn;
	
		$max = array( "max_execution_time"    => array(120, ini_get("max_execution_time"), " 'max_execution_time' parameter on your hosting/server is ### seconds, 120 seconds recommended"),
					  "memory_limit"          => array(256, intval(ini_get("memory_limit")), " 'memory_limit' parameter on your hosting/server is ### Mb, 256Mb recommended" ),
					  "post_max_size"         => array(40, intval(ini_get("post_max_size")), " 'post_max_size' parameter on your hosting/server is ### Mb, 40Mb recommended"),
					  "upload_max_filesize"   => array(40, intval(ini_get("upload_max_filesize")), " 'upload_max_filesize' parameter on your hosting/server is ### Mb, 40Mb recommended"),
					  "allow_url_fopen"   => array(1, ini_get("allow_url_fopen"), " 'allow_url_fopen' parameter on your hosting/server is OFF, we are recommend ON")
		);

		$init_msgs = array();
		foreach($max as $name => $set) {
			if ($set[1] < $set[0]) {
				$init_msgs[] = str_ireplace('###', $set[1], $set[2]);
			}
		}
		
		if (count($init_msgs) > 0) $init_msg = '<div id="init_msg">' . implode($init_msgs, '<br>') . '</div>';
		else $init_msg = '';
		
		$wn['real_capabilities'] = $init_msg;
		
		$wn['recommended_capabilities'] = '<div class="col-md-4 col-sm-12">
		                <h2 class="second">Server Requirements</h2><div class="inform">
						
						<ul>
						<li>max_execution_time 120</li>
						<li>memory_limit 256M</li>
						<li>post_max_size 40M</li>
						<li>upload_max_filesize 40M</li>
						<li>allow_url_fopen on</li>
						</ul>
						</div>
						</div>';
						
		$wn['fail_install'] = '<div class="col-md-4 col-sm-12">
		                <h2 class="second">Fail of installation</h2><div class="inform">
						
						   If you got fail of the installation ask your hosting to check error logs or use debug mode to understand reason of fail. <a href="http://secretlab.pw/doc/seowp/#line18">How to turn debug mode on?</a>
						</div>
						</div>';
					
	}
	
	add_action('delete_attachment', 'seo_correct_imported_attaches_list', 10, 2);
	
	function seo_correct_imported_attaches_list($id) {
	    $imported_attaches = get_option('seo_uploaded_attachments');
		if ( is_array($imported_attaches) && is_numeric($id) ) {
		    $imported_attaches = array_diff($imported_attaches, array($id));
			update_option('seo_uploaded_attachments', $imported_attaches);
		}
	}

	
?>
