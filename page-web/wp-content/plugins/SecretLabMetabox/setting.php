<?php

/*
Plugin Name: SecretLabMetabox
Plugin URI: http://www.secretlab.pw/
Description: Additional post settings support
Author: SecretLab
Version: 2.9.6
Author URI: http://www.secretlab.pw/
*/

if (preg_match('/wp-admin\/nav-menus.php|wp-admin\/admin-ajax.php/', $_SERVER['PHP_SELF'])) return;

/* SecretLab - loading additional shortcodes for Visual Composer */




function show_img_selector($key, $imgs, $setting, $keys, $hidden) {

    echo '<div id="'.$key.'"class="custom_settings_box">';
	echo '<div class="key_header"><span>'.$keys[$key].'</span></div>'.
	          '<ul class="custom settings">';
	foreach ($imgs as $img) {
	    echo '<li class="custom_setting" ><img src='.esc_attr($img).'></li>';
	}
	echo '</ul>
	      <input id="'.$key.'_input" class="custom_setting_input" type="text" name="'.esc_attr($key.$hidden).'" value="'.esc_attr($setting).'">
		  </div>';

}

function show_select_buttons ( $params = array('key' => null,
                                               'name_postfix' => '',
									           'setting' => null,
									           'var1' => 1,
									           'class1' => 'on',
									           'caption1' => 'On',
									           'var2' => 0,
									           'class2' => 'off',
									           'caption2' => 'Off') ) {
			
			if ( $params['setting'] == $params['var1'] ) {
			    $selected1 = $params['class1']; $selected2 = $params['class2']; 
			}
			else if ( $params['setting'] == $params['var2']) {
			    $selected1 = $params['class2']; $selected2 = $params['class1'];
			} 
			else {
			    $selected1 = $selected2 = $params['class2'];
			}
			$out =  '<a class="meta_btn'.$selected1.'" val="'.esc_attr($params['var1']).'" href="#">'.esc_attr($params['caption1']).'</a><a class="meta_btn'.$selected2.'" val="'.esc_attr($params['var2']).'" href="#">'.$params['caption2'].'</a>';
            $out .= '<input id="'.esc_attr($params['key']).'_input" type=text name="'.esc_attr($params['key'].$params['name_postfix']).'" value='.esc_attr($params['setting']).' >';
			
			return $out;
}

$keys = array( 'design-layout' => 'Page Layout',
			   'boxed-background' => 'Boxed Background Image',
               'boxed-background-color' => 'Boxed Background Color',
			   //'header-topbar' => 'Top Bar for Header Section', 
			   //'header-layout' => 'Header Layout type',
			   'header_type' => 'Select Header Type',
			   'header_image' => 'Image',
			   'header14_slider' => 'Select Revolution Slider',			   
               'sidebar-layout' => 'Sidebar Layout', 
			   'left_sidebar_widgets' => 'Widgets for Left Sidebar',
			   'right_sidebar_widgets' => 'Widgets for Right Sidebar'  // ���� �������� �������� �� $secretlab=> �������� � ����������� 
			   );
					
$blog_keys = array ( 'blog-layout' => 'Select Blog Layout',
					 'blog-boxed-background' => 'Blog Boxed Background Image',
                     'blog-boxed-background-color' => 'Blog Boxed Background Color',
                     'blog-sidebar-layout' => 'Select Blog Sidebar Layout',
					 //'blog-columns' => 'Select one/two columns for No-Sidebar Layout',
			         'blog_left_sidebar_widgets' => 'Widgets for Blog Left Sidebar',
			         'blog_right_sidebar_widgets' => 'Widgets for Blog Right Sidebar',					 
					 //'blog-header-layout' => 'Blog Header Layout',
					 'blog-header_type' => 'Select Header Type',
					 'blog-header_image' => 'Image',
					 'blog-header14_slider' => 'Select Revolution Slider'/*,
					 'is_related_posts' => 'Show related posts',
					 'show_post_author' => 'Show post author',
					 'show_post_category' => 'Show post category',
					 'show_post_tags' => 'Show post tags',
					 'show_post_date' => 'Show post date',
					 'show_comments_count' => 'Show post comments count'*/);
					 
			   
$shop_keys = array( //'show-cart' => 'Show shop cart On/Off',
                    //'catalog_css_template' => 'Template for shop catalog',
					'shop-header_type' => 'Select Header Type',
					'shop-header_image' => 'Image',
					'shop-sidebar-layout' => 'Template for Product single-page',
					'shop-header14_slider' => 'Revovution Slider for shop pages' );					 
										 
/* add metaboxes to Post, Page and Shop admin pages */
					 
function add_settings_meta_box() {
    $screens = array( 'post', 'page', 'portfolio', 'service', 'teammate', 'testimonial' );
    foreach ($screens as $screen) {
        add_meta_box( 'layout_settings', 'Layout Settings', 'settings_proccess', $screen, 'normal', 'low' );   
	}
	add_meta_box( 'layout_settings', 'layout settings', 'shop_settings_proccess', 'product', 'normal', 'low' );
}

add_action( 'add_meta_boxes', 'add_settings_meta_box' );

/* function to prevent distortion hierarhical order of output category list at the admin page */

add_filter( 'wp_terms_checklist_args', 'checklist_args' );

function checklist_args ($args) {
    $args['checked_ontop'] = false;
	return $args;
}

add_action('load-post-new.php', 'set_blog_screen');
add_action( 'admin_head-post.php', 'set_blog_screen');

function set_blog_screen($hook) {
    global $secretlab, $post; 
	
	if ($post) {
	    if ($post->post_type == 'page' || (!in_category( 'blog', $post->ID ) || !theseo_post_is_in_descendant_category(get_term_by( 'name', 'blog', 'category' ), $post->ID))) { 
		    $secretlab['admin_post_new'] = false; return; 
		}
	}
	if (preg_match('/post_type=page/', $_SERVER['REQUEST_URI'])) { $secretlab['admin_post_new'] = false; return; }
	
	$secretlab['admin_post_new'] = true;
}

/* main functions */

function settings_proccess($post) {


    global $secretlab, $keys, $shop_keys, $blog_keys;

	$settings = json_decode(get_post_meta($post->ID, 'layout_settings', true), true);
	$native_set = $settings;
	$home = get_template_directory_uri();
	
	$sidebars = get_sidebars();
	
	if (in_category( 'blog', $post->ID ) || theseo_post_is_in_descendant_category(get_term_by( 'name', 'blog', 'category' ), $post->ID) || $secretlab['admin_post_new']) {
	    $blog_page = true; } else { $blog_page = false; }
		
	if (!$blog_page) { 
	    $regular_page = true; 
        $r_hidden = ''; $r_class = ''; $b_hidden = '_locked'; $b_class = 'locked '; $s_hidden = '_locked'; $s_class = ' locked';
    }
    else if ($blog_page) {
	    $regular_page = false; 
		$b_hidden = ''; $b_class = ''; $r_hidden = '_locked'; $r_class = 'locked '; $s_hidden = '_locked'; $s_class = ' locked';	
    }	
	
	echo '<div id="post_layout_settings">';
	
	echo '<div id="manage_type_selector"class="custom_settings_box">';
	 if (!empty($settings) && count($settings) > 0) { $active = 'checked'; $visible_settings_box = 'visible '; $box = $settings; $visible = '';} 
	else { $active = 'checked'; $visible_settings_box = 'visible '; $box = $secretlab; $visible = ''; }
	
	echo "<div id='subtitle'>".'<a id="manage_type_selector_input" class="meta_btn" href="#">Remove Individual Settings</a>'."</div>";
	echo '<input id="sealed" class="hidden" name="sealed" type="text" value="1" >';
	//echo '<input id="manage_type_selector_input" name="manage_type_selector" type="checkbox" '.$active.' >';
	
	echo '<div id="post_layout_settings_box" '.'class="'.$visible_settings_box.$r_class.'"'.' n='.$post->ID.'>';
	
	foreach ($keys as $key=>$description) {
	    if (!isset($settings[$key])) 
		    { if (isset($secretlab[$key])) $setting = $secretlab[$key]; } 
	    else 
		    { $setting = $settings[$key]; }
		if ($key == 'design-layout') {
		    show_img_selector($key, array($home.'/images/framework/wide.gif', $home.'/images/framework/boxed.gif'), $setting, $keys, $r_hidden);
		}
		if ($key == 'boxed-background') { 
			echo '<div id="'.$key.'"class="custom_settings_box'.$visible.'">';
			echo '<div class="key_header"><span>'.$keys[$key].'</span></div>';
			echo "<div id='boxed_img_uploader'>";
			echo "<div id='boxed_upload_img_box'>";
			    if( empty($setting)) {
					//$setting = array('url' => get_template_directory_uri().'/images/head1.jpg', 'id' => 0);
                }
                else {
					if (! is_array($setting)) {
					    $setting = json_decode($setting, true);
				    }
                }
                if (!isset($setting) || $setting['url'] == '') { $visible_el = 'hidden'; } else	{ $visible_el = 'visible'; }
			    echo "<img id='boxed_img_holder' class='".$visible_el."' img_n=".$setting['id']." src='".esc_url($setting['url'])."' width='200px' height='200px'>";					    
                echo"</div>";					
			    echo "<div id='boxed_upload_link_box'>";
					echo "<a href=# id='boxed_change_img'>Add/change bg image</a><br>";
                    echo "<a href=# class='".$visible_el."' id='boxed_remove_img'>Remove Background Image</a>";						
				echo "</div>";
			echo "</div>";
			echo "<input id='".$key."_input' class='hidden' type='text' value='".esc_attr(json_encode(array('url' => $setting['url'], 'id' => $setting['id'])))."' name=".esc_attr($key.$r_hidden)." >";
			echo "</div>";		
		    
		}		
		if ($key == 'boxed-background-color') {
		    if (!empty($setting)) {
                if (!is_array($setting)) {			
			        $color = $setting; 
				}
				else {
				    $color = $setting['color'];
				}
			} 
			else { $color = ''; }
		    echo '<div id="'.$key.'" class="custom_settings_box">';
			echo '<div class="key_header"><span>'.$keys[$key].'</span></div>';
		    echo '<input id="'.$key.'_input" class="wp-color-picker" name="'.esc_attr($key.$r_hidden).'" value='.$color.' >';
			echo '</div>';
		}
		/*if ($key == 'header-topbar') {
		    echo '<div id="'.$key.'"class="custom_settings_box">';
			//var_dump($setting);
			if ($setting == 1) { $active = ' checked'; } else { $active == ''; }
			echo '<div class="key_header"><span>'.$keys[$key].'</span></div>';
			if ($setting != 1) { echo '<input  type="checkbox" id="'.$key.'_input" name='.$key.$r_hidden.' >'; } else { echo '<input  type="checkbox" id="'.$key.'_input" name='.$key.$r_hidden.' checked >'; }
			echo '</div>';
		}*/		
		/*if ($key == 'header-layout') {
		    //echo'</div>';		
            show_img_selector($key, array($home.'/framework/images/h1.jpg', $home.'/framework/images/h2.jpg', 
			                              $home.'/framework/images/h3.jpg', $home.'/framework/images/h4.jpg',
										  $home.'/framework/images/h5.jpg'),
										  $setting, $keys, $r_hidden);		
		}*/
		if ( $key == 'header_type' ) {
			$setting = json_decode(get_post_meta($post->ID, 'layout_settings', true), true);
			if ( isset( $setting['header_type'] ) && $setting['header_type'] != '' ) { $setting = $header_type = $setting['header_type']; } else { $setting = $header_type = null; }
			$visible = ' visible'; $fake = ''; 
			echo '<div id="'.$key.'"class="custom_settings_box'.$visible.'">';
			echo '<div class="key_header"><span>'.$keys[$key].'</span></div>';
			echo "<select id='header_type' name='header_type".esc_attr($fake.$r_hidden)."' >";
			echo '<option value="1" ' . selected( $setting, 1 , false ) . '>' . esc_attr__( 'Revolution Slider', 'the-seo' ) . '</option>';
            echo '<option value="2" ' . selected( $setting, 2 , false ) . '>' . esc_attr__( 'Static Image', 'the-seo' ) . '</option>';
			echo '</select>';
			echo '</div>';			
		}
		if ( $key == 'header_image' ) {
			$setting = json_decode(get_post_meta($post->ID, 'layout_settings', true), true);
			//var_dump(json_decode(get_post_meta($post->ID, 'layout_settings', true), true));
			if ( $setting && $setting != '' && isset($setting['header_image']) && $setting['header_image'] != '' ) { $img = $setting['header_image']; } else { $img = ''; }
			$visible = ' visible'; $fake = '';
			if ( $header_type == "2" ) { $header_image = true; $visible = ' visible'; } else { $header_image = false; $visible = ' hidden'; }
			//echo '<a id="header_image_setter" class="meta_btn" href="#">Set Header Image for this Post</a>';
			echo '<div id="' . $key . '" class="header_image custom_settings_box'.$visible.'">';
			echo '<div class="key_header"><span>'.$keys[$key].'</span></div>';
			echo '<div id="header_image_selector">';
			echo '<input type="text" class="header_img" name="header_image" value="' . $img . '">';  
			echo '<div class="header_img_preview"><img src="' . $img . '"></div>';
			echo '<a id="header_image_loader" class="header_img upload" href="#">Upload Header Image for this Post</a>';
			echo '<a id="header_image_remover" class="header_img remove" href="#">Remove Header Image</a>';
			echo '</div>';
			echo '</div>';			
		}		
		if ($key == 'header14_slider') {
		        if ($setting == '') $setting == 'default';
                $visible = ' visible'; $fake = '';
				if ( $header_image ) $visible = ' hidden';
			    echo '<div id="'.$key.'"class="custom_settings_box'.$visible.'">';
				echo '<div class="key_header"><span>'.$keys[$key].'</span></div>';
			    echo "<select id='header14_slider' name='header14_slider".esc_attr($fake.$r_hidden)."' >";
				$sliders = theseo_get_sliders_array();
				if (isset($native_set[$key])) {
				    if (count($sliders) > 1) $sliders = theseo_array_insert($sliders, array( 'default' => 'Use Global Slider'), 0);
				    foreach ($sliders as $alias => $name) {
					//echo $alias."=>".$setting;
				        if (preg_match('/'.$alias.'/', $setting)) { echo "<option value='".esc_attr($alias)."' selected>".esc_attr($name).'</option>'; } 
					    else { echo "<option value='".esc_attr($alias)."'>".esc_attr($name).'</option>'; }				
				    }
				}
				else {
				    echo "<option value='default' selected>Use Global Slider</option>";
					foreach ($sliders as $alias => $name) {
					    echo "<option value='".esc_attr($alias)."'>".esc_attr($name).'</option>';
					}
				}
				echo "</select>";
				echo "</div>";				
		}					
		if ($key == 'sidebar-layout') {
		    show_img_selector($key, array($home.'/images/framework/nosidebar.gif', $home.'/images/framework/2sidebars.gif', 
			                              $home.'/images/framework/leftsidebar.gif', $home.'/images/framework/rightsidebar.gif'),
										  $setting, $keys, $r_hidden);
			echo '<div id="sidebar_widgets_box">';							   
		}
		if ($key == 'left_sidebar_widgets') {
		    echo '<div id="'.$key.'" class="custom_settings_box">';
			echo '<div class="key_header"><span>'.$keys[$key].'</span></div>';
			echo '<select id="'.$key.'" class="regular_widget" name='.esc_attr($key.$r_hidden).' >';
			foreach ($sidebars as $id => $name) {
			    if ($id == $setting) { $selected = ' selected '; } else { $selected = ' '; }
			    echo '<option '.$selected.' value="'.$id.'">'.$name.'</option>';
			}
			echo '</select>';
			echo '</div>';
		}
		if ($key == 'right_sidebar_widgets') {
		    echo '<div id="'.$key.'" class="custom_settings_box">';
			echo '<div class="key_header"><span>'.$keys[$key].'</span></div>';
			echo '<select id="'.$key.'" class="regular_widget" name='.$key.$r_hidden.' >';
			foreach ($sidebars as $id => $name) {
			    if ($id == $setting) { $selected = ' selected '; } else { $selected = ' '; }
			    echo '<option '.$selected.' value="'.$id.'">'.$name.'</option>';			    
			}
            echo '</select>';			
			echo '</div>';	
            echo '</div>';			
		}		
	}
	
	echo "</div>";
	

		
	echo '<div id="blog_post_layout_settings_box" '.'class="'.$visible_settings_box.$b_class.'"'.' n='.$post->ID.'>';
	
	foreach ($blog_keys as $key=>$description) {
	    if (!isset($settings[$key])) 
		    { if (isset($secretlab[$key])) $setting = $secretlab[$key];  } 
		else 
		    { $setting = $settings[$key]; }
		if ($key == 'blog-layout') {
		    show_img_selector($key, array($home.'/images/framework/wide.gif', $home.'/images/framework/boxed.gif'), $setting, $blog_keys, $b_hidden);
		}
		if ($key == 'blog-boxed-background-color') {
		    if (!empty($setting)) {
                if (!is_array($setting)) {			
			        $color = $setting; 
				}
				else {
				    $color = $setting['color'];
				}
			} 
			else { $color = ''; }
		    echo '<div id="'.$key.'" class="custom_settings_box">';
			echo '<div class="key_header"><span>'.$blog_keys[$key].'</span></div>';
		    echo '<input id="'.$key.'_input" class="wp-color-picker" name="'.esc_attr($key.$b_hidden).'" value='.esc_attr($color).' >';
			echo '</div>';
		}
		if ($key == 'blog-boxed-background') {
			echo '<div id="'.$key.'"class="custom_settings_box'.$visible.'">';
			echo '<div class="key_header"><span>'.$blog_keys[$key].'</span></div>';
			echo "<div id='blog_boxed_img_uploader'>";
			echo "<div id='blog_boxed_upload_img_box'>";
			    if( empty($setting)) {
					//$setting = array('url' => get_template_directory_uri().'/images/head1.jpg', 'id' => 0);
                }
                else {
					if (! is_array($setting)) {
					    $setting = json_decode($setting, true);
				    }
                }
				if (!isset($setting) || $setting['url'] == '') { $visible_el = 'hidden'; } else	{ $visible_el = 'visible'; }
			    echo "<img id='blog-boxed_img_holder' class='".$visible_el."' img_n=".$setting['id']." src='".esc_url($setting['url'])."' width='200px' height='200px'>";					    
                echo"</div>";					
			    echo "<div id='boxed_upload_link_box'>";
					echo "<a href=# id='blog_boxed_change_img'>Add/change bg image</a><br>";
                    echo "<a href=# class='".$visible_el."' id='blog_boxed_remove_img'>Remove Background Image</a>";						
				echo "</div>";
			echo "</div>";
			echo "<input id='".$key."_input' class='hidden' type='text' value='".esc_attr(json_encode(array('url' => $setting['url'], 'id' => $setting['id'])))."' name=".esc_attr($key.$b_hidden)." >";
			echo "</div>";		
		    
		}		
		if ($key == 'blog-sidebar-layout') {
		    show_img_selector($key, array($home.'/images/framework/nosidebar.gif', $home.'/images/framework/2sidebars.gif', 
			                              $home.'/images/framework/leftsidebar.gif', $home.'/images/framework/rightsidebar.gif'),
										  $setting, $blog_keys, $b_hidden);
            if ($setting == 1) { $show_blog_columns = ' visible'; } else { $show_blog_columns = ' hidden'; }			
		}
		/*if ($key == 'blog-columns') {
		    echo '<div id="'.$key.'" class="custom_settings_box '.$show_blog_columns.'">';
			echo '<div class="key_header"><span>'.$blog_keys[$key].'</span></div>';
			echo show_select_buttons(array('setting' => $setting, 'name_postfix'=> $b_hidden, 'key' => $key, 
			                               'var1' => 1, 'class1' => ' selected', 'caption1' => '1 column',
										   'var2' => 2, 'class2' => ' unselected', 'caption2' => '2 columns'));
			echo '</div>';
		}*/
		if ( $key == 'blog-header_type' ) {
			$setting = json_decode(get_post_meta($post->ID, 'layout_settings', true), true);
			if ( isset( $setting['blog-header_type'] ) && $setting['blog-header_type'] != '' ) { $setting = $header_type = $setting['blog-header_type']; } else { $setting = $header_type = null; }
			$visible = ' visible'; $fake = $r_hidden = ''; 
			echo '<div id="'.$key.'"class="custom_settings_box'.$visible.'">';
			echo '<div class="key_header"><span>'.$keys[$key].'</span></div>';
			echo "<select id='blog-header_type' name='blog-header_type".esc_attr($fake.$r_hidden)."' >";
			echo '<option value="1" ' . selected( $setting, 1 , false ) . '>' . esc_attr__( 'Revolution Slider', 'the-seo' ) . '</option>';
            echo '<option value="2" ' . selected( $setting, 2 , false ) . '>' . esc_attr__( 'Static Image', 'the-seo' ) . '</option>';
			echo '</select>';
			echo '</div>';			
		}		
		if ( $key == 'blog-header_image' ) {
			$setting = json_decode(get_post_meta($post->ID, 'layout_settings', true), true);
			//var_dump(json_decode(get_post_meta($post->ID, 'layout_settings', true), true));
			if ( $setting && $setting != '' && isset($setting['blog-header_image']) && $setting['blog-header_image'] != '' ) { $img = $setting['blog-header_image']; $visible = ' visible'; } else { $img = ''; $visible = ' hidden'; }
			if ( $header_type == "2" ) { $header_image = true; $visible = ' visible'; } else { $header_image = false; $visible = ' hidden'; }
			//echo '<a id="blog-header_image_setter" class="meta_btn" href="#">Set Header Image for this Post</a>';
			echo '<div id="' . $key . '" class="header_image custom_settings_box'.$visible.'">';
			echo '<div class="key_header"><span>'.$keys[$key].'</span></div>';
			echo '<div id="blog-header_image_selector">';
			echo '<input type="text" class="header_img" name="blog-header_image" value="' . $img . '">'; 			
			echo '<div class="header_img_preview"><img src="' . $img . '"></div>';
			echo '<a id="blog-header_image_loader" class="header_img upload" href="#">Upload Header Image for this Post</a>';
			echo '<a id="blog-header_image_remover" class="header_img remove" href="#">Remove Header Image</a>';
			echo '</div>';
			echo '</div>';			
		}		
		if ($key == 'sidebar-layout') {
		    show_img_selector($key, array($home.'/framework/images/nosidebar.gif', $home.'/framework/images/2sidebars.gif', 
			                              $home.'/framework/images/leftsidebar.gif', $home.'/framework/images/rightsidebar.gif'),
										  $setting, $keys, $r_hidden);
			echo '<div id="sidebar_widgets_box">';							   
		}
		if ($key == 'blog_left_sidebar_widgets') {
		    echo '<div id="'.$key.'" class="custom_settings_box">';
			echo '<div class="key_header"><span>'.$blog_keys[$key].'</span></div>';
			echo '<select id="'.$key.'" class="regular_widget" name='.$key.$b_hidden.' >';
			foreach ($sidebars as $id => $name) {
			    if ($id == $setting) { $selected = ' selected '; } else { $selected = ' '; }
			    echo '<option '.$selected.' value="'.$id.'">'.$name.'</option>';
			}
			echo '</select>';
			echo '</div>';
		}
		if ($key == 'blog_right_sidebar_widgets') {
		    echo '<div id="'.$key.'" class="custom_settings_box">';
			echo '<div class="key_header"><span>'.$blog_keys[$key].'</span></div>';
			echo '<select id="'.$key.'" class="regular_widget" name='.esc_attr($key.$b_hidden).' >';
			foreach ($sidebars as $id => $name) {
			    if ($id == $setting) { $selected = ' selected '; } else { $selected = ' '; }
			    echo '<option '.$selected.' value="'.$id.'">'.$name.'</option>';			    
			}
            echo '</select>';			
			echo '</div>';	
            //echo '</div>';			
		}		
		/*if ($key == 'blog-header-layout') {
            show_img_selector($key, array($home.'/framework/images/h1.jpg', $home.'/framework/images/h2.jpg', 
			                              $home.'/framework/images/h3.jpg', $home.'/framework/images/h4.jpg',
										  $home.'/framework/images/h5.jpg'),
										  $setting, $blog_keys, $b_hidden);	


		<title><?php  _e( 'Application Authentication Request', 'woocommerce' ); ?></title>
        <title><?php  echo get_bloginfo( 'name', 'display' ); ?></title>		
		}*/
		if ($key == 'blog-header14_slider') {
		        if ($setting == '') $setting == '0';
                $visible = ' visible'; $fake = '';
				if ( $header_image ) $visible = ' hidden';
			    echo '<div id="'.$key.'"class="custom_settings_box'.$visible.'">';
				echo '<div class="key_header"><span>'.$blog_keys[$key].'</span></div>';
			    echo "<select id='blog-header14_slider' name='blog-header14_slider".esc_attr($fake.$b_hidden)."' >";
				$sliders = theseo_get_sliders_array();
				if (isset($native_set[$key])) {
				    if (count($sliders) > 1) $sliders = theseo_array_insert($sliders, array( 'default' => 'Use Global Slider'), 0);
				    foreach ($sliders as $alias => $name) {
				        if (preg_match('/'.$setting.'/', $alias)) { echo "<option value='".esc_attr($alias)."' selected>".esc_attr($name).'</option>'; } 
					    else { echo "<option value='".esc_attr($alias)."'>".esc_attr($name).'</option>'; }				
				    }
				}
				else {
				    echo "<option value='default' selected>Use Global Slider</option>";
					foreach ($sliders as $alias => $name) {
					    echo "<option value='".esc_attr($alias)."'>".esc_attr($name).'</option>';
					}
				}
				echo "</select>";
				echo "</div>";		
		}
        /*if ($key == 'is_related_posts') {
		    echo '<div id="'.$key.'"class="custom_settings_box switcher">';
			echo '<div class="key_header"><span>'.$blog_keys[$key].'</span></div>';
		    echo show_select_buttons(array('key' => $key, 'setting' => $setting, 'name_postfix' => $b_hidden, 'var1' => 1, 'class1' => ' on', 'caption1' => 'On',
			                          'var2' => 0, 'class2' => ' off', 'caption2' => 'Off'));
			echo '</div>';
		}
        if ($key == 'show_post_author') {
		    echo '<div id="'.$key.'"class="custom_settings_box switcher">';
			echo '<div class="key_header"><span>'.$blog_keys[$key].'</span></div>';
		    echo show_select_buttons(array('key' => $key, 'setting' => $setting, 'name_postfix' => $b_hidden, 'var1' => 1, 'class1' => ' on', 'caption1' => 'On',
			                          'var2' => 0, 'class2' => ' off', 'caption2' => 'Off'));
			echo '</div>';
		}
        if ($key == 'show_post_category') {
		    echo '<div id="'.$key.'"class="custom_settings_box switcher">';
			echo '<div class="key_header"><span>'.$blog_keys[$key].'</span></div>';
		    echo show_select_buttons(array('key' => $key, 'setting' => $setting, 'name_postfix' => $b_hidden, 'var1' => 1, 'class1' => ' on', 'caption1' => 'On',
			                          'var2' => 0, 'class2' => ' off', 'caption2' => 'Off'));
			echo '</div>';
		}
        if ($key == 'show_post_tags') {
		    echo '<div id="'.$key.'"class="custom_settings_box switcher">';
			echo '<div class="key_header"><span>'.$blog_keys[$key].'</span></div>';
		    echo show_select_buttons(array('key' => $key, 'setting' => $setting, 'name_postfix' => $b_hidden, 'var1' => 1, 'class1' => ' on', 'caption1' => 'On',
			                          'var2' => 0, 'class2' => ' off', 'caption2' => 'Off'));
			echo '</div>';
		}
        if ($key == 'show_post_date') {
		    echo '<div id="'.$key.'"class="custom_settings_box switcher">';
			echo '<div class="key_header"><span>'.$blog_keys[$key].'</span></div>';
		    echo show_select_buttons(array('key' => $key, 'setting' => $setting, 'name_postfix' => $b_hidden, 'var1' => 1, 'class1' => ' on', 'caption1' => 'On',
			                          'var2' => 0, 'class2' => ' off', 'caption2' => 'Off'));
			echo '</div>';
		}
        if ($key == 'show_comments_count') {
		    echo '<div id="'.$key.'"class="custom_settings_box switcher">';
			echo '<div class="key_header"><span>'.$blog_keys[$key].'</span></div>';
		    echo show_select_buttons(array('key' => $key, 'setting' => $setting, 'name_postfix' => $b_hidden, 'var1' => 1, 'class1' => ' on', 'caption1' => 'On',
			                          'var2' => 0, 'class2' => ' off', 'caption2' => 'Off'));
			echo '</div>';
		}*/		
	}
	
	echo '</div>';	

}



function shop_settings_proccess() {

    global $post, $secretlab, $keys, $shop_keys;
	
	$regular_page = false;
	$s_hidden = ''; $s_class = ''; $r_hidden = '_locked'; $r_class = 'locked '; $b_hidden = '_locked'; $b_class = 'locked ';	
	
	$settings = json_decode(get_post_meta($post->ID, 'layout_settings', true), true); 
	$home = get_template_directory_uri();

	if (count($settings) > 0) { $active = 'checked'; $visible_settings_box = 'visible '; $box = $settings; } 
	else { $active = 'checked'; $visible_settings_box = 'visible '; $box = $secretlab;}	
	
	echo '<div id="shop_post_layout_settings_box" '.'class="visible"'.' n='.$post->ID.'>';
	echo "<div id='subtitle'>".'<a id="manage_type_selector_input" class="meta_btn" href="#">Remove Individual Settings</a>'."</div>";
	echo '<input id="sealed" class="hidden" name="sealed" type="text" value="1" >';
	//echo '<input id="manage_type_selector_input" name="manage_type_selector" type="checkbox" '.$active.' >';
	
	foreach ($shop_keys as $key=>$description) {
	    if (!isset($settings[$key])) $setting = $secretlab[$key]; else $setting = $settings[$key];

		if ($key == 'shop-sidebar-layout') {
		    show_img_selector($key, array($home.'/images/framework/nosidebar.gif', $home.'/images/framework/2sidebars.gif', 
			                              $home.'/images/framework/leftsidebar.gif', $home.'/images/framework/rightsidebar.gif'),
										  $setting, $shop_keys, $s_hidden);										  
		}
		if ( $key == 'shop-header_type' ) {
			$setting = json_decode(get_post_meta($post->ID, 'layout_settings', true), true);
			if ( isset( $setting['shop-header_type'] ) && $setting['shop-header_type'] != '' ) { $setting = $header_type = $setting['shop-header_type']; } else { $setting = $header_type = null; }
			$visible = ' visible'; $fake = ''; 
			echo '<div id="'.$key.'"class="custom_settings_box'.$visible.'">';
			echo '<div class="key_header"><span>'.$keys[$key].'</span></div>';
			echo "<select id='shop-header_type' name='shop-header_type".esc_attr($fake.$r_hidden)."' >";
			echo '<option value="1" ' . selected( $setting, 1 , false ) . '>' . esc_attr__( 'Revolution Slider', 'the-seo' ) . '</option>';
            echo '<option value="2" ' . selected( $setting, 2 , false ) . '>' . esc_attr__( 'Static Image', 'the-seo' ) . '</option>';
			echo '</select>';
			echo '</div>';			
		}		
		if ( $key == 'shop-header_image' ) {
			$setting = json_decode(get_post_meta($post->ID, 'layout_settings', true), true);
			//var_dump(json_decode(get_post_meta($post->ID, 'layout_settings', true), true));
			if ( $setting && $setting != '' && isset($setting['shop-header_image']) && $setting['shop-header_image'] != '' ) { $img = $setting['shop-header_image']; $visible = ' visible'; } else { $img = ''; $visible = ' hidden'; }
			if ( $header_type == "2" ) { $header_image = true; $visible = ' visible'; } else { $header_image = false; $visible = ' hidden'; }
			echo '<a id="shop-header_image_setter" class="meta_btn" href="#">Set Header Image for this Post</a>';
			echo '<div id="' . $key . '" class="header_image custom_settings_box'.$visible.'">';
			echo '<div class="key_header"><span>'.$keys[$key].'</span></div>';
			echo '<div id="shop-header_image_selector">';
			echo '<input type="text" class="header_img" name="shop-header_image" value="' . $img . '">'; 			
			echo '<div class="header_img_preview"><img src="' . $img . '"></div>';
			echo '<a id="shop-header_image_loader" class="header_img upload" href="#">Upload Header Image for this Post</a>';
			echo '<a id="shop-header_image_remover" class="header_img remove" href="#">Remove Header Image</a>';
			echo '</div>';
			echo '</div>';			
		}		
		if ($key == 'shop-header14_slider') {
                $visible = ' visible'; $fake = '';
				if ( $header_image ) $visible = ' hidden';
			    echo '<div id="'.$key.'"class="custom_settings_box'.$visible.'">';
				echo '<div class="key_header"><span>'.$shop_keys[$key].'</span></div>';
			    echo "<select id='shop-header14_slider' name='shop-header14_slider".esc_attr($fake.$s_hidden)."' >";
				$sliders = theseo_get_sliders_array();
				if (isset($native_set[$key])) {
				    if (count($sliders) > 1) $sliders = theseo_array_insert($sliders, array( 'default' => 'Use Global Slider'), 0);
				    foreach ($sliders as $alias => $name) {
				        if (preg_match('/'.$alias.'/', $setting)) { echo "<option value='".esc_attr($alias)."' selected>".esc_attr($name).'</option>'; } 
					    else { echo "<option value='".esc_attr($alias)."'>".esc_attr($name).'</option>'; }				
				    }
				}
				else {
				    echo "<option value='default' selected>Use Global Slider</option>";
					foreach ($sliders as $alias => $name) {
					    echo "<option value='".esc_attr($alias)."'>".esc_attr($name).'</option>';
					}
				}
				echo "</select>";
				echo "</div>";		
		}	
	}
	
	echo '</div>';	
		
	echo "</div>";
	
}




add_action('save_post', 'settings_save');

function settings_save($id) {

    global $secretlab, $keys, $blog_keys, $shop_keys;
	
	if (!isset($_POST['sealed']) || $_POST['sealed'] == 1) return;
	
	$fields = array_merge($keys, $blog_keys);
	$fields = array_merge($fields, $shop_keys);
	
	foreach ($fields as $key=>$description) {
	    //$str .= " ".$key.'='.$_POST[$key].';';
	    if (isset($_POST[$key])) { 
		    $settings[$key] = $_POST[$key]; 
			if ($_POST[$key] == 'on') $settings[$key] = 1; 		
		}
	}
	

	$settings = json_encode($settings);
	
	//file_put_contents('1.txt', $settings);
	
	if ($settings && $settings != 'null') {
	    update_post_meta($id, 'layout_settings', $settings);   	
	}

}


add_action( 'admin_enqueue_scripts', 'enqueue_setting_scripts' );

function enqueue_setting_scripts($hook) {

    global $post;

    if ($hook == 'post.php' || $hook == 'page.php' | $hook == 'post-new.php') {
	
	    wp_enqueue_style( 'wp-color-picker' ); 
	
	    if ($post->post_type == 'product') {
		
            wp_register_script('admin_shop_settings_script', plugin_dir_url(__FILE__) . 'shop_settings.js', array('jquery', 'wp-color-picker'), false, true); 
            wp_enqueue_script('admin_shop_settings_script');		
		
		}
		else {
		
            wp_register_script('admin_settings_script', plugin_dir_url(__FILE__) . 'settings.js', array('jquery', 'wp-color-picker'), false, true); 
            wp_enqueue_script('admin_settings_script');
			
	    }
		
			
        wp_register_style('admin_settings_css', plugin_dir_url(__FILE__) . 'settings.css', array(), false, 'all');
        wp_enqueue_style('admin_settings_css');	
	}
}





?>