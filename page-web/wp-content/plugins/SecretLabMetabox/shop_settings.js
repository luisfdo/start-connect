jQuery(document).ready(function() {
    var val, elt, i,
	    keys = ['shop-sidebar-layout', 'shop-header14_slider'],
		loader = jQuery("<div id='ajax_loader'><div>");
		   
	function imgSelectorHandler(key) {
        val = jQuery('#' + key +'_input').val();	
        jQuery('#' + key + ' .custom_setting').each(function (i, el) {
	        if (val == i + 1) { 
		        jQuery(el).addClass('active');
		    }
		    jQuery(el).attr('n', i + 1);
	    });
	
	    jQuery('#' + key).on('click', function(e) {

	        jQuery('#' + key + ' .active').removeClass('active');
		    jQuery(e.target.parentNode).addClass('active');
		    jQuery('#' + key + '_input').attr('value', jQuery(e.target.parentNode).attr('n'));
			
			jQuery('input#sealed').attr('value', 0); 
			
			});
	}
	
	for ( i = 0; i < keys.length; i++) {
	    imgSelectorHandler(keys[i]);
	}	
	
	jQuery('#manage_type_selector_input').on('click', function (e) {
	
	    var id = jQuery('#shop_post_layout_settings_box').attr('n');
		
		e.preventDefault();
	
		    loader.css( { display : 'block' } );
            jQuery.ajax({		
		        url : localajax['url'], 
			    method : 'POST',
			    data : 'action=handle_post_layout&post_id=' + id,
			    complete : function(answer) {
				    var arr = keys;
					jQuery('.active').each(function(i, el) {
					    jQuery(el).removeClass('active');
					});
				    for (i = 0; i < arr.length; i++) {
					    el = jQuery("[name^='"+arr[i]+"']")[0];
					    name = jQuery(el).attr('name');
					    if (name && !name.match(/_locked/)) {
					        jQuery(el).attr('name', name + '_locked');
					    }
				    }					
					
					loader.css( { display : 'none' } );
					
	                for ( i = 0; i < keys.length; i++) {
	                    imgSelectorHandler(keys[i]);
	                }					
					
		        }
			
            });

	});	
	
	jQuery('#subtitle').append(loader);

	loader.css({ position : 'absolute', top : (jQuery('#subtitle').height() - loader.height())/2 + 'px', left : (jQuery('#subtitle').width() - loader.width())/2 + 'px', 
	             display : 'none' });				 
	
	
});