jQuery(document).ready(function() {

var     regular_keys = ["design-layout", "boxed-background", "boxed-background-color", 
                        //"header-topbar", "header-layout",
                        "header_type",
                        "header_image",						
						"header14_slider",
		                "sidebar-layout", "left_sidebar_widgets", "right_sidebar_widgets"],
		blog_keys = ["blog-layout", "blog-boxed-background", "blog-boxed-background-color", "blog-sidebar-layout", 
		//"blog-columns", 
		"blog_left_sidebar_widgets", "blog_right_sidebar_widgets", 
		             //"blog-header-layout",
					 "blog-header_type",
					 "blog-header_image",	
		             "blog-header14_slider", 
					 //"is_related_posts", "show_post_author", "show_post_category", "show_post_tags", "show_post_date", "show_comments_count"
					 ],
		loader = jQuery("<div id='ajax_loader'><div>");
		blog_cat_id = null;

function handle_regular_posts() {

    var val, elt, i,
	    keys = ['design-layout', 'sidebar-layout' 
		        //,'header-layout'
			   ];

    
	function imgSelectorHandler(key) {
        val = jQuery('#' + key +' input').val(),
        parent;
		
        jQuery('#' + key + ' .custom_setting').each(function (i, el) {
	        if (val == i + 1) { 
		        jQuery(el).addClass('active');
				}
			if (key == 'design-layout') {
				if (val == 2) {
				    jQuery('div#boxed-background-color').removeClass('hidden').addClass('visible');
				    jQuery('div#boxed-background').removeClass('hidden').addClass('visible');
				}
				else {
				    jQuery('div#boxed-background-color').removeClass('visible').addClass('hidden');
				    jQuery('input#boxed-background-color_input').attr('name', 'boxed-background-color_fake');
					jQuery('#boxed-background').removeClass('visible').addClass('hidden');
					jQuery('input#boxed-background_input').attr('name', 'boxed-background_fake');
				}				
            }					
			if (key == 'sidebar-layout') {
				if (val == 3) { 
					jQuery('div#left_sidebar_widgets').removeClass('hidden').addClass('visible');
                    jQuery('div#right_sidebar_widgets').removeClass('visible').addClass('hidden');	
                    jQuery('select#right_sidebar_widgets').attr('name', 'right_sidebar_widgets_fake');						
				}
			    if (val == 2) {
					jQuery('div#left_sidebar_widgets').removeClass('hidden').addClass('visible');
					jQuery('div#right_sidebar_widgets').removeClass('hidden').addClass('visible');
				}
				if (val == 4) { 
					jQuery('div#left_sidebar_widgets').removeClass('visible').addClass('hidden'); 
				    jQuery('div#right_sidebar_widgets').removeClass('hidden').addClass('visible');
					jQuery('select#left_sidebar_widgets').attr('name', 'left_sidebar_widgets_fake');
				}
				if (val == 1) { 
					jQuery('div#left_sidebar_widgets').removeClass('visible').addClass('hidden');
					jQuery('select#left_sidebar_widgets').attr('name', 'left_sidebar_widgets_fake');
					jQuery('div#right_sidebar_widgets').removeClass('visible').addClass('hidden');
					jQuery('select#right_sidebar_widgets').attr('name', 'right_sidebar_widgets_fake');
					/*jQuery('#left_sidebar_blog_widgets').removeClass('visible').addClass('hidden');
					jQuery('select#left_sidebar_blog_widgets').attr('name', 'left_sidebar_blog_widgets_fake');
					jQuery('#right_sidebar_blog_widgets').removeClass('visible').addClass('hidden');
					jQuery('select#right_sidebar_blog_widgets').attr('name', 'right_sidebar_blog_widgets_fake');*/						
				}
			}
		    jQuery(el).attr('n', i + 1);
		});
	
	    jQuery('#' + key).on('click', function(e) {
		
		    parent_dl = jQuery(e.target).parents('div#design-layout');
			parent_sl = jQuery(e.target).parents('div#sidebar-layout');

	        jQuery('#' + key + ' .active').removeClass('active');
		    jQuery(e.target.parentNode).addClass('active');
		    jQuery('input#' + key + '_input').val(jQuery(e.target.parentNode).attr('n'));
			
			if (parent_dl && parent_dl.length > 0) {
			    if (jQuery(e.target.parentNode).attr('n') == 2) {
			        jQuery('#boxed-background-color').removeClass('hidden').addClass('visible');
				    jQuery('input#boxed-background-color_input').attr('name', 'boxed-background-color');
					
					jQuery('#boxed-background').removeClass('hidden').addClass('visible');
					jQuery('input#boxed-background_input').attr('name', 'boxed-background');
			    }
				else {
			        jQuery('#boxed-background-color').removeClass('visible').addClass('hidden');
				    jQuery('input#boxed-background-color_input').attr('name', 'boxed-background-color_fake');

				    jQuery('#boxed-background').removeClass('visible').addClass('hidden');
					jQuery('input#boxed-background_input').attr('name', 'boxed-background_fake');
				}
			}
			if (parent_sl && parent_sl.length > 0) {
				    if (jQuery(e.target.parentNode).attr('n') == 3) {
					    jQuery('#left_sidebar_widgets').removeClass('hidden').addClass('visible');
						jQuery('select#left_sidebar_widgets').attr('name', 'left_sidebar_widgets');
						jQuery('#right_sidebar_widgets').removeClass('visible').addClass('hidden');
						jQuery('select#right_sidebar_widgets').attr('name', 'right_sidebar_widgets_fake');
					}
				    if (jQuery(e.target.parentNode).attr('n') == 4) {
					    jQuery('#right_sidebar_widgets').removeClass('hidden').addClass('visible');
						jQuery('select#right_sidebar_widgets').attr('name', 'right_sidebar_widgets');
						jQuery('#left_sidebar_widgets').removeClass('visible').addClass('hidden');
						jQuery('select#left_sidebar_widgets').attr('name', 'left_sidebar_widgets_fake');					    
					}
				    if (jQuery(e.target.parentNode).attr('n') == 2) {
					    jQuery('#right_sidebar_widgets').removeClass('hidden').addClass('visible');
						jQuery('select#right_sidebar_widgets').attr('name', 'right_sidebar_widgets');
					    jQuery('#left_sidebar_widgets').removeClass('hidden').addClass('visible');
						jQuery('select#left_sidebar_widgets').attr('name', 'left_sidebar_widgets');					
					    
					}
				    if (jQuery(e.target.parentNode).attr('n') == 1) {
						jQuery('#left_sidebar_widgets').removeClass('visible').addClass('hidden');
						jQuery('select#left_sidebar_widgets').attr('name', 'left_sidebar_widgets_fake');
						jQuery('#right_sidebar_widgets').removeClass('visible').addClass('hidden');
						jQuery('select#right_sidebar_widgets').attr('name', 'right_sidebar_widgets_fake');					    
					}					
			}
			
			jQuery('input#sealed').attr('value', 0); 
			
			});
	
	    }
	
	for ( i = 0; i < keys.length; i++) {
	    imgSelectorHandler(keys[i]);
	}
	
	
	jQuery('#subtitle').append(loader);
	loader.css({ position : 'absolute', top : (jQuery('#subtitle').height() - loader.height())/2 + 'px', left : (jQuery('#subtitle').width() - loader.width())/2 + 'px', 
	             display : 'none' });
	
	
	var AltImageUpdate = function ( action, img_id, img_url, input_id, img_holder_id, remove_img_id, key_id ) {
	    var input = jQuery('input#' + input_id);
		if (action != -1) {
		    jQuery('#' + img_holder_id).attr('src', img_url).attr('img_n', img_id).removeClass('hidden').addClass('visible');
			jQuery('#' + remove_img_id).removeClass('hidden').addClass('visible');
		    input.attr('value', JSON.stringify({'url' : img_url, 'id' : img_id})).attr('name', key_id);
		}
		else {
		    jQuery('#' + img_holder_id).removeClass('visible').addClass('hidden');
			jQuery('#' + remove_img_id).removeClass('visible').addClass('hidden');
			//input.attr('name', key_id + '_fake');
			input.attr('value', JSON.stringify({'url' : '', 'id' : 0}));
		}
		jQuery('#post_layuot_settings_box').trigger('change');
		jQuery('input#sealed').attr('value', 0);
    };
	
	jQuery('#boxed_remove_img').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();	
	    AltImageUpdate( -1, null, null, 'boxed-background_input', 'boxed_img_holder', 'boxed_remove_img', 'boxed-background' );
		
		jQuery('input#sealed').attr('value', 0); 
		
	});	
	
	jQuery('#boxed_change_img').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        jQuery('div#boxed-background').trigger('click', { target : jQuery('div#boxed-background') });
                var uploader = wp.media( {
                    title:    'set new image',
                    button:   { text: 'upload image' },
                    multiple: false
                } );
                uploader.on('select', function() {
                    var attachment = uploader.state().get( 'selection' ).first().toJSON();
                    AltImageUpdate( wp.media.view.settings.post.id, attachment.id,  attachment.url, 'boxed-background_input', 'boxed_img_holder', 'boxed_remove_img', 'boxed-background');
                });
                uploader.open();		
	});				
	
	jQuery('div#header14_slider').on('change', function(e) {
		jQuery('input#sealed').attr('value', 0);
	});
		
	jQuery('#post_layout_settings_box').change(function() {
	    jQuery('#sealed').attr('value', 0);
	});
	
	jQuery('#post_layout_settings_box .wp-color-picker').wpColorPicker(
	    { 
		    change : 
			    function (e, ui) {
			        jQuery('input#boxed-background-color_input').attr('value', ui.color.toString()).attr('name', 'boxed-background-color');
					jQuery('#sealed').attr('value', 0);
			    },
                clear: function() {
				    jQuery('input#boxed-background-color_input').attr('value', '').attr('name', 'boxed-background-color');
					jQuery('#sealed').attr('value', 0);
				}				
		}
	);
	
}

    handle_regular_posts();
	
	function get_blog_cat_id() {
	    var inpt;
	    jQuery('#categorychecklist').find('label').each(function(i, el) {
		    if (jQuery(el).html().match(/blog/i)) {
			    inpt = jQuery(el).find('input');
				blog_cat_id = jQuery(inpt).val();
				return false;
			}
		});
	}
	
	get_blog_cat_id();
		
	jQuery('#manage_type_selector_input').on('click', function (e) {
	
	    var id = jQuery('#post_layout_settings_box').attr('n');
		
		e.preventDefault();
	
		    loader.css( { display : 'block' } );
            jQuery.ajax({		
		        url : localajax['url'], 
			    method : 'POST',
			    data : 'action=handle_post_layout&post_id=' + id,
			    complete : function(answer) {
				    var arr = regular_keys.concat(blog_keys);
					jQuery('.active').each(function(i, el) {
					    jQuery(el).removeClass('active');
					});
				    for (i = 0; i < arr.length; i++) {
					    el = jQuery("[name^='"+arr[i]+"']")[0];
					    name = jQuery(el).attr('name');
					    if (name && !name.match(/_locked/)) {
					        jQuery(el).attr('name', name + '_locked');
					    }
				    }
                    handle_regular_posts();	
                    handle_blog_posts();					
					
					loader.css( { display : 'none' } );
		        }
			
            });

	});	
	
	

    jQuery('#categorydiv').change( function(e) {
	    var i, el, name, parent = null, active_blog_cats = [],
		    active_unblog_cats = [], inpt, n = 0, inpt;
		if (!blog_cat_id) return;
		jQuery('#categorychecklist').children('li').each(
		    function (i, el) {
			    if (jQuery(el).attr('id') != 'category-' + blog_cat_id) {
				    inpt = jQuery(el).find('input:checked');
					if (inpt && inpt.length == 1) {
					    active_unblog_cats.push(inpt);
					}
				}
			}); 
		parent = jQuery(e.target).parents('li#category-'+blog_cat_id);
		if (parent && parent.length == 1) {
		    active_blog_cats = parent.find('input:checked');
		    if (jQuery(e.target).is(":checked")) {
			    jQuery('div#post_layout_settings_box').removeClass('unlocked').addClass('locked');
				for (i = 0; i < regular_keys.length; i++) {
					el = jQuery("[name^='"+regular_keys[i]+"']")[0];
					name = jQuery(el).attr('name');
					if (name && !name.match(/_locked/)) {
					    jQuery(el).attr('name', name + '_locked');
					}
				}
				for (i = 0; i < blog_keys.length; i++) {
				    el = jQuery("[name^='"+blog_keys[i]+"']")[0];
				    name = jQuery(el).attr('name').replace(/_locked/g, '');
					jQuery(el).attr('name', name);
				}
                jQuery('div#blog_post_layout_settings_box').removeClass('locked').addClass('unlocked');	
                jQuery('div#blog_post_layout_settings_box').removeClass('hidden').addClass('visible');					
			}
			else {	
                if (active_unblog_cats.length == 0) return;			
			    jQuery('div#blog_post_layout_settings_box').removeClass('unlocked').addClass('locked');
				for (i = 0; i < blog_keys.length; i++) {
				    el = jQuery("[name^='"+blog_keys[i]+"']")[0];
					name = jQuery(el).attr('name');
					if (name && !name.match(/_locked/)) {
					    jQuery(el).attr('name', name + '_locked');
					}
				}
				for (i = 0; i < regular_keys.length; i++) {
				    el = jQuery("[name^='"+regular_keys[i]+"']")[0];
				    name = jQuery(el).attr('name').replace(/_locked/g, '');
					jQuery(el).attr('name', name);
				}
                jQuery('div#post_layout_settings_box').removeClass('locked').addClass('unlocked');
				jQuery('div#post_layout_settings_box').removeClass('hidden').addClass('visible');				
			}
		}
		else {
		    active_blog_cats = jQuery('li#category-' + blog_cat_id).find('input:checked');
			if (active_blog_cats.length > 0) return false;
			else if (jQuery(e.target).is(":checked")) {
			    jQuery('div#blog_post_layout_settings_box').removeClass('unlocked').addClass('locked');
				for (i = 0; i < blog_keys.length; i++) {
				    el = jQuery("[name^='"+blog_keys[i]+"']")[0];
					name = jQuery(el).attr('name');
					if (name && !name.match(/_locked/)) {
					    jQuery(el).attr('name', name + '_locked');
					}
				}
				for (i = 0; i < regular_keys.length; i++) {
				    el = jQuery("[name^='"+regular_keys[i]+"']")[0];
				    name = jQuery(el).attr('name').replace(/_locked/g, '');
					jQuery(el).attr('name', name);
				}
                jQuery('div#post_layout_settings_box').removeClass('locked').addClass('unlocked');
				jQuery('div#post_layout_settings_box').removeClass('hidden').addClass('visible');			    
			}
			else {
			    jQuery('div#post_layout_settings_box').removeClass('unlocked').addClass('locked');
				for (i = 0; i < regular_keys.length; i++) {
					el = jQuery("[name^='"+regular_keys[i]+"']")[0];
					name = jQuery(el).attr('name');
					if (name && !name.match(/_locked/)) {
					    jQuery(el).attr('name', name + '_locked');
					}
				}
				for (i = 0; i < blog_keys.length; i++) {
				    el = jQuery("[name^='"+blog_keys[i]+"']")[0];
				    name = jQuery(el).attr('name').replace(/_locked/g, '');
					jQuery(el).attr('name', name);
				}
                jQuery('div#blog_post_layout_settings_box').removeClass('locked').addClass('unlocked');	
                jQuery('div#blog_post_layout_settings_box').removeClass('hidden').addClass('visible');			
			}
		}
		jQuery('#sealed').attr('value', 0);
	});
	
    jQuery('.regular_widget').on('change', function(e) {
	    jQuery('#sealed').attr('value', 0);  
	});
	
	jQuery('.switcher').on('click', function(e) {
	    e.preventDefault();
		e.stopPropagation();	
        jQuery('input#'+this.id+'_input').attr('value', jQuery(e.target).attr('val'));
        jQuery('#'+this.id+' a.meta_btn.on').removeClass('on').addClass('off');	
		jQuery(e.target).removeClass('off').addClass('on');
		jQuery('#sealed').attr('value', 0);
	});
	
	
	
	
	
function handle_blog_posts() {

    var val, elt, i,
	    keys = ['blog-layout', 'blog-sidebar-layout'
		        //, 'blog-header-layout'
			   ];

    
	function imgSelectorHandler(key) {
        val = jQuery('#' + key +' input').val(),
        parent;
		
        jQuery('#' + key + ' .custom_setting').each(function (i, el) {
	        if (val == i + 1) { 
		        jQuery(el).addClass('active');
				}
			if (key == 'blog-layout') {
				if (val == 2) {
				    jQuery('div#blog-boxed-background-color').removeClass('hidden').addClass('visible');
				    jQuery('div#blogboxed-background').removeClass('hidden').addClass('visible');
				}
				else {
				    jQuery('div#blog-boxed-background-color').removeClass('visible').addClass('hidden');
				    jQuery('input#blog-boxed-background-color_input').attr('name', 'blog-boxed-background-color_fake');
					jQuery('#blog-boxed-background').removeClass('visible').addClass('hidden');
					jQuery('input#blog-boxed-background_input').attr('name', 'blog-boxed-background_fake');
				}
            }					
			if (key == 'blog-sidebar-layout') {
				if (val == 1) { 
					jQuery('div#blog-columns').removeClass('hidden').addClass('visible');
                    jQuery('input#blog-columns_input').attr('name', 'blog-columns');	

					jQuery('div#blog_left_sidebar_widgets').removeClass('visible').addClass('hidden');
					jQuery('select#blog_left_sidebar_widgets').attr('name', 'blog_left_sidebar_widgets_fake');
					jQuery('div#blog_right_sidebar_widgets').removeClass('visible').addClass('hidden');
					jQuery('select#blog_right_sidebar_widgets').attr('name', 'blog_right_sidebar_widgets_fake');						
					
				}
				if (val == 2) {
					jQuery('div#blog_left_sidebar_widgets').removeClass('hidden').addClass('visible');
					jQuery('div#blog_right_sidebar_widgets').removeClass('hidden').addClass('visible');

				    jQuery('div#blog-columns').removeClass('visible').addClass('hidden');
                    jQuery('input#blog-columns_input').attr('name', 'blog-columns_fake');					
				}
                if (val == 3) {
					jQuery('div#blog_left_sidebar_widgets').removeClass('hidden').addClass('visible');
                    jQuery('div#blog_right_sidebar_widgets').removeClass('visible').addClass('hidden');	
                    jQuery('select#blog_right_sidebar_widgets').attr('name', 'blog_right_sidebar_widgets_fake');

				    jQuery('div#blog-columns').removeClass('visible').addClass('hidden');
                    jQuery('input#blog-columns_input').attr('name', 'blog-columns_fake');					
				}
				if (val == 4) {
					jQuery('div#blog_left_sidebar_widgets').removeClass('visible').addClass('hidden'); 
				    jQuery('div#blog_right_sidebar_widgets').removeClass('hidden').addClass('visible');
					jQuery('select#blog_left_sidebar_widgets').attr('name', 'blog_left_sidebar_widgets_fake');	

				    jQuery('div#blog-columns').removeClass('visible').addClass('hidden');
                    jQuery('input#blog-columns_input').attr('name', 'blog-columns_fake');					
				}
			}
		    jQuery(el).attr('n', i + 1);
		});
	
	    jQuery('#' + key).on('click', function(e) {
		
		    parent_dl = jQuery(e.target).parents('div#blog-layout');
			parent_sl = jQuery(e.target).parents('div#blog-sidebar-layout');

	        jQuery('#' + key + ' .active').removeClass('active');
		    jQuery(e.target.parentNode).addClass('active');
		    jQuery('input#' + key + '_input').val(jQuery(e.target.parentNode).attr('n'));
			
			if (parent_dl && parent_dl.length > 0) {
			    if (jQuery(e.target.parentNode).attr('n') == 2) {
			        jQuery('#blog-boxed-background-color').removeClass('hidden').addClass('visible');
				    jQuery('input#blog-boxed-background-color_input').attr('name', 'blog-boxed-background-color');
					
					jQuery('#blog-boxed-background').removeClass('hidden').addClass('visible');
					jQuery('input#blog-boxed-background_input').attr('name', 'blog-boxed-background');
			    }
				else {
			        jQuery('#blog-boxed-background-color').removeClass('visible').addClass('hidden');
				    jQuery('input#blog-boxed-background-color_input').attr('name', 'blog-boxed-background-color_fake');

				    jQuery('#blog-boxed-background').removeClass('visible').addClass('hidden');
					jQuery('input#blog-boxed-background_input').attr('name', 'blog-boxed-background_fake');
				}
			}
			if (parent_sl && parent_sl.length > 0) {
				    if (jQuery(e.target.parentNode).attr('n') == 1) {
					    jQuery('div#blog-columns').removeClass('hidden').addClass('visible');
                        jQuery('input#blog-columns_input').attr('name', 'blog-columns');

					    jQuery('#blog_left_sidebar_widgets').removeClass('visible').addClass('hidden');
						jQuery('select#blog_left_sidebar_widgets').attr('name', 'blog_left_sidebar_widgets_fake');
						jQuery('#blog_right_sidebar_widgets').removeClass('visible').addClass('hidden');
						jQuery('select#blog_right_sidebar_widgets').attr('name', 'blog_right_sidebar_widgets_fake');						
					}
					if (jQuery(e.target.parentNode).attr('n') == 2) {
					    jQuery('#blog_right_sidebar_widgets').removeClass('hidden').addClass('visible');
						jQuery('select#blog_right_sidebar_widgets').attr('name', 'blog_right_sidebar_widgets');
					    jQuery('#blog_left_sidebar_widgets').removeClass('hidden').addClass('visible');
						jQuery('select#blog_left_sidebar_widgets').attr('name', 'blog_left_sidebar_widgets');	

				        jQuery('div#blog-columns').removeClass('visible').addClass('hidden');
                        jQuery('input#blog-columns_input').attr('name', 'blog-columns_fake');											
					}
				    if (jQuery(e.target.parentNode).attr('n') == 3) {
					    jQuery('#blog_left_sidebar_widgets').removeClass('hidden').addClass('visible');
						jQuery('select#blog_left_sidebar_widgets').attr('name', 'blog_left_sidebar_widgets');
						jQuery('#blog_right_sidebar_widgets').removeClass('visible').addClass('hidden');
						jQuery('select#blog_right_sidebar_widgets').attr('name', 'blog_right_sidebar_widgets_fake');	

				        jQuery('div#blog-columns').removeClass('visible').addClass('hidden');
                        jQuery('input#blog-columns_input').attr('name', 'blog-columns_fake');							
					}
					if (jQuery(e.target.parentNode).attr('n') == 4) {
					    jQuery('#blog_right_sidebar_widgets').removeClass('hidden').addClass('visible');
						jQuery('select#blog_right_sidebar_widgets').attr('name', 'blog_right_sidebar_widgets');
						jQuery('#blog_left_sidebar_widgets').removeClass('visible').addClass('hidden');
						jQuery('select#blog_left_sidebar_widgets').attr('name', 'blog_left_sidebar_widgets_fake');	

				        jQuery('div#blog-columns').removeClass('visible').addClass('hidden');
                        jQuery('input#blog-columns_input').attr('name', 'blog-columns_fake');							
					}
			}
			
			jQuery('input#sealed').attr('value', 0); 
			
			});
	
	    }
	
	for ( i = 0; i < keys.length; i++) {
	    imgSelectorHandler(keys[i]);
	}
	
	/*jQuery('#layout_settings').append(loader);
	loader.css({ position : 'absolute', top : (jQuery('#layout_settings').height() - loader.height())/2 + 'px', left : (jQuery('#layout_settings').width() - loader.width())/2 + 'px', 
	             display : 'none' });*/
	
	
	var AltImageUpdate = function ( action, img_id, img_url, input_id, img_holder_id, remove_img_id, key_id ) {
	    var input = jQuery('input#' + input_id);
		if (action != -1) {
		    jQuery('#' + img_holder_id).attr('src', img_url).attr('img_n', img_id).removeClass('hidden').addClass('visible');
			jQuery('#' + remove_img_id).removeClass('hidden').addClass('visible');
		    input.attr('value', JSON.stringify({'url' : img_url, 'id' : img_id})).attr('name', key_id);
		}
		else {
		    jQuery('#' + img_holder_id).removeClass('visible').addClass('hidden');
			jQuery('#' + remove_img_id).removeClass('visible').addClass('hidden');
			//input.attr('name', key_id + '_fake');
			input.attr('value', JSON.stringify({'url' : '', 'id' : 0}));
		}
		jQuery('#blog-post_layout_settings_box').trigger('change');
		jQuery('input#sealed').attr('value', 0); 
    };
	
	jQuery('#blog_boxed_remove_img').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();	
	    AltImageUpdate( -1, null, null, 'blog-boxed-background_input', 'blog-boxed_img_holder', 'blog-boxed_remove_img', 'blog-boxed-background' );
		
		jQuery('input#sealed').attr('value', 0); 
		
	});	
	
	jQuery('#blog_boxed_change_img').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        jQuery('div#blog-boxed-background').trigger('click', { target : jQuery('div#blog-boxed-background') });
                var uploader = wp.media( {
                    title:    'set new image',
                    button:   { text: 'upload image' },
                    multiple: false
                } );
                uploader.on('select', function() {
                    var attachment = uploader.state().get( 'selection' ).first().toJSON();
                    AltImageUpdate( wp.media.view.settings.post.id, attachment.id,  attachment.url, 'blog-boxed-background_input', 'blog-boxed_img_holder', 'blog-boxed_remove_img', 'blog-boxed-background');
                });
                uploader.open();		
	});				
	
	jQuery('div#blog-header14_slider').on('change', function(e) {
		jQuery('input#sealed').attr('value', 0);
	});
	
	jQuery('#blog_post_layout_settings_box .wp-color-picker').wpColorPicker(
	    { 
		    change : 
			    function (e, ui) {
			        jQuery('input#blog-boxed-background-color_input').attr('value', ui.color.toString()).attr('name', 'blog-boxed-background-color');
					jQuery('#sealed').attr('value', 0);
			    },
                clear: function() {
				    jQuery('input#blog-boxed-background-color_input').attr('value', '').attr('name', 'blog-boxed-background-color');
					jQuery('#sealed').attr('value', 0);
				}				
		}
	);
	
}

handle_blog_posts();

jQuery('#header_type, #blog-header_type, #shop-header_type').on( 'change', function(e) {
	console.log(jQuery(e.target).val());
	console.log(jQuery(e.target).parents('.custom_settings_box').next().attr('class'));
	if ( jQuery(e.target).val() == 2 ) { 
	    jQuery(e.target).parents('.custom_settings_box').next().removeClass( 'hidden' ).addClass( 'visible' );
		jQuery(e.target).parents('.custom_settings_box').next().next().removeClass( 'visible' ).addClass( 'hidden' );
	}
	if ( jQuery(e.target).val() == 1 ) {
		jQuery(e.target).parents('.custom_settings_box').next().removeClass( 'visible' ).addClass( 'hidden' );
		jQuery(e.target).parents('.custom_settings_box').next().next().removeClass( 'hidden' ).addClass( 'visible' );
	}
	jQuery('#sealed').attr('value', 0);
});

jQuery('#header_image_remover, #blog-header_image_remover, #shop-header_image_remover').on( 'click', function(e) {
	
	e.preventDefault();
	jQuery(e.target).parent().find('input.header_img').attr( 'value', '' );
	jQuery(e.target).parent().find('.header_img_preview img').attr( 'src', '' );
	jQuery('#sealed').attr('value', 0);
	return false;
});

jQuery('#header_image_loader, #blog-header_image_loader, #shop-header_image_loader').click(function(e) {
	
	e.preventDefault();
	var image = wp.media({ 
		title: 'Upload Image',
		// mutiple: true if you want to upload multiple files at once
		multiple: false
	}).open()
	.on('select', function(e1){
		var viewport = jQuery(e.target).parent().find('.header_img_preview img'), url;
		var uploaded_image = image.state().get('selection').first();
		var image_url = uploaded_image.toJSON().url; 

		jQuery(e.target).parent().find('input.header_img').attr( 'value', image_url );
		if ( viewport && viewport.length > 0 ) jQuery(viewport).attr('src', image_url);
		jQuery('#sealed').attr('value', 0);
	});
});
	
});