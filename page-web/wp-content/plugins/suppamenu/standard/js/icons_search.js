jQuery(document).ready(function(){
    var quering = false,
	    query = '',
		type,
	    container = jQuery('.suppa_fontAwesome_container div.era_admin_widget_box_header'),
		sb_box = jQuery('<div id="sb_box"></div>'),
	    search_box = jQuery('<input type="text" placeholder="icon name" id="search_box" />'),
		search_results = jQuery('.suppa_fontAwesome_container .era_admin_widget_box_inside'),
		loader = jQuery('<div id="suppa_loader" class="hidden"></div>'),
		q_result,
		request;
	
	sb_box.prepend(search_box);
	sb_box.append(loader);
	container.append(sb_box);
	
	function send_request() {
	
	loader.css({ top : (sb_box.outerHeight() - loader.outerHeight())/2 + 'px', left : search_box.outerWidth() - loader.outerWidth() - 15 + 'px' });
	loader.removeClass('hidden').addClass('visible');
	
	request =   jQuery.ajax({
			    url      : localajax['url'], 
				method   : 'POST',
				cache    : false,
			    action   : 'handle_iconsearch',
				data     : 'action=handle_iconsearch&type=' + type + '&query=' + query,
				
			    success  : function(result) {
				               var r, i, arr = [];
								  
							   r = result.substring(0, result.length - 1);
					           r = jQuery.parseJSON(r);
							   if (r[0] == "1") {
							      arr = r[1];
							      for (i = 0; i < arr.length; i++) {
								      q_result = q_result + '<span class="admin_suppa_fontAwesome_icon_box"><span aria-hidden="true" class="'   + arr[i] + '"></span></span>';													
								  }
								  search_results.html(q_result);
							   }
							   else if (r[0] == "0") {
							       q_result = '';
								   search_results.empty();
							   }
							   loader.removeClass('visible').addClass('hidden');
							   quering = false;
						   }
					   });	
	
	}
	
	function keyup_handler(e) {
		if (request) {
			query = search_box.val(),
		    q_result = '';			
		    request.abort();
			send_request()
		}
		else {
			query = search_box.val(),
			type = 'get_icon';
		    q_result = '';			
		    send_request();
		}
		
	}
	
    search_box.keyup(keyup_handler);
	
	search_results.on('click', function(e) {
	    var id = jQuery('.admin_suppa_addIcon_button').attr('id'),
		    icon;
			
		if (e.target.nodeType == 'span') {
		    icon = jQuery(e.target).attr('class');
			jQuery('.admin_suppa_fontAwesome_icon_hidden-'+id).val( icon );
			jQuery('.admin_suppa_fontAwesome_icon_box_preview-'+id).children('span').attr('class',icon);
			
			jQuery('.era_admin_widgets_container').fadeOut(100);
			jQuery('.suppa_fontAwesome_container').fadeOut(100);

			return false;			
			
		}

	});
	
});