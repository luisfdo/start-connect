<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '' ); // Don't call directly
}

add_action( 'vc_after_init', 'sl_team_3_shortcode' );
add_shortcode('sell_teammates3', 'sl_team_3_path');
		vc_map( array(
			'name'        => __( 'Teammates with carousel. Layout #3', 'sell' ),
			'base'        => 'sell_teammates3',
			'description' => __( 'Display 4 teammates per slide with carousel', 'sell' ),
			'category'    => __( 'SecretLab', 'sell' ),
			'icon'        => 'sell_ai team3',
			'params'      => array(
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Extra Class Name', 'sell' ),
					'param_name'	=> 'extra_class',
					'description'	=> __( 'May be used for styling with CSS', 'sell' ),
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Members quantity ', 'sell' ),
					'param_name'	=> 'posts_per_page',
					'description'	=> __( 'how many members to display on the page?', 'sell' ),
					'value'         => 6
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> __( 'Enable\disable navigation', 'sell' ),
					'param_name'	=> 'enable_nav',
					'description'	=> __( 'set ability of navigation', 'sell' ),
					'value'         => array( 'disable' => 'false',
						'enable'  => 'true', ),
					'default'       => 'true'
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> __( 'Enable\disable autoplay', 'sell' ),
					'param_name'	=> 'autoplay',
					'description'	=> __( 'set autoplay on/off', 'sell' ),
					'value'         => array( 'disable' => 'false',
						'enable'  => 'true', ),
					'default'       => 'true'
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Duration between slides in ms', 'sell' ),
					'param_name'	=> 'speed',
					'description'	=> __( 'time of pause between slides', 'sell' ),
					'value'         => 5000
				)

			)
		));


function sl_team_3_shortcode() {
	// admin area functions

}

function sl_team_3_path( $atts, $content = null ) {
	// front-end functions
	extract( shortcode_atts( array(
		'extra_class'      => '',
		'main_title'       => '',
		'description'      => '',
		'posts_per_page'   => '',
		'enable_nav'       => 'true',
		'autoplay'         => 'false',
		'speed'            => '5000'
	), $atts ) );


	global $secretlab;

	$slick_params = array( 'class'      => $extra_class ? $extra_class : 'teammate_box_three',
		'enable_nav' => $enable_nav,
		'autoplay'   => $autoplay,
		'speed'      => $speed ? $speed : 5000,
		'dots'       => 'true',
		'arrows'     => 'false',
		'sp_row'     => 4,
		'sp_show'    => 4,
		'sp_scroll'  => 4,
		'992'        => array ( 'sp_row' => 3, 'sp_show' => 3, 'sp_scroll' => 3 ),
		'769'        => array ( 'sp_row' => 2, 'sp_show' => 2, 'sp_scroll' => 2 ),
		'521'        => array ( 'sp_row' => 1, 'sp_show' => 1, 'sp_scroll' => 1 ),
	);
	$secretlab['slick'][] = $slick_params;

	$result = new WP_query('post_type=teammate&posts_per_page='.$posts_per_page);

	$output = '';

	//Output posts

	if ( $result->posts ) :

		add_action('wp_footer', 'theseo_add_slick_carousel');

		$output .=  '<div class="carousel '.$slick_params['class'].' team-slide">';
		$i = 1;

		// Loop through posts
		foreach ( $result->posts as $post ) :

			// Post VARS
			$post_id          = $post->ID;
			if (function_exists("types_render_field")) {
			$member_post = types_render_field("post-of-member", array("post_id" => $post_id, "output"=>"normal"));
			//$member_photo = get_the_post_thumbnail($post->ID, 'theseo_smalldigital');
			$member_photo = types_render_field("photo-of-member", array("post_id" => $post_id, "output"=>"raw"));
			$fb_profile = types_render_field("facebook-profile", array("post_id" => $post_id, "output"=>"normal"));
			$yt_profile = types_render_field("youtube-profile", array("post_id" => $post_id, "output"=>"normal"));
			$tw_profile = types_render_field("twitter-profile", array("post_id" => $post_id, "output"=>"normal"));
			$bh_profile = types_render_field("behance-profile", array("post_id" => $post_id, "output"=>"normal"));
			$ln_profile = types_render_field("linkedin-profile", array("post_id" => $post_id, "output"=>"normal"));
			} else {$member_post = $member_photo = $fb_profile = $yt_profile = $tw_profile = $bh_profile = $ln_profile = '';}
			$output .= '<div class="team-item">';
			$output .= '<div class="photo"><img src="'.$member_photo.'" alt="" class="img-responsive"/></div>';
		$output .= '<div class="personal-info"><span class="name"><a href="'.get_permalink($post_id).'" rel="bookmark">'.$post->post_title.'</a></span><span class="desrdivider">'.$member_post.'</span>';
																	
																		$output .= '<p class="team-overlay-link">';
			if (!empty($fb_profile)) {
				$output .= '<a href="'.$fb_profile.'" target="_blank"><i class="fa fa-facebook"></i></a>';
			}
			if (!empty($yt_profile)) {
				$output .= '<a href="'.$yt_profile.'" target="_blank"><i class="fa fa-youtube-play"></i></a>';
			}
			if (!empty($tw_profile)) {
				$output .= '<a href="'.$tw_profile.'" target="_blank"><i class="fa fa-twitter"></i></a>';
			}
			if (!empty($bh_profile)) {
				$output .= '<a href="'.$bh_profile.'" target="_blank"><i class="fa fa-behance"></i></a>';
			}
			if (!empty($ln_profile)) {
				$output .= '<a href="'.$ln_profile.'" target="_blank"><i class="fa fa-linkedin"></i></a>';
			}
			$output .= '                                         </p>
                                                						</div>
                                                						<div class="overmember"><a href="'.get_permalink($post_id).'"><span class="icon-chain-broken"></span></a>
															</div>                            						
                                        				
                                    				</div>';

			$i++;
			// End foreach loop
		endforeach;

		$output .= '</div>';


	endif; // End has posts check



	// Set things back to normal
	$result = null;
	wp_reset_postdata();

	// Return output
	return $output;
}


