<?php

if(!class_exists('sell_price4')){

	class sell_price4 {
	
		function __construct(){
			add_action('init',array($this,'sell_price4_init'));
			add_shortcode('price_table_4',array($this,'sell_price4_shortcode'));
		}
		
		function sl_id_field($settings, $value) {

			if ( ! $value || $value == '' || $value == 'undefined' ) $val = rand( 10001, 100000 ); else $val = $value;
			$out .= '<input name="' . esc_attr( $settings['param_name'] ) . '" class="sl_classes_hidden_input wpb_vc_param_value wpb-textinput ' .
				esc_attr( $settings['param_name'] ) . ' ' .
				esc_attr( $settings['type'] ) . '_field" type="hidden" value="' . esc_attr( $val ) . '" />';
			return $out;	
		
		}					

		function sell_price4_init() {

			vc_add_shortcode_param( 'sl_id', array($this, 'sl_id_field') );
			if(function_exists("vc_map")){
				vc_map( array(
					'name'        => __( 'Price Table Layout#4', 'sell' ),
					'base'        => 'price_table_4',
					'description' => __( 'Displays a Single Price Table', 'symple' ),
					'category'    => __( 'SecretLab', 'symple' ),
					'icon'        => 'sell_ai pricetable4',
					'params'      => array(
						array(
							'type'			=> 'sl_id',
							'heading'		=> __( '', 'sell' ),
							'param_name'	=> 'sl_id4',
							'value'         => null,
							'description'	=> __( '', 'sell' ),
							'group'         => 'Content'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Corner Caption', 'sell' ),
							'param_name'	=> 'title',
							'value'         => 'Most Popular!',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Content'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Main Title', 'sell' ),
							'param_name'	=> 'main_title',
							'value'         => 'Standard',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Content'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Price', 'sell' ),
							'param_name'	=> 'price',
							'value'         => 100,
							'description'	=> __( '', 'sell' ),
							'group'         => 'Content'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Currency', 'sell' ),
							'param_name'	=> 'currency',
							'value'         => '$',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Content'
						),
						array(
							'type'			=> 'attach_image',
							'heading'		=> __( 'Image for Hover Mode', 'sell' ),
							'param_name'	=> 'hover_img',
							'description'	=> __( '', 'sell' ),
							'value'         => '',
							'group'         => 'Content'
						),
						array(
							'type'			=> 'colorpicker',
							'heading'		=> __( 'Main Color', 'sell' ),
							'param_name'	=> 'main_color',
							'description'	=> __( '', 'sell' ),
							'value'         => '#f2f2f2',
							'group'         => 'Content'
						),
						array(
							'type'			=> 'textarea_html',
							'heading'		=> __( 'HTML Content', 'sell' ),
							'param_name'	=> 'content',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Content'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Period', 'sell' ),
							'param_name'	=> 'subtitle',
							'value'         => 'per 6 month',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Content'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Order Button Text', 'sell' ),
							'param_name'	=> 'order_button_text',
							'value'         => 'Choose plan',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Content'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Order Button URL', 'sell' ),
							'param_name'	=> 'order_button_url',
							'value'         => '',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Content'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Extra class ', 'sell' ),
							'param_name'	=> 'extra_class',
							'value'         => '',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Content'
						)

					)
				));
			}
		}

		function sell_price4_shortcode($atts, $content = null) {
			$default = array (
				'sl_id4' =>        null,
				'extra_class'              => '',
				'title'                    => '',
				'main_title'               => 'Basic',
				'price'                    => '50',
				'subtitle'                 => '',
				'hover_img'                => null,
				'main_color'               => '#f2f2f2',
				'currency'                 => '',
				'order_button_text'        => '',
				'order_button_url'        => '',

			);

			$args = vc_map_get_attributes( 'price_table_4', $atts );
			$set = shortcode_atts( $default, $args );

			$content = preg_replace('/^<.p>/', '', $content);
			$html = $content;
			$style = array();

			$id = ( isset ( $set['sl_id4'] ) ) ? $set['sl_id4'] : rand(10001, 100000);
			if ( $set['extra_class'] != '' ) $ex_class = $set['extra_class'] . ' '; else $ex_class = '';
			if ( isset ( $set['hover_img'] ) && is_numeric( $set['hover_img'] ) ) { $img_url = wp_get_attachment_url( $set['hover_img'] ); }
			if ( isset ( $set['main_color'] ) && $set['main_color'] != '' ) {
				$style['hover_block'] = '#pricetable_4_' . $id . ' .hw { background-color : ' . $set['main_color'] . '; }';
				$style['body_border'] = '#pricetable_4_' . $id . ' .body-price-wrapper { border-color : ' . $set['main_color'] . '; }';
				$style['wrapper'] = '#pricetable_4_' . $id . ' .header-price-wrapper-4 { background-color : ' . $set['main_color'] . '; }';
				/* $style['icons'] = '#pricetable_4_' . $id . ' li i { color : ' . $set['main_color'] . '; }';*/
				$style['button'] = '#pricetable_4_' . $id . ' div.ult_price_link-4 a.btn { border-color : ' . $set['main_color'] . '!important; color : ' . $set['main_color'] . '!important; }';
				$style['button_hove'] = '#pricetable_4_' . $id . ' div.ult_price_link-4 a.btn:hover { background-color : ' . $set['main_color'] . '!important;color:#fff !important; }';
			}

			if ( count ( $style ) > 0 ) { $out = '<style type="text/css">' . implode( ' ', $style ) . '</style>'; }

			$out .= '<div id="pricetable_4_' . $id . '" class="' . $ex_class . 'price-table-4">
		            <div class="hw">
					    <div class="most-text">';
			if ($set['title'] != '') {
				$out .= $set['title'];
			}
						$out .= '</div>
						<h2>' . $set['main_title'] . '</h2>
						<div class="cost-money-4">
							<span class="price-cost-4">' . $set['price'] . '</span>
							<span class="money-currency-4">' . $set['currency'] . '</span>
							<span class="price-time-4">' . $set['subtitle'] . '</span>
						</div>
						<div class="pict"><img src="' . $img_url . '" alt=""></div>
					</div>
					<div class="body-price-wrapper-4">
					    <div class="body-price-4 body-price-inner">' . $html . '</div>
					<div class="ult_price_link-4">';
			if ($set['order_button_text'] != '' && $set['order_button_url'] != '') {
				$out .= '<a class="btn btn-default" href="' . $set['order_button_url']  .'">' . $set['order_button_text'] . '</a>';
			}
			$out .= '</div>
				</div>
			</div>';


			return $out;
		}

	}

}

new sell_price4();

if(class_exists('WPBakeryShortCode'))
{
	class WPBakeryShortCode_sell_price4 extends WPBakeryShortCode {
	}
}	

