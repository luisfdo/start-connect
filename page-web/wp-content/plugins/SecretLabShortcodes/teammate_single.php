


<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '' ); // Don't call directly
}

add_action( 'vc_after_init', 'sl_single_team_shortcode' );
add_shortcode('single_teammate', 'sl_single_team_path');

		vc_map(array(
			'name' => __('Single Teammate', 'sell'),
			'base' => 'single_teammate',
			'description' => __('Display Info About Single Teammate', 'sell'),
			'category' => __('SecretLab', 'sell'),
			//'admin_enqueue_css' => trailingslashit( plugins_url( 'sl_shortcodes' ) ) . 'css/admin.css',
			'icon' => 'sell_ai team3',
			'params' => array(
				array(
					'type' => 'dropdown',
					'heading' => __('Teammates', 'sell'),
					'param_name' => 'teammate',
					'description' => __('', 'sell'),
					'group' => 'Content',
					'value' => $this->get_teammates()
				),
			)
		));


function sl_single_team_shortcode() {
	// admin area functions
	function get_teammates() {
		$args = array('post_type' => array('teammate'),
			'orderby' => 'date',
			'order' => 'DESC');

		$teammates = new WP_Query($args);

		$out = array();

		foreach ($teammates->posts as $tm) {
			$out[$tm->post_title] = $tm->ID;
		}

		if (count($out > 0)) return $out; else return 'There is no teammates found';

	}
}

function sl_single_team_path( $atts, $content = null ) {
	// front-end functions
	$defaults = array('teammate' => false);
	$args = vc_map_get_attributes('single_teammate', $atts);
	$set = shortcode_atts($defaults, $args);

	$out = '';

	if ($set['teammate']) {
		$tm = get_post($set['teammate']); // Take a post
		if (function_exists("types_render_field")) {
		$member_post = types_render_field("post-of-member", array("post_id" => $tm->ID, "output" => "normal"));
		$member_photo = types_render_field("photo-of-member", array("post_id" => $tm->ID, "output" => "raw"));
		} else {$member_post = $member_photo = '';}

		$out .= get_the_title($tm->ID);
		$out .= '<div><img src="' . esc_url($member_photo) . '" alt="' . get_the_title($tm->ID) . '" ></div>';
		$out .= '<div>'.$member_post.'</div>';
	}

	return $out;
}

