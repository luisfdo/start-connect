<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '' ); // Don't call directly
}

add_action( 'vc_after_init', 'sl_testi_2_shortcode' );
add_shortcode('digital_testimonial_query', 'sl_testi_2_path');


		vc_map( array(
			'name'        => __( 'Testimonials. Layout #2', 'sell' ),
			'base'        => 'digital_testimonial_query',
			'description' => __( 'Get testimonial post type', 'sell' ),
			'category'    => __( 'SecretLab', 'sell' ),
			'icon'        => 'sell_ai testimonial2',
			'params'      => array(
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Extra class name', 'sell' ),
					'param_name'	=> 'extra_class',
					'description'	=> __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'sell' )
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Posts quantity ', 'sell' ),
					'param_name'	=> 'posts_per_page',
					'description'	=> __( 'how many posts to retrive on page', 'sell' ),
					'value'         => 3
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> __( 'Enable\disable navigation', 'sell' ),
					'param_name'	=> 'enable_nav',
					'description'	=> __( 'set ability of navigation', 'sell' ),
					'value'         => array( 'disable' => 'false',
						'enable'  => 'true', ),
					'default'       => 'true'
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> __( 'Enable\disable autoplay', 'sell' ),
					'param_name'	=> 'autoplay',
					'description'	=> __( 'set autoplay on/off', 'sell' ),
					'value'         => array( 'disable' => 'false',
						'enable'  => 'true', ),
					'default'       => 'true'
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Duration between slides', 'sell' ),
					'param_name'	=> 'speed',
					'description'	=> __( 'time of pause between slides', 'sell' ),
					'value'         => 5000
				)

			)
		));

function sl_testi_2_shortcode() {
 // admin area functions

}

function sl_testi_2_path( $atts, $content = null ) {
	// front-end functions
	extract( shortcode_atts( array(
		'uid'              => '',
		'posts_per_page'   => '',
		'extra_class'	   => null,
		'enable_nav'       => 'false',
		'autoplay'         => 'false',
		'speed'            => '1000'
	), $atts ) );

	global $secretlab;

	$slick_params = array( 'class'      => $extra_class ? $extra_class : 'digital_testimonials_box',
		'enable_nav' => $enable_nav,
		'autoplay'   => $autoplay,
		'speed'      => $speed ? $speed : 5000,
		'dots'       => 'false',
		'arrows'     => 'true',
		'prevArrow'  => '<div class="slick-prev">prev</div>',
		'nextArrow'  => '<div class="slick-next">next</div>',
		'sp_row'     => 1,
		'sp_show'    => 1,
		'sp_scroll'  => 1);
	$secretlab['slick'][] = $slick_params;


	$result = new WP_query('post_type=testimonial&posts_per_page='.$posts_per_page);

	$output = '';

	//Output posts

	if ( $result->posts ) :

		add_action('wp_footer', 'theseo_add_slick_carousel');

		// Main wrapper div

		$output .= '
				<div class="text-center paddingleft">
					<div id="digitalti" class="carousel '.$slick_params['class'].'">';

		// Loop through posts
		foreach ( $result->posts as $post ) :

			// Post VARS
			$post_id          = $post->ID;

			if (function_exists("types_render_field")) {
			$client_name = types_render_field("name-of-client", array("post_id" => $post_id, "output"=>"normal"));
			$client_post = types_render_field("post-of-client", array("post_id" => $post_id, "output"=>"normal"));
			$client_photo = types_render_field("photo-of-client", array("post_id" => $post_id, "url" => "true"));
			} else {$client_name = $client_post = $client_photo = '';}
			// Testimonial post article start			

			// Open details div
			$output .= '<div class="item">
										<div class="mention"><p>'.$post->post_content.'</p></div>
										<div class="face">'.'<a href="'.get_permalink($post_id).'"><img src="'.$client_photo.'" alt=""></a><strong>'.$client_name.'</strong><p>'.$client_post.'</p></div>
								</div>';



			// End foreach loop
		endforeach;

		$output .= '</div>
					 </div>';


	endif; // End has posts check	



	// Set things back to normal
	$result = null;
	wp_reset_postdata();

	// Return output
	return $output;
}


