<?php

    if(!class_exists('SL_Icons')){
	    
		class SL_Icons {
		
			function __construct(){
				add_action('init',array($this,'sl_icons_init'));
				add_shortcode('sl_icons',array($this,'sl_icons_shortcode'));
				add_action('wp_enqueue_scripts', array($this, 'register_sl_icons_assets'),1);
				add_action('admin_enqueue_scripts', array($this, 'register_sl_icons_assets'),1);
				add_action('wp_ajax_sl_icon_search', array( $this, 'icon_search' ) );
			}
			
			function create_icon_config($font, $css_file, $dest) {
			
			    include_once ABSPATH.'/wp-admin/includes/file.php';
		        WP_Filesystem();
		
		        global $wp_filesystem; 
				$out = '<?php
				$icons = array();';
				$css = $wp_filesystem->get_contents($css_file);
				preg_match_all('/\.([^:]+):before\s?\{[^"]+([^"]+)"/ims', $css, $sets);
				for ( $i = 0; $i < count($sets[1]); $i++ ) {
					$out .= '$icons' . "['" . $font . "']['" . $sets[1][$i]. "'] = array(" . "'class'=>'" . $sets[1][$i] . "','tag'=>'" . $sets[1][$i] . "','unicode'=>'" . $sets[2][$i] . "');";
				}
				$out .= '
				?>';
				$result = $wp_filesystem->put_contents($dest, $out); 				
			
			}
			
			function display_sl_icon_control($smile_fonts, $settings, $value) {
			
			    $upload_dir = wp_upload_dir();
				$selected = false;
				foreach ( $smile_fonts as $smile_font=>$params ) {
					
					if ( array_key_exists($smile_font, $this->spare_fonts['front']) || array_key_exists($smile_font, $this->spare_fonts['back']) ) include_once( $params['config'] );
					else include( $upload_dir['basedir'] . '/' . $params['include'] . '/' . $params['config'] );				

					foreach ( $icons as $set=>$data ) {
					
						$content[$set] = '<div id="' . $smile_font . '-icons" class="sl_icons_block">';
						foreach ($data as $icon_name=>$icon_set) {
							if ( preg_match('/default/i', $smile_font ) ) $class = $smile_font . '-' . $icon_set['class'];
							else if ( $smile_font == 'font-awesome' ) $class = 'fa ' . $icon_set['class'];
							else if ( $smile_font == 'bgricons' ) $class = $icon_set['class'];
							else { $class = $set . '-' . $icon_set['class']; }
							$content[$set] .= '<span class="sl_icon_container"><i class="'. $class . '"></i></span> ';
							if ( $icon_set['class'] == $value ) $selected = array( 'class' => $icon_set['class'], 'font' => $set );
						}
						$content[$set] .= '</div>';
					}
						
				}
				
				$out = '<div class="sl_icon_block">'; $icons = '';
				
				$out .= '<input name="' . esc_attr( $settings['param_name'] ) . '" class="sl_icon_input wpb_vc_param_value wpb-textinput ' .
						 esc_attr( $settings['param_name'] ) . ' ' .
						 esc_attr( $settings['type'] ) . '_field" type="hidden" value="' . esc_attr( $value ) . '" />';	

			    if ( $settings['param_name'] == 'icon' ) {
				    $out .= '<select name="font_set" class="sl_font_set_select">';
				    if ( !$selected ) $out .= '<option value="all" selected="selected">all</option>'; else $out .= '<option value="all">all</option>';
				    foreach ( $smile_fonts as $smile_font=>$params ) {
					    if ( $smile_font == $selected['font'] ) $marked = ' selected'; else $marked = '';
					    $out .= '<option value="' . $smile_font . '"' . $marked .'>' . $smile_font . '</option>';
				    }				
				    $out .= '</select>';
				}

				$selected_icon = '<span class="sl_icon_container"><i class="'. $value . '"></i></span> ';
				$out .= '<div class="sl_selected_icon">' . $selected_icon . '</div>';

                $out .= '<div id="search_' . $settings['param_name'] . '" class="sl_icons_search_box"><input type="text" placeholder="icons search"><div class="sl_icons_loader"></div></div>';				
				
				$out .= '</div>';			
				
				$out .= '<div class="sl_icon_display">' . implode ('', $content) . '</div>';
				return $out;				
			
			}

            function register_sl_icons_assets() {
			
			    global $post;
			
			    wp_register_style('sl_icon_css', SL_ICONS_URL . 'css/style.css', array() );
				wp_enqueue_style('sl_icon_css');			
					
			    wp_register_style('sl_icon_bgricons', SL_ICONS_URL . 'css/fonts/bgricons/bgricons.css', array() );
				wp_enqueue_style('sl_icon_bgricons');	

                wp_register_style('font-awesome', plugins_url() . '/js_composer/assets/lib/bower/font-awesome/css/font-awesome.min.css', array() );
				wp_enqueue_style('font-awesome');

                wp_register_script('ult_responsive', plugins_url() . '/Ultimate_VC_Addons/admin/vc_extend/js/ultimate-responsive.js', array('jquery'), null );
				wp_enqueue_script('ult_responsive');
				
                wp_register_script('ult_responsive_js', plugins_url() . '/Ultimate_VC_Addons/assets/js/ultimate-params.js', array('jquery') );
				wp_enqueue_script('ult_responsive_js');				

				wp_enqueue_script( 'wp-color-picker' );
				wp_enqueue_style( 'wp-color-picker' );

				if ( ! is_admin() && is_single() ) {
				
					if(stripos($post->post_content, 'font_call:'))
					{
						preg_match_all('/font_call:(.*?)"/',$post->post_content, $display);
						enquque_ultimate_google_fonts_optimzed($display[1]);
					}			
				
				}
				
            }
	
	
			function sl_icon_settings_field($settings, $value) {
			
				if ( $settings['param_name'] == 'icon' ) $fonts_group = 'front'; else $fonts_group = 'back';
					
				foreach ( $this->spare_fonts[$fonts_group] as $font=>$params ) {
					$dest = $params['config'];
					if ( !is_file($dest) ) {
						$this->create_icon_config( $font, $params['style'], $dest );
					}
				}
				$fonts = array_merge($this->spare_fonts[$fonts_group], $this->smile_fonts);	
				
                if ( $settings['param_name'] != 'icon' ) $fonts = $this->spare_fonts[$fonts_group];
				
				return $this->display_sl_icon_control($fonts, $settings, $value);
			
			}	
			
			function create_ult_responsive_data ($id, $fs_param, $lh_param) {
				$args = array(
					'target'      =>  $id,  
					'media_sizes' => array(
					   'font-size' => $fs_param, 		
					   'line-height' => $lh_param 	
					)
				);
				$data_list = get_ultimate_vc_responsive_media_css($args);
				return $data_list;					
			}

            function icon_search() {
				
				$type = str_ireplace('search_', '',  $_POST['type']);
				if ( $type == 'icon' ) { $fonts = array_merge( $this->smile_fonts, $this->spare_fonts['front'] ); }
				else if ( $type == 'back_icon' ) { $fonts = $this->spare_fonts['back']; }
				
				$icons_set = array();
				foreach ( $fonts as $font=>$params ) {
					if ($params['config'] == 'charmap.php') { $config = wp_upload_dir(); $config = $config['basedir'] . '/smile_fonts/' . $font . '/' . $params['config']; } else $config = $params['config'];
					include_once ($config);
					$icons_set = array_merge ( $icons_set, $icons );
				}
				
				$out = '';
				foreach ( $icons_set as $font=>$icons ) {
					$out .= '<div id="' . $font . '-icons" class="sl_icons_block">';
					foreach ($icons as $icon=>$params) {
					    if ( preg_match('/' . $_POST['needle'] . '/i', $params['class']) ) {
						    if ( preg_match('/default/i', $font ) ) $class = $font . '-' . $params['class'];
						    else if ( $font == 'font-awesome' ) $class = 'fa ' . $params['class'];
						    else if ( $font == 'bgricons' ) $class = $params['class'];
						    else { $class = $font . '-' . $params['class']; }						
						    $out .= '<span class="sl_icon_container"><i class="'. $class . '"></i></span> ';
					    }
					}
					$out .= '</div>';
				}
				
				echo $out;
				wp_die();
				
			}
			
			function build_link($link, $title) {
				if($link !== 'none'){
					$href = vc_build_link($link);
					if(isset($href['target']) && trim($href['target']) !== ''){ $target = 'target="'.$href['target'].'"'; }
					else $target = '';
					$link_prefix = '<a class="sl_megaicons-link" href="'.$href['url'].'" '.$target.'>';
					$link_sufix = '</a>';
					$html = $link_prefix.$title.$link_sufix;
				}
				else $html = '';
				return $html;					
			}			
  			

            function sl_icons_init() {
			
			    DEFINE ( 'SL_ICONS_PATH', plugin_dir_path( __FILE__ ) );
				DEFINE ( 'SL_ICONS_URL', trailingslashit( plugins_url( 'SecretLabShortcodes' ) . '/sl_mega_icons' ) );
				if ( class_exists( 'Vc_Manager' ) )
				    DEFINE ( 'VC_FA_PATH', WP_PLUGIN_DIR . '/js_composer/assets/lib/bower/font-awesome/' );
				else 
				    DEFINE ( 'VC_FA_PATH', false );			
			
			    $this->smile_fonts = get_option('smile_fonts');
				
				if ( VC_FA_PATH ) {
				$this->spare_fonts =
                    array( 'front' => array (			
					    'font-awesome' =>
						    array ( 'include'  => VC_FA_PATH,
							        'folder'   => VC_FA_PATH,
									'style'    => VC_FA_PATH . 'css/font-awesome.min.css',
									'config'   => SL_ICONS_PATH . 'css/fonts/font-awesome/charmap.php'
							)
						),
						  'back' => array (
						'bgricons' => 
						    array ( 'include' => SL_ICONS_PATH,
							        'folder'  => SL_ICONS_PATH,
									'style'   => SL_ICONS_PATH . 'css/fonts/bgricons/bgricons.css',
									'config'  => SL_ICONS_PATH . 'css/fonts/bgricons/charmap.php'
							)
						)
				    );
				}				
							
				vc_add_shortcode_param( 'sl_icon', array($this, 'sl_icon_settings_field'), SL_ICONS_URL . 'js/sl_icons.js?version=' . time() );
			
			    if(function_exists("vc_map")){
				
					vc_map( array(
						'name'        => __( 'Mega Icons', 'sell' ),
						'base'        => 'sl_icons',
						'description' => __( 'Displays Icon/Text Over Background', 'sell' ),
						'category'    => __( 'SecretLab', 'sell' ),
						//'admin_enqueue_css' => trailingslashit( plugins_url( 'sl_shortcodes' ) ) . 'css/admin.css',
						'icon'        => 'sell_ai megaicons',
						'params'      => array(	
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Title', 'sell' ),
								'param_name'	=> 'title',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Content',
								'value'         => ''
							),							
							array(			
								'type'			=> 'textarea_html',
								'heading'		=> __( 'Description', 'sell' ),
								'param_name'	=> 'content',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Content',
								'value'         => ''
							),
							array(			
								'type'			=> 'dropdown',
								'heading'		=> __( 'Icon Block Position', 'sell' ),
								'param_name'	=> 'icon_block_position',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Content',
								'value'         => array ( __( 'Heading and Icon at Left', 'sell' ) => 'left_1',
								                           __( 'Heading and Icon at Right', 'sell' ) => 'right_1',
														   __( 'Heading and Icon at Top', 'sell' ) => 'top_1',
								                           __( 'Icon at Top', 'sell' ) => 'top',
														   __( 'Icon at Right', 'sell' ) => 'right',
														   __( 'Icon at Left', 'sell' ) => 'left',
														   __( 'Icon and Text Inside of Bgr Icon', 'sell' ) => 'inside',
														   __( 'Title and Text Inside of Bgr Icon (For "Only Text" option)', 'sell' ) => 'title_inside',
														 ),
								'std' => 'left'
							),
							array(
								'type'			=> 'dropdown',
								'heading'		=> __( 'Has Link', 'sell' ),
								'param_name'	=> 'has_link',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Content',
								'value'         => array ( 'yes' => 'true', 'no' => 'false' ),
								'std'           => 'false'
							),	
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Text of Link', 'sell' ),
								'param_name'	=> 'link_text',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Content',
								'value'         => '',
								'dependency'    => array ( 'element' => 'has_link', 'value' => array ( 'true' ) )
							),
							array(
								'type'			=> 'vc_link',
								'heading'		=> __( 'Link URL', 'sell' ),
								'param_name'	=> 'link_url',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Content',
								'value'         => '',
								'dependency'    => array ( 'element' => 'has_link', 'value' => array ( 'true' ) )
							),							
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Custom CSS Class', 'sell' ),
								'param_name'	=> 'custom_class',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Style',
								'value'         => ''
							),							
							array(
								'type'			=> 'dropdown',
								'heading'		=> __( 'Has Background Icon', 'sell' ),
								'param_name'	=> 'has_bg_icon',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Style',
								'value'         => array ( 'yes' => 'true', 'no' => 'false' ),
								'std'           => 'true'
							),							
							array(
								'type'			=> 'sl_icon',
								'heading'		=> __( 'Background Icon', 'sell' ),
								'param_name'	=> 'back_icon',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Style',
								'value'         => '',
								'dependency'    => array ( 'element' => 'has_bg_icon', 'value' => array ( 'true' ) )
							),							
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Background Icon Font Size, in px', 'sell' ),
								'param_name'	=> 'back_icon_fontsize',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Style',
								'value'         => '100px',
								'dependency'    => array ( 'element' => 'has_bg_icon', 'value' => array ( 'true' ) )
							),
							array(
								'type'			=> 'colorpicker',
								'heading'		=> __( 'Background Icon Color', 'sell' ),
								'param_name'	=> 'back_icon_color',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Style',
								'value'         => '#3acc7e',
								'dependency'    => array ( 'element' => 'has_bg_icon', 'value' => array ( 'true' ) )
							),
							array(
								'type'			=> 'colorpicker',
								'heading'		=> __( 'Background Icon Hover Color', 'sell' ),
								'param_name'	=> 'back_icon_hover_color',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Style',
								'value'         => '#3ac3cc',
								'dependency'    => array ( 'element' => 'has_bg_icon', 'value' => array ( 'true' ) )
							),
							array(
								'type'			=> 'dropdown',
								'heading'		=> __( 'Display Text and Icon Over Background Icon?', 'sell' ),
								'param_name'	=> 'has_icon',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Style',
								'value'         => array ( __( 'Only Icon', 'sell' ) => 'icon',
									__( 'Only Text', 'sell' ) => 'text',
									__( 'Icon and Text', 'sell' ) => 'both',
								),
								'std' => 'icon'
							),
							array(
								'type'			=> 'sl_icon',
								'heading'		=> __( 'Main Icon', 'sell' ),
								'param_name'	=> 'icon',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Style',
								'value'         => 'fa fa-glass',
								'dependency'    => array ( 'element' => 'has_icon', 'value' => array ( 'icon', 'both' ) )
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Text Over Background Icon', 'sell' ),
								'param_name'	=> 'text_instead_of_icon',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Style',
								'value'         => '',
								'dependency'    => array ( 'element' => 'has_icon', 'value' => array ( 'text', 'both' ) )
							),

							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Icon Box Height, in px', 'sell' ),
								'param_name'	=> 'icon_line_height',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Style',
								'value'         => '100px',
								'dependency'    => array ( 'element' => 'has_bg_icon', 'value' => array ( 'false' ) )
							),								
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Main Icon Font Size, in px', 'sell' ),
								'param_name'	=> 'icon_fontsize',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Style',
								'value'         => '50px',
								'dependency'    => array ( 'element' => 'has_icon', 'value' => array ( 'icon', 'both' ) )
							),
							array(
								'type'			=> 'colorpicker',
								'heading'		=> __( 'Main Icon Color', 'sell' ),
								'param_name'	=> 'icon_color',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Style',
								'value'         => '#2a3548',
								'dependency'    => array ( 'element' => 'has_icon', 'value' => array ( 'icon', 'both' ) )
							),
							array(
								'type'			=> 'colorpicker',
								'heading'		=> __( 'Main Icon Hover Color', 'sell' ),
								'param_name'	=> 'icon_hover_color',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Style',
								'value'         => '#385ea0',
								'dependency'    => array ( 'element' => 'has_icon', 'value' => array ( 'icon', 'both' ) )
							),			

							array(			
								'type'			=> 'dropdown',
								'heading'		=> __( 'Animation', 'sell' ),
								'param_name'	=> 'animation',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Style',
								'value'         => array (  __( 'No Animation', 'sell' ) => 'no_animation',
								                            __( 'Zoom', 'sell' ) => 'zoom',
															__( 'Borders', 'sell' ) => 'border_parts',
															__( 'Background rotation', 'sell' ) => 'background_rotate'
														 ),
								'std' => 'zoom'
							),				
							array(
								'type'			=> 'colorpicker',
								'heading'		=> __( 'Animation Elements Color', 'sell' ),
								'param_name'	=> 'animation_color',
								'description'	=> __( '', 'sell' ),
								'group'         => 'Style',
								'value'         => '#fdee00'
							),				
							array(
								'type'          => 'css_editor',
								'heading'       => 'Block Design Options',
								'param_name'    => 'css',
								'group'         => 'Design Options'				
							),
							
						)
					));

	                if (class_exists('Ultimate_VC_Addons')) {
						require_once('ult_typography.php');
						vc_add_params( 'sl_icons', $attributes );
					}
	

				
				}
            }

            function sl_icons_shortcode($atts, $content = null)
			{

				$defaults = array('id' => false,
					'custom_class' => '',
					'title' => '',
					'content' => '',
					'icon_block_position' => 'left',
					'has_link' => '',
					'link_text' => '',
					'link_url' => '',
					'has_bg_icon' => 'yes',
					'back_icon' => '',
					'back_icon_position' => 'center',
					'back_icon_box_width' => '30%',
					'back_icon_fontsize' => '50px',
					'back_icon_color' => '#e5e5e5',
					'back_icon_hover_color' => '#e7e7e7',
					'has_icon' => 'Only Icon',
					'text_instead_of_icon' => '',
					'icon' => 'fa fa-glass',
					'icon_line_height' => '100px',
					'icon_fontsize' => '14px',
					'icon_color' => '#d5d5d5',
					'icon_hover_color' => '#bbb',
					'animation' => 'no_animation',
					'animation_color' => '#bbb',
					'css' => '',
					'title_font_family' => '',
					'title_font_style' => '',
					'title_font_size' => '',
					'title_line_height' => '',
					'title_text_color' => '',

					'content_font_family' => '',
					'content_font_style' => '',
					'content_font_size' => '',
					'content_line_height' => '',
					'content_text_color' => '',

					'count_font_family' => '',
					'count_font_style' => '',
					'count_font_size' => '',
					'count_line_height' => '',
					'count_text_color' => '',
					'count_hover_text_color' => '',


				);

				$args = vc_map_get_attributes('sl_icons', $atts);
				$set = shortcode_atts($defaults, $args);

				$description = $content;

				preg_match('/{([^{]+)}/ims', $set['css'], $rules);
				if (isset($rules) && isset ($rules[1])) $rules = ' style="' . $rules[1] . '"'; else $rules = '';

				$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class($set['css'], ' '), 'sl_icons', $atts);
				$id = $set['id'] != '' ? $set['id'] : 'sl_mega_icon_' . rand(10001, 99999);
				$class = $set['custom_class'] == '' ? '' : $set['custom_class'];
				$animation_class = ' animation_' . $set['animation'];

				//$icon_block_background_color = isset($set['icon_block_background_color']) && $set['icon_block_background_color'] != '' ? 'background-color : ' . $set['icon_block_background_color'] . ';' : '';
				//$icon_block_hover_background_color = isset($set['icon_block_hover_background_color']) && $set['icon_block_hover_background_color'] != '' ? 'background-color : ' . $set['icon_block_hover_background_color'] . ';' : '';
				//#'.$id.' {'. $icon_block_background_color .'}
				//#'.$id.':hover {'. $icon_block_hover_background_color .'}

				preg_match('/font_family:([^\|]+)/', $set['title_font_family'], $font);
				$title_font_family = isset($font[1]) ? ' font-family : ' . $font[1] . '; text-align:center;' : '';
				$title_font_style = str_ireplace(',', ' ', $set['title_font_style']);
				$title_font_style = strlen($title_font_style) > 2 ? $title_font_style : '';
				$title_text_color = isset($set['title_text_color']) && $set['title_text_color'] != '' ? ' color : ' . $set['title_text_color'] . ';' : '';
				//$title_line_height = isset($set['title_line_height']) && $set['title_line_height'] != '' ? ' line-height : ' . $set['title_line_height'] . ';' : '';
				//$title_text_background = isset( $set['title_text_background'] ) && $set['title_text_background'] != '' ? ' background-color : ' . $set['title_text_background'] . ';' : '';

				preg_match('/font-family:([^\|]+)/', $set['content_font_family'], $font);
				$content_font_family = isset($font[1]) ? ' font-family : ' . $font[1] . '; text-align:center;' : '';
				$content_font_style = str_ireplace(',', ' ', $set['content_font_style']);
				$content_font_style = strlen($content_font_style) > 2 ? $content_font_style : '';
				$content_text_color = isset($set['content_text_color']) && $set['content_text_color'] != '' ? ' color : ' . $set['content_text_color'] . ';' : '';
				//$content_text_background = isset( $set['content_text_background'] ) && $set['content_text_background'] != '' ? ' background-color : ' . $set['content_text_background'] . ';' : '';
				preg_match('/font_family:([^\|]+)/', $set['count_font_family'], $font);
				$count_font_family = isset($font[1]) ? ' font-family : ' . $font[1] . '; text-align:center;' : '';
				$count_font_style = str_ireplace(',', ' ', $set['count_font_style']);
				$count_font_style = strlen($count_font_style) > 2 ? $count_font_style : '';
				$count_text_color = isset($set['count_text_color']) && $set['count_text_color'] != '' ? ' color : ' . $set['count_text_color'] . ';' : '';
				$count_hover_text_color = isset($set['count_hover_text_color']) && $set['count_hover_text_color'] != '' ? ' color : ' . $set['count_hover_text_color'] . ';' : '';
				// if no background icon
				if (isset($set['has_bg_icon']) && $set['has_bg_icon'] == 'false') {
					$set['back_icon_fontsize'] = $set['icon_line_height'];
				}

				$style = '<style type="text/css">';
				if ( isset($set['has_bg_icon']) && $set['has_bg_icon'] == 'true' ) {
					$style .=  '#' . $id . ' .mi_icon.box { width : calc(' . $set['back_icon_fontsize'] . ' + 2px); height : calc(' . $set['back_icon_fontsize'] . ' + 2px); }
				';
				} else {
					$style .=  '#' . $id . ' .mi_icon.box { width :100%; height : auto; }
				';
				}
				$mode_class = ' icons_block ';
				if ( $set['icon_block_position'] != 'top' && $set['icon_block_position'] != 'top_1' ) {
					$direction = preg_replace( '/_.+/', '', $set['icon_block_position'] );
					if ( $set['icon_block_position'] != 'left_1' || $set['icon_block_position'] != 'right_1' ) { $mode_class = ' with_title '; $content_align = ' text_' . $direction . ' '; }
				} else {
				    $content_align = ' text_center ';
				    if ( $set['icon_block_position'] == 'top' ) 
					$style .= '#' . $id . ' [class*="mi_box_"] { text-align : ' . $set['back_icon_position'] . '; }
					';				
				}
                if ( isset($set['has_bg_icon']) && $set['has_bg_icon'] == 'true' ) {
                    $style .=  '#' . $id . ' .mi_icon.box:before { font-size : ' . $set['back_icon_fontsize'] . '; color : ' . $set['back_icon_color'] . '; }
				    ';
                    $style .=  '#' . $id . ':hover .mi_icon.box:before { color : ' . $set['back_icon_hover_color'] . '; }
				    ';				
				} else {
					$style .=  '#' . $id . ' .mi_icon.box .mi_icon { line-height : ' . $set['icon_line_height'] . '; }
				    ';
				}
				if ( isset($set['has_bg_icon']) && $set['has_bg_icon'] == 'false' && $set['has_icon'] == 'text' ) {
				    $style .=  '#' . $id . ' .mi_icon.box .mi_icon { line-height : unset !important; }
				    ';
					$style .=  '#' . $id . ' .mi_icon.box .mi_icon > span {line-height :unset !important; }
				    ';
				}
				if ( isset($set['has_icon']) && $set['has_icon'] == 'icon') {
					$style .= '#' . $id . ' .mi_icon.box .mi_icon i { font-size : ' . $set['icon_fontsize'] . '; color : ' . $set['icon_color'] . '; line-height : ' . $set['back_icon_fontsize'] . '; }	
					';
					$style .=  '#' . $id . ':hover .mi_icon.box .mi_icon i { color : ' . $set['icon_hover_color'] . '; }
					';
				}
				if ( isset($set['has_icon']) && $set['has_icon'] == 'text' && $set['icon_block_position'] != 'title_inside') {
					$style .= '#' . $id . ' .mi_icon.box .mi_icon > span {line-height : ' . $set['back_icon_fontsize'] . '; color : ' . $set['icon_color'] . ';'. $count_font_family . $count_font_style . $count_text_color .'}
					';
					$style .=  '#' . $id . ':hover .mi_icon.box .mi_icon > span { ' . $count_hover_text_color . '; }
					';
				}
				if ( isset($set['has_icon']) && $set['has_icon'] == 'text' && $set['icon_block_position'] == 'title_inside') {
					$style .= '#' . $id . ' .mi_icon.box .mi_icon { line-height : ' . $set['back_icon_fontsize'] . '; }	
					';
					$style .= '#' . $id . ' .mi_icon.box .mi_icon > span { line-height : calc(' . $set['back_icon_fontsize'] . '/4); padding-top: calc(' . $set['back_icon_fontsize'] . '/3); color : ' . $set['icon_color'] . ';'. $count_font_family . $count_font_style . $count_text_color .'}
					';
					$style .=  '#' . $id . ':hover .mi_icon.box .mi_icon > span { ' . $count_hover_text_color . '; }
					';
					$style .=  '#' . $id . ' .mi_title {}
                	';
				}
				if ( isset($set['has_icon']) && $set['has_icon'] == 'both') {
					$style .= '#' . $id . ' .mi_icon.box .mi_icon { line-height : ' . $set['back_icon_fontsize'] . '; }	
					';
					$style .= '#' . $id . ' .mi_icon.box .mi_icon i {line-height : calc(' . $set['back_icon_fontsize'] . '/4); font-size : ' . $set['icon_fontsize'] . '; color : ' . $set['icon_color'] . '; padding-top: calc(' . $set['back_icon_fontsize'] . '/4);}	
					';
					$style .=  '#' . $id . ':hover .mi_icon.box .mi_icon i { color : ' . $set['icon_hover_color'] . '; }
					';
					$style .= '#' . $id . ' .mi_icon.box .mi_icon > span {line-height : calc(' . $set['back_icon_fontsize'] . '/4); color : ' . $set['icon_color'] . ';'. $count_font_family . $count_font_style . $count_text_color .'}
					';
					$style .=  '#' . $id . ':hover .mi_icon.box .mi_icon > span { color : ' . $count_hover_text_color . '; }
					';
				}

				$style .=  '#' . $id . ' .mi_title { ' . $title_font_family . $title_font_style . $title_text_color . ' }
                ';
				$style .=  '#' . $id . ' .mi_description, #' . $id . ' .mi_box a { ' . $content_font_family . $content_font_style . $content_text_color . ' }';
				$style .=  '#' . $id . '.animation_border_parts [class*="mi_box_"]:before, #' . $id . '.animation_border_parts [class*="mi_box_"]:after {background-color: '.$set['animation_color'].'}';
				$style .=  '</style>';

				
				$out = '';
				$result = '
				<div id="' . $id . '" class="mi_general ' . esc_attr($mode_class) . ' mode_' . esc_attr($set['icon_block_position']) . ' ' . esc_attr($set['custom_class']) . ' ' . esc_attr($css_class) . $animation_class . '" ' . $rules . '>
				    __title__
					<div class="mi_box">';
				
				if ( isset($set['has_bg_icon']) && $set['has_bg_icon'] == 'true' ) {		
					$back_icon_block = 
						'<div class="mi_box_' . $set['icon_block_position'] . '"> 
							<div class="mi_icon box ' . $set['back_icon'] . '">
								__icon__
							</div>
						</div>';
				}
				else { 
				    $back_icon_block = 
					    '<div class="mi_box_' . $set['icon_block_position'] . '"> 
						    <div class="mi_icon box">
							    __icon__
							</div>
						</div>';
				}

				if ( isset($set['has_icon']) && $set['has_icon'] == 'icon') {
					if (isset($set['icon']) && $set['icon'] != '') {
						$icon_block =
							'<div class="mi_icon">
								<i class="' . $set['icon'] . '"></i>
							</div>';
					} else {
						$icon_block = '';
					}
				}
				if ( isset($set['has_icon']) && $set['has_icon'] == 'text') {
					if (isset($set['text_instead_of_icon']) && $set['text_instead_of_icon'] != '') {
						$ult_data = $this->create_ult_responsive_data('#' . $id . ' .mi_title', $set['title_font_size'], $set['title_line_height']);
						if ($set['icon_block_position'] == 'title_inside') {
							$title =
								'<div class="' . $content_align . ' mi_title_box_' . $set['icon_block_position'] . '"> 
							<div class="ult-responsive mi_title"' . $ult_data . '>' . $set['title'] . '</div>
						</div>';
						} else {
							$title = '';
						}
						$ult_data_mc = $this->create_ult_responsive_data('#' . $id . ' .mi_count', $set['count_font_size'], $set['count_line_height']);
						$icon_block =
							'<div class="mi_icon">
								<span><span class="ult-responsive mi_count" ' . $ult_data_mc . '>' . $set['text_instead_of_icon'] . '</span></span>'.$title.'
							</div>';
					}
				}
				if ( isset($set['has_icon']) && $set['has_icon'] == 'both') {
					if (isset($set['text_instead_of_icon']) && $set['text_instead_of_icon'] != '') {
						$ult_data = $this->create_ult_responsive_data('#' . $id . ' .mi_count', $set['count_font_size'], $set['count_line_height']);
						$icon_block =
							'<div class="mi_icon">
								<i class="' . $set['icon'] . '"></i>
								<span><span class="ult-responsive mi_count" ' . $ult_data . '>' . $set['text_instead_of_icon'] . '</span></span>
							</div>';
					}
				}


				$icons_block = preg_replace('/__icon__/', $icon_block, $back_icon_block);

				
				if ( isset( $set['title']) && $set['title'] != '' && $set['icon_block_position'] != 'title_inside' ) {
				    $ult_data = $this->create_ult_responsive_data('#' . $id . ' .mi_title', $set['title_font_size'], $set['title_line_height']);
					$title = 		 
						'<div class="' . $content_align . ' mi_title_box_' . $set['icon_block_position'] . '"> 
							<div class="ult-responsive mi_title"' . $ult_data . '>' . $set['title'] . '</div>
						</div>';
				}
				else $title = '';

				if ( isset($description) && $description != '') {
					$description = preg_replace('/<\/p>/u', '', $description);
				    $ult_data = $this->create_ult_responsive_data('#' . $id . ' .mi_description', $set['content_font_size'], $set['content_line_height']);
					$content_section = 
						'<div class="' . $content_align . 'ult-responsive mi_description"' . $ult_data . '>' . $description . '</div>';
				}
				else $content_section = '';	
				
				if ( isset($set['has_link']) && $set['has_link'] == 'true' && isset($set['link_text']) && isset($set['link_url']) ) {
					$content_section .= $this->build_link($set['link_url'], $set['link_text']);
				}

				if ( isset($set['icon_block_position']) ) {
					if ( $set['icon_block_position'] == 'top') { $out =  '<div class="mi_content_box_top">' . $icons_block . $title . $content_section . '</div>'; }
					else if ( $set['icon_block_position'] == 'left'  ) { $out = $icons_block . '<div class="mi_content_box_left">' . $title . $content_section . '</div>'; }
					else if ( $set['icon_block_position'] == 'right'  ) { $out = '<div class="mi_content_box_right">' . $title . $content_section . '</div>' . $icons_block; }
					else if ( $set['icon_block_position'] == 'top_1' ) { $out = $icons_block . $content_section; }
					else if ( $set['icon_block_position'] == 'left_1' ) { $out = $icons_block . $content_section; }
					else if ( $set['icon_block_position'] == 'right_1' ) { $out = $content_section . $icons_block; }
					else if ( $set['icon_block_position'] == 'inside' ) { $out = '<div class="mi_content_box_top">' . $icons_block . $title . $content_section . '</div>'; }
					else if ( $set['icon_block_position'] == 'title_inside' ) {	$out = '<div class="mi_content_box_top">' . $icons_block . $content_section . '</div>'; }
				}

				$result = $result . $out .
					'</div>
				</div>';
				
				if ( $set['icon_block_position'] == 'top_1' || $set['icon_block_position'] == 'left_1' || $set['icon_block_position'] == 'right_1' )
				    $result = preg_replace('/__title__/', $title, $result);
				else $result = preg_replace('/__title__/', '', $result);
                
				return $style . $result;			
			
            }			
		
		}
		
	}
	
	new SL_Icons();
	
	if(class_exists('WPBakeryShortCode'))
	{
		class WPBakeryShortCode_SL_Icons extends WPBakeryShortCode {
		}
	}	