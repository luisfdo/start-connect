<?php
    if ( class_exists('Ultimate_VC_Addons') ) {
        $attributes = array(
							array(
								"type" => "ult_param_heading",
								"param_name" => "title_typography",
								"text" => __("Title Text Settings","ultimate_vc"),
								"value" => "",
								"group" => "Typography",
								"class" => "ult-param-heading",
								'edit_field_class' => 'ult-param-heading-wrapper no-top-margin vc_column vc_col-sm-12',
							),
							array(
								"type" => "ultimate_google_fonts",
								"heading" => __("Font Family", "ultimate_vc"),
								"param_name" => "title_font_family",
								"description" => __("Select the font of your choice.","ultimate_vc")." ".__("You can","ultimate_vc")." <a target='_blank' href='".admin_url('admin.php?page=bsf-google-font-manager')."'>".__("add new in the collection here","ultimate_vc")."</a>.",
								"group" => "Typography"
							),
							array(
								"type" => "ultimate_google_fonts_style",
								"heading" 		=>	__("Font Style", "ultimate_vc"),
								"param_name"	=>	"title_font_style",
								"group" => "Typography"
							),
							array(
                                "type" => "ultimate_responsive",
                                "class" => "",
                                "heading" => __("Font Size, in px", 'ultimate_vc'),
                                "param_name" => "title_font_size",
                                "unit" => "px",
                                "media" => array(
                                    /*"Large Screen"      => '',*/
                                    "Desktop" => '',
                                    "Tablet" => '',
                                    "Tablet Portrait" => '',
                                    "Mobile Landscape" => '',
                                    "Mobile" => '',
                                ),
                                "group" => "Typography"
                            ),
							array(
                                "type" => "ultimate_responsive",
                                "class" => "",
                                "heading" => __("Line Height, in px", 'ultimate_vc'),
                                "param_name" => "title_line_height",
                                "unit" => "px",
                                "media" => array(
                                    /*"Large Screen"      => '',*/
                                    "Desktop" => '',
                                    "Tablet" => '',
                                    "Tablet Portrait" => '',
                                    "Mobile Landscape" => '',
                                    "Mobile" => '',
                                ),
                                "group" => "Typography"
                            ),
							array(
								"type" => "colorpicker",
								"heading" => __("Text Color","ultimate_vc"),
								"param_name" => "title_text_color",
								"group" => "Typography",
								//"dependency" => array("element" => "fancytext_effect", "value" => array("typewriter","ticker","ticker-down"))
							),
							/*array(
								"type" => "colorpicker",
								"heading" => __("Text Background","ultimate_vc"),
								"param_name" => "title_text_background",
								"group" => "Advanced Settings",
								"group" => "Typography",
								//"dependency" => array("element" => "fancytext_effect", "value" => array("typewriter","ticker","ticker-down"))
							),*/
							
							array(
								"type" => "ult_param_heading",
								"param_name" => "content_typography",
								"text" => __("Content Area Text Settings","ultimate_vc"),
								"value" => "",
								"group" => "Typography",
								"class" => "ult-param-heading",
								'edit_field_class' => 'ult-param-heading-wrapper no-top-margin vc_column vc_col-sm-12',
							),							
							array(
								"type" => "ultimate_google_fonts",
								"heading" => __("Font Family", "ultimate_vc"),
								"param_name" => "content_font_family",
								"description" => __("Select the font of your choice.","ultimate_vc")." ".__("You can","ultimate_vc")." <a target='_blank' href='".admin_url('admin.php?page=bsf-google-font-manager')."'>".__("add new in the collection here","ultimate_vc")."</a>.",
								"group" => "Typography"
							),
							array(
								"type" => "ultimate_google_fonts_style",
								"heading" 		=>	__("Font Style", "ultimate_vc"),
								"param_name"	=>	"content_font_style",
								"group" => "Typography"
							),
							array(
                                "type" => "ultimate_responsive",
                                "class" => "",
                                "heading" => __("Font Size, in px", 'ultimate_vc'),
                                "param_name" => "content_font_size",
                                "unit" => "px",
                                "media" => array(
                                    /*"Large Screen"      => '',*/
                                    "Desktop" => '',
                                    "Tablet" => '',
                                    "Tablet Portrait" => '',
                                    "Mobile Landscape" => '',
                                    "Mobile" => '',
                                ),
                                "group" => "Typography"
                            ),
							array(
                                "type" => "ultimate_responsive",
                                "class" => "",
                                "heading" => __("Line Height, in px", 'ultimate_vc'),
                                "param_name" => "content_line_height",
                                "unit" => "px",
                                "media" => array(
                                    /*"Large Screen"      => '',*/
                                    "Desktop" => '',
                                    "Tablet" => '',
                                    "Tablet Portrait" => '',
                                    "Mobile Landscape" => '',
                                    "Mobile" => '',
                                ),
                                "group" => "Typography"
                            ),
							array(
								"type" => "colorpicker",
								"heading" => __("Text Color","ultimate_vc"),
								"param_name" => "content_text_color",

								"group" => "Typography",
								//"dependency" => array("element" => "fancytext_effect", "value" => array("typewriter","ticker","ticker-down"))
							),/*,
							array(
								"type" => "colorpicker",
								"heading" => __("Text Background","ultimate_vc"),
								"param_name" => "content_text_background",
								"group" => "Advanced Settings",
								"group" => "Typography",
								//"dependency" => array("element" => "fancytext_effect", "value" => array("typewriter","ticker","ticker-down"))
							)*/
			array(
				"type" => "ult_param_heading",
				"param_name" => "count_typography",
				"text" => __("Text Instead of Icon","ultimate_vc"),
				"value" => "",
				"group" => "Typography",
				"class" => "ult-param-heading",
				'edit_field_class' => 'ult-param-heading-wrapper no-top-margin vc_column vc_col-sm-12',
				"dependency" => array ( 'element' => 'has_icon', 'value' => array ( 'text', 'both' ) )
			),
			array(
				"type" => "ultimate_google_fonts",
				"heading" => __("Font Family", "ultimate_vc"),
				"param_name" => "count_font_family",
				"description" => __("Select the font of your choice.","ultimate_vc")." ".__("You can","ultimate_vc")." <a target='_blank' href='".admin_url('admin.php?page=bsf-google-font-manager')."'>".__("add new in the collection here","ultimate_vc")."</a>.",
				"group" => "Typography",
				"dependency" => array ( 'element' => 'has_icon', 'value' => array ( 'text', 'both' ) )
			),
			array(
				"type" => "ultimate_google_fonts_style",
				"heading" 		=>	__("Font Style", "ultimate_vc"),
				"param_name"	=>	"count_font_style",
				"group" => "Typography",
				"dependency" => array ( 'element' => 'has_icon', 'value' => array ( 'text', 'both' ) )
			),
			array(
				"type" => "ultimate_responsive",
				"class" => "",
				"heading" => __("Font Size, in px", 'ultimate_vc'),
				"param_name" => "count_font_size",
				"unit" => "px",
				"media" => array(
					/*"Large Screen"      => '',*/
					"Desktop" => '',
					"Tablet" => '',
					"Tablet Portrait" => '',
					"Mobile Landscape" => '',
					"Mobile" => '',
				),
				"group" => "Typography",
				"dependency" => array ( 'element' => 'has_icon', 'value' => array ( 'text', 'both' ) )
			),
			array(
				"type" => "ultimate_responsive",
				"class" => "",
				"heading" => __("Line Height, in px", 'ultimate_vc'),
				"param_name" => "count_line_height",
				"unit" => "px",
				"media" => array(
					/*"Large Screen"      => '',*/
					"Desktop" => '',
					"Tablet" => '',
					"Tablet Portrait" => '',
					"Mobile Landscape" => '',
					"Mobile" => '',
				),
				"group" => "Typography",
				"dependency" => array ( 'element' => 'has_icon', 'value' => array ( 'text', 'both' ) )
			),
			array(
				"type" => "colorpicker",
				"heading" => __("Text Color","ultimate_vc"),
				"param_name" => "count_text_color",
				"group" => "Typography",
				"dependency" => array ( 'element' => 'has_icon', 'value' => array ( 'text', 'both' ) )
			),
			array(
				"type" => "colorpicker",
				"heading" => __("Text Color (hover)","ultimate_vc"),
				"param_name" => "count_hover_text_color",
				"group" => "Typography",
				"dependency" => array ( 'element' => 'has_icon', 'value' => array ( 'text', 'both' ) )
			)

				);
	}
?>