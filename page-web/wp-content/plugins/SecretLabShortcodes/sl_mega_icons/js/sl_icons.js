
jQuery(document).ready(function($){

    function sl_set_icons_select_content(e) {
	    var font = $(e.target).find('option:selected').text(),
		    regex = font;
		    regex = new RegExp(regex, 'i');
		$(e.target).parents('.edit_form_line').find('.sl_icon_display').find('div').each(function (i, el) {
		    if ( font == 'all' ) $(el).css( { 'display' : 'block' } );
			else {
			    if ( el.id.match(regex) ) $(el).css( { 'display' : 'block' } );
				else $(el).css( { 'display' : 'none' } );
			}
		});		
	}

    $('.sl_icon_display').on('click', function(e) {
	    if ( e.target.nodeName == 'I' ) {
		    $(e.target).parents('.edit_form_line').find('.sl_selected_icon span i').attr( 'class', $(e.target).attr('class'));
			$(e.target).parents('.edit_form_line').find('.sl_icon_input').val($(e.target).attr('class'));
		}
	});
	$('.sl_font_set_select').on('change', function(e) {
        sl_set_icons_select_content(e);
	});
	
	/*$('.sl_icon_block').each( function(i, el) {
	    sl_set_icons_select_content( { 'target' : $(el).find('>span>i') } );
	});*/
	
	var sl_icons_request, sl_icons_query;
	function send_request(e) {
	
	var type = $(e.target).parent().attr('id'),
	    loader = $(e.target).parent().find('.sl_icons_loader');
		
     $(loader).css( { display : 'block' } );
	
	sl_icons_request =   jQuery.ajax({
			    url      : localajax['url'], 
				method   : 'POST',
				cache    : false,
			    action   : 'sl_icon_search',
				data     : 'action=sl_icon_search&type=' + type + '&needle=' + sl_icons_query,
				
			    success  : function(result) {
                               $(e.target).parents('.edit_form_line').find('.sl_icon_display').empty().html(result);
							   $(loader).css( { display : 'none' } );
                               sl_icons_request = false;
						   }
					   });	
	
	}	

	
    $('.sl_icons_search_box input').on('keyup', function(e) {
		sl_icons_query = $(e.target).attr('value')
		if (sl_icons_request) {		
		    sl_icons_request.abort();
			send_request(e);
		}
		else {	
		    send_request(e);
		}		
	});	
	
});