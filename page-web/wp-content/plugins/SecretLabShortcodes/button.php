<?php

if(!class_exists('sell_button')){

	class sell_button {

		function __construct(){
			add_action('init',array($this,'sell_button_init'));
			add_shortcode('sell_button',array($this,'sell_button_shortcode'));
		}
		
		function sell_button_init() {
			if(function_exists("vc_map")){
				vc_map( array(
					'name'        => __( 'Flex Buttons', 'sell' ),
					'base'        => 'sell_button',
					'description' => __( 'Displays custom button with typography options', 'sell' ),
					'category'    => __( 'SecretLab', 'sell' ),
					'icon'        => 'sell_ai button',
					'params'      => array(
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Text', 'sell' ),
								'param_name'	=> 'text',
								'holder'		=> 'span',
								'class'			=> 'vc_admin_label admin_label_text',
								'value'         => 'Button'
							),
							array(
								'type'			=> 'colorpicker',
								'heading'		=> __( 'Color', 'sell' ),
								'param_name'	=> 'color',
								'description'	=> __( '', 'sell' ),
								'value'         => '#ffffff'
							),
							array(
								'type'			=> 'colorpicker',
								'heading'		=> __( 'Color (hover)', 'sell' ),
								'param_name'	=> 'color_h',
								'description'	=> __( '', 'sell' ),
								'value'         => ''
							),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Font Size, in px', 'sell' ),
							'param_name'	=> 'font_size',
							'value'         => ''
						),
						array(
							'type'			=> 'dropdown',
							'heading'		=> __( 'Font Weight', 'sell' ),
							'param_name'	=> 'font_weight',
							'value'         => array(
								'normal' 	=> 'normal',
								'bold'  	=> 'bold',
							),
							'std'       => 'bold'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'URL Address', 'sell' ),
							'param_name'	=> 'link',
							'value'         => '#'
						),
						array(
							'type'			=> 'dropdown',
							'heading'		=> __( 'Target', 'sell' ),
							'param_name'	=> 'target',
							'value'         => array(
								'current window' 	=> '_self',
								'new window'  	=> '_blank',
							),
							'std'       => '_self'
						),
						array(
							'type'			=> 'colorpicker',
							'heading'		=> __( 'Background Color', 'sell' ),
							'param_name'	=> 'bgr_color',
							'description'	=> __( '', 'sell' ),
							'value'         => ''
						),
						array(
							'type'			=> 'colorpicker',
							'heading'		=> __( 'Background Color (hover)', 'sell' ),
							'param_name'	=> 'bgr_color_h',
							'description'	=> __( '', 'sell' ),
							'value'         => ''
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Border Width', 'sell' ),
							'param_name'	=> 'border_width',
							'description'	=> __( 'Examples: 0, 1px, 0 0 4px 0', 'sell' ),
							'value'         => '2px',
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Border Width (hover)', 'sell' ),
							'param_name'	=> 'border_width_h',
							'description'	=> __( 'Examples: 0, 1px, 0 0 4px 0', 'sell' ),
							'value'         => '2px',
						),
						array(
							'type'			=> 'dropdown',
							'heading'		=> __( 'Border Style', 'sell' ),
							'param_name'	=> 'border_style',
							'value'         => array (
								__( 'none', 'sell' ) => 'none',
								__( 'dashed', 'sell' ) => 'dashed',
								__( 'dotted', 'sell' ) => 'dotted',
								__( 'double', 'sell' ) => 'double',
								__( 'groove', 'sell' ) => 'groove',
								__( 'inset', 'sell' ) => 'inset',
								__( 'outset', 'sell' ) => 'outset',
								__( 'ridge', 'sell' ) => 'ridge',
								__( 'solid', 'sell' ) => 'solid',
							),
							'std' => 'none'
						),
						array(
							'type'			=> 'dropdown',
							'heading'		=> __( 'Border Style (hover)', 'sell' ),
							'param_name'	=> 'border_style_h',
							'value'         => array (
								__( 'none', 'sell' ) => 'none',
								__( 'dashed', 'sell' ) => 'dashed',
								__( 'dotted', 'sell' ) => 'dotted',
								__( 'double', 'sell' ) => 'double',
								__( 'groove', 'sell' ) => 'groove',
								__( 'inset', 'sell' ) => 'inset',
								__( 'outset', 'sell' ) => 'outset',
								__( 'ridge', 'sell' ) => 'ridge',
								__( 'solid', 'sell' ) => 'solid',
							),
							'std' => 'none'
						),
						array(
							'type'			=> 'colorpicker',
							'heading'		=> __( 'Border Color', 'sell' ),
							'param_name'	=> 'border_color',
							'description'	=> __( '', 'sell' ),
							'value'         => ''
						),
						array(
							'type'			=> 'colorpicker',
							'heading'		=> __( 'Border Color (hover)', 'sell' ),
							'param_name'	=> 'border_color_h',
							'description'	=> __( '', 'sell' ),
							'value'         => ''
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Border Radius', 'sell' ),
							'param_name'	=> 'border_radius',
							'description'	=> __( 'Examples: 0, 1px, 0 0 4px 0', 'sell' ),
							'value'         => '0',
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Border Radius (hover)', 'sell' ),
							'param_name'	=> 'border_radius_h',
							'description'	=> __( 'Examples: 0, 1px, 0 0 4px 0', 'sell' ),
							'value'         => '0',
						),
						array(
							'type'			=> 'dropdown',
							'heading'		=> __( 'Button position', 'sell' ),
							'param_name'	=> 'position',
							'value'         => array(
								'left' 	=> 'tal',
								'center'  	=> 'tac',
								'right'  	=> 'tar',
							),
							'std'       => 'left'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Extra class name', 'sell' ),
							'param_name'	=> 'extra_class',
							'description'	=> __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'sell' ),
						),
						array(
							'type'          => 'css_editor',
							'heading'       => 'Block Design Options',
							'param_name'    => 'css',
							'group'         => 'Design Options'
						),
					)
				));

			}
		}

		function sell_button_shortcode($atts, $content = null) {
			extract( shortcode_atts( array(
				'id' 						=> false,
				'text'						=> 'Button',
				'color'						=> '#ffffff',
				'color_h'					=> '',
				'font_size'					=> '',
				'font_weight'				=> 'bold',
				'link'						=> '#',
				'target' 					=> '_self',
				'bgr_color'					=> '',
				'bgr_color_h'				=> '',
				//'hover_type'				=> '',
				'border_width'				=> '',
				'border_width_h'			=> '',
				'border_style'				=> '',
				'border_style_h'			=> '',
				'border_color'				=> '',
				'border_color_h'			=> '',
				'border_radius'				=> '',
				'border_radius_h'			=> '',
				'position'					=> '',

				'extra_class'				=> '',
				'css'						=> '',

			), $atts ) );

			$out = '';

			$id = $id != '' ? $id : 'sl_bttn_' . rand(10001, 99999);
			$sell_color = $color ? ' color: ' . $color . '; ' : '';
			$sell_color_h = $color_h ? ' color: ' . $color_h . '; ' : '';
			$sell_font_size = $font_size ? ' font-size: ' . $font_size . 'px; ' : '';
			$sell_font_weight = $font_weight ? ' font-weight: ' . $font_weight . '; ' : 'font-weight: bold';
			$sell_bgr_color = $bgr_color ? ' background-color: ' . $bgr_color . '; ' : '';
			$sell_bgr_color_h = $bgr_color_h ? ' background-color: ' . $bgr_color_h . '; ' : '';
			$sell_border_width = $border_width ? ' border-width: ' . $border_width . '; ' : '';
			$sell_border_width_h = $border_width_h ? ' border-width: ' . $border_width_h . '; ' : '';
			$sell_border_style = $border_style ? ' border-style: ' . $border_style . '; ' : '';
			$sell_border_style_h = $border_style_h ? ' border-style: ' . $border_style_h . '; ' : '';
			$sell_border_color = $border_color ? ' border-color: ' . $border_color . '; ' : '';
			$sell_border_color_h = $border_color_h ? ' border-color: ' . $border_color_h . '; ' : '';
			$sell_border_radius = $border_radius ? ' border-radius: ' . $border_radius . '; ' : '';
			$sell_border_radius_h = $border_radius_h ? ' border-radius: ' . $border_radius_h . '; ' : '';


			preg_match('/{([^{]+)}/ims', $css, $rules);
			if ( isset( $rules ) && isset ( $rules[1] ) ) $rules = ' style="' . $rules[1] . '"'; else $rules = '';
			$css_design = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), 'sell_button', $atts );

			$out = '<div class="'.$position.'">
				<a href="'.$link.'" target="'.$target.'" class="bttn '. $css_design . $extra_class . '" id="' . $id . '">'.$text.'</a>
			</div>';

			$style  =  '<style type="text/css">';
			$style .=  '#'.$id.'.bttn {'.$sell_font_size. $sell_font_weight. $sell_color . $sell_bgr_color. $sell_border_width. $sell_border_style. $sell_border_color. $sell_border_radius. '}';
			$style .=  '#'.$id.'.bttn:hover {'.$sell_color_h . $sell_bgr_color_h. $sell_border_width_h. $sell_border_style_h. $sell_border_color_h. $sell_border_radius_h. '}';

			$style .=  '</style>';

			return $style . $out;
		}

	}

}

new sell_button();

if(class_exists('WPBakeryShortCode'))
{
	class WPBakeryShortCode_sell_button extends WPBakeryShortCode {
	}
}	

