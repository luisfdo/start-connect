<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '' ); // Don't call directly
}

add_action( 'vc_after_init', 'sl_team_1_shortcode' );
add_shortcode( 'teammate_query', 'sl_team_1_path' );

		vc_map( array(
			'name'        => __( 'Teammate Posts with carousel', 'sell' ),
			'base'        => 'teammate_query',
			'description' => __( 'Get teammates post type', 'sell' ),
			'category'    => __( 'SecretLab', 'sell' ),
			'icon'        => 'sell_ai team1',
			'params'      => array(
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Unique ID', 'sell' ),
					'param_name'	=> 'uid',
					'description'	=> __( 'May be used for styling', 'sell' ),
				),

				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Posts quantity ', 'sell' ),
					'param_name'	=> 'posts_per_page',
					'description'	=> __( 'how many posts to retrive on page', 'sell' ),
					'value'         => 3
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Offset for query', 'sell' ),
					'param_name'	=> 'offset',
					'description'	=> __( 'start offset', 'sell' ),
					'value'         => 0
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> __( 'Enable\disable navigation', 'sell' ),
					'param_name'	=> 'enable_nav',
					'description'	=> __( 'set ability of navigation', 'sell' ),
					'value'         => array( 'disable' => 'false',
						'enable'  => 'true', ),
					'default'       => 'true'
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> __( 'Enable\disable autoplay', 'sell' ),
					'param_name'	=> 'autoplay',
					'description'	=> __( 'set autoplay on/off', 'sell' ),
					'value'         => array( 'disable' => 'false',
						'enable'  => 'true', ),
					'default'       => 'true'
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Duration between slides', 'sell' ),
					'param_name'	=> 'speed',
					'description'	=> __( 'time of pause between slides', 'sell' ),
					'value'         => 5000
				)

			)
		));


function sl_team_1_shortcode() {
 // admin area functions

}

function sl_team_1_path( $atts, $content = null ) {
	// front-end functions
	extract( shortcode_atts( array(
		'uid'              => '',
		'main_title'       => '',
		'description'      => '',
		'posts_per_page'   => '',
		'offset'           => '',
		'extra_class'	   => null,
		'enable_nav'       => 'false',
		'autoplay'         => 'false',
		'speed'            => '1000'
	), $atts ) );

	global $secretlab;

	$slick_params = array( 'class'      => $extra_class ? $extra_class : 'teammate_box',
		'enable_nav' => $enable_nav,
		'autoplay'   => $autoplay,
		'speed'      => $speed ? $speed : 5000,
		'dots'       => 'false',
		'arrows'     => 'true',
		'sp_row'     => 2,
		'sp_show'    => 2,
		'sp_scroll'  => 1,
		'992'        => array ( 'sp_row' => 1, 'sp_show' => 1, 'sp_scroll' => 1 ),
		'769'        => array ( 'sp_row' => 1, 'sp_show' => 1, 'sp_scroll' => 1 ),
		'601'        => array ( 'sp_row' => 1, 'sp_show' => 1, 'sp_scroll' => 1 ),
		'521'        => array ( 'sp_row' => 1, 'sp_show' => 1, 'sp_scroll' => 1 )
	);
	$secretlab['slick'][] = $slick_params;

	$result = new WP_query('post_type=teammate&posts_per_page='.$posts_per_page.'&offset='.$offset);

	$output = '';

	//Output posts

	if ( $result->posts ) :

		add_action('wp_footer', 'theseo_add_slick_carousel');


		//$unique_id = $unique_id ? ' id="'. $unique_id .'"' : null;

		// Main wrapper div


		$output .=  '

							<div id="team-index" class="carousel '.$slick_params['class'].'">';

		// Loop through posts
		foreach ( $result->posts as $post ) :

			// Post VARS
			$post_id          = $post->ID;
			if (function_exists("types_render_field")) {
			$member_post = types_render_field("post-of-member", array("post_id" => $post_id, "output"=>"normal"));
			$member_photo = types_render_field("photo-of-member", array("post_id" => $post_id, "url"=>"true", "width"=>"310"));
			$fb_profile = types_render_field("facebook-profile", array("post_id" => $post_id, "output"=>"normal"));
			$yt_profile = types_render_field("youtube-profile", array("post_id" => $post_id, "output"=>"normal"));
			$tw_profile = types_render_field("twitter-profile", array("post_id" => $post_id, "output"=>"normal"));
			$bh_profile = types_render_field("behance-profile", array("post_id" => $post_id, "output"=>"normal"));
			$ln_profile = types_render_field("linkedin-profile", array("post_id" => $post_id, "output"=>"normal"));
			} else {$member_post = $member_photo = $fb_profile = $yt_profile = $tw_profile = $bh_profile = $ln_profile = '';}
			$excerpt = apply_filters( 'get_the_excerpt', $post->post_excerpt, $post );
			$output .= '<div class="item">';
								   $output .= '<img class="text-left" alt="" src="'.$member_photo.'">';
									$output .= '<div class="overmember"><a href="'.get_permalink($post_id).'"><i class="icon-link"></i></a></div>';
								   $output .= '<strong>'.$post->post_title.'</strong><span class="desrdivider">'.$member_post.'</span><p>'.$excerpt.'</p>';
								   $output .= '<div class="socialprofiles">';
			if (!empty($bh_profile)) {
				$output .= '<a href="'.$bh_profile.'"><i class="icon-behance"></i></a>';
			}
			if (!empty($fb_profile)) {
				$output .= '<a href="'.$fb_profile.'"><i class="icon-facebook"></i></a>';
			}
			if (!empty($yt_profile)) {
				$output .= '<a href="'.$yt_profile.'"><i class="icon-youtube-play"></i></a>';
			}
			if (!empty($tw_profile)) {
				$output .= '<a href="'.$tw_profile.'"><i class="icon-twitter"></i></a>';
			}
			if (!empty($ln_profile)) {
				$output .= '<a href="'.$ln_profile.'"><i class="icon-linkedin"></i></a>';
			}
			$output .= '
								   </div>
								</div>';



			// End foreach loop
		endforeach;

		$output .= '</div>';
	
	endif; // End has posts check	
	
	// Set things back to normal
	$result = null;
	wp_reset_postdata();

	// Return output
	return $output;
}


