<?php

/*
Plugin Name: SecretLab Shortcodes
Plugin URI: http://www.secretlab.pw/
Description: Additional shortcodes fo VC
Author: SecretLab
Version: 2.9.8
Author URI: http://www.secretlab.pw/
*/
	
add_action( 'vc_before_init', 'SL_integrateWithVC' );

function SL_integrateWithVC () {

    if ( ! class_exists('post_format') ) {

	class post_format {
		
		public $p;
		
		public function __construct () {
			
		}
		
		public function standart($p, $args) {
			$out = '<article id="'.$p->ID.'" class="post '.implode(' ', get_post_class('', $p->ID)).'">
						<header class="entry-header">';
			if ( has_post_thumbnail($p->ID) && $p->post_type != 'attachment' ) {
				$out .= '<div class="entry-thumbnail">'.
							get_the_post_thumbnail( $p->ID, array(700, 525) ).
						'</div>';
				} else {
				$out .= '<div class="entry-thumbnail"><img src="'.esc_url(get_template_directory_uri()).'/images/not.jpg" alt="' . $p->post_title . '" /></div>';
			}
				$out .=	'<h3 class="entry-title">
							<a href="'.get_permalink($p->ID).'" rel="bookmark">'.get_the_title($p->ID).'</a>
						</h3>';


						if ( isset($args['enable_meta']) ) {
							if ($args['enable_meta'] = true) {
								$out .= '<div class="entry-meta">' .
									theseo_get_entry_meta(false, $p);
								if (is_user_logged_in()) {
									$out .= '<i class="icon icon-pencil-square-o"></i><span class="edit-link"><a href="' . get_edit_post_link($p->ID, '') . '">' . __('Edit', 'sell') . '</a></span>';
								}
								$out .= '</div>';
							}
						}
						$out .= '</header>					
						
						<div class="entry-content"><a href="'.get_permalink($p->ID).'">'.
							theseo_get_excerpt($p, $p->post_content).
						'</a></div>						

					</article>';
					
				return $out;
				
		}	
		
		public function image($p, $args) {
			$out = '<article id="'.$p->ID.'" class="post '.implode(' ', get_post_class('', $p->ID)).'">
						<span class="icon icon-image3"></span>
						<header class="entry-header">
							<h3 class="entry-title">
								<a href="'.get_permalink($p->ID).'" rel="bookmark">'.get_the_title($p->ID).'</a>
							</h3>
						</header>
						
					<div class="entry-content">'.
						theseo_get_content($p, __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'sell' )).										   
						'
					</div>';

						if ( isset($args['enable_meta']) ) {
							if ($args['enable_meta'] = true) {
								$out .= '<div class="entry-meta">' .
									theseo_get_entry_meta(true, $p);
								if (is_user_logged_in()) {
									$out .= '<i class="icon icon-pencil-square-o"></i><span class="edit-link"><a href="' . get_edit_post_link($p->ID, '') . '">' . __('Edit', 'sell') . '</a></span>';
								}
								$out .= '</div>';
							}
						}
						$out .= '</article>';
				
			return $out;
		}

		public function audio ($p, $args) {
			$out = '<article id="'.$p->ID.'" class="post '.implode(' ', get_post_class('', $p->ID)).'">
				<span class="icon-music4"></span>
				<header class="entry-header">
					<h3 class="entry-title">
						<a href="'.get_permalink($p->ID).'" rel="bookmark">'.get_the_title($p->ID).'</a>
					</h3>';

						if ( isset($args['enable_meta']) ) {
							if ($args['enable_meta'] = true) {
								$out .= '<div class="entry-meta">' .
									theseo_get_entry_meta(true, $p);
								if (is_user_logged_in()) {
									$out .= '<i class="icon icon-pencil-square-o"></i><span class="edit-link"><a href="' . get_edit_post_link($p->ID, '') . '">' . __('Edit', 'sell') . '</a></span>';
								}
								$out .= '</div>';
							}
						}
						$out .= '</header>
				
				<div class="entry-content">
					<div class="audio-content">'.
					
						theseo_get_content($p, __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'sell' )).										   
					'</div>
				</div>
				</article>';

			return $out;			
					
		}

		public function video ($p, $args) {
			$out = '<article id="'.$p->ID.'" class="post '.implode(' ', get_post_class('', $p->ID)).'">
				<span class="icon icon-movie"></span>
				<header class="entry-header">
					<h3 class="entry-title">
						<a href="'.get_permalink($p->ID).'" rel="bookmark">'.get_the_title($p->ID).'</a>
					</h3>
				</header>		

				<div class="entry-content">
					<div class="embed-container">'.
					
					theseo_get_content($p, __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'sell' )).

					'</div>'.
					
					wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'sell' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>', 'echo' => 0 ) ).
					
				'</div>';

						if ( isset($args['enable_meta']) ) {
							if ($args['enable_meta'] = true) {
								$out .= '<footer class="entry-meta">' .
									theseo_get_entry_meta(true, $p);
								if (is_user_logged_in()) {
									$out .= '<i class="icon icon-pencil-square-o"></i><span class="edit-link"><a href="' . get_edit_post_link($p->ID, '') . '">' . __('Edit', 'sell') . '</a></span>';
								}
								$out .= '</footer>';
							}
						}
						$out .= '</article>';
				
			return $out;
		}

		public function gallery ($p, $args) {
		
			$out = '<article id="'.$p->ID.'" class="post '.implode(' ', get_post_class('', $p->ID)).'">
				<span class="icon icon-images3"></span>
				<header class="entry-header">
					<h3 class="entry-title">
						<a href="'.get_permalink($p->ID).'" rel="bookmark">'.get_the_title($p->ID).'</a>
					</h3>
				</header>

				<div class="entry-content">'.
					get_post_gallery($p).
				'</div>';

						if ( isset($args['enable_meta']) ) {
							if ($args['enable_meta'] = true) {
								$out .= '<footer class="entry-meta">' .
									theseo_get_entry_meta(true, $p);
								if (is_user_logged_in()) {
									$out .= '<i class="icon icon-pencil-square-o"></i><span class="edit-link"><a href="' . get_edit_post_link($p->ID, '') . '">' . __('Edit', 'sell') . '</a></span>';
								}
								$out .= '</footer>';
							}
						}
						$out .= '
			</article>';

			return $out;
			
		}

		public function link ($p, $args) {
			$out = '<article id="'.$p->ID.'" class="post '.implode(' ', get_post_class('', $p->ID)).'">
				<span class="icon-link22"></span>
				<header class="entry-header">
					<h3 class="entry-title">';
					preg_match('/<a.+<\/a>/Uims', $p->post_content, $link);
					if (!empty($link[0])) $link = $link[0]; else $link = 'This post has not any external links';
					$out .= '<a href="'.get_permalink($p->ID).'" rel="bookmark">'.$link.'</a>
					</h3>
				</header>

				<div class="entry-content">'.
					theseo_get_content($p, __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'sell' )).	
				'</div>
				</article>';
			
			return $out;
				
		}	
		
		public function quote ($p, $args) {
			$out = '<article id="'.$p->ID.'" class="post '.implode(' ', get_post_class('', $p->ID)).'">
				<span class="icon-quote-left"></span>
				<div class="entry-content">'.
					theseo_get_excerpt($p, $p->post_excerpt).
				'</div>';

						if ( isset($args['enable_meta']) ) {
							if ($args['enable_meta'] = true) {
								$out .= '<footer class="entry-meta">' .
									theseo_get_entry_meta(true, $p);
								if (is_user_logged_in()) {
									$out .= '<i class="icon icon-pencil-square-o"></i><span class="edit-link"><a href="' . get_edit_post_link($p->ID, '') . '">' . __('Edit', 'sell') . '</a></span>';
								}
								$out .= '</footer>';
							}
						}
						$out .= '</article>';
			
			return $out;
			
		}
		
		public function none ($p) {
		
			$search_form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
							<label class="screen-reader-text" for="s">Search query:</label>
							<input type="text" value="' . get_search_query() . '" name="s" id="s" />
							<input type="submit" id="searchsubmit" value="Search" />
						   </form>';
		
			$out = '<header class="page-header">
						<h1 class="page-title">'.__( 'Nothing Found', 'sell' ).'</h1>
					</header>
					
					<div class="page-content text-center">';
					
			if ( is_home() && current_user_can( 'publish_posts' ) ) :

				$out .= '<p>'.sprintf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'sell' ), admin_url( 'post-new.php' ) ).'</p>';

			elseif ( is_search() ) :

				$out .= '<p>'.	__( 'Sorry, but nothing matched your search terms. Please try again with different keywords.', 'sell' ).'</p>'.$search_form;

			else :
	 
				$out .= '<p>'. __( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'sell' ) .'</p>'.$search_form;
				
			endif;
			
			$out .= '</div>';
			
			return $out;
		}

	}
	
	}
	
    if ( ! function_exists('get_registered_post_types') ) {
	
	function get_registered_post_types() {
	
		global $wp_post_types, $wpdb;
		
		$pt = array();
		$post_types = $wpdb->get_results("SELECT DISTINCT post_type FROM wp_posts", ARRAY_N);
		
		foreach ($post_types as $n => $post_type) {
			$pt[] = $post_type[0];
		}

		return $pt;
	}
	
	}
	// The function get all Theme Options
	function get_sl_option($option) {
		global $secretlab;

		if ( isset( $secretlab ) ) {
			if ( isset( $secretlab[$option] ) ) return $secretlab[$option];
			else return false;
		}
		else return false;
	}

	if ( ! function_exists('get_post_categories') ) {

	function get_post_categories() {
		$cats = array('');
		$args = array ( 'hide_empty' => 0 );
		$categories = get_categories( $args );
		if( $categories ){
			foreach( $categories as $cat ){
				$cats[] = $cat->slug;
			}
			return $cats;
		}
		return false;
	}
	
	}
	
	
	if ( ! function_exists('get_site_posts') ) {

	function get_site_posts() {
		$posts = get_posts(array('numberposts' => -1, 'post_type' => array('post', 'portfolio', 'testimonial', 'teammate')));
		$out = array('global post' => 0);
		foreach ($posts as $p) {
			$out[$p->post_title] = $p->ID;
		}
		return $out;
	}
	
	}
	
	add_shortcode( 'category_query', 'category_query_shortcode' );

	
	if ( ! function_exists('category_query_shortcode') ) {
	
	function category_query_shortcode($atts, $content = null) {
		extract( shortcode_atts( array(
		    'custom_class'             => 'sl_category_posts',
			'column_layout'         => 1,
			'category'				=> '',
			'post_type'             => 'post',		
			'enable_meta'        	=> true,
			'posts_per_page'        => -1,
			'npg'                   => 1
		), $atts ) ); 

        if (isset($_POST['page'])) { $paged = $_POST['page']; $wrapperId = $_POST['id']; $base = $_POST['base']; $echo = true; } 
		else { $paged = 1; $base = get_pagenum_link( 999999999 ); $echo = false; }
		if (isset($_POST['cat'])) { $category = $_POST['cat']; }
		if (isset($_POST['pt'])) { $post_type = $_POST['pt']; }
		if (isset($_POST['ppp'])) { $posts_per_page = $_POST['ppp']; }
        if (isset($_POST['npg'])) { $npg = $_POST['npg']; }
		if (isset($_POST['enablemeta'])) { $enable_meta = $_POST['enablemeta']; }

		$output = '';
		
		$column_classes = array ( '1' => ' onecolumn', '2' => ' blog2columnpage' );
		if (isset($column_layout)) {
			if (isset($column_classes[ $column_layout ])) {
				$columns = $column_classes[ $column_layout ];    
			}
			else {
				$columns = ' onecolumnnsb';
			}
		}
						
		$args = array ( 'category_name' => $category,
						'post_type' => explode(',', $post_type),
						'posts_per_page' => $posts_per_page,
						'ignore_sticky_posts' => false,
						'paged' => $paged,
						'orderby' => 'date',
						'order' => 'DESC' );
							
		$cat_posts = new WP_Query($args);

		$post_format_display = 'true';

		$format_methods = array( ''      => 'standart',
			'image'   => 'image',
			'audio'   => 'audio',
			'gallery' => 'gallery',
			'link'    => 'link',
			'video'   => 'video',
			'quote'   => 'quote');

		$output = '';

		//Output posts

		if ( $cat_posts->posts ) :

			if (!isset($g)) $g = new post_format();

			// Loop through posts

			foreach ( $cat_posts->posts as $p ) :

				// Post VARS
				$post_id  = $p->ID;

				$format = get_post_format( $post_id );
				$args = array( 'enable_meta' => $enable_meta );

				if ($post_format_display == 'true') {

					if (!$format) $format_methods[$format] = 'standart';

					if ($format_methods[$format]) {
						if ( $format ) { $output .= $g->$format($p, $args); }
						else { $output .= $g->standart($p, $args);  }
					}

				}

			endforeach;

		endif; // End has posts check
		
        $waiter = '<img id="lawyer_ajax_waiter" src="' . get_template_directory_uri() . '/images/ajax-loader2.gif' .'" style="position:absolute; display:none;">';		
		$output .= $waiter;
		
	    $args = array(
		    'base' => str_replace( 999999999, '%#%', $base ),
		    'format' => '',
		    'current' => max( 1, $paged ),
		    'total' => $cat_posts->max_num_pages,
		    'prev_next' => false,
			'type' => 'array'
	    );

        $result = paginate_links( $args );	

		if (count($result) > 0 && $npg) {
		    $output .= '<div class="category_query_pagination blogpagination">'.
		               implode(' ', preg_replace('/\/page/', '/_id='.$custom_class.'_cat='.$category.'_pt='.$post_type.'_ppp='.$posts_per_page.'/page', $result)).
					   '<div id="category_query_service_info" style="display : none;">' . $base . '</div>
					</div>';
		}		

		// Set things back to normal
		$cat_posts = null;
		wp_reset_postdata(); 

		// Return output
		if (!$echo) {
		    return '<div class="' . $custom_class . ' category_query_block'. $columns .'">'.$output.'</div>';
		}
		else {
		    echo $output;
		}

	}
	
	}
	
add_action( 'wp_ajax_handle_category_query', 'category_query_shortcode' );
add_action( 'wp_ajax_nopriv_handle_category_query', 'category_query_shortcode' );		

	vc_map( array(
		'name'        => __( 'Posts Feed', 'sell' ),
		'base'        => 'category_query',
		'description' => __( 'Display posts by category and post type', 'sell' ),
		'category'    => __( 'SecretLab', 'sell' ),
		'icon'        => 'sell_ai blog1',
		'params'      => array(
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Extra class name', 'sell' ),
				'param_name'	=> 'custom_class',
				'description'	=> __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'sell' )
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> __( 'Select column layout', 'sell' ),
				'param_name'	=> 'column_layout',
				'description'	=> __( 'set one or two columns', 'sell' ),
				'value'         => array ( '1 Column' => 1,
										   '2 Columns' => 2 ),
                'std'       => 1
			),			
			array(
				'type'			=> 'posttypes', //'dropdown',
				'heading'		=> __( 'Post type', 'sell' ),
				'param_name'	=> 'post_type',
				'description'	=> __( 'Select needed post-type', 'sell' ),
				//'value'         => array_merge(array(''=>'any'), get_registered_post_types())
			),				
			array(
				'type'			=> 'dropdown',
				'heading'		=> __( 'Category', 'sell' ),
				'param_name'	=> 'category',
				'description'	=> __( 'Attention! this list can be applied to POST post_type only', 'sell' ),
				'value'         => get_post_categories()
			),			
			array(
				'type'			=> 'checkbox',
				'heading'		=> __( 'Enable post meta info', 'sell' ),
				'param_name'	=> 'enable_meta',
				'description'	=> __( 'set to show meta info', 'sell' ),

			),		
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Posts per page', 'sell' ),
				'param_name'	=> 'posts_per_page',
				'description'	=> __( 'how many posts to display on the page', 'sell' ),
				'value'         => 3
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> __( 'Use Pagination', 'sell' ),
				'param_name'	=> 'npg',
				'description'	=> __( '', 'sell' ),
				'value'         => array( 'yes' => 1,
				                          'no'  => 0 ),
				'std'           => 1
			),
           /* array(
                'type'			=> 'textfield',
                'heading'		=> __( 'Read more link text', 'sell' ),
                'param_name'	=> 'rmorelink',
                'value'         => 'Continue reading'
            ) */
			
		)
	));
	
	
	


	



	add_shortcode( 'archive_format_post', 'archive_format_post_shortcode' );
	
	
	if ( ! function_exists('archive_format_post_shortcode') ) {

	function archive_format_post_shortcode($atts, $content = null) {	
		extract( shortcode_atts( array(
			'q'                => '',
			'npg'              => 0
		), $atts ) );

        if (isset($_POST['page'])) { $paged = $_POST['page']; $wrapperId = $_POST['id']; $base = $_POST['base']; $echo = true; } 
		else { $paged = 1; $base = get_pagenum_link( 999999999 ); $echo = false; }	
		if (isset($_POST['ppp'])) { $q = $_POST['ppp']; }
        if (isset($_POST['npg'])) { $npg = $_POST['npg']; }

        $out = '';

        $args = array(  'posts_per_page' => $q,
		                'ignore_sticky_posts' => true,
						'paged' => $paged,
						'orderby' => 'date',
						'order' => 'DESC' 					   
                     );		
		
		$result = new WP_query( $args );
		
		if ( $result->posts ) :	
			
			foreach ( $result->posts as $p ) :
			
				$out .= '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 postfb">
							<article class="post digital-format format-standard has-post-thumbnail clearfix">
								<header class="entry-header">';
								if ( has_post_thumbnail($p->ID) && $p->post_type != 'attachment' ) {
								$out .= '<div class="entry-thumbnail">'.get_the_post_thumbnail( $p->ID, 'theseo_smalldigital' );
									if( comments_open($p->ID) ) {
										$out .= '<div class="ccount"><a href="' . get_comments_link($p->ID) . '"><span class="icon-comment-o"> ' . get_comments_number($p->ID) . '</span></a></div>';
									}
									$out .='</div>';
									
								} else {
									$out .= '<div class="entry-thumbnail"></div>';
								}
								$out.= 
									'<h3 class="entry-title">';
										$title = get_the_title($p->ID);
										$t = explode(' ', $title);
										if (count($t) > 6) { $title = implode(' ', array_slice($t, 0, 6, true))."..."; }
										$out .= '<a href="'.get_permalink($p->ID).'" rel="bookmark">'.$title.'</a>
									</h3>';
									$author = get_user_by('id', $p->post_author);
									$out .= '
									<div class="entry-meta">
										<span class="author vcard">BY '.strtoupper($author->user_login).'</span>
										<span class="date">
											<time class="entry-date" datetime="'.get_the_date("c", $p->ID).'">'.strtoupper(get_the_date("F n, Y", $p->ID)).'</time>
										</span>
									</div>
								</header>
								<div class="entry-content">';
									$content = theseo_get_excerpt($p, $p->post_excerpt, 12);
									if (strlen($content) > 2) $content = preg_replace('/continue reading[^<]+/i', ' MORE <span class="icon icon-arrow-right8"></span>', $content);
									else 
									$content = '<a class="more-link" href="'.get_permalink($p->ID).'/">'.__( 'MORE', 'sell' ).' <span class="icon icon-arrow-right8"></span></a>';
									$out .= $content.
								'</div>
							</article>
						</div>';
						
			endforeach;
			
		endif; // End has posts check			

            $waiter = '<img id="seo_ajax_waiter" src="' . get_template_directory_uri() . '/images/ajax-loader2.gif' .'" style="position:absolute; display:none;">';		
		    $out .= $waiter;
			
	    $args = array(
		    'base' => str_replace( 999999999, '%#%', $base ),
		    'format' => '',
		    'current' => max( 1, $paged ),
		    'total' => $result->max_num_pages,
		    'prev_next' => false,
			'type' => 'array'
	    );	

        $result = paginate_links( $args );

		if (count($result) > 0 && $npg) {
		    $out .= '<div class="post_format_query_pagination blogpagination">'.
		               implode(' ', preg_replace('/\/page/', '/_id=afp_ppp='.$q.'_npg='.$npg.'/page', $result)).
					   '<div id="post_format_query_service_info" style="display : none;">' . $base . '</div>
					</div>';
		}		
		
        wp_reset_postdata();	

		if (!$echo) {
		    return '<div class="post_format_query_block">'.$out.'</div>'; 	
		}
		else {
		    echo $out;
		}		
    		
	}
	
	}
	
add_action( 'wp_ajax_handle_post_format_query', 'archive_format_post_shortcode' );
add_action( 'wp_ajax_nopriv_handle_post_format_query', 'archive_format_post_shortcode' );	

	vc_map( array(
		'name'        => __( 'Digital Theme Post Format, 2 columns', 'sell' ),
		'base'        => 'archive_format_post',
		'description' => __( 'Show Any Post in Archive Format of Digital Design', 'sell' ),
		'category'    => __( 'SecretLab', 'sell' ),
		'icon'        => 'sell_ai blog2',
		'params'      => array(	
			array(
				'type'          => 'textfield',
				'heading'       => 'Posts per Page',
				'param_name'    => 'q',	
                'value'         => 5
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> __( 'Use Pagination', 'sell' ),
				'param_name'	=> 'npg',
				'value'         => array( 'yes' => 1,
				                          'no'  => 0 ),
				'std'           => 0
			),
			
		)
	));





	add_shortcode( 'price_table1', 'price_table1_shortcode' );
	
	
	if ( ! function_exists('price_table1_shortcode') ) {

	function price_table1_shortcode($atts, $content = null) {
		extract( shortcode_atts( array(
			'uid'                   => '',
			'extra_class'           => '',
			'title1'                 => 'Title',
			'subtitle1'              => 'Subtitle',
			'best_offer1'             => false,
			'contents1'              => '',
			'last_line1'             => 'Price',
			'href1'                  => '#',
			'order_button_text1'     => 'order!',
			
			'title2'                 => 'Title',
			'subtitle2'              => 'Subtitle',
			'best_offer2'             => false,		
			'contents2'              => '',
			'last_line2'             => 'Price',
			'href2'                  => '#',
			'order_button_text2'     => 'order!',

			'title3'                 => 'Title',
			'subtitle3'              => 'Subtitle',
			'best_offer3'             => false,		
			'contents3'              => '',
			'last_line3'             => 'Price',
			'href3'                  => '#',
			'order_button_text3'     => 'order!',

			'title4'                 => 'Title',
			'subtitle4'              => 'Subtitle',
			'best_offer4'             => false,		
			'contents4'              => '',
			'last_line4'             => 'Price',
			'href4'                  => '#',
			'order_button_text4'     => 'order!'		
		), $atts ) ); 
		
		$j = 0;
		
		$out = '
		<div class="row center-price '.$extra_class.'">';
			for ($i = 1; $i <= 4; $i++) {
				$title = 'title'.$i;
				$subtitle = 'subtitle'.$i;
				$best_offer = 'best_offer'.$i;
				$contents = 'contents'.$i;
				$last_line = 'last_line'.$i;
				$href = 'href'.$i;
				$order_button_text = 'order_button_text'.$i;
				
				if (!preg_match('/<li>/', html_entity_decode($$contents))) continue;

				$j++;
				if ($$best_offer) $best_offer_class = ' class="bestgreen"'; else $best_offer_class = '';
				
				$out .= '
				
				<div class="col-lg-#n# col-md-#n# col-sm-12 col-xs-12">
					<ul class="pricetable1">
						<li'.$best_offer_class.'>
							<h3>
								<span>'.$$subtitle.'</span>'.
								$$title.
							'</h3>
						</li>';
			
				preg_match_all('/<li>(.+)<\/li>/Uims', html_entity_decode($$contents), $lines);

				if (count($lines[0]) > 0) {
					foreach ($lines[0] as $line) {		    
						$out .= $line;
					}
				}
		
				$out .= '<li class="worth">'.$$last_line.'</li>';
		
				$out .= '<li><a class="btn btn-info" href="'.$$href.'"><span class="icon icon-compass2"></span>'.$$order_button_text.'</a></li>';
		
				$out .= '</ul>
			</div>';
			}
			
		$out .= '</div>';
		
		//if ($j > 1) $j = $j - 1; else $j = $j;
		
		if ( $j != 0 ) {
		
			$n = 12/$j;
			
			$out = preg_replace('/#n#/', $n, $out);

		} else {
			$out = preg_replace('/#n#/', 12, $out);
		}
		
		return $out;
		
	}
	
	}

	vc_map( array(
		'name'        => __( 'Price Table Layout#1', 'sell' ),
		'base'        => 'price_table1',
		'description' => __( 'Echoes Price Table#1', 'sell' ),
		'category'    => __( 'SecretLab', 'sell' ),
		'icon'        => 'sell_ai pricetable1',
		'params'      => array(
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Unique ID', 'sell' ),
				'param_name'	=> 'uid',
				'description'	=> __( 'May be used for styling', 'sell' ),
			),	
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Extra class ', 'sell' ),
				'param_name'	=> 'extra_class',
				'description'	=> __( 'extra css class', 'sell' )
			),	
			
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Title', 'sell' ),
				'param_name'	=> 'title1',
				'description'	=> __( 'Main title - column will appear if Title field filled', 'sell' ),
				'group'         => 'Column #1'
			),	
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Subtitle', 'sell' ),
				'param_name'	=> 'subtitle1',
				'description'	=> __( 'subtitle', 'sell' ),
				'group'         => 'Column #1'
			),	
			array(
				'type'			=> 'dropdown',
				'heading'		=> __( 'Best Offer', 'sell' ),
				'param_name'	=> 'best_offer1',
				'description'	=> __( 'set to TRUE, if you want to set to column Best Offer style', 'sell' ),
				'value'         => array( 'false' => 'false',
										  'true'  => 'true' ),			
				'group'         => 'Column #1'
			),				
			array(
				'type'			=> 'textarea',
				'heading'		=> __( 'Content', 'sell' ),
				'param_name'	=> 'contents1',
				'description'	=> __( 'item contents, each item must be bordered in "(_"..."_)"', 'sell' ),
				'group'         => 'Column #1'
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Pricing line', 'sell' ),
				'param_name'	=> 'last_line1',
				'description'	=> __( 'content for the pricing area', 'sell' ),
				'group'         => 'Column #1'
			),		
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Form URL', 'sell' ),
				'param_name'	=> 'href1',
				'description'	=> __( 'url of handler od this block', 'sell' ),
				'group'         => 'Column #1'
			),		
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Order Button Text', 'sell' ),
				'param_name'	=> 'order_button_text1',
				'description'	=> __( 'content for the order button', 'sell' ),
				'group'         => 'Column #1'
			),

			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Title', 'sell' ),
				'param_name'	=> 'title2',
				'description'	=> __( 'Main title - column will appear if Title field filled', 'sell' ),
				'group'         => 'Column #2'
			),	
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Subtitle', 'sell' ),
				'param_name'	=> 'subtitle2',
				'description'	=> __( 'subtitle', 'sell' ),
				'group'         => 'Column #2'
			),	
			array(
				'type'			=> 'dropdown',
				'heading'		=> __( 'Best Offer', 'sell' ),
				'param_name'	=> 'best_offer2',
				'description'	=> __( 'set to TRUE, if you want to set to column Best Offer style', 'sell' ),
				'value'         => array( 'false' => 'false',
										  'true'  => 'true' ),			
				'group'         => 'Column #2'
			),			
			array(
				'type'			=> 'textarea',
				'heading'		=> __( 'Content', 'sell' ),
				'param_name'	=> 'contents2',
				'description'	=> __( 'item contents, each item must be bordered in "(_"..."_)"', 'sell' ),
				'group'         => 'Column #2'
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Pricing line', 'sell' ),
				'param_name'	=> 'last_line2',
				'description'	=> __( 'content for the pricing area', 'sell' ),
				'group'         => 'Column #2'
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Form URL', 'sell' ),
				'param_name'	=> 'href2',
				'description'	=> __( 'url of handler od this block', 'sell' ),
				'group'         => 'Column #2'
			),		
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Order Button Text', 'sell' ),
				'param_name'	=> 'order_button_text2',
				'description'	=> __( 'content for the order button', 'sell' ),
				'group'         => 'Column #2'
			),
			
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Title', 'sell' ),
				'param_name'	=> 'title3',
				'description'	=> __( 'Main title - column will appear if Title field filled', 'sell' ),
				'group'         => 'Column #3'
			),	
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Subtitle', 'sell' ),
				'param_name'	=> 'subtitle3',
				'description'	=> __( 'subtitle', 'sell' ),
				'group'         => 'Column #3'
			),	
			array(
				'type'			=> 'dropdown',
				'heading'		=> __( 'Best Offer', 'sell' ),
				'param_name'	=> 'best_offer3',
				'description'	=> __( 'set to TRUE, if you want to set to column Best Offer style', 'sell' ),
				'value'         => array( 'false' => 'false',
										  'true'  => 'true' ),
				'group'         => 'Column #3'
			),			
			array(
				'type'			=> 'textarea',
				'heading'		=> __( 'Content', 'sell' ),
				'param_name'	=> 'contents3',
				'description'	=> __( 'item contents, each item must be bordered in "(_"..."_)"', 'sell' ),
				'group'         => 'Column #3'
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Pricing line', 'sell' ),
				'param_name'	=> 'last_line3',
				'description'	=> __( 'content for the pricing area', 'sell' ),
				'group'         => 'Column #3'
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Form URL', 'sell' ),
				'param_name'	=> 'href3',
				'description'	=> __( 'url of handler od this block', 'sell' ),
				'group'         => 'Column #3'
			),		
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Order Button Text', 'sell' ),
				'param_name'	=> 'order_button_text3',
				'description'	=> __( 'content for the order button', 'sell' ),
				'group'         => 'Column #3'
			),		

			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Title', 'sell' ),
				'param_name'	=> 'title4',
				'description'	=> __( 'Main title - column will appear if Title field filled', 'sell' ),
				'group'         => 'Column #4'
			),	
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Subtitle', 'sell' ),
				'param_name'	=> 'subtitle4',
				'description'	=> __( 'subtitle', 'sell' ),
				'group'         => 'Column #4'
			),	
			array(
				'type'			=> 'dropdown',
				'heading'		=> __( 'Best Offer', 'sell' ),
				'param_name'	=> 'best_offer4',
				'description'	=> __( 'set to TRUE, if you want to set to column Best Offer style', 'sell' ),
				'value'         => array( 'false' => 'false',
										  'true'  => 'true' ),			
				'group'         => 'Column #4'
			),			
			array(
				'type'			=> 'textarea',
				'heading'		=> __( 'Content', 'sell' ),
				'param_name'	=> 'contents4',
				'description'	=> __( 'item contents, each item must be bordered in "(_"..."_)"', 'sell' ),
				'group'         => 'Column #4'
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Pricing line', 'sell' ),
				'param_name'	=> 'last_line4',
				'description'	=> __( 'content for the pricing area', 'sell' ),
				'group'         => 'Column #4'
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Form URL', 'sell' ),
				'param_name'	=> 'href4',
				'description'	=> __( 'url of handler od this block', 'sell' ),
				'group'         => 'Column #4'
			),		
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Order Button Text', 'sell' ),
				'param_name'	=> 'order_button_text4',
				'description'	=> __( 'content for the order button', 'sell' ),
				'group'         => 'Column #4'
			)		
			
		)
	));






	add_shortcode( 'free_seo_audit', 'free_seo_audit_shortcode' );
	
	
	if ( ! function_exists('free_seo_audit_shortcode') ) {

	function free_seo_audit_shortcode($atts, $content = null) {

		global $secretlab;

		extract( shortcode_atts( array(
			'uid'                     => '',
			'title'                   => 'Free SEO audit report of your website',
			'bgr_main_image'                => '1977',
			'send_button_content'     => 'Send request now!',
			'portfolio_href'          => '#',
			'watch_portfolio_content' => 'Watch portfolio!',
			'form_title'              => 'We appreciate your business and guarantee a response within 24 Business hours which would include our Packages and Pricing.'
		), $atts ) ); 
		
		$secretlab['cf7_formtitle']= $form_title;
		
		$inline_style = '';
		if (isset($bgr_main_image) && $bgr_main_image != '') {
			$image = wp_get_attachment_url( $bgr_main_image );
			$inline_style = ' style="background-image:url('."'".$image."'".');"';
		}
		//$inline_style = '';
		
		$out = '
		<section class="calltoactionblock">
		<div class="container-fluid ctaction1"'.$inline_style.'>
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
							<div class="cta1desc animated bounceInLeft">
								<h2>'.$title.'</h2>'.
								'<p>'.$content.'</p>
								<span class="btn btn-info btn-lg hiddesc">'.$send_button_content.'</span>';
								if ($watch_portfolio_content != '') {
									$out .= '<a class="btn btn-primary btn-lg marginleft" href="'.$portfolio_href.'">'.$watch_portfolio_content.'</a>';
								}
							$out .= '</div>
							<div class="requestform animated bounceOutLeft">
								'.do_shortcode('[contact-form-7 id="1715" title="Send request for SEO audit shortcode" form_title="'.$form_title.']').
							'</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</section>';
		
		return $out;
		
	}
	
	}

	vc_map( array(
		'name'        => __( 'Free Seo Audit Layout', 'sell' ),
		'base'        => 'free_seo_audit',
		'description' => __( 'Echoes Free Seo Audit Blck', 'sell' ),
		'category'    => __( 'SecretLab', 'sell' ),
		'icon'        => 'sell_ai audit',
		'params'      => array(
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Unique ID', 'sell' ),
				'param_name'	=> 'uid',
				'description'	=> __( 'May be used for styling', 'sell' ),
			),	
			/*array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Extra class ', 'sell' ),
				'param_name'	=> 'extra_class',
				'description'	=> __( 'extra css class', 'sell' )
			),*/		
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Title', 'sell' ),
				'param_name'	=> 'title',
				'value'         => 'Free SEO audit report of your website',
				'description'	=> __( 'Main title', 'sell' )
			),
			array(
				'type'			=> 'attach_image',
				'heading'		=> __( 'Background Image', 'sell' ),
				'param_name'	=> 'bgr_main_image',
				'value'         => 1977,
				'description'	=> __( '', 'sell' )
			),		
			array(
				"type"			=> "textarea_html",
				"heading"		=> __( "Content", "sell" ),
				"param_name"	=> "content",
				"value"         => "Your website will be scanned and checked for On page, off page, domain authority, social engagement and other SEO factors. Not only that but we will also do competition analysis. Receive 5 point evaluation covering 60 major aspects",
				"description"	=> __( "", "sell" )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Send Button Content', 'sell' ),
				"param_name"	=> "send_button_content",
				"value"         => "Send request now!",
				'description'	=> __( 'content for Send Button', 'sell' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Portfolio URL', 'sell' ),
				'param_name'	=> 'portfolio_href',
				'value'         => '#',
				'description'	=> __( 'url for Portfolio link', 'sell' )
			),				
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Portfolio Link Text', 'sell' ),
				"param_name"	=> "watch_portfolio_content",
				"value"         => "Watch portfolio!",
				'description'	=> __( 'content Porfolio link button', 'sell' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Form Title', 'sell' ),
				'param_name'	=> 'form_title',
				'value'         => 'We appreciate your business and guarantee a response within 24 Business hours which would include our Packages and Pricing.',
				'description'	=> __( 'title for Request Form', 'sell' )
			)		
			
		)
	));



	add_shortcode( 'why_us', 'why_us_shortcode' );
	
	
	if ( ! function_exists('why_us_shortcode') ) {

	function why_us_shortcode($atts, $content = null) {

		$home = get_stylesheet_uri().'/';

		$vals = shortcode_atts( array(
			'uid'          => '',
			'extra_class'  => '',
			'bgr_main_image'     => '1983',
			'title'        => 'Why Us?',
			'subtitle'     => 'We offer synergistic online products, social media marketing and dynamic SEO strategies deliverin.', 
			
			'bg_alt1'      => 'Global reach',
			'bg_alt2'    => 'Best Prices',
			'bg_alt3'    => 'Big Experience',
			'bg_alt4'    => 'Convenience',
			'bg_alt5'    => 'Team Strength',
			'bg_src1'      => '1978',
			'bg_src2'    => '1979',
			'bg_src3'    => '1980',
			'bg_src4'    => '1981',
			'bg_src5'    => '1982',
			'alt1'         => 'Global reach',
			'alt2'       => 'Best Prices',
			'alt3' =>  'Big Experience',
			'alt4'       => 'Convenience',
			'alt5'       => 'Team Strength',
			'src1'         => '1984',
			'src2'       => '1985',
			'src3'       => '1986',
			'src4'       => '1987',
			'src5'       => '1988',
			'content1'     => 'Global Reach',
			'content2'   => 'Best Prices',
			'content3'   => 'Big Experience',
			'content4'   => 'Convenience',
			'content5'   => 'Team Strength',
			'color1'       => 'blue',    'color2'     => 'blue',     'color3'     => 'green',     'color4'     => 'blue',     'color5'     => 'green'

		), $atts );	
		
		$inline_style = '';
		if (isset($vals['bgr_main_image']) && $vals['bgr_main_image'] != '') {
			$image = wp_get_attachment_url( $vals['bgr_main_image'] );
			$inline_style = ' style="background-image:url('."'".$image."'".');"';
		}	
		
		$out = '
		<section class="benefits '.$vals['extra_class'].' id="'.$vals['uid'].'">
			<div class="beninside"'.$inline_style.'>
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="why">
							<div class="headinginfo">
								<h2>'.$vals['title'].'</h2>'.$vals['subtitle'].'
							</div>
						</div>';
						for ($i = 1; $i <=5; $i++) {
						
							$bg_alt = 'bg_alt'.$i;
							$bg_src = 'bg_src'.$i;
							$alt = 'alt'.$i;
							$src = 'src'.$i;
							$content = 'content'.$i;
							$color = 'color'.$i;
							
							$out .= '
							<div class="rhombus beniconsize'.$i.'">
								<div class="tralign">
										<img class="imgbgr" alt="'.$vals[$bg_alt].'" src="'.wp_get_attachment_url($vals[$bg_src]).'">
									<div class="beniconblock '.$vals[$color].'bgr bico o0-o02">
										<img alt="'.$vals[$alt].'" src="'.wp_get_attachment_url($vals[$src]).'">
										<b>'.$vals[$content].'</b>
									</div>
								</div>
							</div>';
							
						}
					$out .= '
					</div>
				</div>
			</div>
			</div><div class="clearfix"></div>
		</section>';
		
		return $out;
		
	}
	
	}

	vc_map( array(
		'name'        => __( 'Why Us Block Layout', 'sell' ),
		'base'        => 'why_us',
		'description' => __( 'Echoes Why Us Block', 'sell' ),
		'category'    => __( 'SecretLab', 'sell' ),
		'icon'        => 'sell_ai whyus',
		'params'      => array(
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Unique ID', 'sell' ),
				'param_name'	=> 'uid',
				'description'	=> __( 'May be used for styling', 'sell' ),
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Extra class name', 'sell' ),
				'param_name'	=> 'extra_class',
				'description'	=> __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'sell' )
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Title', 'sell' ),
				'param_name'	=> 'title',
				"value"         => "Why Us?",
				'description'	=> __( 'Main title', 'sell' )
			),	
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Subitle', 'sell' ),
				'param_name'	=> 'subtitle',
				"value"         => "We offer synergistic online products, social media marketing and dynamic SEO strategies deliverin.",
				'description'	=> __( 'Subtitle', 'sell' )
			),
			array(
				'type'			=> 'attach_image',
				'heading'		=> __( 'Background Image', 'sell' ),
				'param_name'	=> 'bgr_main_image',
				'description'	=> __( 'For whole section', 'sell' )
			),


			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'alt attribute for background #1', 'sell' ),
				'param_name'	=> 'bg_alt1',
				"value"         => "Global reach",			
				'description'	=> __( 'alt attribute', 'sell' ),
				'group'         => 'block 1'
			),
			array(
				'type'			=> 'attach_image',
				'heading'		=> __( 'background image for block #1', 'sell' ),
				'param_name'	=> 'bg_src1',
				'description'	=> __( 'background image', 'sell' ),
				'group'         => 'block 1'
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'alt attribute for main image in block #1', 'sell' ),
				'param_name'	=> 'alt1',
				"value"         => "Global reach",			
				'description'	=> __( 'alt attribute for main image', 'sell' ),
				'group'         => 'block 1'
			),	
			array(
				'type'			=> 'attach_image',
				'heading'		=> __( 'main image for block #1', 'sell' ),
				'param_name'	=> 'src1',
				'description'	=> __( 'main image', 'sell' ),
				'group'         => 'block 1'
			),
			array(
				'type'			=> 'textarea',
				'heading'		=> __( 'text content #1', 'sell' ),
				'param_name'	=> 'content1',
				"value"         => "Global reach",			
				'description'	=> __( 'content for block #1', 'sell' ),
				'group'         => 'block 1'
			),	
			array(
				'type'			=> 'dropdown',
				'heading'		=> __( 'background color for block #1', 'sell' ),
				'param_name'	=> 'color1',
				'description'	=> __( 'background color #1', 'sell' ),
				'value'         => array ( 'white' => 'white',     
										   'green' => 'green',
										   'blue'  => 'blue' ),
				'group'         => 'block 1'
			),	


			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'alt attribute for background #2', 'sell' ),
				'param_name'	=> 'bg_alt2',
				'description'	=> __( 'alt attribute', 'sell' ),
				'value'         => 'Best Prices',
				'group'         => 'block 2'
			),	
			array(
				'type'			=> 'attach_image',
				'heading'		=> __( 'background image for block #2', 'sell' ),
				'param_name'	=> 'bg_src2',
				'description'	=> __( 'background image', 'sell' ),
				'group'         => 'block 2'
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'alt attribute for main image in block #2', 'sell' ),
				'param_name'	=> 'alt2',
				'description'	=> __( 'alt attribute for main image', 'sell' ),
				'value'         => 'Best Prices',
				'group'         => 'block 2'
			),	
			array(
				'type'			=> 'attach_image',
				'heading'		=> __( 'main image for block #2', 'sell' ),
				'param_name'	=> 'src2',
				'description'	=> __( 'main image', 'sell' ),
				'group'         => 'block 2'
			),
			array(
				'type'			=> 'textarea',
				'heading'		=> __( 'text content #2', 'sell' ),
				'param_name'	=> 'content2',
				'description'	=> __( 'content for block #2', 'sell' ),
				'value'         => 'Best Prices',
				'group'         => 'block 2'
			),	
			array(
				'type'			=> 'dropdown',
				'heading'		=> __( 'background color for block #2', 'sell' ),
				'param_name'	=> 'color2',
				'description'	=> __( 'background color #2', 'sell' ),
				'value'         => array ( 'white' => 'white',     
										   'green' => 'green',
										   'blue'  => 'blue' ),
				'group'         => 'block 2'
			),

			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'alt attribute for background #3', 'sell' ),
				'param_name'	=> 'bg_alt3',
				'description'	=> __( 'alt attribute', 'sell' ),
				'value'         => 'Big Experience',
				'group'         => 'block 3'
			),	
			array(
				'type'			=> 'attach_image',
				'heading'		=> __( 'background image for block #3', 'sell' ),
				'param_name'	=> 'bg_src3',
				'description'	=> __( 'background image', 'sell' ),
				'group'         => 'block 3'
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'alt attribute for main image in block #3', 'sell' ),
				'param_name'	=> 'alt3',
				'description'	=> __( 'alt attribute for main image', 'sell' ),
				'value'         => 'Big Experience',
				'group'         => 'block 3'
			),	
			array(
				'type'			=> 'attach_image',
				'heading'		=> __( 'main image for block #3', 'sell' ),
				'param_name'	=> 'src3',
				'description'	=> __( 'main image', 'sell' ),
				'group'         => 'block 3'
			),
			array(
				'type'			=> 'textarea',
				'heading'		=> __( 'text content #3', 'sell' ),
				'param_name'	=> 'content3',
				'description'	=> __( 'content for block #3', 'sell' ),
				'value'         => 'Big Experience',
				'group'         => 'block 3'
			),	
			array(
				'type'			=> 'dropdown',
				'heading'		=> __( 'background color for block #3', 'sell' ),
				'param_name'	=> 'color3',
				'description'	=> __( 'background color #3', 'sell' ),
				'value'         => array ( 'white' => 'white',     
										   'green' => 'green',
										   'blue'  => 'blue' ),
				'group'         => 'block 3'
			),		
			
			
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'alt attribute for background #4', 'sell' ),
				'param_name'	=> 'bg_alt4',
				'description'	=> __( 'alt attribute', 'sell' ),
				'value'         => 'Convenience',
				'group'         => 'block 4'
			),	
			array(
				'type'			=> 'attach_image',
				'heading'		=> __( 'background image for block #4', 'sell' ),
				'param_name'	=> 'bg_src4',
				'description'	=> __( 'background image', 'sell' ),
				'group'         => 'block 4'
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'alt attribute for main image in block #4', 'sell' ),
				'param_name'	=> 'alt4',
				'description'	=> __( 'alt attribute for main image', 'sell' ),
				'value'         => 'Convenience',
				'group'         => 'block 4'
			),	
			array(
				'type'			=> 'attach_image',
				'heading'		=> __( 'main image for block #4', 'sell' ),
				'param_name'	=> 'src4',
				'description'	=> __( 'main image', 'sell' ),
				'group'         => 'block 4'
			),
			array(
				'type'			=> 'textarea',
				'heading'		=> __( 'text content #4', 'sell' ),
				'param_name'	=> 'content4',
				'description'	=> __( 'content for block #4', 'sell' ),
				'value'         => 'Convenience',
				'group'         => 'block 4'
			),	
			array(
				'type'			=> 'dropdown',
				'heading'		=> __( 'background color for block #4', 'sell' ),
				'param_name'	=> 'color4',
				'description'	=> __( 'background color #4', 'sell' ),
				'value'         => array ( 'white' => 'white',     
										   'green' => 'green',
										   'blue'  => 'blue' ),
				'group'         => 'block 4'
			),


			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'alt attribute for background #5', 'sell' ),
				'param_name'	=> 'bg_alt5',
				'description'	=> __( 'alt attribute', 'sell' ),
				'value'         => 'Team Strength',
				'group'         => 'block 5'
			),	
			array(
				'type'			=> 'attach_image',
				'heading'		=> __( 'background image for block #5', 'sell' ),
				'param_name'	=> 'bg_src5',
				'description'	=> __( 'background image', 'sell' ),
				'group'         => 'block 5'
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'alt attribute for main image in block #5', 'sell' ),
				'param_name'	=> 'alt5',
				'description'	=> __( 'alt attribute for main image', 'sell' ),
				'value'         => 'Team Strength',
				'group'         => 'block 5'
			),	
			array(
				'type'			=> 'attach_image',
				'heading'		=> __( 'main image for block #5', 'sell' ),
				'param_name'	=> 'src5',
				'description'	=> __( 'main image', 'sell' ),
				'group'         => 'block 5'
			),
			array(
				'type'			=> 'textarea',
				'heading'		=> __( 'text content #5', 'sell' ),
				'param_name'	=> 'content5',
				'description'	=> __( 'content for block #4', 'sell' ),
				'value'         => 'Team Strength',
				'group'         => 'block 5'
			),	
			array(
				'type'			=> 'dropdown',
				'heading'		=> __( 'background color for block #5', 'sell' ),
				'param_name'	=> 'color5',
				'description'	=> __( 'background color #5', 'sell' ),
				'value'         => array ( 'white' => 'white',     
										   'green' => 'green',
										   'blue'  => 'blue' ),
				'group'         => 'block 5'
			),			
			
			)
		)
	);

	// Shortcodes
	require_once( plugin_dir_path( __FILE__ ) . 'sl_social_icons.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'blog_one_three.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'sl_mega_icons/sl_mega_icons.php' );
	//require_once( plugin_dir_path( __FILE__ ) . 'teammate_single.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'teammate_1.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'teammate_2.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'teammates3.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'teammates4.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'testimonials_1.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'testimonials_2.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'testimonials_4.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'testimonials_5.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'price_table2.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'price_table3.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'price_table4.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'portfolio_list_1.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'portfolio_list_2.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'services_list_2.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'seo_result_1.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'title.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'button.php' );
	//vc_lean_map( 'testimonial_query', null, plugin_dir_path( __FILE__ ) .'/testimonials_1.php' );

	
}
?>
