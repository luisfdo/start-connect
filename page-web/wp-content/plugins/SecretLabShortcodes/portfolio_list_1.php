<?php

    if(!class_exists('sell_plist_1')){
	    
		class sell_plist_1 {
		
			function __construct(){
				add_action('init',array($this,'sell_plist_1_init'));
				add_shortcode('sell_plist_1',array($this,'sell_plist_1_shortcode'));
			}
			
		

            function sell_plist_1_init() {			
			    if(function_exists("vc_map")){
				
					vc_map( array(
						'name'        => __( 'Portfolio List Carousel', 'sell' ),
						'base'        => 'sell_plist_1',
						'description' => __( 'Displays portfolio in 4 columns', 'sell' ),
						'category'    => __( 'SecretLab', 'sell' ),
						'icon'        => 'sell_ai portfolio1',
						'params'      => array(
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Extra class name', 'sell' ),
								'param_name'	=> 'css_class',
								'description'	=> __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'sell' ),
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Portfolio items quantity ', 'sell' ),
								'param_name'	=> 'posts_per_page',
								'description'	=> __( 'how many works displays on the page. 4 works per slide', 'sell' ),
								'value'         => 8
							),
							array(
								'type'			=> 'dropdown',
								'heading'		=> __( 'Enable\disable navigation', 'sell' ),
								'param_name'	=> 'enable_nav',
								'description'	=> __( 'Sets availability of navigation', 'sell' ),
								'value'         => array( 'disable' => 'false',
									'enable'  => 'true', ),
								'default'       => 'true'
							),
							array(
								'type'			=> 'dropdown',
								'heading'		=> __( 'Enable\disable autoplay', 'sell' ),
								'param_name'	=> 'autoplay',
								'description'	=> __( 'Sets autoplay option', 'sell' ),
								'value'         => array( 'disable' => 'false',
									'enable'  => 'true', ),
								'default'       => 'true'
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Duration between autoplay slides', 'sell' ),
								'param_name'	=> 'speed',
								'description'	=> __( 'time of pause between slides leaf', 'sell' ),
								'value'         => 5000
							)

						)
					));
		
				}
            }

            function sell_plist_1_shortcode($atts, $content = null) {
				extract( shortcode_atts( array(
					'css_class'        => '',
					'posts_per_page'   => '8',
					'enable_nav'       => 'true',
					'autoplay'         => 'false',
					'speed'            => '5000'
				), $atts ) );

				global $secretlab;

				$slick_params = array( 'class'      => 'sell_plist_1',
					'enable_nav' => $enable_nav,
					'autoplay'   => $autoplay,
					'speed'      => $speed ? $speed : 5000,
					'dots'       => 'true',
					'arrows'     => 'false',
					'sp_row'     => 1,
					'sp_show'    => 4,
					'sp_scroll'  => 4,

					'769' => array ( 'sp_row' => 1, 'sp_show' => 2, 'sp_scroll' => 2 ),
					'521' => array ( 'sp_row' => 1, 'sp_show' => 1, 'sp_scroll' => 1 )
				);
				$secretlab['slick'][] = $slick_params;


				$result = new WP_query('post_type=portfolio&posts_per_page='.$posts_per_page);

				$output = '';

				//Output posts

				if ( $result->posts ) :

					add_action('wp_footer', 'theseo_add_slick_carousel');

					// Main wrapper div

					$output .= '<div class="carousel '.$slick_params['class'].' '.$css_class.'">';

					// Loop through posts
					foreach ( $result->posts as $post ) :

						// Post VARS
						$post_id          = $post->ID;


						// Open details div
						$output .= '<div class="item">
										<div class="block">'.get_the_post_thumbnail($post_id, 'theseo_portfolio').'
										<a href="'.get_permalink($post_id).'"><i class="icon-plus3"></i></a>
								</div></div>';

						// End foreach loop
					endforeach;
					$output .= '</div>';
				endif; // End has posts check

				// Set things back to normal
				$result = null;
				wp_reset_postdata();

				// Return output
				return $output;				
			
            }			
		
		}
		
	}
	
	new sell_plist_1();
	
	if(class_exists('WPBakeryShortCode'))
	{
		class WPBakeryShortCode_sell_plist_1 extends WPBakeryShortCode {
		}
	}	