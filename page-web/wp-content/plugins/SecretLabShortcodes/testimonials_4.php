<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '' ); // Don't call directly
}

add_action( 'vc_after_init', 'sl_testi_4_shortcode' );
add_shortcode('sell_testimonials4', 'sl_testi_4_path');

		vc_map( array(
			'name'        => __( 'Testimonials. Layout #4', 'sell' ),
			'base'        => 'sell_testimonials4',
			'description' => __( 'Displays testimonials with 2 columns and carousel', 'sell' ),
			'category'    => __( 'SecretLab', 'sell' ),
			'icon'        => 'sell_ai testimonial4',
			'params'      => array(
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Extra class name', 'sell' ),
					'param_name'	=> 'css_class',
					'description'	=> __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'sell' ),
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Testimonials quantity ', 'sell' ),
					'param_name'	=> 'posts_per_page',
					'description'	=> __( 'how many testimonials displays on the page', 'sell' ),
					'value'         => 6
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> __( 'Enable\disable navigation', 'sell' ),
					'param_name'	=> 'enable_nav',
					'description'	=> __( 'Sets availability of navigation', 'sell' ),
					'value'         => array( 'disable' => 'false',
						'enable'  => 'true', ),
					'default'       => 'true'
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> __( 'Enable\disable autoplay', 'sell' ),
					'param_name'	=> 'autoplay',
					'description'	=> __( 'Sets autoplay option', 'sell' ),
					'value'         => array( 'disable' => 'false',
						'enable'  => 'true', ),
					'default'       => 'true'
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Duration between autoplay slides', 'sell' ),
					'param_name'	=> 'speed',
					'description'	=> __( 'time of pause between slides leaf', 'sell' ),
					'value'         => 5000
				)

			)
		));

function sl_testi_4_shortcode() {
	// admin area functions

}

function sl_testi_4_path( $atts, $content = null ) {
	// front-end functions
	extract( shortcode_atts( array(
		'extra_class'	   => null,
		'css_class'        => '',
		'posts_per_page'   => '',
		'enable_nav'       => 'true',
		'autoplay'         => 'false',
		'speed'            => '5000'
	), $atts ) );

	global $secretlab;

	$slick_params = array( 'class'      => $extra_class ? $extra_class : 'testi_box_four',
		'enable_nav' => $enable_nav,
		'autoplay'   => $autoplay,
		'speed'      => $speed ? $speed : 5000,
		'dots'       => 'true',
		'arrows'     => 'false',
		'sp_row'     => 1,
		'sp_show'    => 2,
		'sp_scroll'  => 2,
		'769' => array ( 'sp_row' => 1, 'sp_show' => 1, 'sp_scroll' => 1 ));
	$secretlab['slick'][] = $slick_params;


	$result = new WP_query('post_type=testimonial&posts_per_page='.$posts_per_page);

	$output = '';

	//Output posts

	if ( $result->posts ) :

		add_action('wp_footer', 'theseo_add_slick_carousel');

		// Main wrapper div

		$output .= '<div class="carousel '.$slick_params['class'].' '.$css_class.'">';

		// Loop through posts
		foreach ( $result->posts as $post ) :

			// Post VARS
			$post_id          = $post->ID;
			if (function_exists("types_render_field")) {
			$client_name = types_render_field("name-of-client", array("post_id" => $post_id, "output"=>"normal"));
			$client_photo = types_render_field("photo-of-client", array("post_id" => $post_id, "url" => "true"));
			} else {$client_name = $client_post = $client_photo = '';}
			// Testimonial post article start

			// Open details div
			$output .= '<div class="item"><div class="mention"><p>'.theseo_get_excerpt($post, null, 55).'</p></div><div class="face">'.'<a href="'.get_permalink($post_id).'"><img src="'.$client_photo.'" alt=""></a><strong>'.$client_name.'</strong></div></div>';



			// End foreach loop
		endforeach;

		$output .= '</div>';


	endif; // End has posts check



	// Set things back to normal
	$result = null;
	wp_reset_postdata();

	// Return output
	return $output;
}


