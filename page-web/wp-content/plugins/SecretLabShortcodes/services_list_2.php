<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '' ); // Don't call directly
}

add_action( 'vc_build_admin_page', 'sell_slist_2_init' );
add_action( 'vc_after_init', 'sell_slist_2_shortcode' );
add_shortcode('sell_slist_2', 'sell_slist_2_path');
function sell_slist_2_init() {
	

		vc_map( array(
			'name'        => __( 'Services List #2', 'sell' ),
			'base'        => 'sell_slist_2',
			'description' => __( 'Displays services in 4 columns', 'sell' ),
			'category'    => __( 'SecretLab', 'sell' ),
			'icon'        => 'sell_ai slist2',
			'params'      => array(
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Extra class name', 'sell' ),
					'param_name'	=> 'css_class',
					'description'	=> __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'sell' ),
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Services quantity ', 'sell' ),
					'param_name'	=> 'posts_per_page',
					'description'	=> __( 'how many services displays on the page', 'sell' ),
					'value'         => 8
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> __( 'Columns', 'sell' ),
					'param_name'	=> 'columns',
					'description'	=> __( '', 'sell' ),
					'value'         => array (
						__( '3 Columns', 'sell' ) => '3',
						__( '4 Columns', 'sell' ) => '4',
					),
					'std' => '4'
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> __( 'Text and icon align', 'sell' ),
					'param_name'	=> 'block_align',
					'description'	=> __( '', 'sell' ),
					'value'         => array (
						__( 'Center', 'sell' ) => 'center',
						__( 'Right', 'sell' ) => 'right',
						__( 'Left', 'sell' ) => 'left'
					),
					'std' => 'left'
				),
				array(
					'type'			=> 'colorpicker',
					'heading'		=> __( 'Background Color', 'sell' ),
					'param_name'	=> 'bgr_color',
					'description'	=> __( '', 'sell' ),
					'value'         => '#44dd61'
				),
				array(
					'type'			=> 'colorpicker',
					'heading'		=> __( 'Background Color (hover)', 'sell' ),
					'param_name'	=> 'bgr_color_h',
					'description'	=> __( '', 'sell' ),
					'value'         => '#26b7e7'
				),
				array(
					'type'			=> 'colorpicker',
					'heading'		=> __( 'Heading Color', 'sell' ),
					'param_name'	=> 'heading_color',
					'description'	=> __( '', 'sell' ),
					'value'         => '#ffffff'
				),
				array(
					'type'			=> 'colorpicker',
					'heading'		=> __( 'Heading Color (hover)', 'sell' ),
					'param_name'	=> 'heading_color_h',
					'description'	=> __( '', 'sell' ),
					'value'         => '#ffffff'
				),
				array(
					'type'			=> 'colorpicker',
					'heading'		=> __( 'Text Color', 'sell' ),
					'param_name'	=> 'text_color',
					'description'	=> __( '', 'sell' ),
					'value'         => '#ffffff'
				),
				array(
					'type'			=> 'colorpicker',
					'heading'		=> __( 'Text Color (hover)', 'sell' ),
					'param_name'	=> 'text_color_h',
					'description'	=> __( '', 'sell' ),
					'value'         => '#ffffff'
				),
				array(
					'type'			=> 'colorpicker',
					'heading'		=> __( 'Border Color', 'sell' ),
					'param_name'	=> 'border_color',
					'description'	=> __( '', 'sell' ),
					'value'         => '#eeeeee'
				),
				array(
					'type'			=> 'colorpicker',
					'heading'		=> __( 'Border Color (hover)', 'sell' ),
					'param_name'	=> 'border_color_h',
					'description'	=> __( '', 'sell' ),
					'value'         => '#26b7e7'
				),
			)
		));

}

function sell_slist_2_shortcode() {
	// admin area functions

}

function sell_slist_2_path( $atts, $content = null ) {
	// front-end functions
	extract(shortcode_atts(array(
		'css_class' => '',
		'posts_per_page' => '8',
		'columns' => '4',
		'bgr_color' => '#44dd61',
		'bgr_color_h' => '#26b7e7',
		'heading_color' => '#ffffff',
		'heading_color_h' => '#ffffff',
		'text_color' => '#ffffff',
		'text_color_h' => '#ffffff',
		'border_color' => '#eeeeee',
		'border_color_h' => '#26b7e7',
		'block_align' => 'left',
	), $atts));

	global $secretlab;


	$result = new WP_query('post_type=service&posts_per_page=' . $posts_per_page);

	// Style section
	$style = '<style type="text/css">';
	$style .= '.slist_sc_2 .slist_item {border: 5px ' . $border_color . ' solid; background-color: ' . $bgr_color . '}';
	$style .= '.slist_sc_2 .slist_item:hover {border-color:' . $border_color_h . '; background-color: ' . $bgr_color_h . '}';
	$style .= '.slist_sc_2 .slist_item .ss2_header h3 a {color: ' . $heading_color . '}';
	$style .= '.slist_sc_2 .slist_item:hover .ss2_header h3 a {color: ' . $heading_color_h . '}';
	$style .= '.slist_sc_2 .slist_item .ss2_description, .slist_sc_2 .slist_item .ss2_description a {color: ' . $text_color . '}';
	$style .= '.slist_sc_2 .slist_item:hover .ss2_description, .slist_sc_2 .slist_item:hover .ss2_description a {color: ' . $text_color_h . '}';
	$style .= '</style>';

	$cols = '';
	if ($columns == 3) {
		$cols = ' cols3';
	}

	$output = '';

	//Output posts

	if ( $result->posts ) :
		//if ( $block_align == 'left') {

		// Main wrapper div

		$output .= '<div class="slist_sc_2 '.$css_class.' text-'.$block_align.$cols.'">';

		// Loop through posts
		foreach ( $result->posts as $post ) :

			// Post VARS
			$post_id          = $post->ID;
			if (function_exists("types_render_field")) {
			$sell_servico = types_render_field( "sicon", array("post_id" => $post_id, "url" => "true"));
			} else {$sell_servico = '';}
			// Testimonial post article start

			// Open details div
			$output .= '<div class="slist_item">
										<div class="ss2_icon"><img src="'.esc_url($sell_servico).'"  alt="'.get_the_title($post_id).'"></div>
										<div class="ss2_header"><h3><a href="'.get_permalink($post_id).'">'.get_the_title($post_id).'</a></h3></div>
										<div class="ss2_description"><a href="'.get_permalink($post_id).'">'.get_the_excerpt($post_id).'</a></div>
									</div>';

			// End foreach loop
		endforeach;
		$output .= '</div>';
	endif; // End has posts check

	// Set things back to normal
	$result = null;
	wp_reset_postdata();

	// Return output
	return $style . $output;
}


