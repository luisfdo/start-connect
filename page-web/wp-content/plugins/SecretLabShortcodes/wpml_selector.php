<?php
if(!class_exists('sell_wmpl')){

	class sell_wmpl {

		function __construct(){
			add_action('init',array($this,'sell_wmpl_init'));
			add_shortcode('sell_wmpl',array($this,'sell_wmpl_shortcode'));
		}
		
		function sell_wmpl_init() {
			if(function_exists("vc_map")){
				vc_map( array(
					'name'        => __( 'Simple WMPL', 'sell' ),
					'base'        => 'sell_wmpl',
					'description' => __( 'Displays title with typography options', 'sell' ),
					'category'    => __( 'SecretLab', 'sell' ),
					'icon'        => 'sell_ai title',
					'params'      => array(

						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Text', 'sell' ),
							'param_name'	=> 'text',
							'holder'		=> 'span',
							'class'			=> 'vc_admin_label admin_label_text',
							'value'         => ''
						),

					)
				));
			}
		}

		function sell_wmpl_shortcode($atts, $content = null) {
			extract( shortcode_atts( array(
				'css_class'		=> '',
			), $atts ) );

			$out = '';


		ob_start();
    $wrp_classes = apply_filters( 'kc-el-class', $atts );
   
    do_action('wpml_add_language_selector');
    $action = ob_get_contents();
    ob_end_clean();
    
    $out .= '<div  class="'.$css_class.'">'.$action.'</div>';


			return $out;
		}

	}

}

new sell_wmpl();

if(class_exists('WPBakeryShortCode'))
{
	class WPBakeryShortCode_sell_wmpl extends WPBakeryShortCode {
	}
}	


