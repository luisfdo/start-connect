<?php

/*
* Last Blog Posts Shortcode with 4 posts
*/

function display_blog_4_post ( $p ) {
	$out = '';
	$out .= '<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 last_4_posts one">
							<article class="post format-standard has-post-thumbnail clearfix">
								<header class="entry-header">';
	if ( has_post_thumbnail($p->ID) && $p->post_type != 'attachment' ) {
		$out .=
			'<div class="entry-thumbnail">'.
			get_the_post_thumbnail( $p->ID, 'theseo_last41').
			'<div class="over"><a href="'.get_permalink($p->ID).'"><i class="icon icon-eye"></i></a></div></div>';

	} else {
		$out .= '';
	}
	$author = get_user_by('id', $p->post_author);
	$out.= '<div class="entry-meta">
										<span class="date">
											<time class="entry-date" datetime="'.get_the_date("c", $p->ID).'">'.get_the_date("M n, Y", $p->ID).'</time> /
										</span>
										<span class="author vcard">posted by '.$author->user_login.'</span>
									</div>
									<h3 class="entry-title">';
	$out .= '<a href="'.get_permalink($p->ID).'" rel="bookmark">'.get_the_title($p->ID).'</a></h3>';

	$out .= '</header>
					<div class="entry-content">';

	if( !post_password_required( $p->ID ) ){
		$content = theseo_get_excerpt($p, null, 37);
	} else {
		$content = '<p>'.esc_html__( 'There is no excerpt because this is a protected post.', 'the-guard' ).'</p>';
	}
	$out .= $content;


	$out .= '</div>';
	if (comments_open($p->ID)) {
		$comments = wp_count_comments( $p->ID );
		if ($comments->approved > 0) {
			$out .= '<span class="comments-link"><span class="icon-comments"></span> ';
			$out .= '<a href="'.get_permalink($p->ID).'#comments'.'">'.$comments->approved.' '.esc_html__('comments', 'the-lawyer').'</a>';
			$out .= '</span>';
		}
		else {
			$out .= '<span class="comments-link"><span class="icon-comments"></span> 
		                    <a href="'.get_permalink($p->ID).'#respond"> '.esc_html__( 'Leave a comment', 'the-lawyer' ).'</a>
				        </span>';
		}

	}
	$out .= '</article></div>';

	return $out;

}

function display_blog_4_post_last ( $p ) {
	$out = '';
	$out .= '
							<article class="post format-standard has-post-thumbnail clearfix">
								<header class="entry-header">';
	if ( has_post_thumbnail($p->ID) && $p->post_type != 'attachment' ) {
		$out .=
			'<div class="entry-thumbnail">'.
			get_the_post_thumbnail( $p->ID, 'theseo_last4' ).
			'<div class="over"><a href="'.get_permalink($p->ID).'"><i class="icon icon-eye"></i></a></div></div>';

	} else {
		$out .= '';
	}
	$author = get_user_by('id', $p->post_author);
	$out.= '<div class="entry-meta">
										<span class="date">
											<time class="entry-date" datetime="'.get_the_date("c", $p->ID).'">'.get_the_date("M j, Y", $p->ID).'</time> /
										</span>
										<span class="author vcard">posted by '.$author->user_login.'</span>
									</div>
									<h3 class="entry-title">';
	$out .= '<a href="'.get_permalink($p->ID).'" rel="bookmark">'.get_the_title($p->ID).'</a></h3>';

	$out .= '</header>
					<div class="entry-content">';

	if( !post_password_required( $p->ID ) ){
		$content = theseo_get_excerpt($p, null, 7);
	} else {
		$content = '<p>'.esc_html__( 'There is no excerpt because this is a protected post.', 'the-guard' ).'</p>';
	}
	$out .= $content;

	$out .= '</div>';
	if (comments_open($p->ID)) {
		$comments = wp_count_comments( $p->ID );
		if ($comments->approved > 0) {
			$out .= '<span class="comments-link"><span class="icon-comments"></span> ';
			$out .= '<a href="'.get_permalink($p->ID).'#comments'.'">'.$comments->approved.' '.esc_html__('comments', 'the-lawyer').'</a>';
			$out .= '</span>';
		}
		else {
			$out .= '<span class="comments-link"><span class="icon-comments"></span> 
		                    <a href="'.get_permalink($p->ID).'#respond"> '.esc_html__( 'Leave a comment', 'the-lawyer' ).'</a>
				        </span>';
		}

	}
	$out .= '</article>';

	return $out;

}

add_shortcode( 'last_4_blog_posts', 'last_4_blog_posts_shortcode' );

function last_4_blog_posts_shortcode($atts, $content = null) {
	extract( shortcode_atts( array(
		'category'				=> '',
		'post_type'             => 'post',
		'class'                 => ''
	), $atts ) );

	$args = array(  'posts_per_page' => 4,
		'ignore_sticky_posts' => true,
		'category_name' => $category,
		'post_type' => explode(',', $post_type),
		'orderby' => 'date',
		'order' => 'DESC'
	);

	$result = new WP_query( $args );


	$i = 1; $out = ''; 
	$class = $class = '' ? '' : ' ' . $class . ' ';
	if ( $result->posts ) :
 
        $out = '<div id="sl_4_poster' . rand( 10001, 100000 ) . '" class="sl_4_poster' . $class . '">';
 
		foreach ( $result->posts as $p ) :
		
            if ( $i == 1 ) {
			    $out .= display_blog_4_post( $p );
			}
			else {
			    $out .= '<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 last_4_posts three">';
			    $out .= display_blog_4_post_last( $p );
				$out .= '</div>';
			}
			$i++;

		endforeach;
		
		$out .= '</div>';

	endif; // End has posts check

	$result = null;
	wp_reset_postdata();

	return $out;

}


vc_map( array(
	'name'        => __( 'Latest news: 4 Blog Posts', 'sell' ),
	'base'        => 'last_4_blog_posts',
	'description' => __( 'Display 4 Posts, any Post Type in 2 columns', 'sell' ),
	'category'    => __( 'SecretLab', 'sell' ),
	'icon'        => 'sell_ai blog13',
	'params'      => array(


		array(
			'type'			=> 'posttypes', //'dropdown',
			'heading'		=> __( 'Post type', 'sell' ),
			'param_name'	=> 'post_type',
			'description'	=> __( 'Select needed post-type', 'sell' ),
			//'value'         => array_merge(array(''=>'any'), get_registered_post_types())
		),
		array(
			'type'			=> 'dropdown',
			'heading'		=> __( 'Category', 'sell' ),
			'param_name'	=> 'category',
			'description'	=> __( 'Attention! this list can be applied to POST post_type only', 'sell' ),
			'value'         => get_post_categories()
		),

		array(
			'type'			=> 'textfield',
			'heading'		=> __( 'Custom CSS Class', 'sell' ),
			'param_name'	=> 'class',
			'description'	=> __( '', 'sell' ),
			'value'         => ''
		),

	)
));

?>
