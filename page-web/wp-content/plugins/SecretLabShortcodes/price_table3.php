<?php

    if(!class_exists('sell_pricetable3')){
	    
		class sell_pricetable3 {
		
			function __construct(){
				add_action('init',array($this,'sell_pricetable3_init'));
				add_shortcode('price_table3',array($this,'sell_pricetable3_shortcode'));
			}
			
			
            function sell_pricetable3_init() {
			    if(function_exists("vc_map")){
					vc_map( array(
						'name'        => __( 'Price Table Layout#3', 'sell' ),
						'base'        => 'price_table3',
						'description' => __( 'Display price table in 2, 3 or 4 columns', 'sell' ),
						'category'    => __( 'SecretLab', 'sell' ),
						'icon'        => 'sell_ai pricetable3',
						'params'      => array(
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Unique ID', 'sell' ),
								'param_name'	=> 'uid',
								'description'	=> __( 'May be used for styling', 'sell' ),
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Extra class ', 'sell' ),
								'param_name'	=> 'extra_class',
								'description'	=> __( 'extra css class', 'sell' )
							),

							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Title', 'sell' ),
								'param_name'	=> 'title1',
								'description'	=> __( 'Main title - column will appear if Title field filled', 'sell' ),
								'group'         => 'Column #1'
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Subtitle', 'sell' ),
								'param_name'	=> 'subtitle1',
								'description'	=> __( 'subtitle', 'sell' ),
								'group'         => 'Column #1'
							),
							array(
								'type'			=> 'dropdown',
								'heading'		=> __( 'Best Offer', 'sell' ),
								'param_name'	=> 'best_offer1',
								'description'	=> __( 'set to TRUE, if you want to set to column Best Offer style', 'sell' ),
								'value'         => array( 'false' => 'false',
									'true'  => 'true' ),
								'group'         => 'Column #1'
							),
							array(
								'type'			=> 'textarea',
								'heading'		=> __( 'Content', 'sell' ),
								'param_name'	=> 'contents1',
								'description'	=> __( 'You can use HTML here. For example, &lt;ul&gt;&lt;li&gt;Item 1&lt;/li&gt;&lt;li&gt;Item 2&lt;/li&gt;&lt;/ul&gt;', 'sell' ),
								'group'         => 'Column #1'
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Pricing line', 'sell' ),
								'param_name'	=> 'last_line1',
								'description'	=> __( 'content for the pricing area', 'sell' ),
								'group'         => 'Column #1'
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Form URL', 'sell' ),
								'param_name'	=> 'href1',
								'description'	=> __( 'url of handler od this block', 'sell' ),
								'group'         => 'Column #1'
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Order Button Text', 'sell' ),
								'param_name'	=> 'order_button_text1',
								'description'	=> __( 'content for the order button', 'sell' ),
								'group'         => 'Column #1'
							),

							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Title', 'sell' ),
								'param_name'	=> 'title2',
								'description'	=> __( 'Main title - column will appear if Title field filled', 'sell' ),
								'group'         => 'Column #2'
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Subtitle', 'sell' ),
								'param_name'	=> 'subtitle2',
								'description'	=> __( 'subtitle', 'sell' ),
								'group'         => 'Column #2'
							),
							array(
								'type'			=> 'dropdown',
								'heading'		=> __( 'Best Offer', 'sell' ),
								'param_name'	=> 'best_offer2',
								'description'	=> __( 'set to TRUE, if you want to set to column Best Offer style', 'sell' ),
								'value'         => array( 'false' => 'false',
									'true'  => 'true' ),
								'group'         => 'Column #2'
							),
							array(
								'type'			=> 'textarea',
								'heading'		=> __( 'Content', 'sell' ),
								'param_name'	=> 'contents2',
								'description'	=> __( 'You can use HTML here. For example, &lt;ul&gt;&lt;li&gt;Item 1&lt;/li&gt;&lt;li&gt;Item 2&lt;/li&gt;&lt;/ul&gt;', 'sell' ),
								'group'         => 'Column #2'
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Pricing line', 'sell' ),
								'param_name'	=> 'last_line2',
								'description'	=> __( 'content for the pricing area', 'sell' ),
								'group'         => 'Column #2'
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Form URL', 'sell' ),
								'param_name'	=> 'href2',
								'description'	=> __( 'url of handler od this block', 'sell' ),
								'group'         => 'Column #2'
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Order Button Text', 'sell' ),
								'param_name'	=> 'order_button_text2',
								'description'	=> __( 'content for the order button', 'sell' ),
								'group'         => 'Column #2'
							),

							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Title', 'sell' ),
								'param_name'	=> 'title3',
								'description'	=> __( 'Main title - column will appear if Title field filled', 'sell' ),
								'group'         => 'Column #3'
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Subtitle', 'sell' ),
								'param_name'	=> 'subtitle3',
								'description'	=> __( 'subtitle', 'sell' ),
								'group'         => 'Column #3'
							),
							array(
								'type'			=> 'dropdown',
								'heading'		=> __( 'Best Offer', 'sell' ),
								'param_name'	=> 'best_offer3',
								'description'	=> __( 'set to TRUE, if you want to set to column Best Offer style', 'sell' ),
								'value'         => array( 'false' => 'false',
									'true'  => 'true' ),
								'group'         => 'Column #3'
							),
							array(
								'type'			=> 'textarea',
								'heading'		=> __( 'Content', 'sell' ),
								'param_name'	=> 'contents3',
								'description'	=> __( 'You can use HTML here. For example, &lt;ul&gt;&lt;li&gt;Item 1&lt;/li&gt;&lt;li&gt;Item 2&lt;/li&gt;&lt;/ul&gt;', 'sell' ),
								'group'         => 'Column #3'
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Pricing line', 'sell' ),
								'param_name'	=> 'last_line3',
								'description'	=> __( 'content for the pricing area', 'sell' ),
								'group'         => 'Column #3'
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Form URL', 'sell' ),
								'param_name'	=> 'href3',
								'description'	=> __( 'url of handler od this block', 'sell' ),
								'group'         => 'Column #3'
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Order Button Text', 'sell' ),
								'param_name'	=> 'order_button_text3',
								'description'	=> __( 'content for the order button', 'sell' ),
								'group'         => 'Column #3'
							),

							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Title', 'sell' ),
								'param_name'	=> 'title4',
								'description'	=> __( 'Main title - column will appear if Title field filled', 'sell' ),
								'group'         => 'Column #4'
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Subtitle', 'sell' ),
								'param_name'	=> 'subtitle4',
								'description'	=> __( 'subtitle', 'sell' ),
								'group'         => 'Column #4'
							),
							array(
								'type'			=> 'dropdown',
								'heading'		=> __( 'Best Offer', 'sell' ),
								'param_name'	=> 'best_offer4',
								'description'	=> __( 'set to TRUE, if you want to set to column Best Offer style', 'sell' ),
								'value'         => array( 'false' => 'false',
									'true'  => 'true' ),
								'group'         => 'Column #4'
							),
							array(
								'type'			=> 'textarea',
								'heading'		=> __( 'Content', 'sell' ),
								'param_name'	=> 'contents4',
								'description'	=> __( 'You can use HTML here. For example, &lt;ul&gt;&lt;li&gt;Item 1&lt;/li&gt;&lt;li&gt;Item 2&lt;/li&gt;&lt;/ul&gt;', 'sell' ),
								'group'         => 'Column #4'
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Pricing line', 'sell' ),
								'param_name'	=> 'last_line4',
								'description'	=> __( 'content for the pricing area', 'sell' ),
								'group'         => 'Column #4'
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Form URL', 'sell' ),
								'param_name'	=> 'href4',
								'description'	=> __( 'url of handler od this block', 'sell' ),
								'group'         => 'Column #4'
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Order Button Text', 'sell' ),
								'param_name'	=> 'order_button_text4',
								'description'	=> __( 'content for the order button', 'sell' ),
								'group'         => 'Column #4'
							)

						)
					));
				}
            }

			function sell_pricetable3_shortcode($atts, $content = null) {
				extract( shortcode_atts( array(
					'uid'                   => '',
					'extra_class'           => '',
					'title1'                 => 'Title',
					'subtitle1'              => '',
					'best_offer1'             => false,
					'contents1'              => '',
					'last_line1'             => 'Price',
					'href1'                  => '',
					'order_button_text1'     => '',

					'title2'                 => 'Title',
					'subtitle2'              => '',
					'best_offer2'             => false,
					'contents2'              => '',
					'last_line2'             => 'Price',
					'href2'                  => '',
					'order_button_text2'     => '',

					'title3'                 => 'Title',
					'subtitle3'              => '',
					'best_offer3'             => false,
					'contents3'              => '',
					'last_line3'             => 'Price',
					'href3'                  => '',
					'order_button_text3'     => '',

					'title4'                 => 'Title',
					'subtitle4'              => '',
					'best_offer4'             => false,
					'contents4'              => '',
					'last_line4'             => 'Price',
					'href4'                  => '',
					'order_button_text4'     => ''
				), $atts ) );

				$j = 0;

				$out = '
		<div class="row center-price '.$extra_class.'">';
				for ($i = 1; $i <= 4; $i++) {
					$title = 'title'.$i;
					$subtitle = 'subtitle'.$i;
					$best_offer = 'best_offer'.$i;
					$contents = 'contents'.$i;
					$last_line = 'last_line'.$i;
					$href = 'href'.$i;
					$order_button_text = 'order_button_text'.$i;

					if (!preg_match('/<li>/', html_entity_decode($$contents))) continue;

					$j++;
					if ($$best_offer) $best_offer_class = ' bestoffer'; else $best_offer_class = '';

					$out .= '
				
				<div class="col-lg-#n# col-md-#n# col-sm-12 col-xs-12">
					<ul class="pricetable3 '.$best_offer_class.'">
						<li>
							<h3>
								<span>'.$$subtitle.'</span>'.
						$$title.
						'</h3>
						</li>';

					preg_match_all('/<li>(.+)<\/li>/Uims', html_entity_decode($$contents), $lines);

					if (count($lines[0]) > 0) {
						foreach ($lines[0] as $line) {
							$out .= $line;
						}
					}

					$out .= '<li class="worth">'.$$last_line.'</li>';
					if ($$href != '' && $$order_button_text != '') {
					$out .= '<li><a class="btn btn-info" href="'.$$href.'"><span class="icon icon-compass2"></span>'.$$order_button_text.'</a></li>';
					}
					$out .= '</ul>
			</div>';
				}

				$out .= '</div>';

				//if ($j > 1) $j = $j - 1; else $j = $j;

				$n = 12/$j;

				$out = preg_replace('/#n#/', $n, $out);

				return $out;

			}

		}
		
	}
	
	new sell_pricetable3();
	
	if(class_exists('WPBakeryShortCode')) {
		class WPBakeryShortCode_sell_pricetable3 extends WPBakeryShortCode {

		}
	}	