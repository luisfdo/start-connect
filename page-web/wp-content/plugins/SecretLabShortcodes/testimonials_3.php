<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '' ); // Don't call directly
}

add_action( 'vc_after_init', 'sl_team_2_shortcode' );
add_shortcode('teammates_2', 'sl_team_2_path');

		vc_map(array(
			'name' => __('Teammates #2 ', 'sell'),
			'base' => 'teammates_2',
			'description' => __('Displays 3 teammates per row.', 'sell'),
			'category' => __('SecretLab', 'sell'),
			'icon' => 'sell_ai team2',
			'params' => array(



			)
		));


function sl_team_2_shortcode() {
 // admin area functions

}

function sl_team_2_path( $atts, $content = null ) {
	// front-end functions
	
}


