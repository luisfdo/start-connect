<?php
    
/*	if(!class_exists('SL_team_2')) {

		class SL_team_2
		{

			function __construct()
			{
				add_action('init', array($this, 'init'));
				add_shortcode('teammates_2', array($this, 'sl_team_2_shortcode'));
			}

			function init(){

				if (function_exists("vc_map")) {

					vc_map( array(
						'name'        => __( 'Teammates #2 ', 'sell' ),
						'base'        => 'teammates_2',
						'description' => __( 'Displays 3 teammates per row', 'sell' ),
						'category'    => __( 'SecretLab', 'sell' ),
						'icon'        => 'sell_ai team2',
						'params'      => array(

							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Posts quantity ', 'sell' ),
								'param_name'	=> 'posts_per_page',
								'description'	=> __( 'how many teammates to retrieve on page', 'sell' ),
								'value'         => 9
							),
							array(
								'type'			=> 'dropdown',
								'heading'		=> __( 'Enable\disable navigation', 'sell' ),
								'param_name'	=> 'enable_nav',
								'description'	=> __( 'set ability of navigation', 'sell' ),
								'value'         => array( 'disable' => 'false',
									'enable'  => 'true', ),
								'default'       => 'true'
							),
							array(
								'type'			=> 'dropdown',
								'heading'		=> __( 'Enable\disable autoplay', 'sell' ),
								'param_name'	=> 'autoplay',
								'description'	=> __( 'set autoplay on/off', 'sell' ),
								'value'         => array( 'disable' => 'false',
									'enable'  => 'true', ),
								'default'       => 'false'
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Duration between slides in ms', 'sell' ),
								'param_name'	=> 'speed',
								'description'	=> __( 'time of pause between slides', 'sell' ),
								'value'         => 5000
							),
							array(
								'type'			=> 'textfield',
								'heading'		=> __( 'Extra Class Name', 'sell' ),
								'param_name'	=> 'extra_class',
								'description'	=> __( 'May be used for styling with CSS', 'sell' ),
							),

						)
					));

				}
			}


			function sl_team_2_shortcode($atts)	{

				extract( shortcode_atts( array(
					'extra_class'      => '',
					'main_title'       => '',
					'description'      => '',
					'posts_per_page'   => '9',
					'enable_nav'       => 'true',
					'autoplay'         => 'false',
					'speed'            => '5000'
				), $atts ) );


				global $secretlab;

				$slick_params = array( 'class'      => $extra_class ? $extra_class : 'teammate_box_second',
					'enable_nav' => $enable_nav,
					'autoplay'   => $autoplay,
					'speed'      => $speed ? $speed : 5000,
					'dots'       => 'true',
					'arrows'     => 'false',
					'sp_row'     => 3,
					'sp_show'    => 3,
					'sp_scroll'  => 3,
					'992'        => array ( 'sp_row' => 2, 'sp_show' => 2, 'sp_scroll' => 2 ),
					'769'        => array ( 'sp_row' => 2, 'sp_show' => 2, 'sp_scroll' => 2 ),
					'521'        => array ( 'sp_row' => 1, 'sp_show' => 1, 'sp_scroll' => 1 ),
				);
				$secretlab['slick'][] = $slick_params;

				$result = new WP_query('post_type=teammate&posts_per_page='.$posts_per_page);

				$output = '';



				//Output posts
				if ( $result->posts ) :

					add_action('wp_footer', 'theseo_add_slick_carousel');

					$output .=  '<div id="teamsecond" class="carousel '.$slick_params['class'].' team-slide">';

					// Loop through posts
					foreach ( $result->posts as $post ) :

						// Post VARS
						$post_id          = $post->ID;

						$member_post = types_render_field("post-of-member", array("post_id" => $post_id, "output"=>"normal"));
						$member_photo = types_render_field("photo-of-member", array("post_id" => $post_id, "output"=>"raw"));
						$fb_profile = types_render_field("facebook-profile", array("post_id" => $post_id, "output"=>"normal"));
						$yt_profile = types_render_field("youtube-profile", array("post_id" => $post_id, "output"=>"normal"));
						$tw_profile = types_render_field("twitter-profile", array("post_id" => $post_id, "output"=>"normal"));
						$bh_profile = types_render_field("behance-profile", array("post_id" => $post_id, "output"=>"normal"));
						$ln_profile = types_render_field("linkedin-profile", array("post_id" => $post_id, "output"=>"normal"));



						$output .= '
										<div class="photo">
										
										<a href="'.get_permalink($post_id).'" rel="bookmark">
										<img src="'.$member_photo.'" alt="" class="img-responsive"/>
										</a>

										<span class="name"><a href="'.get_permalink($post_id).'" rel="bookmark">'.$post->post_title.'</a></span>
										<span class="regalies">'.$member_post.'</span><div class="sl_t2_soc">';

						if (!empty($fb_profile)) {
							$output .= '<a href="'.$fb_profile.'" target="_blank"><i class="fa fa-facebook"></i></a>';
						}
						if (!empty($yt_profile)) {
							$output .= '<a href="'.$yt_profile.'" target="_blank"><i class="fa fa-youtube-play"></i></a>';
						}
						if (!empty($tw_profile)) {
							$output .= '<a href="'.$tw_profile.'" target="_blank"><i class="fa fa-twitter"></i></a>';
						}
						if (!empty($bh_profile)) {
							$output .= '<a href="'.$bh_profile.'" target="_blank"><i class="fa fa-behance"></i></a>';
						}
						if (!empty($ln_profile)) {
							$output .= '<a href="'.$ln_profile.'" target="_blank"><i class="fa fa-linkedin"></i></a>';
						}
						$output .= '
                                                </div>

                                        </div>
                                    ';


						// End foreach loop
					endforeach;

					$output .= '</div>';


				endif; // End has posts check



				// Set things back to normal
				$result = null;
				wp_reset_postdata();

				// Return output
				return $output;

			}

		}

	}
	
	new SL_team_2();
	
	if(class_exists('WPBakeryShortCode'))
	{
		class WPBakeryShortCode_SL_team_2 extends WPBakeryShortCode {
		}
	}	
	
*/



if ( ! defined( 'ABSPATH' ) ) {
die( '' ); // Don't call directly
}

add_action( 'vc_after_init', 'sl_team_2_shortcode' );
add_shortcode('teammates_2', 'sl_team_2_path');
		vc_map(array(
			'name' => __('Teammates #2 ', 'sell'),
			'base' => 'teammates_2',
			'description' => __('Displays 3 teammates per row.', 'sell'),
			'category' => __('SecretLab', 'sell'),
			'icon' => 'sell_ai team2',
			'params' => array(

				array(
					'type' => 'textfield',
					'heading' => __('Posts quantity ', 'sell'),
					'param_name' => 'posts_per_page',
					'description' => __('how many teammates to retrieve on page', 'sell'),
					'value' => 9
				),
				array(
					'type' => 'dropdown',
					'heading' => __('Enable\disable navigation', 'sell'),
					'param_name' => 'enable_nav',
					'description' => __('set ability of navigation', 'sell'),
					'value' => array('disable' => 'false',
						'enable' => 'true',),
					'default' => 'true'
				),
				array(
					'type' => 'dropdown',
					'heading' => __('Enable\disable autoplay', 'sell'),
					'param_name' => 'autoplay',
					'description' => __('set autoplay on/off', 'sell'),
					'value' => array('disable' => 'false',
						'enable' => 'true',),
					'default' => 'false'
				),
				array(
					'type' => 'textfield',
					'heading' => __('Duration between slides in ms', 'sell'),
					'param_name' => 'speed',
					'description' => __('time of pause between slides', 'sell'),
					'value' => 5000
				),
				array(
					'type' => 'textfield',
					'heading' => __('Extra Class Name', 'sell'),
					'param_name' => 'extra_class',
					'description' => __('May be used for styling with CSS', 'sell'),
				),

			)
		));

function sl_team_2_shortcode() {

}
function sl_team_2_path( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'extra_class'      => '',
		'posts_per_page'   => '9',
		'enable_nav'       => 'true',
		'autoplay'         => 'false',
		'speed'            => '5000'
	), $atts ) );

	global $secretlab;

	$slick_params = array( 'class'      => 'teammate_box_second',
		'enable_nav' => $enable_nav,
		'autoplay'   => $autoplay,
		'speed'      => $speed ? $speed : 5000,
		'dots'       => 'true',
		'arrows'     => 'false',
		'sp_row'     => 3,
		'sp_show'    => 3,
		'sp_scroll'  => 3,
		'992'        => array ( 'sp_row' => 2, 'sp_show' => 2, 'sp_scroll' => 2 ),
		'769'        => array ( 'sp_row' => 2, 'sp_show' => 2, 'sp_scroll' => 2 ),
		'521'        => array ( 'sp_row' => 1, 'sp_show' => 1, 'sp_scroll' => 1 ),
	);
	$secretlab['slick'][] = $slick_params;
	$result = new WP_query('post_type=teammate&posts_per_page='.$posts_per_page);
	$output = '';

	//Output posts
	if ( $result->posts ) :
		add_action('wp_footer', 'theseo_add_slick_carousel');
		$output .=  '<div id="teamsecond" class="carousel '.$slick_params['class'].' teammate_box_second team-slide">';
		// Loop through posts
		foreach ( $result->posts as $post ) :

			// Post VARS
			$post_id          = $post->ID;
			if (function_exists("types_render_field")) {
			$member_post = types_render_field("post-of-member", array("post_id" => $post_id, "output"=>"normal"));
			$member_photo = types_render_field("photo-of-member", array("post_id" => $post_id, "output"=>"raw"));
			$fb_profile = types_render_field("facebook-profile", array("post_id" => $post_id, "output"=>"normal"));
			$yt_profile = types_render_field("youtube-profile", array("post_id" => $post_id, "output"=>"normal"));
			$tw_profile = types_render_field("twitter-profile", array("post_id" => $post_id, "output"=>"normal"));
			$bh_profile = types_render_field("behance-profile", array("post_id" => $post_id, "output"=>"normal"));
			$ln_profile = types_render_field("linkedin-profile", array("post_id" => $post_id, "output"=>"normal"));
			} else {$member_post = $member_photo = $fb_profile = $yt_profile = $tw_profile = $bh_profile = $ln_profile = $sell_memberphone = $sell_memberemail = '';}
			$output .= '<div class="photo"><a href="'.get_permalink($post_id).'" rel="bookmark"><img src="'.$member_photo.'" alt="" class="img-responsive"/></a>';
										$output .= '<span class="name"><a href="'.get_permalink($post_id).'" rel="bookmark">'.$post->post_title.'</a></span>';
										$output .= '<span class="regalies">'.$member_post.'</span><div class="sl_t2_soc">';

			if (!empty($fb_profile)) {
				$output .= '<a href="'.$fb_profile.'" target="_blank"><i class="fa fa-facebook"></i></a>';
			}
			if (!empty($yt_profile)) {
				$output .= '<a href="'.$yt_profile.'" target="_blank"><i class="fa fa-youtube-play"></i></a>';
			}
			if (!empty($tw_profile)) {
				$output .= '<a href="'.$tw_profile.'" target="_blank"><i class="fa fa-twitter"></i></a>';
			}
			if (!empty($bh_profile)) {
				$output .= '<a href="'.$bh_profile.'" target="_blank"><i class="fa fa-behance"></i></a>';
			}
			if (!empty($ln_profile)) {
				$output .= '<a href="'.$ln_profile.'" target="_blank"><i class="fa fa-linkedin"></i></a>';
			}
			$output .= '
                                                </div>

                                        </div>
                                    ';
			// End foreach loop
		endforeach;
		$output .= '</div>';
	endif; // End has posts check

	// Set things back to normal
	$result = null;
	wp_reset_postdata();

	// Return output
	return $output;
}


