<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '' ); // Don't call directly
}

add_action( 'vc_after_init', 'sl_testi_5_shortcode' );
add_shortcode('sell_testimonials5', 'sl_testi_5_path');
		vc_map( array(
			'name'        => __( 'Testimonials #5: tabs view', 'sell' ),
			'base'        => 'sell_testimonials5',
			'description' => __( 'Display testimonials', 'sell' ),
			'category'    => __( 'SecretLab', 'sell' ),
			'icon'        => 'sell_ai testimonial5',
			'params'      => array(
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Extra class name', 'sell' ),
					'param_name'	=> 'css_class',
					'description'	=> __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'sell' ),
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Testimonials quantity ', 'sell' ),
					'param_name'	=> 'posts_per_page',
					'description'	=> __( 'how many posts to display on page', 'sell' ),
					'value'         => 3
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Offset for query', 'sell' ),
					'param_name'	=> 'offset',
					'description'	=> __( 'start offset', 'sell' ),
					'value'         => 0
				),
			)
		));

function sl_testi_5_shortcode() {
	// admin area functions

}

function sl_testi_5_path( $atts, $content = null ) {
	// front-end functions
	extract( shortcode_atts( array(
		'css_class'	   => '',
		'posts_per_page'   => '',
		'offset'           => '',
	), $atts ) );

	global $secretlab;

	$result = new WP_query('post_type=testimonial&posts_per_page='.$posts_per_page.'&offset='.$offset);

	//Output posts

	if ( $result->posts ) :

		$i = 1;
		$out = '<div class="'.$css_class.'" id="sell_tetim_tabs">';
		$tabs = '<div class="tab-content clearfix">';
		$nav = '<ul class="nav nav-pills" id="stta">';

		$out .= '';

		foreach ( $result->posts as $post ) :

			$post_id          = $post->ID;
			if (function_exists("types_render_field")) {
			$client_name = types_render_field("name-of-client", array("post_id" => $post_id, "output"=>"normal"));
			$client_post = types_render_field("post-of-client", array("post_id" => $post_id, "output"=>"normal"));
			$client_photo = types_render_field("photo-of-client", array("post_id" => $post_id, "url" => "true"));
			} else {$client_name = $client_post = $client_photo = '';}
			
			$tabs .= '<div class="tab-pane " id="' . $i . 'a"><p>' . $post->post_content . '</p><div class="face"><strong>- ' . $client_name . ' -</strong></div></div>';

			$nav  .= '<li><a href="#' . $i . 'a" data-toggle="tab"><img src="' . $client_photo . '" alt="client photo"></a></li>';

			$i++;

		endforeach;

		$tabs .= '</div>';
		$nav .= '</ul>';
		$out .= $tabs . $nav . '</div>';


	endif; // End has posts check


	// Set things back to normal
	$result = null;
	wp_reset_postdata();

	// Return output
	return $out;
}


