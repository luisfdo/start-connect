<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '' ); // Don't call directly
}

add_action( 'vc_after_init', 'sl_result_1_shortcode' );
add_shortcode('sell_seo_result_1', 'sl_result_1_path');


		vc_map( array(
			'name'        => __( 'SEO Result Items Carousel', 'sell' ),
			'base'        => 'sell_seo_result_1',
			'description' => __( 'Displays SEO results 1 per slide, with project name', 'sell' ),
			'category'    => __( 'SecretLab', 'sell' ),
			'icon'        => 'sell_ai seoresult',
			'params'      => array(
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Extra class name', 'sell' ),
					'param_name'	=> 'css_class',
					'description'	=> __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'sell' ),
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Items quantity ', 'sell' ),
					'param_name'	=> 'posts_per_page',
					'description'	=> __( 'how many works displays on the page. 1 work per slide', 'sell' ),
					'value'         => 5
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> __( 'Enable\disable navigation', 'sell' ),
					'param_name'	=> 'enable_nav',
					'description'	=> __( 'Sets availability of navigation', 'sell' ),
					'value'         => array( 'disable' => 'false',
						'enable'  => 'true', ),
					'default'       => 'true'
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> __( 'Enable\disable autoplay', 'sell' ),
					'param_name'	=> 'autoplay',
					'description'	=> __( 'Sets autoplay option', 'sell' ),
					'value'         => array( 'disable' => 'false',
						'enable'  => 'true', ),
					'default'       => 'true'
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Duration between autoplay slides', 'sell' ),
					'param_name'	=> 'speed',
					'description'	=> __( 'time of pause between slides leaf', 'sell' ),
					'value'         => 5000
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Text at Phrases in TOP-10', 'sell' ),
					'param_name'	=> 'phrases',
					'value'         => 'Phrases in TOP-10'
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Text at Traffic growth', 'sell' ),
					'param_name'	=> 'traffic',
					'value'         => 'Traffic growth'
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __( 'Text at Read more link', 'sell' ),
					'param_name'	=> 'more',
					'value'         => 'Read more'
				)

			)
		));

function sl_result_1_shortcode() {
	// admin area functions

}

function sl_result_1_path( $atts, $content = null ) {
	// front-end functions
	extract( shortcode_atts( array(
		'css_class'        => '',
		'posts_per_page'   => '5',
		'enable_nav'       => 'true',
		'autoplay'         => 'false',
		'speed'            => '5000',
		'phrases'            => 'Phrases in TOP-10',
		'traffic'            => 'Traffic growth',
		'more'            => 'Read more'
	), $atts ) );

	global $secretlab;

	$slick_params = array( 'class'      => 'sell_seo_result_1',
		'enable_nav' => $enable_nav,
		'autoplay'   => $autoplay,
		'speed'      => $speed ? $speed : 5000,
		'dots'       => 'false',
		'arrows'     => 'true',
		'sp_row'     => 1,
		'sp_show'    => 1,
		'sp_scroll'  => 1,

	);
	$secretlab['slick'][] = $slick_params;



	$result = new WP_query('post_type=portfolio&posts_per_page='.$posts_per_page.'&tag=seo');

	$output = '';

	//Output posts

	if ( $result->posts ) :

		add_action('wp_footer', 'theseo_add_slick_carousel');

		// Main wrapper div

		$output .= '<div class="carousel '.$slick_params['class'].' '.$css_class.'">';

		// Loop through posts
		foreach ( $result->posts as $post ) :

			// Post VARS
			$post_id          = $post->ID;
			if (function_exists("types_render_field")) {
			$seor_addr = types_render_field("website-address", array("post_id" => $post_id, "output"=>"raw"));
			$seor_top10 = types_render_field("search-phrases-in-top-10", array("post_id" => $post_id, "output"=>"normal"));
			$seor_growth = types_render_field("traffic-growth", array("post_id" => $post_id, "output"=>"normal"));
			$seor_logo = types_render_field("client-logo", array("post_id" => $post_id, "output"=>"raw"));
			$seor_stats = types_render_field("graph-of-website-traffic", array("post_id" => $post_id, "output"=>"raw"));
			} else {$seor_addr = $seor_top10 = $seor_growth = $seor_stats = '';}
			// Open details div
			$output .= '<div class="item">
										<div class="col-md-6 col-sm-12 graph">
											<img src="'.get_template_directory_uri() . '/images/macbook.png" alt="Macbook">
											<div class="under"><img src="'.$seor_stats.'" alt="'.get_the_title($post_id).'"></div>
										</div>
										<div class="col-md-6 col-sm-12 about">
											<h3>'.get_the_title($post_id).'</h3>
											<div class="link"><i class="fa fa-external-link"></i><a href="'.$seor_addr.'" target="blank">'.$seor_addr.'</a></div>
											<ul>
												<li class="top10">'.$phrases.'</li>
												<li class="top10span">'.$seor_top10.'%</li>
												<li class="gap"></li>
												<li class="traffic">'.$traffic.'</li>
												<li class="trafficspan">'.$seor_growth.'%</li>
											</ul>';
			if ($more != '') {
				$output .= '<a href="'.get_permalink($post_id).'" class="btn btn-info">'.$more.'</a>';
			}
			$output .= '</div>
									</div>';

			// End foreach loop
		endforeach;
		$output .= '</div>';
	endif; // End has posts check

	// Set things back to normal
	$result = null;
	wp_reset_postdata();

	// Return output
	return $output;
}


