<?php

/*
* Social Icon Shortcode
*/
//"icon" => get_template_directory_uri() . "/vc_extend/my_shortcode_icon.png", // Simply pass url to your icon here

    add_shortcode( 'sl_social_icons', 'sl_social_icons_shortcode' );

	if ( ! function_exists('sl_social_icons_shortcode') ) {
	
	    function sl_social_icons_shortcode ($atts, $content = null) {
		
		    $defaults = array( 'twitter_url' => false,
			                   'facebook_url' => false,
							   'myspace_url' => false,
							   'linkedin_url' => false,
							   'google_url' => false,
							   'tumbrl_url' => false,
							   'pinterest_url' => false,
							   'youtube_url' => false,
							   'instagram_url' => false,
							   'vk_url' => false,
							   'reddit_url' => false,
							   'blogger_url' => false,
							   'wordpress_url' => false,
							   'behance_url' => false,
							   );

            $style = array ( 'class'  => '',
			                 'height' => '40',
							 'width' => '40',
							 'container_background_color' => '#000000',
							 'border_radius' => '5',
							 'border_width' => '0',
							 'border_color' => '',
							 'border_color_h' => '',
							 'border_style' => 'none',

							 'font_size' => '14',
							 'font_color' => '#ffffff',
							 'hover_font_color' => '#f5f5f5',
							 'hover_background_color' => '#353535',
							 'css' => '' );
							 
			$class_names = array( 'twitter' => 'fa fa-twitter',
            'facebook' => 'fa fa-facebook',
            'myspace' => 'fa fa-users',
            'linkedin' => 'fa fa-linkedin',
            'google' => 'fa fa-google-plus',
            'tumbrl' => 'fa fa-tumblr',
            'pinterest' => 'fa fa-pinterest-p',
            'youtube' => 'fa fa-youtube-play',
            'instagram' => 'fa fa-instagram',
            'vk' => 'fa fa-vk',
            'reddit' => 'fa fa-reddit',
            'blogger' => 'fa fa-pencil',
            'wordpress' => 'fa fa-wordpress',
            'behance' => 'fa fa-behance' );
								  
								
					   
			$args = vc_map_get_attributes( 'sl_social_icons', $atts );
            $urls = shortcode_atts( $defaults, $args );	
            $style = shortcode_atts( $style, $args ); 
			
			preg_match('/{([^{]+)}/ims', $style['css'], $rules);
            if ( isset( $rules ) && isset ( $rules[1] ) ) $rules = ' style="' . $rules[1] . '"'; else $rules = '';			
			
			$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $style['css'], ' ' ), 'sl_social_icons', $atts );
			$class = $style['class'] == '' ? '' : ' ' . $style['class'];

			if ($style['border_color'] == '') {$style['border_color'] = 'transparent';}
			if ($style['border_color_h'] == '') {$style['border_color_h'] = 'transparent';}
			if ($style['container_background_color'] == '') {$style['container_background_color'] = 'transparent';}
			if ($style['hover_background_color'] == '') {$style['hover_background_color'] = 'transparent';}
			if ($style['hover_font_color'] == '') {$style['hover_font_color'] = 'transparent';}
			if ($style['border_radius'] == '') {$style['border_radius'] = '0';}

			$echo = false;
            foreach ( $urls as $name => $url ) { if ( $url != false ) { $echo = true; break; } }
			if ( $echo ) {
				$id = 'sell_social_' . rand(10001, 100000);
					$out  =  '<style type="text/css">
							#' . $id . '.sell_socials li a { height : ' . $style['height'] . 'px; width : ' . $style['width'] . 'px;  }
						';
					$out .=  '#' . $id . '.sell_socials li {background-color : ' . $style['container_background_color'] . '; border-radius : ' . $style['border_radius'] . 'px; border-width:'.$style['border_width'].'px; border-color:'.$style['border_color'].'; border-style:'.$style['border_style'].';}';
					$out .=  '#' . $id . '.sell_socials li:hover {background-color : ' . $style['hover_background_color'] . '; border-color:'.$style['border_color_h'].';}	';
					$out .=  '#' . $id . '.sell_socials li a { font-size : ' . $style['font_size'] . 'px; color : ' . $style['font_color'] . ';}';
					$out .=  '#' . $id . '.sell_socials li a:hover { color : ' . $style['hover_font_color'] . '; }	';

					$out .=  '#' . $id . '.sell_socials li a i {  line-height : ' . $style['height'] . 'px; }
						';
				$out .=  '</style>';				
			    $out .= '<ul id="' . $id . '" class="sell_socials ' . esc_attr($css_class) . esc_attr($class) . '"' . $rules . '>';
				$keys = array_keys ( $defaults );
			    foreach ( $keys as $key ) {
				    if ( isset( $urls[$key] ) && $urls[$key] != false ) {
				        $icon_name = str_ireplace ( '_url', '', $key);
						if ( $class_names[$icon_name] )
				            $out .= '<li><a href="' . esc_url($urls[$key]) . '" target="_blank"><i class="' . $class_names[$icon_name] . '"></i></a></li>';
					}
				}			
				return $out . '</ul>';
			}
			else return false;
		
		}
	
	}
	
	vc_map( array(
		'name'        => __( 'Social Icons', 'sell' ),
		'base'        => 'sl_social_icons',
		'description' => __( 'Display your socials', 'sell' ),
		'category'    => __( 'SecretLab', 'sell' ),
		'icon'        => 'sell_ai social',
		'params'      => array(	
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'facebook', 'sell' ),
				'param_name'	=> 'facebook_url',
				'description'	=> __( '', 'sell' ),
				'group'         => 'General',
				'value'         => get_sl_option('social_link_facebook')
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Twitter', 'sell' ),
				'param_name'	=> 'twitter_url',
				'description'	=> __( '', 'sell' ),
				'group'         => 'General',
				'value'         => get_sl_option('social_link_twitter')
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'My Space', 'sell' ),
				'param_name'	=> 'myspace_url',
				'description'	=> __( '', 'sell' ),
				'group'         => 'General',
				'value'         => get_sl_option('social_link_myspace')
			),	
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Linkedin', 'sell' ),
				'param_name'	=> 'linkedin_url',
				'description'	=> __( '', 'sell' ),
				'group'         => 'General',
				'value'         => get_sl_option('social_link_linkedin')
			),	
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Google', 'sell' ),
				'param_name'	=> 'google_url',
				'description'	=> __( '', 'sell' ),
				'group'         => 'General',
				'value'         => get_sl_option('social_link_google')
			),	
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Tumblr', 'sell' ),
				'param_name'	=> 'tumblr_url',
				'description'	=> __( '', 'sell' ),
				'group'         => 'General',
				'value'         => get_sl_option('social_link_tumblr')
			),	
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Pinterest', 'sell' ),
				'param_name'	=> 'pinterest_url',
				'description'	=> __( '', 'sell' ),
				'group'         => 'General',
				'value'         => get_sl_option('social_link_pinterest')
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Youtube', 'sell' ),
				'param_name'	=> 'youtube_url',
				'description'	=> __( '', 'sell' ),
				'group'         => 'General',
				'value'         => get_sl_option('social_link_youtube')
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Instagram', 'sell' ),
				'param_name'	=> 'instagram_url',
				'description'	=> __( '', 'sell' ),
				'group'         => 'General',
				'value'         => get_sl_option('social_link_instagram')
			),	
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'VK', 'sell' ),
				'param_name'	=> 'vk_url',
				'description'	=> __( '', 'sell' ),
				'group'         => 'General',
				'value'         => get_sl_option('social_link_vkcom')
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Reddit', 'sell' ),
				'param_name'	=> 'reddit_url',
				'description'	=> __( '', 'sell' ),
				'group'         => 'General',
				'value'         => get_sl_option('social_link_reddit')
			),	
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Blogger', 'sell' ),
				'param_name'	=> 'blogger_url',
				'description'	=> __( '', 'sell' ),
				'group'         => 'General',
				'value'         => get_sl_option('social_link_blogger')
			),	
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Wordpress', 'sell' ),
				'param_name'	=> 'wordpress_url',
				'description'	=> __( '', 'sell' ),
				'group'         => 'General',
				'value'         => get_sl_option('social_link_wordpress')
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Behance', 'sell' ),
				'param_name'	=> 'behance_url',
				'description'	=> __( '', 'sell' ),
				'group'         => 'General',
				'value'         => get_sl_option('social_link_behance')
			),
			
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Custom CSS Class', 'sell' ),
				'param_name'	=> 'class',
				'description'	=> __( '', 'sell' ),
				'group'         => 'Style',
				'value'         => ''
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Icon block height in px', 'sell' ),
				'param_name'	=> 'height',
				'description'	=> __( '', 'sell' ),
				'group'         => 'Style',
				'value'         => '40'
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Icon block width in px', 'sell' ),
				'param_name'	=> 'width',
				'description'	=> __( '', 'sell' ),
				'group'         => 'Style',
				'value'         => '40'
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> __( 'Icon Background Color', 'sell' ),
				'param_name'	=> 'container_background_color',
				'description'	=> __( '', 'sell' ),
				'group'         => 'Style',
				'value'         => '#000000'
			),				
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Icon block border Radius in px', 'sell' ),
				'param_name'	=> 'border_radius',
				'description'	=> __( '', 'sell' ),
				'group'         => 'Style',
				'value'         => '5'
			),
			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Icon block border width in px', 'sell' ),
				'param_name'	=> 'border_width',
				'description'	=> __( '', 'sell' ),
				'group'         => 'Style',
				'value'         => '0'
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> __( 'Icon block border color', 'sell' ),
				'param_name'	=> 'border_color',
				'description'	=> __( '', 'sell' ),
				'group'         => 'Style',
				'value'         => ''
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> __( 'Icon block border color (hover)', 'sell' ),
				'param_name'	=> 'border_color_h',
				'description'	=> __( '', 'sell' ),
				'group'         => 'Style',
				'value'         => ''
			),
			array(
				'type'			=> 'dropdown',
				'heading'		=> __( 'Icon block border style', 'sell' ),
				'param_name'	=> 'border_style',
				'description'	=> __( '', 'sell' ),
				'group'         => 'Style',
				'value'         => array (  __( 'none', 'sell' ) => 'none',
					__( 'hidden', 'sell' ) => 'hidden',
					__( 'dotted', 'sell' ) => 'dotted',
					__( 'dashed', 'sell' ) => 'dashed',
					__( 'solid', 'sell' ) => 'solid',
					__( 'double', 'sell' ) => 'double',
					__( 'groove', 'sell' ) => 'groove',
					__( 'ridge', 'sell' ) => 'ridge',
					__( 'inset', 'sell' ) => 'inset',
					__( 'outset', 'sell' ) => 'outset',
				),
				'std' => 'none'
			),


			array(
				'type'			=> 'textfield',
				'heading'		=> __( 'Icon font size in px', 'sell' ),
				'param_name'	=> 'font_size',
				'description'	=> __( '', 'sell' ),
				'group'         => 'Style',
				'value'         => '14'
			),	
			array(
				'type'			=> 'colorpicker',
				'heading'		=> __( 'Font Color', 'sell' ),
				'param_name'	=> 'font_color',
				'description'	=> __( '', 'sell' ),
				'group'         => 'Style',
				'value'         => '#ffffff'
			),	
			array(
				'type'			=> 'colorpicker',
				'heading'		=> __( 'Hover Font Color', 'sell' ),
				'param_name'	=> 'hover_font_color',
				'description'	=> __( '', 'sell' ),
				'group'         => 'Style',
				'value'         => '#f5f5f5'
			),
			array(
				'type'			=> 'colorpicker',
				'heading'		=> __( 'Hover Background Color', 'sell' ),
				'param_name'	=> 'hover_background_color',
				'description'	=> __( '', 'sell' ),
				'group'         => 'Style',
				'value'         => '#353535'
			),
            array(
                'type'          => 'css_editor',
                'heading'       => 'Block Disign Options',
                'param_name'    => 'css',
                'group'         => 'Design Options'				
		    )
		)
	));



?>
