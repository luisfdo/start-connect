<?php

if(!class_exists('sell_title')){

	class sell_title {

		function __construct(){
			add_action('init',array($this,'sell_title_init'));
			add_shortcode('sell_title',array($this,'sell_title_shortcode'));
		}
		
		function sell_title_init() {
			if(function_exists("vc_map")){
				vc_map( array(
					'name'        => __( 'Simple Title', 'sell' ),
					'base'        => 'sell_title',
					'description' => __( 'Displays title with typography options', 'sell' ),
					'category'    => __( 'SecretLab', 'sell' ),
					'icon'        => 'sell_ai title',
					'params'      => array(

						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Text', 'sell' ),
							'param_name'	=> 'text',
							'holder'		=> 'span',
							'class'			=> 'vc_admin_label admin_label_text',
							'value'         => ''
						),

						array(
							'type'			=> 'dropdown',
							'heading'		=> __( 'Tag', 'sell' ),
							'param_name'	=> 'tag',
							'description'	=> __( 'H1 by default', 'sell' ),
							'value'         => array(
								'H1' 	=> 'H1',
								'H2'  	=> 'H2',
								'H3'  	=> 'H3',
								'H4'  	=> 'H4',
								'H5'  	=> 'H5',
								'H6'  	=> 'H6',
								'p'  	=> 'p',
								'div'  	=> 'div',
								'span'  => 'span',
								'b'  	=> 'b',
								'strong'  => 'strong',
								'i'  	=> 'i',
								'pre'  	=> 'pre',
								),
							'default'       => 'H1'
						),
						array(
							'type'			=> 'colorpicker',
							'heading'		=> __( 'Color', 'sell' ),
							'param_name'	=> 'color',
							'description'	=> __( '', 'sell' ),
							'value'         => ''
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Font Size, in px', 'sell' ),
							'description'	=> __( 'Examples: 32, 22, 11, 86', 'sell' ),
							'param_name'	=> 'fsize',
							'value'         => ''
						),
						array(
							'type'			=> 'dropdown',
							'heading'		=> __( 'Heading Position', 'sell' ),
							'param_name'	=> 'position',
							'value'         => array(
								'' 		=> '',
								'left' 		=> 'left',
								'center'  	=> 'center',
								'right'  	=> 'right',
							),
						),
						array(
							'type'			=> 'dropdown',
							'heading'		=> __( 'Font Weight', 'sell' ),
							'param_name'	=> 'font_weight',
							'value'         => array(
								'' 	=> '',
								'300' 	=> '300',
								'400'  	=> '400',
								'500'  	=> '500',
								'600'  	=> '600',
								'700'  	=> '700',
								'900'  	=> '900',
							),
						),
						array(
							'type'			=> 'dropdown',
							'heading'		=> __( 'Text Transform', 'sell' ),
							'param_name'	=> 'tt',
							'value'         => array(
								'unset' 		=> 'none',
								'capitalize' 		=> 'capitalize',
								'inherit' 		=> 'inherit',
								'full-width'  	=> 'full-width',
								'lowercase'  	=> 'lowercase',
								'uppercase'  	=> 'uppercase',
							),
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Extra class name', 'sell' ),
							'param_name'	=> 'css_class',
							'description'	=> __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'sell' )
						),
						array(
							'type'          => 'css_editor',
							'heading'       => 'Design',
							'param_name'    => 'css',
							'group'         => 'Design Options'
						),

					)
				));
			}
		}

		function sell_title_shortcode($atts, $content = null) {
			extract( shortcode_atts( array(
				'text'			=> '',
				'tag'			=> 'H1',
				'position'		=> '',
				'color'			=> '',
				'fsize'			=> '',
				'font_weight'	=> '',
				'tt'			=> '',

				'css_class'		=> '',
				'css'			=> '',
			), $atts ) );

			$out = '';

			$sell_p = $position ? 'text-align: ' . $position . '; ' : '';
			$sell_c = $color ? 'color: ' . $color . '; ' : '';
			$sell_fw = $font_weight ? ' font-weight: ' . $font_weight . '; ' : '';
			$sell_tt = $tt ? ' text-transform: ' . $tt . '; ' : '';
			$sell_fs = $fsize ? ' font-size: ' . $fsize . 'px; ' : '';
			$sell_s = ' style="' . $sell_p . $sell_c . $sell_fw . $sell_tt . $sell_fs . '"';

			preg_match('/{([^{]+)}/ims', $css, $rules);
			if ( isset( $rules ) && isset ( $rules[1] ) ) $rules = ' style="' . $rules[1] . '"'; else $rules = '';
			$css_design = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), 'sell_title', $atts );

			$out = '
				<div class="sell_title ' . $css_class . '">
					<' . $tag . ' class="'. $css_design .'" '.$sell_s.'>'. $text .'</' . $tag . '>
				</div>';
			//if ( ! empty( $param['admin_label'] ) && true === $param['admin_label'] ) {
			//	$out .= '<span class="vc_admin_label admin_label_' . $param['param_name'] . ( empty( $value ) ? ' hidden-label' : '' ) . '"><label>' . $param['heading'] . '</label>: ' . $text . '</span>';
			//}

			return $out;
		}

	}

}

new sell_title();

if(class_exists('WPBakeryShortCode'))
{
	class WPBakeryShortCode_sell_title extends WPBakeryShortCode {
	}
}	

