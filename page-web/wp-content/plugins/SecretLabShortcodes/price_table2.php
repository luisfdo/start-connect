<?php

if(!class_exists('sell_price_table2')){

	class sell_price_table2 {

		function __construct(){
			add_action('init',array($this,'sell_price_table2_init'));
			add_shortcode('price_table2',array($this,'sell_price_table2_shortcode'));
		}
		
		function sell_price_table2_init() {
			if(function_exists("vc_map")){
				vc_map( array(
					'name'        => __( 'Price Table Layout#2', 'sell' ),
					'base'        => 'price_table2',
					'description' => __( 'Echoes Price Table#2', 'sell' ),
					'category'    => __( 'SecretLab', 'sell' ),
					'icon'        => 'sell_ai pricetable2',
					'params'      => array(
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Unique ID', 'sell' ),
							'param_name'	=> 'uid',
							'description'	=> __( 'May be used for styling', 'sell' ),
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Extra class ', 'sell' ),
							'param_name'	=> 'extra_class',
							'description'	=> __( 'extra css class', 'sell' )
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #1 Title', 'sell' ),
							'param_name'	=> 'title1',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #2 Title', 'sell' ),
							'param_name'	=> 'title2',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #3 Title', 'sell' ),
							'param_name'	=> 'title3',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #4 Title', 'sell' ),
							'param_name'	=> 'title4',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #5 Title', 'sell' ),
							'param_name'	=> 'title5',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #6 Title', 'sell' ),
							'param_name'	=> 'title6',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #7 Title', 'sell' ),
							'param_name'	=> 'title7',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #8 Title', 'sell' ),
							'param_name'	=> 'title8',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #9 Title', 'sell' ),
							'param_name'	=> 'title9',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #10 Title', 'sell' ),
							'param_name'	=> 'title10',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #11 Title', 'sell' ),
							'param_name'	=> 'title11',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #12 Title', 'sell' ),
							'param_name'	=> 'title12',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #13 Title', 'sell' ),
							'param_name'	=> 'title13',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #14 Title', 'sell' ),
							'param_name'	=> 'title14',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #15 Title', 'sell' ),
							'param_name'	=> 'title15',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #16 Title', 'sell' ),
							'param_name'	=> 'title16',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #17 Title', 'sell' ),
							'param_name'	=> 'title17',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #18 Title', 'sell' ),
							'param_name'	=> 'title18',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #19 Title', 'sell' ),
							'param_name'	=> 'title19',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #20 Title', 'sell' ),
							'param_name'	=> 'title20',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #21 Title', 'sell' ),
							'param_name'	=> 'title21',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #22 Title', 'sell' ),
							'param_name'	=> 'title22',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #23 Title', 'sell' ),
							'param_name'	=> 'title23',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #24 Title', 'sell' ),
							'param_name'	=> 'title24',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #25 Title', 'sell' ),
							'param_name'	=> 'title25',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #26 Title', 'sell' ),
							'param_name'	=> 'title26',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #27 Title', 'sell' ),
							'param_name'	=> 'title27',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #28 Title', 'sell' ),
							'param_name'	=> 'title28',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #29 Title', 'sell' ),
							'param_name'	=> 'title29',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Line #30 Title', 'sell' ),
							'param_name'	=> 'title30',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Line Titles'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Title', 'sell' ),
							'param_name'	=> 'column1_title',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'checkbox',
							'heading'		=> __( 'Column #1 Recommended Sign', 'sell' ),
							'param_name'	=> 'column1_recommended',
							'description'	=> __( '', 'sell' ),
							'value'         => array ( 'Off' => 'false',
								'On'  => 'true' ),
							'default'       => 'Off',
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #1 Content', 'sell' ),
							'param_name'	=> 'column1_line1',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #2 Content', 'sell' ),
							'param_name'	=> 'column1_line2',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #3 Content', 'sell' ),
							'param_name'	=> 'column1_line3',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #4 Content', 'sell' ),
							'param_name'	=> 'column1_line4',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #5 Content', 'sell' ),
							'param_name'	=> 'column1_line5',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #6 Content', 'sell' ),
							'param_name'	=> 'column1_line6',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #7 Content', 'sell' ),
							'param_name'	=> 'column1_line7',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #8 Content', 'sell' ),
							'param_name'	=> 'column1_line8',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #9 Content', 'sell' ),
							'param_name'	=> 'column1_line9',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #10 Content', 'sell' ),
							'param_name'	=> 'column1_line10',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #11 Content', 'sell' ),
							'param_name'	=> 'column1_line11',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #12 Content', 'sell' ),
							'param_name'	=> 'column1_line12',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),

						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #13 Content', 'sell' ),
							'param_name'	=> 'column1_line13',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #14 Content', 'sell' ),
							'param_name'	=> 'column1_line14',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #15 Content', 'sell' ),
							'param_name'	=> 'column1_line15',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #16 Content', 'sell' ),
							'param_name'	=> 'column1_line16',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #17 Content', 'sell' ),
							'param_name'	=> 'column1_line17',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #18 Content', 'sell' ),
							'param_name'	=> 'column1_line18',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #19 Content', 'sell' ),
							'param_name'	=> 'column1_line19',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #20 Content', 'sell' ),
							'param_name'	=> 'column1_line20',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #21 Content', 'sell' ),
							'param_name'	=> 'column1_line21',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #22 Content', 'sell' ),
							'param_name'	=> 'column1_line22',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #23 Content', 'sell' ),
							'param_name'	=> 'column1_line23',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #24 Content', 'sell' ),
							'param_name'	=> 'column1_line24',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #25 Content', 'sell' ),
							'param_name'	=> 'column1_line25',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #26 Content', 'sell' ),
							'param_name'	=> 'column1_line26',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #27 Content', 'sell' ),
							'param_name'	=> 'column1_line27',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #28 Content', 'sell' ),
							'param_name'	=> 'column1_line28',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #29 Content', 'sell' ),
							'param_name'	=> 'column1_line29',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Line #30 Content', 'sell' ),
							'param_name'	=> 'column1_line30',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),

						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #1 Order Button Caption', 'sell' ),
							'param_name'	=> 'order_text1',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),
						array(
							'type'			=> 'vc_link',
							'heading'		=> __( 'Column #1 Order Button Link', 'sell' ),
							'param_name'	=> 'order_link1',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #1'
						),

						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Title', 'sell' ),
							'param_name'	=> 'column2_title',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'checkbox',
							'heading'		=> __( 'Column #2 Recommended Sign', 'sell' ),
							'param_name'	=> 'column2_recommended',
							'description'	=> __( '', 'sell' ),
							'value'         => array ( 'Off' => 'false',
								'On'  => 'true' ),
							'default'       => 'Off',
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #1 Content', 'sell' ),
							'param_name'	=> 'column2_line1',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #2 Content', 'sell' ),
							'param_name'	=> 'column2_line2',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #3 Content', 'sell' ),
							'param_name'	=> 'column2_line3',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #4 Content', 'sell' ),
							'param_name'	=> 'column2_line4',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #5 Content', 'sell' ),
							'param_name'	=> 'column2_line5',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #6 Content', 'sell' ),
							'param_name'	=> 'column2_line6',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #7 Content', 'sell' ),
							'param_name'	=> 'column2_line7',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #8 Content', 'sell' ),
							'param_name'	=> 'column2_line8',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #9 Content', 'sell' ),
							'param_name'	=> 'column2_line9',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #10 Content', 'sell' ),
							'param_name'	=> 'column2_line10',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #11 Content', 'sell' ),
							'param_name'	=> 'column2_line11',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #12 Content', 'sell' ),
							'param_name'	=> 'column2_line12',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),

						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #13 Content', 'sell' ),
							'param_name'	=> 'column2_line13',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #14 Content', 'sell' ),
							'param_name'	=> 'column2_line14',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #15 Content', 'sell' ),
							'param_name'	=> 'column2_line15',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #16 Content', 'sell' ),
							'param_name'	=> 'column2_line16',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #17 Content', 'sell' ),
							'param_name'	=> 'column2_line17',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #18 Content', 'sell' ),
							'param_name'	=> 'column2_line18',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #19 Content', 'sell' ),
							'param_name'	=> 'column2_line19',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #20 Content', 'sell' ),
							'param_name'	=> 'column2_line20',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #21 Content', 'sell' ),
							'param_name'	=> 'column2_line21',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #22 Content', 'sell' ),
							'param_name'	=> 'column2_line22',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #23 Content', 'sell' ),
							'param_name'	=> 'column2_line23',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #24 Content', 'sell' ),
							'param_name'	=> 'column2_line24',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #25 Content', 'sell' ),
							'param_name'	=> 'column2_line25',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #26 Content', 'sell' ),
							'param_name'	=> 'column2_line26',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #27 Content', 'sell' ),
							'param_name'	=> 'column2_line27',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #28 Content', 'sell' ),
							'param_name'	=> 'column2_line28',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #29 Content', 'sell' ),
							'param_name'	=> 'column2_line29',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Line #30 Content', 'sell' ),
							'param_name'	=> 'column2_line30',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),

						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #2 Order Button Caption', 'sell' ),
							'param_name'	=> 'order_text2',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),
						array(
							'type'			=> 'vc_link',
							'heading'		=> __( 'Column #2 Order Button Link', 'sell' ),
							'param_name'	=> 'order_link2',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #2'
						),

						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Title', 'sell' ),
							'param_name'	=> 'column3_title',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'checkbox',
							'heading'		=> __( 'Column #3 Recommended Sign', 'sell' ),
							'param_name'	=> 'column3_recommended',
							'description'	=> __( '', 'sell' ),
							'value'         => array ( 'Off' => 'false',
								'On'  => 'true' ),
							'default'       => 'Off',
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #1 Content', 'sell' ),
							'param_name'	=> 'column3_line1',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #2 Content', 'sell' ),
							'param_name'	=> 'column3_line2',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #3 Content', 'sell' ),
							'param_name'	=> 'column3_line3',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #4 Content', 'sell' ),
							'param_name'	=> 'column3_line4',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #5 Content', 'sell' ),
							'param_name'	=> 'column3_line5',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #6 Content', 'sell' ),
							'param_name'	=> 'column3_line6',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #7 Content', 'sell' ),
							'param_name'	=> 'column3_line7',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #8 Content', 'sell' ),
							'param_name'	=> 'column3_line8',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #9 Content', 'sell' ),
							'param_name'	=> 'column3_line9',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #10 Content', 'sell' ),
							'param_name'	=> 'column3_line10',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #11 Content', 'sell' ),
							'param_name'	=> 'column3_line11',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #12 Content', 'sell' ),
							'param_name'	=> 'column3_line12',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),

						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #13 Content', 'sell' ),
							'param_name'	=> 'column3_line13',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #14 Content', 'sell' ),
							'param_name'	=> 'column3_line14',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #15 Content', 'sell' ),
							'param_name'	=> 'column3_line15',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #16 Content', 'sell' ),
							'param_name'	=> 'column3_line16',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #17 Content', 'sell' ),
							'param_name'	=> 'column3_line17',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #18 Content', 'sell' ),
							'param_name'	=> 'column3_line18',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #19 Content', 'sell' ),
							'param_name'	=> 'column3_line19',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #20 Content', 'sell' ),
							'param_name'	=> 'column3_line20',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #21 Content', 'sell' ),
							'param_name'	=> 'column3_line21',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #22 Content', 'sell' ),
							'param_name'	=> 'column3_line22',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #23 Content', 'sell' ),
							'param_name'	=> 'column3_line23',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #24 Content', 'sell' ),
							'param_name'	=> 'column3_line24',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #25 Content', 'sell' ),
							'param_name'	=> 'column3_line25',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #26 Content', 'sell' ),
							'param_name'	=> 'column3_line26',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #27 Content', 'sell' ),
							'param_name'	=> 'column3_line27',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #28 Content', 'sell' ),
							'param_name'	=> 'column3_line28',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #29 Content', 'sell' ),
							'param_name'	=> 'column3_line29',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Line #30 Content', 'sell' ),
							'param_name'	=> 'column3_line30',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),

						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #3 Order Button Caption', 'sell' ),
							'param_name'	=> 'order_text3',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),
						array(
							'type'			=> 'vc_link',
							'heading'		=> __( 'Column #3 Order Button Link', 'sell' ),
							'param_name'	=> 'order_link3',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #3'
						),

						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Title', 'sell' ),
							'param_name'	=> 'column4_title',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'checkbox',
							'heading'		=> __( 'Column #4 Recommended Sign', 'sell' ),
							'param_name'	=> 'column4_recommended',
							'description'	=> __( '', 'sell' ),
							'value'         => array ( 'Off' => 'false',
								'On'  => 'true' ),
							'default'       => 'Off',
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #1 Content', 'sell' ),
							'param_name'	=> 'column4_line1',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #2 Content', 'sell' ),
							'param_name'	=> 'column4_line2',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #3 Content', 'sell' ),
							'param_name'	=> 'column4_line3',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #4 Content', 'sell' ),
							'param_name'	=> 'column4_line4',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #5 Content', 'sell' ),
							'param_name'	=> 'column4_line5',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #6 Content', 'sell' ),
							'param_name'	=> 'column4_line6',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #7 Content', 'sell' ),
							'param_name'	=> 'column4_line7',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #8 Content', 'sell' ),
							'param_name'	=> 'column4_line8',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #9 Content', 'sell' ),
							'param_name'	=> 'column4_line9',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #10 Content', 'sell' ),
							'param_name'	=> 'column4_line10',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #11 Content', 'sell' ),
							'param_name'	=> 'column4_line11',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #12 Content', 'sell' ),
							'param_name'	=> 'column4_line12',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),

						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #13 Content', 'sell' ),
							'param_name'	=> 'column4_line13',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #14 Content', 'sell' ),
							'param_name'	=> 'column4_line14',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #15 Content', 'sell' ),
							'param_name'	=> 'column4_line15',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #16 Content', 'sell' ),
							'param_name'	=> 'column4_line16',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #17 Content', 'sell' ),
							'param_name'	=> 'column4_line17',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #18 Content', 'sell' ),
							'param_name'	=> 'column4_line18',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #19 Content', 'sell' ),
							'param_name'	=> 'column4_line19',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #20 Content', 'sell' ),
							'param_name'	=> 'column4_line20',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #21 Content', 'sell' ),
							'param_name'	=> 'column4_line21',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #22 Content', 'sell' ),
							'param_name'	=> 'column4_line22',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #23 Content', 'sell' ),
							'param_name'	=> 'column4_line23',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #24 Content', 'sell' ),
							'param_name'	=> 'column4_line24',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #25 Content', 'sell' ),
							'param_name'	=> 'column4_line25',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #26 Content', 'sell' ),
							'param_name'	=> 'column4_line26',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #27 Content', 'sell' ),
							'param_name'	=> 'column4_line27',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #28 Content', 'sell' ),
							'param_name'	=> 'column4_line28',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #29 Content', 'sell' ),
							'param_name'	=> 'column4_line29',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Line #30 Content', 'sell' ),
							'param_name'	=> 'column4_line30',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),


						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #4 Order Button Caption', 'sell' ),
							'param_name'	=> 'order_text4',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),
						array(
							'type'			=> 'vc_link',
							'heading'		=> __( 'Column #4 Order Button Link', 'sell' ),
							'param_name'	=> 'order_link4',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #4'
						),

						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Title', 'sell' ),
							'param_name'	=> 'column5_title',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'checkbox',
							'heading'		=> __( 'Column #5 Recommended Sign', 'sell' ),
							'param_name'	=> 'column5_recommended',
							'description'	=> __( '', 'sell' ),
							'value'         => array ( 'Off' => 'false',
								'On'  => 'true' ),
							'default'       => 'Off',
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #1 Content', 'sell' ),
							'param_name'	=> 'column5_line1',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #2 Content', 'sell' ),
							'param_name'	=> 'column5_line2',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #3 Content', 'sell' ),
							'param_name'	=> 'column5_line3',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #4 Content', 'sell' ),
							'param_name'	=> 'column5_line4',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #5 Content', 'sell' ),
							'param_name'	=> 'column5_line5',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #6 Content', 'sell' ),
							'param_name'	=> 'column5_line6',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #7 Content', 'sell' ),
							'param_name'	=> 'column5_line7',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #8 Content', 'sell' ),
							'param_name'	=> 'column5_line8',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #9 Content', 'sell' ),
							'param_name'	=> 'column5_line9',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #10 Content', 'sell' ),
							'param_name'	=> 'column5_line10',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #11 Content', 'sell' ),
							'param_name'	=> 'column5_line11',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #12 Content', 'sell' ),
							'param_name'	=> 'column5_line12',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),

						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #13 Content', 'sell' ),
							'param_name'	=> 'column5_line13',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #14 Content', 'sell' ),
							'param_name'	=> 'column5_line14',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #15 Content', 'sell' ),
							'param_name'	=> 'column5_line15',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #16 Content', 'sell' ),
							'param_name'	=> 'column5_line16',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #17 Content', 'sell' ),
							'param_name'	=> 'column5_line17',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #18 Content', 'sell' ),
							'param_name'	=> 'column5_line18',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #19 Content', 'sell' ),
							'param_name'	=> 'column5_line19',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #20 Content', 'sell' ),
							'param_name'	=> 'column5_line20',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #21 Content', 'sell' ),
							'param_name'	=> 'column5_line21',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #22 Content', 'sell' ),
							'param_name'	=> 'column5_line22',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #23 Content', 'sell' ),
							'param_name'	=> 'column5_line23',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #24 Content', 'sell' ),
							'param_name'	=> 'column5_line24',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #25 Content', 'sell' ),
							'param_name'	=> 'column5_line25',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #26 Content', 'sell' ),
							'param_name'	=> 'column5_line26',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #27 Content', 'sell' ),
							'param_name'	=> 'column5_line27',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #28 Content', 'sell' ),
							'param_name'	=> 'column5_line28',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #29 Content', 'sell' ),
							'param_name'	=> 'column5_line29',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Line #30 Content', 'sell' ),
							'param_name'	=> 'column5_line30',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),

						array(
							'type'			=> 'textfield',
							'heading'		=> __( 'Column #5 Order Button Caption', 'sell' ),
							'param_name'	=> 'order_text5',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
						array(
							'type'			=> 'vc_link',
							'heading'		=> __( 'Column #5 Order Button Link', 'sell' ),
							'param_name'	=> 'order_link5',
							'description'	=> __( '', 'sell' ),
							'group'         => 'Column #5'
						),
					)
				));

			}
		}

		function sell_price_table2_shortcode($atts, $content = null) {
			extract( shortcode_atts( array(
				'uid'                   => '',
				'extra_class'           => '',
				'title1'                => '',
				'title2'                => '',
				'title3'                => '',
				'title4'                => '',
				'title5'                => '',
				'title6'                => '',
				'title7'                => '',
				'title8'                => '',
				'title9'                => '',
				'title10'               => '',
				'title11'               => '',
				'title12'               => '',
				'title13'               => '',
				'title14'               => '',
				'title15'               => '',
				'title16'               => '',
				'title17'               => '',
				'title18'               => '',
				'title19'               => '',
				'title20'               => '',
				'title21'               => '',
				'title22'               => '',
				'title23'               => '',
				'title24'               => '',
				'title25'               => '',
				'title26'               => '',
				'title27'               => '',
				'title28'               => '',
				'title29'               => '',
				'title30'               => '',

				'column1_title'         => '',
				'column1_recommended'   => '',
				'column1_line1'         => '',
				'column1_line2'         => '',
				'column1_line3'         => '',
				'column1_line4'         => '',
				'column1_line5'         => '',
				'column1_line6'         => '',
				'column1_line7'         => '',
				'column1_line8'         => '',
				'column1_line9'         => '',
				'column1_line10'        => '',
				'column1_line11'        => '',
				'column1_line12'        => '',
				'column1_line13'        => '',
				'column1_line14'        => '',
				'column1_line15'        => '',
				'column1_line16'        => '',
				'column1_line17'        => '',
				'column1_line18'        => '',
				'column1_line19'        => '',
				'column1_line20'        => '',
				'column1_line21'        => '',
				'column1_line22'        => '',
				'column1_line23'        => '',
				'column1_line24'        => '',
				'column1_line25'        => '',
				'column1_line26'        => '',
				'column1_line27'        => '',
				'column1_line28'        => '',
				'column1_line29'        => '',
				'column1_line30'        => '',

				'order_text1'           => 'order1',
				'order_link1'           => '',
				'column2_title'         => '',
				'column2_recommended'   => '',
				'column2_line1'         => '',
				'column2_line2'         => '',
				'column2_line3'         => '',
				'column2_line4'         => '',
				'column2_line5'         => '',
				'column2_line6'         => '',
				'column2_line7'         => '',
				'column2_line8'         => '',
				'column2_line9'         => '',
				'column2_line10'        => '',
				'column2_line11'        => '',
				'column2_line12'        => '',
				'column2_line13'        => '',
				'column2_line14'        => '',
				'column2_line15'        => '',
				'column2_line16'        => '',
				'column2_line17'        => '',
				'column2_line18'        => '',
				'column2_line19'        => '',
				'column2_line20'        => '',
				'column2_line21'        => '',
				'column2_line22'        => '',
				'column2_line23'        => '',
				'column2_line24'        => '',
				'column2_line25'        => '',
				'column2_line26'        => '',
				'column2_line27'        => '',
				'column2_line28'        => '',
				'column2_line29'        => '',
				'column2_line30'        => '',


				'order_text2'           => 'order2',
				'order_link2'           => '',
				'column3_title'         => '',
				'column3_recommended'   => '',
				'column3_line1'         => '',
				'column3_line2'         => '',
				'column3_line3'         => '',
				'column3_line4'         => '',
				'column3_line5'         => '',
				'column3_line6'         => '',
				'column3_line7'         => '',
				'column3_line8'         => '',
				'column3_line9'         => '',
				'column3_line10'        => '',
				'column3_line11'        => '',
				'column3_line12'        => '',
				'column3_line13'        => '',
				'column3_line14'        => '',
				'column3_line15'        => '',
				'column3_line16'        => '',
				'column3_line17'        => '',
				'column3_line18'        => '',
				'column3_line19'        => '',
				'column3_line20'        => '',
				'column3_line21'        => '',
				'column3_line22'        => '',
				'column3_line23'        => '',
				'column3_line24'        => '',
				'column3_line25'        => '',
				'column3_line26'        => '',
				'column3_line27'        => '',
				'column3_line28'        => '',
				'column3_line29'        => '',
				'column3_line30'        => '',

				'order_text3'           => 'order3',
				'order_link3'           => '',
				'column4_title'         => '',
				'column4_recommended'   => '',
				'column4_line1'         => '',
				'column4_line2'         => '',
				'column4_line3'         => '',
				'column4_line4'         => '',
				'column4_line5'         => '',
				'column4_line6'         => '',
				'column4_line7'         => '',
				'column4_line8'         => '',
				'column4_line9'         => '',
				'column4_line10'        => '',
				'column4_line11'        => '',
				'column4_line12'        => '',
				'column4_line13'        => '',
				'column4_line14'        => '',
				'column4_line15'        => '',
				'column4_line16'        => '',
				'column4_line17'        => '',
				'column4_line18'        => '',
				'column4_line19'        => '',
				'column4_line20'        => '',
				'column4_line21'        => '',
				'column4_line22'        => '',
				'column4_line23'        => '',
				'column4_line24'        => '',
				'column4_line25'        => '',
				'column4_line26'        => '',
				'column4_line27'        => '',
				'column4_line28'        => '',
				'column4_line29'        => '',
				'column4_line30'        => '',

				'order_text4'           => 'order4',
				'order_link4'           => '',
				'column5_title'         => '',
				'column5_recommended'   => '',
				'column5_line1'         => '',
				'column5_line2'         => '',
				'column5_line3'         => '',
				'column5_line4'         => '',
				'column5_line5'         => '',
				'column5_line6'         => '',
				'column5_line7'         => '',
				'column5_line8'         => '',
				'column5_line9'         => '',
				'column5_line10'        => '',
				'column5_line11'        => '',
				'column5_line12'        => '',
				'column5_line13'        => '',
				'column5_line14'        => '',
				'column5_line15'        => '',
				'column5_line16'        => '',
				'column5_line17'        => '',
				'column5_line18'        => '',
				'column5_line19'        => '',
				'column5_line20'        => '',
				'column5_line21'        => '',
				'column5_line22'        => '',
				'column5_line23'        => '',
				'column5_line24'        => '',
				'column5_line25'        => '',
				'column5_line26'        => '',
				'column5_line27'        => '',
				'column5_line28'        => '',
				'column5_line29'        => '',
				'column5_line30'        => '',

				'order_text5'           => 'order5',
				'order_link5'           => '',
			), $atts ) );

			$title_out = '<div class="row center-price zeopadding">
					<div class="col-lg-#n# col-md-#n# col-sm-#n# col-xs-6">
						<ul class="pricetable2caption">
							<li><h3>&nbsp;</h3></li>';
			$column_out = '';
			$out = '';
			$n = 0; $price_lines = array();

			for ($i = 1; $i <= 30; $i++) {
				$title = 'title'.$i;
				if ($$title == '' || !$$title) continue;
				if (preg_match('/#price_line#/', $$title)) {
					$$title = preg_replace('/#price_line#/', '', $$title);
					$price_lines[] = $i;
				}
				$title_out .= '<li>'.$$title.'</li>';
			}
			$title_out .= '</ul></div>';

			for ($j = 1; $j <= 5; $j++) {
				$column_title = 'column'.$j.'_title';
				if ($$column_title == '' || !$$column_title) continue;
				$column_recommended = 'column'.$j.'_recommended';
				if ($$column_recommended == 'true') { $r_ul_class = ' recommended'; $r_li_class = 'class="pro"'; } else { $r_ul_class = $r_li_class = ''; }
				$order_text = 'order_text'.$j;
				$column_out .= '<div class="col-lg-#n# col-md-#n# col-sm-#n# col-xs-6">
							   <ul class="pricetable2'.$r_ul_class.'">
								   <li '.$r_li_class.'><h3>'.$$column_title.'</h3></li>';
				for ($k = 1; $k <= 30; $k++) {
					$column_line = 'column'.$j.'_line'.$k;
					$$column_line = html_entity_decode($$column_line);
					if ($$column_line == '' || !$$column_line) continue;
					$worth_class = '';
					if ($$column_line == 'tick') $content = '<span class="icon icon-checkmark4"></span>';
					else if ($$column_line == 'mark') $content = '<span class="icon icon-cross2"></span>';
					else $content = $$column_line;
					if (in_array($k, $price_lines)) {
						$worth_class = ' class="worth"';
					}
					$column_out .= '<li'.$worth_class.'>'.$content.'</li>';
				}
				$order_link = 'order_link'.$j;
				$link = vc_build_link($$order_link);
				$column_out .=	'<li><a class="btn btn-info" href="'.$link['url'].'" title="'.$link['title'].'">
								 <span class="icon icon-compass2"></span>'.$$order_text.'
							 </a></li>
							</ul>
						</div>';
				$n++;
			}

			$out = $title_out.$column_out.'</div>';
			$rest = 12 % ($n + 1);
			if ($rest == 0) $n = 12/($n + 1); else $n = 2;

			$out = preg_replace('/#n#/', $n, $out);

			return $out;
		}

	}

}

new sell_price_table2();

if(class_exists('WPBakeryShortCode'))
{
	class WPBakeryShortCode_sell_price_table2 extends WPBakeryShortCode {
	}
}	

