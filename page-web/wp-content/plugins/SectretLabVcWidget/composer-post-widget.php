<?php
/*
 Plugin Name: SecretLab Visual Composer Content Widgetizer
 Plugin URI: http://www.secretlab.pw/
 Description: Show the content of a custom post of the type 'composer_block' ( it can be made with Visual Composer Plugin ) in a widget or with a shortcode.
 Version: 1.2
 Author: SecretLab
 License: GPL2
*/


//add_action( 'wp_footer', 'composer_widget_js', 100 );

function composer_widget_js () {
	echo "
<script type='text/javascript'>
jQuery(document).ready(function ($) {
	'use strict';
	alert($('.sl_composer_widget_parent_id').length);
	$('.sl_composer_widget_parent_id').each(function(i, el) {

		var target_el = $(el).parents('.widget-area').find('.vc_grid-container'),
			vc_data = $(target_el).attr('data-vc-grid-settings'),
			id = $(this).attr('parent_id');

		if ( typeof( vc_data ) == 'string' ) vc_data = vc_data.replace(/.page_id..[0-9]+/, '"; echo '"page_id"'; echo ":' + id);
		$(target_el).attr('data-vc-grid-settings', vc_data);

	});
	
});
</script>
";

}

// Launch the plugin.
function custom_post_widget_plugin_init() {
	add_action( 'widgets_init', 'custom_post_widget_load_widgets' );
}
add_action( 'plugins_loaded', 'custom_post_widget_plugin_init' );

// Loads the widgets packaged with the plugin.
function custom_post_widget_load_widgets() {
	require_once( 'post-widget.php' );
	register_widget( 'custom_post_widget' );
}

function filter_composer_widget_init() {
	$composer_widget_public = true;
	return $composer_widget_public;
}
add_filter('composer_widget_post_type','filter_composer_widget_init');

add_action( 'vc_before_init', 'composer_widget_setup_vc' );

function composer_widget_setup_vc() {
	if(function_exists('vc_set_default_editor_post_types') && function_exists('vc_default_editor_post_types')) {
	    $list = vc_default_editor_post_types();
		$list[] = 'composer_widget';
		vc_set_default_editor_post_types( $list );
	}
	
	wp_register_script( 'cw_js', trailingslashit( plugins_url( 'SectretLabVcWidget' ) ) . 'js/composer_widget.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'cw_js' );	

	//wp_register_script( 'google_map_js', 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true', array( 'jquery' ), false, false );
	//wp_enqueue_script( 'google_map_js' );	
	
}

require_once( 'meta-box.php' );
require_once( 'popup.php' );
