// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  // production: false,
  // socks: 'http://localhost:3000',
  // backendUrl: 'http://localhost:3000',
  // apiToken: 'Bearer fe28f5be-afbd-4a77-b5a6-1b784e224047'

     production: true,
     socks: 'http://184.73.17.177:3000',
     backendUrl: 'http://184.73.17.177:3000',
     apiToken: 'Bearer fe28f5be-afbd-4a77-b5a6-1b784e224047'
};
