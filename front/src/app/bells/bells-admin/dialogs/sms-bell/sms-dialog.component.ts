import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
// import { BellsService } from '../../../../providers/bells/bells.service';
import { CustomersService } from '../../../../providers/customers/customers.service';

import { SmsService } from '../../../../providers/sms/sms.service';

import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
declare var $: any;
import * as moment from 'moment';

@Component({
  selector: 'app-sms-dialog',
  templateUrl: './sms-dialog.component.html',
  styleUrls: ['./sms-dialog.component.scss'],
  providers: [ SmsService, CustomersService ]
})
export class SmsDialogBellComponent {

  cont:number = 0;
  resData:any;
  spinner:boolean = false;
  constructor(public dialogRef: MatDialogRef<SmsDialogBellComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public dataCustomers : CustomersService,
    public dataSms : SmsService){}

  onNoClick(): void {
    this.dialogRef.close();
  }

  sendSms(){
    this.spinner = true;
    this.dataCustomers.getCustomers()
    .then(response => {
      this.resData = response
      this.initBell(response[this.cont])
    })
    .catch(error => {
      this.spinner = false;
      this.errorUser();
      console.log(error);
    });
  }

  initBell(sms){

     if(this.cont > this.resData.length-1){
        this.spinner = false;
        
        $.notify({
          icon: "add_alert", message: "La campaña se esta ejecutando a los diferentes contactos"
          },{
            type: 'warning', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });

        this.onNoClick()

     }else{
        this.dataSms.sendSms(sms, this.data)
        .then(response => {
          this.cont++
          this.initBell(this.resData[this.cont])
        })
        .catch(error => {
          this.spinner = false;
          this.errorUser();
          console.log(error);
        });
     }
     
  }


  errorUser(){
    $.notify({
      icon: "add_alert", message: "A ocurrido un error"
      },{
        type: 'warning', timer: 4000,
        placement: {
        from: 'top',
        align: 'right'
      }
    });
  }

}
