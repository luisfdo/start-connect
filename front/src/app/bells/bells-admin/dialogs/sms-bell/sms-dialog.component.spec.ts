import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsDialogBellComponent } from './sms-dialog.component';

describe('SmsDialogBellComponent', () => {
  let component: SmsDialogBellComponent;
  let fixture: ComponentFixture<SmsDialogBellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsDialogBellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsDialogBellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
