import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import { BellsService } from '../../../../providers/bells/bells.service';

import {FormControl, Validators} from '@angular/forms';
import {Bells} from '../../../../models/bells';

import * as moment from 'moment';

@Component({
  selector: 'app-add-dialog-rol',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.scss'],
  providers: [ BellsService ]
})
export class AddDialogBellComponent {
  
  hour:any

  constructor(
    public dialogRef: MatDialogRef<AddDialogBellComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Bells,
    public dataService: BellsService
    
  ) { 
  }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('name') ? 'Not a valid name' :
        '';
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {

    // var dateString = this.data.date_start
    // var m = moment(dateString, 'ddd MMM D YYYY HH:mm');
    // m.set({h: this.hour.substring(0,2), m: this.hour.substring(3,5)});
    // this.data.date_start = m.format('x');
    this.data.status = 0;
    this.dataService.addBell(this.data);
    
  }

}
