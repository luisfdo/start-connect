import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDialogBellComponent } from './add-dialog.component';

describe('AddDialogBellComponent', () => {
  let component: AddDialogBellComponent;
  let fixture: ComponentFixture<AddDialogBellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDialogBellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDialogBellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
