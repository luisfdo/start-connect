import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { BellsService } from '../../../../providers/bells/bells.service';

// import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

import * as moment from 'moment';

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.scss'],
  providers: [ BellsService ]
})
export class EditDialogBellComponent {

  hour:any

  constructor(public dialogRef: MatDialogRef<EditDialogBellComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public dataService: BellsService) {

        // var datePipe = new DatePipe("en-US");
        // this.hour = datePipe.transform(data.date_start, 'HH:mm');
        // data.date_start = datePipe.transform(data.date_start, 'MM/d/yyyy');
        // data.date_start = new Date(data.date_start);

    }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
    this.formControl.hasError('name') ? 'Not a valid name' :
    '';
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {

    // var dateString = this.data.date_start
    // var m = moment(dateString, 'ddd MMM D YYYY HH:mm');
    // m.set({h: this.hour.substring(0,2), m: this.hour.substring(3,5)});
    // this.data.date_start = m.format('x');
    this.data.status = 0;
    
    this.dataService.updateBell(this.data);
  }

}
