import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDialogBellComponent } from './edit-dialog.component';

describe('EditDialogBellComponent', () => {
  let component: EditDialogBellComponent;
  let fixture: ComponentFixture<EditDialogBellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDialogBellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDialogBellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
