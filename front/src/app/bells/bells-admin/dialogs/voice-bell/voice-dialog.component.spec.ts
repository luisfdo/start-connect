import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoiceDialogBellComponent } from './voice-dialog.component';

describe('VoiceDialogBellComponent', () => {
  let component: VoiceDialogBellComponent;
  let fixture: ComponentFixture<VoiceDialogBellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoiceDialogBellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoiceDialogBellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
