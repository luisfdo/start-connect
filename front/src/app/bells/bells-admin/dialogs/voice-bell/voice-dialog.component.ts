import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import { CustomersService } from '../../../../providers/customers/customers.service';
import { VoiceService } from '../../../../providers/voice/voice.service';
declare var $: any;

@Component({
  selector: 'app-voice-dialog',
  templateUrl: './voice-dialog.component.html',
  styleUrls: ['./voice-dialog.component.scss'],
  providers: [ VoiceService, CustomersService ]
})
export class VoiceDialogBellComponent {

  cont:number = 0;
  resData:any;
  spinner:boolean = false;
  constructor(public dialogRef: MatDialogRef<VoiceDialogBellComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public dataCustomers : CustomersService,
    public dataVoice : VoiceService){}

  onNoClick(): void {
    this.dialogRef.close();
  }

  sendVoice(){
    this.spinner = true;
    this.dataCustomers.getCustomers()
    .then(response => {
      this.resData = response
      this.initBell(response[this.cont])
    })
    .catch(error => {
      this.spinner = false;
      this.errorUser();
      console.log(error);
    });
  }

  initBell(voice){

     if(this.cont > this.resData.length-1){
        this.spinner = false;
        $.notify({
          icon: "add_alert", message: "La campaña se esta ejecutando a los diferentes contactos"
          },{
            type: 'warning', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
        this.onNoClick()
     }else{
        this.dataVoice.sendVoice(voice, this.data)
        .then(response => {
          this.cont++
          this.initBell(this.resData[this.cont])
        })
        .catch(error => {
          this.spinner = false;
          this.errorUser();
          console.log(error);
        });
     }
     
  }


  errorUser(){
    $.notify({
      icon: "add_alert", message: "A ocurrido un error"
      },{
        type: 'warning', timer: 4000,
        placement: {
        from: 'top',
        align: 'right'
      }
    });
  }

}

