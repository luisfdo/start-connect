import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteDialogBellComponent } from './delete-dialog.component';

describe('DeleteDialogBellComponent', () => {
  let component: DeleteDialogBellComponent;
  let fixture: ComponentFixture<DeleteDialogBellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteDialogBellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteDialogBellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
