import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import { BellsService } from '../../../../providers/bells/bells.service';

@Component({
  selector: 'app-delete-dialog-bell',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss'],
  providers: [ BellsService ]
})
export class DeleteDialogBellComponent  {

  constructor(
    public dialogRef: MatDialogRef<DeleteDialogBellComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public dataService: BellsService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.dataService.deleteBell(this.data._id);
  }

}
