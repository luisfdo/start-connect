
import { BellsService } from '../../providers/bells/bells.service';

import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {Bells} from '../../models/bells';
import {DataSource} from '@angular/cdk/collections';
import {AddDialogBellComponent} from './dialogs/add-bell/add-dialog.component';
import {EditDialogBellComponent} from './dialogs/edit-bell/edit-dialog.component';
import {DeleteDialogBellComponent} from './dialogs/delete-bell/delete-dialog.component';

import {SmsDialogBellComponent} from './dialogs/sms-bell/sms-dialog.component';
import {VoiceDialogBellComponent} from './dialogs/voice-bell/voice-dialog.component';


import {BehaviorSubject, fromEvent, merge, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-bells-admin',
  templateUrl: './bells-admin.component.html',
  styleUrls: ['./bells-admin.component.scss'],
  providers: [ BellsService ]
})
export class BellsAdminComponent implements OnInit {

  displayedColumns = ['name', 'message', 'country', 'actions'];

  exampleDatabase: BellsService | null;
  dataSource: ExampleDataSource | null;
  index: number;
  _id: string;


  constructor(
    private bellsService: BellsService,
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public dataService: BellsService
  ){}

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;

  ngOnInit(){
    this.loadData();
  }

  public loadData() {
    this.exampleDatabase = new BellsService(this.httpClient);
    this.dataSource = new ExampleDataSource(this.exampleDatabase, this.paginator, this.sort);
    fromEvent(this.filter.nativeElement, 'keyup')
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }

  addNew(bell: Bells) {

    const dialogRef = this.dialog.open(AddDialogBellComponent, {
      data: {bell: bell }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.loadData();
      }
    });

  }

  startEdit(i: number, _id: string, name: string, date_start: string, message:string, country:string, company:string, status:number) {
    
    this._id = _id;
    this.index = i;

    const dialogRef = this.dialog.open(EditDialogBellComponent, {
      data: {_id: _id, name: name, date_start: date_start, message:message, country:country, company: company, status:status}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.loadData();
      }
    });
  }


  startSms(i: number, _id: string, name: string, date_start: string, message:string, country:string, company:string, status:number) {
    
    this._id = _id;
    this.index = i;

    const dialogRef = this.dialog.open(SmsDialogBellComponent, {
      data: {_id: _id, name: name, date_start: date_start, message:message, country:country, company: company, status:status}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.loadData();
      }
    });
  }


  startVoice(i: number, _id: string, name: string, date_start: string, message:string, country:string, company:string, status:number) {
    
    this._id = _id;
    this.index = i;

    const dialogRef = this.dialog.open(VoiceDialogBellComponent, {
      data: {_id: _id, name: name, date_start: date_start, message:message, country:country, company: company, status:status}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.loadData();
      }
    });
  }

  deleteItem(i: number, _id: string, name: string, date_start: Date, message:string, country:string, company:string, status:number) {
    this.index = i;
    this._id = _id;
    const dialogRef = this.dialog.open(DeleteDialogBellComponent, {
      data: {_id: _id, name: name, date_start: date_start, message:message, country:country, company: company, status:status}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.loadData();
      }
    });
  }


  // private refreshTable() {
  //   // Refreshing table using paginator
  //   // Thanks yeager-j for tips
  //   // https://github.com/marinantonio/angular-mat-table-crud/issues/12
    
  // }
}


export class ExampleDataSource extends DataSource<Bells> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Bells[] = [];
  renderedData: Bells[] = [];

  constructor(public _exampleDatabase: BellsService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Bells[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._exampleDatabase.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    this._exampleDatabase.getAllBells();


    return merge(...displayDataChanges).pipe(map( () => {
        // Filter data
        this.filteredData = this._exampleDatabase.data.slice().filter((bell: Bells) => {
          const searchStr = (bell._id + bell.name + bell.date_start +  bell.message + bell.country + bell.company + bell.status).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        });

        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());

        // Grab the page's slice of the filtered sorted data.
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
        return this.renderedData;
      }
    ));
  }

  disconnect() {}


  /** Returns a sorted copy of the database data. */
  sortData(data: Bells[]): Bells[] {

    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA:  Date | number | string = '';
      let propertyB:  Date | number | string = '';

      switch (this._sort.active) {
        case '_id': [propertyA, propertyB] = [a._id, b._id]; break;
        case 'name': [propertyA, propertyB] = [a.name, b.name]; break;
        case 'date_start': [propertyA, propertyB] = [a.date_start, b.date_start]; break;
        case 'message': [propertyA, propertyB] = [a.message, b.message]; break;
        case 'country': [propertyA, propertyB] = [a.country, b.country]; break;
        case 'company': [propertyA, propertyB] = [a.company, b.company]; break;
        case 'status' : [propertyA, propertyB] = [a.status, b.status]; break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}
