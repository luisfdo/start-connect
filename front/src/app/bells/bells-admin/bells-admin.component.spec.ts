import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BellsAdminComponent } from './bells-admin.component';

describe('BellsAdminComponent', () => {
  let component: BellsAdminComponent;
  let fixture: ComponentFixture<BellsAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BellsAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BellsAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
