import { Component, OnInit, ViewChild } from '@angular/core';
import { BellsAdminComponent } from './bells-admin/bells-admin.component';

@Component({
  selector: 'app-bells',
  templateUrl: './bells.component.html',
  styleUrls: ['./bells.component.scss']
})
export class BellsComponent implements OnInit {

  @ViewChild('appBellsAdmin') appBellsAdmin: BellsAdminComponent;


  step: number = 0;

  constructor() {}

  ngOnInit() {
  }

  activeBells(){
    this.step = 1;
  }
 
  closeChild(){
    this.step = 0;
  }

}
