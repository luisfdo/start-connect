import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDialogCustomerComponent } from './edit-dialog.component';

describe('EditDialogCustomerComponent', () => {
  let component: EditDialogCustomerComponent;
  let fixture: ComponentFixture<EditDialogCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDialogCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDialogCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
