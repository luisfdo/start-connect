import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {CustomersService} from '../../../providers/customers/customers.service';

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.scss'],
  providers: [ CustomersService ]
})
export class EditDialogCustomerComponent {

  constructor(public dialogRef: MatDialogRef<EditDialogCustomerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public dataService: CustomersService) {

      console.log('estos son los datos que vienen!!');
      console.log(data)
    }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
    this.formControl.hasError('name') ? 'Not a valid name' :
    '';
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    this.dataService.updateCustomer(this.data);
  }

}
