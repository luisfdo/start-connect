import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDialogCustomerComponent } from './add-dialog.component';

describe('AddDialogCustomerComponent', () => {
  let component: AddDialogCustomerComponent;
  let fixture: ComponentFixture<AddDialogCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDialogCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDialogCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
