import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {CustomersService} from '../../../providers/customers/customers.service';
import {FormControl, Validators} from '@angular/forms';
import {Customers} from '../../../models/customers';


@Component({
  selector: 'app-add-dialog-contact',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.scss'],
  providers: [ CustomersService ]
})
export class AddDialogCustomerComponent {
  
  constructor(
    public dialogRef: MatDialogRef<AddDialogCustomerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Customers,
    public dataService: CustomersService
  ) { 
  }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('name') ? 'Not a valid name' :
        '';
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.dataService.addCustomer(this.data);
  }

}
