import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {CustomersService} from '../../../providers/customers/customers.service';

@Component({
  selector: 'app-delete-dialog-contact',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss'],
  providers: [ CustomersService ]
})
export class DeleteDialogCustomerComponent  {

  constructor(
    public dialogRef: MatDialogRef<DeleteDialogCustomerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public dataService: CustomersService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.dataService.deleteCustomer(this.data._id);
  }

}
