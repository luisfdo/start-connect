import { Component, OnInit } from '@angular/core';
import { CustomersService } from '../../../providers/customers/customers.service'
import { BillingsService } from '../../../providers/billings/billings.service';
import * as XLSX from 'ts-xlsx';
declare var $: any;

@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.scss'],
  providers: [ CustomersService, BillingsService ]
})
export class BillingComponent implements OnInit {

  spinner:boolean = false
  file:File
  sizeMax:boolean=false
  fileExist:boolean = false
  arrayBuffer:any
  dataImport = []
  importExe:boolean = false
  importSuccess:number = 0
  importFaild:number = 0
  cont:number = 0
  resSaveCustomer:any
  succesData = []
  errorData = []
  resultData:any

  constructor(
    public dataService: CustomersService,
    public dataServiceBillings: BillingsService
  ) { }

  ngOnInit(){
  }

  incomingfile(event) 
  {
    this.file = event.target.files[0];
    if (this.file.size / 1024 / 1024 >= 0.04414154052734375) {
      this.sizeMax = false;
    } else {
      this.sizeMax = true;
    }
    this.fileExist = true;
  }


  Upload(){
    if(this.fileExist){
      let fileReader = new FileReader();
        fileReader.onload = (e) => {
            this.arrayBuffer = fileReader.result;
            var data = new Uint8Array(this.arrayBuffer);
            var arr = new Array();
            for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
            var bstr = arr.join("");
            var workbook = XLSX.read(bstr, {type:"binary"});
            var first_sheet_name = workbook.SheetNames[0];
            var worksheet = workbook.Sheets[first_sheet_name];
            this.dataImport = XLSX.utils.sheet_to_json(worksheet,{raw:true});
            this.importContacts();
        }

        if(this.file && this.sizeMax){
          this.importExe = false;
          this.importSuccess = 0;
          this.importFaild = 0;
          this.spinner = true;
          fileReader.readAsArrayBuffer(this.file);
        }else{
          this.notifications('top','center', 'Por favor valide que este cargando el excel con la estructura adecuada y que el archivo no supere el peso de 34 KB', 'warning');    
        } 
    }else{
      this.notifications('top','center', 'Por favor valide que este cargando el excel con la estructura adecuada', 'warning');
    } 
      
  }



  importContacts(){
    
    if(this.cont  < this.dataImport.length){
      
        if(this.isValidAll(this.dataImport[this.cont])){

              this.dataService.importCustomer(this.dataImport[this.cont]).subscribe(data => {

                this.resSaveCustomer = data;
                if(!this.resSaveCustomer.customer.exist){
                    const dataBilling = { value: this.dataImport[this.cont].value, customer: this.resSaveCustomer.customer._id }
                    this.saveBilling(dataBilling)
                    .then(() => {
                        this.resultData = this.resSaveCustomer
                        this.succesData.push(this.resultData.contact)
                        this.cont++;
                        this.importContacts();
                    })
                    .catch(error => console.log(error))
                }else{
                  this.errorData.push(this.dataImport[this.cont]);
                  this.cont++;
                  this.importContacts();
                }

              },
              (err) => {  
                console.log(err);
              });
            
        }else{
          this.errorData.push(this.dataImport[this.cont]);
          this.cont++;
          this.importContacts();
        }

    }else{
      this.notifications('top','center', 'Importación finalizada', 'success');
      this.importSuccess = this.succesData.length;
      this.importFaild = this.errorData.length;
      this.succesData = [];
      this.errorData = [];
      this.spinner = false;
      this.cont = 0;
      this.dataImport = [];
      this.importExe = true;
    }
    
  }


  saveBilling(dataBilling){   
    return this.dataServiceBillings.importBilling(dataBilling)
      .then(response => {        
        return true;
      })
      .catch(error => console.log(error))
  }


  notifications(from, align, message, type_){

    const type = [type_];

    $.notify({
        icon: "notifications",
        message: message
    },{
        type: type,
        timer: 4000,
        placement: {
            from: from,
            align: align
        },
        template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });

  }


  isValid(p){
    var l = p;
    var phoneRe = /^[2-9]\d{2}[2-9]\d{2}\d{4}$/;
    var digits = l.replace(/\D/g, "");
    return phoneRe.test(digits);
  }

  isValidAll(data){

    if(data.name && data.identification && data.country && 
      data.state && data.city && data.address && data.phone &&
      data.email && data.value){
      return true;
    }else{
      return false;
    }

  }


}
