import { CustomersService } from '../providers/customers/customers.service';
import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatDialog} from '@angular/material';

import { ListComponent } from './data/list/list.component';
import { QuoteComponent } from './imports/quote/quote.component';
import { BillingComponent } from './imports/billing/billing.component';




@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss'],
  providers: [ CustomersService]
})

export class CustomersComponent implements OnInit {

  @ViewChild('appItemList') appItemList: ListComponent;
  @ViewChild('appItemQuote') appItemQuote: QuoteComponent;
  @ViewChild('appItemBilling') appItemBilling: BillingComponent;


    
  constructor(
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public dataService: CustomersService
  ){}

  ngOnInit(){
  }

  

  loadDataSet($event){
    
    if($event.index == 0){
      this.appItemList.loadData();
    }
    
  }


}



