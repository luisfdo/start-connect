import { Routes } from '@angular/router';
import { AuthGuard } from '../providers/guard/auth.guard'

import { PlanComponent } from '../plans/plan.component';
import { AddPlanComponent } from '../plans/add-plan/add-plan.component';
import { EditPlanComponent } from '../plans/edit-plan/edit-plan.component';
import { DeletePlanComponent } from '../plans/delete-plan/delete-plan.component';



export const PlanRoutes: Routes = [
    { path: 'plans-internet',  component: PlanComponent, canActivate:[AuthGuard] },
    { path: 'add-plan',        component: AddPlanComponent, canActivate:[AuthGuard] },
    { path: 'edit-plan',       component: EditPlanComponent, canActivate:[AuthGuard] },
    { path: 'delete-plan',       component: DeletePlanComponent, canActivate:[AuthGuard] }

];
