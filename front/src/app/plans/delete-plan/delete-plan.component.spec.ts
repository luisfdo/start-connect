import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteDialogBankComponent } from './delete-dialog.component';

describe('DeleteDialogBankComponent', () => {
  let component: DeleteDialogBankComponent;
  let fixture: ComponentFixture<DeleteDialogBankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteDialogBankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteDialogBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
