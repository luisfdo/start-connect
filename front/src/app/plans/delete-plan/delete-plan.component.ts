import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {PlanService} from '../../providers/plans/plan.service';
declare var $: any;

@Component({
  selector: 'app-delete-plan',
  templateUrl: './delete-plan.component.html',
  styleUrls: ['./delete-plan.component.scss'],
  providers: [PlanService]
})
export class DeletePlanComponent  {

  spinner:boolean = false

  constructor(
    public dialogRef: MatDialogRef<DeletePlanComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public dataService: PlanService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    //this.dataService.deleteBank(this.data._id);
    this.spinner = true;
      this.dataService.deletePlanAll(this.data._id)
      .then(response => {
        $.notify({
          icon: "add_alert", message: "Plan Eliminado con exíto"
          },{
            type: 'successs', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
        this.spinner = false;
        this.dialogRef.close();
      })
      .catch(error => {
        console.log(error);
      });

  }

}
