import {SelectionModel} from '@angular/cdk/collections';
import { PlanService } from '../providers/plans/plan.service';
import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {Plan} from '../models/plan';
import {DataSource} from '@angular/cdk/collections';
import {AddPlanComponent} from './add-plan/add-plan.component';
import {EditPlanComponent} from './edit-plan/edit-plan.component';
import {DeletePlanComponent} from './delete-plan/delete-plan.component';
import {BehaviorSubject, fromEvent, merge, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
declare var $: any;

export interface PeriodicElement {
  name: string;
  maxRise: string;
  uploadUnit: string;
  maxDesc: string;
  descentUnit: string;
  frequency: string;
}

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.scss'],
  providers: [ PlanService ]
})
export class PlanComponent implements OnInit {

  displayedColumns = ['select', 'name', 'maxRise', 'uploadUnit', 'maxDesc', 'descentUnit', 'frequency', 'actions'];

  exampleDatabase: PlanService | null;
  dataSource: any;
  selection :any;
  index: number;
  _id: string;
  cont:number = 0
  succesData = []
  errorData = []
  showView:boolean = false;

  constructor(
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public dataService: PlanService
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;

  ngOnInit() {
    this.loadData();
  }

  public loadData(){

    this.exampleDatabase = new PlanService(this.httpClient);
    this.dataSource = new ExampleDataSource(this.exampleDatabase, this.paginator, this.sort);
    this.selection = new SelectionModel<PeriodicElement>(true, []);
    fromEvent(this.filter.nativeElement, 'keyup')
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });

  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected(){
    
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.filteredData.length;

    return numSelected === numRows;

  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle(){

    if(this.selection.selected.length > 0){
      // this.showDelete = false;
      this.selection.clear()
    }else{
      // this.showDelete = true;
      this.dataSource.filteredData.forEach(row => this.selection.select(row));
    }

  }

  deleteSelectedMikrotik(){
    if(this.selection.selected.length > 0){
      this.showView = true
      this.iteraDelete();
    }else{
      this.notifications('top','right', 'Usted no a seleccionado los registros a eliminar', 'warning');
    }

    
  }

  iteraDelete(){
    if(this.cont  < this.selection.selected.length){
        
      this.deleteServer(this.selection.selected[this.cont]._id)
        .then(() => {
            this.succesData.push(this.selection.selected[this.cont])
            this.cont++;
            this.iteraDelete();
        })
        .catch(error => console.log(error))


    }else{
      this.showView = false;
      this.loadData();
      this.notifications('top','right', 'Los registros seleccionados fueron eliminados', 'success');
      this.cont = 0;
    }
  }


  deleteServer(_id){   
    return this.dataService.deletePlanAll(_id)
      .then(response => {        
        return true;
      })
      .catch(error => console.log(error))
  }


  notifications(from, align, message, type_){

    const type = [type_];

    $.notify({
        icon: "notifications",
        message: message
    },{
        type: type,
        timer: 4000,
        placement: {
            from: from,
            align: align
        },
        template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });

  }

  addNew(plan: Plan) {

    const dialogRef = this.dialog.open(AddPlanComponent, {
      data: { plan: plan }
    });

    dialogRef.afterClosed().subscribe(result => {
        this.loadData();
    });

  }

  startEdit(i: number, _id: string, name: string, maxRise: string, uploadUnit:string, maxDesc:string, descentUnit:string, frequency:string) {
    
    this._id = _id;
    this.index = i;

    const dialogRef = this.dialog.open(EditPlanComponent, {
      data: {_id: _id, name: name, maxRise: maxRise, uploadUnit:uploadUnit, maxDesc: maxDesc, descentUnit:descentUnit, frequency:frequency}
    });

    dialogRef.afterClosed().subscribe(result => {
      //if (result === 1) {
        this.loadData();
      //}
    });
  }

  deleteItem(i: number, _id: string, name: string, maxRise: string, uploadUnit:string, maxDesc:string, descentUnit:string, frequency:string){
    this.index = i;
    this._id = _id;
    const dialogRef = this.dialog.open(DeletePlanComponent, {
      data: {_id: _id, name: name, maxRise: maxRise, uploadUnit:uploadUnit, maxDesc: maxDesc, descentUnit:descentUnit, frequency:frequency}
    });

    dialogRef.afterClosed().subscribe(result => {
      //if (result === 1) {
        this.loadData();
      //}
    });
  }


}



export class ExampleDataSource extends DataSource<Plan> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Plan[] = [];
  renderedData: Plan[] = [];

  constructor(public _exampleDatabase: PlanService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Plan[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._exampleDatabase.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    this._exampleDatabase.getAllPlans();


    return merge(...displayDataChanges).pipe(map( () => {
        // Filter data
        this.filteredData = this._exampleDatabase.data.slice().filter((plan: Plan) => {
          const searchStr = (plan._id + plan.name + plan.maxRise + plan.uploadUnit + plan.maxDesc + plan.descentUnit + plan.frequency + plan.company).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        });

        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());

        // Grab the page's slice of the filtered sorted data.
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
        return this.renderedData;
      }
    ));
  }

  disconnect() {}


  /** Returns a sorted copy of the database data. */
  sortData(data: Plan[]): Plan[] {

    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA:  Date | number | string = '';
      let propertyB:  Date | number | string = '';

      switch (this._sort.active) {
        case '_id': [propertyA, propertyB] = [a._id, b._id]; break;
        case 'name': [propertyA, propertyB] = [a.name, b.name]; break;
        case 'maxRise': [propertyA, propertyB] = [a.maxRise, b.maxRise]; break;
        case 'uploadUnit': [propertyA, propertyB] = [a.uploadUnit, b.uploadUnit]; break;
        case 'maxDesc': [propertyA, propertyB] = [a.maxDesc, b.maxDesc]; break;
        case 'descentUnit': [propertyA, propertyB] = [a.descentUnit, b.descentUnit]; break;
        case 'frequency': [propertyA, propertyB] = [a.frequency, b.frequency]; break;
        case 'company' : [propertyA, propertyB] = [a.company, b.company]; break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}
