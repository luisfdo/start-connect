import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {PlanService} from '../../providers/plans/plan.service';
import {FormControl, Validators} from '@angular/forms';
import {Plan} from '../../models/plan';
declare var $: any;

@Component({
  selector: 'app-add-plan',
  templateUrl: './add-plan.component.html',
  styleUrls: ['./add-plan.component.scss'],
  providers: [ PlanService ]
})

export class AddPlanComponent {

  data = {
    name: null,
    maxRise: null,
    uploadUnit: null,
    maxDesc: null,
    descentUnit: null,
    frequency: null
  }

  spinner:boolean = false

  constructor(
    private planService:PlanService,
    public dialogRef: MatDialogRef<AddPlanComponent>,
  ){}

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage(){
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('name') ? 'Not a valid' :
        '';
  }


  private onNoClick(): void {
    this.dialogRef.close();
  }


  private savePlan(){

      this.spinner = true;
      this.planService.addPlan(this.data)
      .then(response => {
        $.notify({
          icon: "add_alert", message: "Plan guardado con exíto"
          },{
            type: 'successs', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
        this.spinner = false;
        this.dialogRef.close();
      })
      .catch(error => {
        console.log(error);
      });

  }



}
