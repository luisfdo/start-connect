import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {PlanService} from '../../providers/plans/plan.service';
import {FormControl, Validators} from '@angular/forms';
import {Plan} from '../../models/plan';
declare var $: any;

@Component({
  selector: 'app-edit-plan',
  templateUrl: './edit-plan.component.html',
  styleUrls: ['./edit-plan.component.scss'],
  providers: [ PlanService ]
})

export class EditPlanComponent  {
  spinner:boolean = false
  constructor(public dialogRef: MatDialogRef<EditPlanComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public dataService: PlanService) {
    }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
    this.formControl.hasError('name') ? 'Not a valid name' :
    '';
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {

    this.spinner = true;
      this.dataService.updatePlan(this.data)
      .then(response => {
        $.notify({
          icon: "add_alert", message: "Plan Editado con exíto"
          },{
            type: 'successs', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
        this.spinner = false;
        this.dialogRef.close();
      })
      .catch(error => {
        console.log(error);
      });

  }



}
