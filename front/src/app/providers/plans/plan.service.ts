import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Plan} from '../../models/plan';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { AppSettings } from '../../app-settings';

@Injectable({
  providedIn: 'root'
})
export class PlanService {

    dataChange: BehaviorSubject<Plan[]> = new BehaviorSubject<Plan[]>([]);
    user:any

    constructor(
      private httpClient: HttpClient){
      this.user = JSON.parse(localStorage.getItem("currentUser"));
    }

    getAllPlans(): void {

      const user = JSON.parse(localStorage.getItem("currentUser"));
      let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
      
      if(user.super){
        this.httpClient.get<Plan[]>(AppSettings.API_ENDPOINT + '/plans/', headers).subscribe(data => {
          this.dataChange.next(data);
        },
        (error: HttpErrorResponse) => {
          console.log (error.name + ' ' + error.message);
        });
      }else{

        this.httpClient.get<Plan[]>(AppSettings.API_ENDPOINT + '/plans/'+ user.company, headers).subscribe(data => {
          this.dataChange.next(data);
        },
        (error: HttpErrorResponse) => {
          console.log (error.name + ' ' + error.message);
        });
      }
  
    }

    get data(): Plan[] {
      return this.dataChange.value;
    }


    getPlans(){

      const user = JSON.parse(localStorage.getItem("currentUser"));
      let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
  
      return this.httpClient.get(AppSettings.API_ENDPOINT + '/plans/'+ user.company, headers)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  
     }
    

    addPlan(plan){
      plan.company = this.user.company
      let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
      return this.httpClient.post(AppSettings.API_ENDPOINT + '/plans', plan, headers)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
    }


    updatePlan(plan){
      let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
      return this.httpClient.put(AppSettings.API_ENDPOINT + '/plans?planID=' + plan._id, plan, headers)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
    }

    deletePlanAll(id: number){
      let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
      return this.httpClient.delete(AppSettings.API_ENDPOINT + '/plans?planID=' + id, headers)
      .toPromise()
    }


    private extractData(res: Response){
      let response = res;
      return response || {};
    }

    private handleError(error: any) {
      console.error('Ha ocurrido un error', error);
      return Promise.reject(error.message || error);
    } 
}
