import {Injectable} from '@angular/core';
import {Voice} from '../../models/voice';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { AppSettings } from '../../app-settings';
declare var $: any;

@Injectable()
export class VoiceService {

  dialogData: any;

  constructor (private httpClient: HttpClient) {}

  sendVoice(voice, bell){
    
    var number = ''

    // if(sms.country == 'COLOMBIA'){
      number = '+57'+voice.phone
    // }

    const body = {
      'body': bell.message,
      'from': '+16143285329',
      'to' : number,
      'bells' : bell._id,
      'company': bell.company
    }

    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

      return this.httpClient.post(AppSettings.API_ENDPOINT + '/voices/', body, headers)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);

   }

   private extractData(res: Response){
    let response = res;
    return response || {};
  }

  private handleError(error: any) {
    console.error('Ha ocurrido un error', error);
    return Promise.reject(error.message || error);
  }


  
}


