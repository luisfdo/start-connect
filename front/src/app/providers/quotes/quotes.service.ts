import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Quote} from '../../models/quote';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
// import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { AppSettings } from '../../app-settings';

declare var $: any;

@Injectable()
export class QuotesService {

  dataChange: BehaviorSubject<Quote[]> = new BehaviorSubject<Quote[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor (private httpClient: HttpClient) {}

  get data(): Quote[] {
    return this.dataChange.value;
  }


  /** CRUD METHODS */
  getAllQuote(): void {

    const user = JSON.parse(localStorage.getItem("currentUser"));
    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
    
    if(user.super){
      this.httpClient.get<Quote[]>(AppSettings.API_ENDPOINT + '/quotes/', headers).subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log (error.name + ' ' + error.message);
      });
    }else{

      this.httpClient.get<Quote[]>(AppSettings.API_ENDPOINT + '/quotes/'+ user.company, headers).subscribe(data => {      
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log (error.name + ' ' + error.message);
      });
    }

  }

  importQuote(quote){

    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
    const user = JSON.parse(localStorage.getItem("currentUser"));
    quote.company = user.company
    return this.httpClient.post<Quote[]>(AppSettings.API_ENDPOINT + '/quotes/', quote, headers)
    .toPromise()
    

  }


  addQuote(quote: Quote): void {
    
    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
    const user = JSON.parse(localStorage.getItem("currentUser"));
    quote.company = user.company

    this.httpClient.post<Quote[]>(AppSettings.API_ENDPOINT + '/quotes/', quote, headers).subscribe(data => {
        $.notify({
          icon: "add_alert", message: "cita creada con éxito!"
          },{
            type: 'success', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      },
      (err: HttpErrorResponse) => {
        $.notify({
          icon: "add_alert", message: "No fue posible crear una cita, valide si ya esta creada!"
          },{
            type: 'error', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
        
      });
   }

  updateQuote(quote: Quote): void {

    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

    this.httpClient.put(AppSettings.API_ENDPOINT + '/quotes?quoteID=' + quote._id, quote, headers).subscribe(data => {
        $.notify({
          icon: "add_alert", message: "Cita editada con éxito!"
          },{
            type: 'success', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      },
      (err: HttpErrorResponse) => {
        $.notify({
          icon: "add_alert", message: "No fue posible editar la cita!"
          },{
            type: 'error', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      }
    );
  }


  deleteQuote(id: number): void {

    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

    this.httpClient.delete(AppSettings.API_ENDPOINT + '/quotes?quoteID=' + id, headers).subscribe(data => {
          $.notify({
            icon: "add_alert", message: "Cita eliminada con éxito!"
            },{
              type: 'success', timer: 4000,
              placement: {
              from: 'top',
              align: 'right'
            }
          });
      },
      (err: HttpErrorResponse) => {
        $.notify({
          icon: "add_alert", message: "No fue posible eliminar la cita!"
          },{
            type: 'error', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      }
    );
  }


}


