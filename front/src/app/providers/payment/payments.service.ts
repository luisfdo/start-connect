import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Payment} from '../../models/payment';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { AppSettings } from '../../app-settings';

declare var $: any;

@Injectable()
export class PaymentsService {

  dataChange: BehaviorSubject<Payment[]> = new BehaviorSubject<Payment[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor (private httpClient: HttpClient) {}


    addPayment(payment){

        let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

        return this.httpClient.post(AppSettings.API_ENDPOINT + '/payments/', payment, headers)
        .toPromise()
        .then(this.extractData)
        .catch(this.handleError);

    }

    
    private extractData(res: Response){
        let response = res;
        return response || {};
    }

    private handleError(error: any) {
        console.error('Ha ocurrido un error', error);
        return Promise.reject(error.message || error);
    }

 
}


