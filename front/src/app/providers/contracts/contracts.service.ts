import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Contract} from '../../models/contract';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { AppSettings } from '../../app-settings';

@Injectable({
  providedIn: 'root'
})
export class ContractService {

    dataChange: BehaviorSubject<Contract[]> = new BehaviorSubject<Contract[]>([]);
    user:any

    constructor(
      private httpClient: HttpClient){
      this.user = JSON.parse(localStorage.getItem("currentUser"));
    }

    getAllContracts(): void {

      const user = JSON.parse(localStorage.getItem("currentUser"));
      let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
      
      
      
      if(user.super){
        this.httpClient.get<Contract[]>(AppSettings.API_ENDPOINT + '/contracts/', headers).subscribe(data => {
          this.dataChange.next(data);
        },
        (error: HttpErrorResponse) => {
          console.log (error.name + ' ' + error.message);
        });
      }else{

        this.httpClient.get<Contract[]>(AppSettings.API_ENDPOINT + '/contracts/'+ user.company, headers).subscribe(data => {
          this.dataChange.next(data);
        },
        (error: HttpErrorResponse) => {
          console.log (error.name + ' ' + error.message);
        });
      }
  
    }

    get data(): Contract[] {
      return this.dataChange.value;
    }
    

    addContract(contract){
      contract.company = this.user.company
      let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
      return this.httpClient.post(AppSettings.API_ENDPOINT + '/contracts', contract, headers)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
    }


    updateContract(contract){
      contract.company = this.user.company
      let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
      return this.httpClient.put(AppSettings.API_ENDPOINT + '/contracts?contractID=' + contract._id, contract, headers)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
    }

    deleteContractAll(id: number){
      let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
      return this.httpClient.delete(AppSettings.API_ENDPOINT + '/contracts?contractID=' + id, headers)
      .toPromise()
    }

    private extractData(res: Response){
      let response = res;
      return response || {};
    }

    private handleError(error: any) {
      console.error('Ha ocurrido un error', error);
      return Promise.reject(error.message || error);
    } 
}
