import { Injectable } from '@angular/core';
// import { HttpClientModule } from '@angular/common/http';
import { User } from '../../models/user'
// import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { AppSettings } from '../../app-settings';


@Injectable()
export class AuthService {

  constructor(private http : Http) { }

  setUser(user: User): void {
    let user_string = JSON.stringify(user);
    localStorage.setItem("currentUser", user_string);
  }

  setToken(token): void {
    localStorage.setItem("accessToken", token);
  }

  loginuser(email, password) {

    let headers = new Headers({ 'Authorization': AppSettings.API_TOKEN });
    let options = new RequestOptions({ headers: headers });

    let body = {
      'email': email,
      'password': password
    };

    return this.http.post(AppSettings.API_ENDPOINT+'/users/auth/login', body, options)
    .toPromise()
    .then(this.extractData)
    .catch(this.handleError);

  }

  

  private extractData(res: Response) {
    let response = res.json();
    return response || { };
  }

  private handleError(error: any) {
    console.error('Ha ocurrido un error', error);
    return Promise.reject(error.message || error);
  }
  
  
}
