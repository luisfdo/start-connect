import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Sms} from '../../models/sms';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { AppSettings } from '../../app-settings';

declare var $: any;

@Injectable()
export class SmsService {

  dialogData: any;

  constructor (private httpClient: HttpClient) {}

  sendSms(sms, bell){
    
    var number = ''

    // if(sms.country == 'COLOMBIA'){
      number = '+57'+sms.phone
    // }

    const body = {
      'body': bell.message,
      'from': '+16143285329',
      'to' : number,
      'bells' : bell._id,
      'company': bell.company
    }

    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

      return this.httpClient.post(AppSettings.API_ENDPOINT + '/smss/addSms', body, headers)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);

   }

   private extractData(res: Response){
    let response = res;
    return response || {};
  }

  private handleError(error: any) {
    console.error('Ha ocurrido un error', error);
    return Promise.reject(error.message || error);
  }


  
}


