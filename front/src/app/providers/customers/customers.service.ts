import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Customers} from '../../models/customers';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { AppSettings } from '../../app-settings';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
declare var $: any;

@Injectable()
export class CustomersService {

  dataChange: BehaviorSubject<Customers[]> = new BehaviorSubject<Customers[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor (private httpClient: HttpClient) {}

  get data(): Customers[] {
    return this.dataChange.value;
  }


  

  getAllCustomers(): void {

    const user = JSON.parse(localStorage.getItem("currentUser"));
    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
    
    if(user.super){
      this.httpClient.get<Customers[]>(AppSettings.API_ENDPOINT + '/customers/', headers).subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log (error.name + ' ' + error.message);
      });
    }else{

      this.httpClient.get<Customers[]>(AppSettings.API_ENDPOINT + '/customers/'+ user.company, headers).subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log (error.name + ' ' + error.message);
      });
    }

  }


  getAllCustomersAll(): void {

    const user = JSON.parse(localStorage.getItem("currentUser"));
    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
    
    if(user.super){
      this.httpClient.get<Customers[]>(AppSettings.API_ENDPOINT + '/customers/', headers).subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log (error.name + ' ' + error.message);
      });
    }else{



      this.httpClient.get<Customers[]>(AppSettings.API_ENDPOINT + '/customers/'+ user.company, headers).subscribe(data => {
        return data;
      },
      (error: HttpErrorResponse) => {
        console.log (error.name + ' ' + error.message);
      });
    }

  }

  addCustomer(customer: Customers): void {
    
    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
    const user = JSON.parse(localStorage.getItem("currentUser"));
    customer.company = user.company

    this.httpClient.post<Customers[]>(AppSettings.API_ENDPOINT + '/customers/', customer, headers).subscribe(data => {
        $.notify({
          icon: "add_alert", message: "cliente creado con éxito!"
          },{
            type: 'success', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      },
      (err: HttpErrorResponse) => {
        $.notify({
          icon: "add_alert", message: "No fue posible crear el cliente, valide si ya esta creado!"
          },{
            type: 'error', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
        
      });
   }


   

   importCustomer(customer: Customers){

    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
    const user = JSON.parse(localStorage.getItem("currentUser"));
    customer.company = user.company
    return this.httpClient.post<Customers[]>(AppSettings.API_ENDPOINT + '/customers/', customer, headers);

  }

  

  updateCustomer(customer: Customers): void {

    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

    this.httpClient.put(AppSettings.API_ENDPOINT + '/customers?customerID=' + customer._id, customer, headers).subscribe(data => {
        $.notify({
          icon: "add_alert", message: "Cliente editado con éxito!"
          },{
            type: 'success', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      },
      (err: HttpErrorResponse) => {
        $.notify({
          icon: "add_alert", message: "No fue posible editar el cliente!"
          },{
            type: 'error', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      }
    );
  }


  deleteCustomer(id: number): void {

    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

    this.httpClient.delete(AppSettings.API_ENDPOINT + '/customers?customerID=' + id, headers).subscribe(data => {
          $.notify({
            icon: "add_alert", message: "Cliente eliminada con éxito!"
            },{
              type: 'success', timer: 4000,
              placement: {
              from: 'top',
              align: 'right'
            }
          });
      },
      (err: HttpErrorResponse) => {
        $.notify({
          icon: "add_alert", message: "No fue posible eliminar el cliente!"
          },{
            type: 'error', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      }
    );
  }


  deleteCustomerAll(id: number){
    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
    return this.httpClient.delete(AppSettings.API_ENDPOINT + '/customers?customerID=' + id, headers)
    .toPromise()

  }


  getCustomers(){

    const user = JSON.parse(localStorage.getItem("currentUser"));
    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

    return this.httpClient.get(AppSettings.API_ENDPOINT + '/customers/'+ user.company, headers)
    .toPromise()
    .then(this.extractData)
    .catch(this.handleError);

   }


  private extractData(res: Response){
    let response = res;
    return response || {};
  }

  private handleError(error: any) {
    console.error('Ha ocurrido un error', error);
    return Promise.reject(error.message || error);
  }

}


