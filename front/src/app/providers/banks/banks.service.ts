import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Bank} from '../../models/bank';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { AppSettings } from '../../app-settings';

declare var $: any;

@Injectable()
export class BanksService {

  dataChange: BehaviorSubject<Bank[]> = new BehaviorSubject<Bank[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor (private httpClient: HttpClient) {}

  get data(): Bank[] {
    return this.dataChange.value;
  }


  /** CRUD METHODS */
  getAllBank(): void {

    const user = JSON.parse(localStorage.getItem("currentUser"));
    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
    
    if(user.super){
      this.httpClient.get<Bank[]>(AppSettings.API_ENDPOINT + '/banks/', headers).subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log (error.name + ' ' + error.message);
      });
    }else{

      this.httpClient.get<Bank[]>(AppSettings.API_ENDPOINT + '/banks/'+ user.company, headers).subscribe(data => {
      
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log (error.name + ' ' + error.message);
      });
    }

  }


  addBank(bank: Bank): void {
    
    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
    const user = JSON.parse(localStorage.getItem("currentUser"));
    bank.company = user.company

    this.httpClient.post<Bank[]>(AppSettings.API_ENDPOINT + '/banks/', bank, headers).subscribe(data => {
        $.notify({
          icon: "add_alert", message: "Banco creada con éxito!"
          },{
            type: 'success', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      },
      (err: HttpErrorResponse) => {
        $.notify({
          icon: "add_alert", message: "No fue posible crear el banco, valide si ya esta creada!"
          },{
            type: 'error', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
        
      });
   }

  updateBank(bank: Bank): void {

    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

    this.httpClient.put(AppSettings.API_ENDPOINT + '/banks?bankID=' + bank._id, bank, headers).subscribe(data => {
        $.notify({
          icon: "add_alert", message: "Banco editada con éxito!"
          },{
            type: 'success', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      },
      (err: HttpErrorResponse) => {
        $.notify({
          icon: "add_alert", message: "No fue posible editar el banco!"
          },{
            type: 'error', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      }
    );
  }


  deleteBank(id: number): void {

    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

    this.httpClient.delete(AppSettings.API_ENDPOINT + '/banks?bankID=' + id, headers).subscribe(data => {
          $.notify({
            icon: "add_alert", message: "Banco eliminada con éxito!"
            },{
              type: 'success', timer: 4000,
              placement: {
              from: 'top',
              align: 'right'
            }
          });
      },
      (err: HttpErrorResponse) => {
        $.notify({
          icon: "add_alert", message: "No fue posible eliminar el banco!"
          },{
            type: 'error', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      }
    );
  }
}


