import { TestBed, inject } from '@angular/core/testing';
import { BillingsService } from './billings.service';

describe('BillingsService', () => {  
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BillingsService]
    });
  });

  it('should be created', inject([BillingsService], (service: BillingsService) => {
    expect(service).toBeTruthy();
  }));

});
