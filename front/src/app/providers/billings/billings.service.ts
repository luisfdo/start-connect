import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Billing} from '../../models/billing';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { AppSettings } from '../../app-settings';
declare var $: any;

@Injectable()
export class BillingsService {

  constructor (private httpClient: HttpClient) {}

  importBilling(billing){

    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
    const user = JSON.parse(localStorage.getItem("currentUser"));
    billing.company = user.company
    return this.httpClient.post<Billing[]>(AppSettings.API_ENDPOINT + '/billings/', billing, headers)
    .toPromise()

  }




}


