import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {User} from '../../models/user';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { AppSettings } from '../../app-settings';
import { ConstantPool } from '@angular/compiler';

declare var $: any;

@Injectable()
export class UsersService {

  // dataChange: BehaviorSubject<Bank[]> = new BehaviorSubject<Bank[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor (private httpClient: HttpClient) {}

  

  addUser(user){

    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

      return this.httpClient.post(AppSettings.API_ENDPOINT + '/users/', user, headers)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);

   }

   editUser(user){

    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

      return this.httpClient.put(AppSettings.API_ENDPOINT + '/users?userID=' + user._id, user, headers)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);

   }


   private extractData(res: Response){
    let response = res;
    return response || {};
  }

  private handleError(error: any) {
    console.error('Ha ocurrido un error', error);
    return Promise.reject(error.message || error);
  }

  


  
}


