import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Card} from '../../models/card';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { AppSettings } from '../../app-settings';

declare var $: any;

@Injectable()
export class CardsService {

  dataChange: BehaviorSubject<Card[]> = new BehaviorSubject<Card[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor (private httpClient: HttpClient) {}


    getCardUser(iduser){

        let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

        return this.httpClient.get(AppSettings.API_ENDPOINT + '/cards?cardIdUser=' + iduser, headers)
        .toPromise()
        .then(this.extractData)
        .catch(this.handleError);

    }



    addCard(card){

        let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

        return this.httpClient.post(AppSettings.API_ENDPOINT + '/cards/', card, headers)
        .toPromise()
        .then(this.extractData)
        .catch(this.handleError);

    }

    addCustomerPay(customerPay){

        let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

        return this.httpClient.post(AppSettings.API_ENDPOINT + '/users/userPay', customerPay, headers)
        .toPromise()
        .then(this.extractData)
        .catch(this.handleError);

    }

    private extractData(res: Response){
        let response = res;
        return response || {};
    }

    private handleError(error: any) {
        console.error('Ha ocurrido un error', error);
        return Promise.reject(error.message || error);
    }

 
}


