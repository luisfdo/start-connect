import { TestBed, inject } from '@angular/core/testing';

import { BellsService } from './bells.service';

describe('BellsService', () => {
  
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BellsService]
    });
  });

  it('should be created', inject([BellsService], (service: BellsService) => {
    expect(service).toBeTruthy();
  }));

});
