import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Bells} from '../../models/bells';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { AppSettings } from '../../app-settings';

declare var $: any;

@Injectable()
export class BellsService {

  dataChange: BehaviorSubject<Bells[]> = new BehaviorSubject<Bells[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor (private httpClient: HttpClient) {}

  get data(): Bells[] {
    return this.dataChange.value;
  }


  /** CRUD METHODS */
  getAllBells(): void {

    const user = JSON.parse(localStorage.getItem("currentUser"));
    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
    
    if(user.super){
      this.httpClient.get<Bells[]>(AppSettings.API_ENDPOINT + '/bells/', headers).subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log (error.name + ' ' + error.message);
      });
    }else{

      this.httpClient.get<Bells[]>(AppSettings.API_ENDPOINT + '/bells/'+ user.company, headers).subscribe(data => {
      
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log (error.name + ' ' + error.message);
      });
    }

  }


  addBell(bell: Bells): void {
    
    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
    const user = JSON.parse(localStorage.getItem("currentUser"));
    bell.company = user.company

    this.httpClient.post<Bells[]>(AppSettings.API_ENDPOINT + '/bells/', bell, headers).subscribe(data => {
        $.notify({
          icon: "add_alert", message: "Campaña creada con éxito!"
          },{
            type: 'success', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      },
      (err: HttpErrorResponse) => {
        $.notify({
          icon: "add_alert", message: "No fue posible crear la campaña, valide si ya esta creada!"
          },{
            type: 'error', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
        
      });
   }

  updateBell(bell: Bells): void {

    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

    this.httpClient.put(AppSettings.API_ENDPOINT + '/bells?bellID=' + bell._id, bell, headers).subscribe(data => {
        $.notify({
          icon: "add_alert", message: "Campaña editado con éxito!"
          },{
            type: 'success', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      },
      (err: HttpErrorResponse) => {
        $.notify({
          icon: "add_alert", message: "No fue posible editar la campaña!"
          },{
            type: 'error', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      }
    );
  }


  deleteBell(id: number): void {

    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

    this.httpClient.delete(AppSettings.API_ENDPOINT + '/bells?bellID=' + id, headers).subscribe(data => {
          $.notify({
            icon: "add_alert", message: "Campaña eliminada con éxito!"
            },{
              type: 'success', timer: 4000,
              placement: {
              from: 'top',
              align: 'right'
            }
          });
      },
      (err: HttpErrorResponse) => {
        $.notify({
          icon: "add_alert", message: "No fue posible eliminar la campaña!"
          },{
            type: 'error', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      }
    );
  }
}


