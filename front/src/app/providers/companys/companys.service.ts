import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Company} from '../../models/company';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { AppSettings } from '../../app-settings';

declare var $: any;

@Injectable()
export class CompanysService {

  dataChange: BehaviorSubject<Company[]> = new BehaviorSubject<Company[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor (private httpClient: HttpClient) {}

  get data(): Company[] {
    return this.dataChange.value;
  }

  // getDialogData() {
  //   return this.dialogData;
  // }

  

  /** CRUD METHODS */
  getAllCompanys(): void {

    const user = JSON.parse(localStorage.getItem("currentUser"));
    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

    if(user.super){
      this.httpClient.get<Company[]>(AppSettings.API_ENDPOINT + '/companys/', headers).subscribe(data => {
          this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log (error.name + ' ' + error.message);
      });
    }else{

      this.httpClient.get<Company[]>(AppSettings.API_ENDPOINT + '/companys/'+ user.company, headers).subscribe(data => {
          let dataR = Array();
          dataR.push(data);
          this.dataChange.next(dataR);
      },
      (error: HttpErrorResponse) => {
        console.log (error.name + ' ' + error.message);
      });
    }
    
    
  }


  addCompany(company: Company): void {
    
    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

    this.httpClient.post<Company[]>(AppSettings.API_ENDPOINT + '/companys/', company, headers).subscribe(data => {
        $.notify({
          icon: "add_alert", message: "Empresa creada con éxito!"
          },{
            type: 'success', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      },
      (err: HttpErrorResponse) => {
        $.notify({
          icon: "add_alert", message: "No fue posible crear la empresa, valide si ya esta creada!"
          },{
            type: 'error', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
        
      });
   }

   createCompany(company){
    
      let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

      return this.httpClient.post(AppSettings.API_ENDPOINT + '/companys/', company, headers)
        .toPromise()
        .then(this.extractData)
        .catch(this.handleError);
   }

  updateCompany(company: Company): void {

    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

    this.httpClient.put(AppSettings.API_ENDPOINT + '/companys?companyID=' + company._id, company, headers).subscribe(data => {
        $.notify({
          icon: "add_alert", message: "Empresa editada con éxito!"
          },{
            type: 'success', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      },
      (err: HttpErrorResponse) => {
        $.notify({
          icon: "add_alert", message: "No fue posible editar la empresa!"
          },{
            type: 'error', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      }
    );
  }


  deleteCompany(id: number): void {

    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

    this.httpClient.delete(AppSettings.API_ENDPOINT + '/companys?companyID=' + id, headers).subscribe(data => {
          $.notify({
            icon: "add_alert", message: "Empresa eliminada con éxito!"
            },{
              type: 'success', timer: 4000,
              placement: {
              from: 'top',
              align: 'right'
            }
          });
      },
      (err: HttpErrorResponse) => {
        $.notify({
          icon: "add_alert", message: "No fue posible eliminar la empresa!"
          },{
            type: 'error', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      }
    );
  }

  getCompanyBy(_id){

    let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
    return this.httpClient.get(AppSettings.API_ENDPOINT + '/companys/' + _id, headers)
    .toPromise()

  }


  private extractData(res: Response) {
    let response = res;
    return response || {};
  }

  private handleError(error: any) {
    console.error('Ha ocurrido un error', error);
    return Promise.reject(error.message || error);
  }


}


