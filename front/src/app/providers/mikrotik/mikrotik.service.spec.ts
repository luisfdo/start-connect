import { TestBed, inject } from '@angular/core/testing';

import { MikrotikService } from './mikrotik.service';

describe('MikrotikService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MikrotikService]
    });
  });

  it('should be created', inject([MikrotikService], (service: MikrotikService) => {
    expect(service).toBeTruthy();
  }));
});
