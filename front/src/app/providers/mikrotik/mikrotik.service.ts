import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Mikrotik} from '../../models/mikrotik';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { AppSettings } from '../../app-settings';

@Injectable({
  providedIn: 'root'
})
export class MikrotikService {

    dataChange: BehaviorSubject<Mikrotik[]> = new BehaviorSubject<Mikrotik[]>([]);
    user:any

    constructor(
      private httpClient: HttpClient){
      this.user = JSON.parse(localStorage.getItem("currentUser"));
    }

    getAllMikrotik(): void {

      const user = JSON.parse(localStorage.getItem("currentUser"));
      let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
      
      if(user.super){
        this.httpClient.get<Mikrotik[]>(AppSettings.API_ENDPOINT + '/mikrotik/servers/', headers).subscribe(data => {
          this.dataChange.next(data);
        },
        (error: HttpErrorResponse) => {
          console.log (error.name + ' ' + error.message);
        });
      }else{
        const mikrotik = {
          "company": this.user.company,
          "status": true
        }

        console.log(mikrotik);

        this.httpClient.post<Mikrotik[]>(AppSettings.API_ENDPOINT + '/mikrotik/servers', mikrotik, headers).subscribe(data => {
          //console.log(data.data);
          this.dataChange.next(data);
        },
        (error: HttpErrorResponse) => {
          console.log (error.name + ' ' + error.message);
        });
      }
  
    }

    get data(): Mikrotik[] {
      return this.dataChange.value;
    }
    
    getMikrotiks(){

      const user = JSON.parse(localStorage.getItem("currentUser"));
      let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};

      const mikrotik = {
        "company": this.user.company,
        "status": true
      }
  
      return this.httpClient.post(AppSettings.API_ENDPOINT + '/mikrotik/servers', mikrotik, headers)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  
     }


    addMikrotik(mikrotik){
      mikrotik.company = this.user.company
      let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
      return this.httpClient.post(AppSettings.API_ENDPOINT + '/mikrotik/createServer', mikrotik, headers)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
    }


    updateMikrotik(mikrotik){

      let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
      return this.httpClient.put(AppSettings.API_ENDPOINT + '/mikrotik/updateServer?serverID=' + mikrotik._id, mikrotik, headers)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
      
    }

    deleteMikrotikAll(id: number){
      let headers =  {headers: new  HttpHeaders({ 'Authorization': AppSettings.API_TOKEN})};
      return this.httpClient.delete(AppSettings.API_ENDPOINT + '/mikrotik?serverID=' + id, headers)
      .toPromise()
    }


    private extractData(res: Response){
      let response = res;
      return response || {};
    }

    private handleError(error: any) {
      console.error('Ha ocurrido un error', error);
      return Promise.reject(error.message || error);
    } 
}
