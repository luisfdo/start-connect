import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard',  icon: 'dashboard', class: '' },
    { path: '/companys', title: 'Empresas',  icon: 'work', class: '' },
    { path: '/customers', title: 'Clientes', icon:'group', class: ''},
    { path: '/bells', title: 'Campañas',  icon:'speaker', class: '' },
    { path: '/contrats', title: 'Contratos',  icon:'attach_money', class: '' },
    { path: '/plans-internet', title: 'Planes internet',  icon:'cloud', class: '' },
    { path: '/mikrotik', title: 'Servidores',  icon:'settings_input_antenna', class: '' },
    

];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  private login : boolean = false

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
