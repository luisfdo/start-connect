import { Component, OnInit } from '@angular/core';
import { CompanysService } from '../../providers/companys/companys.service';
import { UsersService } from '../../providers/users/users.service';
import { User } from '../../models/user'
declare var $: any;


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [ CompanysService, UsersService ]
})
export class ProfileComponent implements OnInit {

  spinner:boolean = false;
  user:User
  company:any

  constructor(
    public serviceCompany: CompanysService,
    public serviceUser: UsersService,

  ){
    this.user = JSON.parse(localStorage.getItem("currentUser"));
  }

  ngOnInit(){

    this.spinner = true;
    this.getCompany()

  }

  getCompany(){
    this.serviceCompany.getCompanyBy(this.user.company)
    .then(response => {        
      this.company = response
      console.log(this.company)
      this.spinner = false;
    })
    .catch(error => console.log(error))
  }

  confirmEdit(){
    this.spinner = true;
    this.serviceUser.editUser(this.user)
    .then(response => {        
        $.notify({
          icon: "add_alert", message: "Sus datos fueron actualizados, recuerde tener sus datos al dia para sus clientes!"
          },{
            type: 'success', timer: 6000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
        this.spinner = true;
    })

    .catch(error => console.log(error))

  }


}
