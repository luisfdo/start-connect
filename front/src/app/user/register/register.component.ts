import { Component, OnInit } from '@angular/core';
declare var $: any;
import { CompanysService } from '../../providers/companys/companys.service';
import { RolsService } from '../../providers/rols/rols.service';
import { UsersService } from '../../providers/users/users.service';
import * as moment from 'moment';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [ CompanysService, RolsService, UsersService ]
})
export class RegisterComponent implements OnInit {

  spinner:boolean = false;
  hide = true;
  data = {companyName:null, 
          companyNit:null,
          companyDescription:null,
          userName:null, 
          userIdentification:null, 
          userEmail:null, 
          userCity:null,
          userAddress:null,
          userPhone:null,
          userPassword:null,
          userAfterPassword:null
        };
  constructor(
      public companysService: CompanysService,
      public rolsService: RolsService,
      public usersService: UsersService,
    ) { }

  ngOnInit(){
  }

  validCompany(){
    
    this.spinner = true;

    if(this.data.companyName && this.data.companyNit && 
       this.data.companyDescription && this.data.userName && 
       this.data.userIdentification && this.data.userName &&
       this.data.userIdentification && this.data.userEmail &&
       this.data.userCity && this.data.userAddress &&  this.data.userPhone &&
       this.data.userPassword && this.data.userAfterPassword){

          if(this.data.userPassword === this.data.userAfterPassword){
              this.saveCompany()
          }else{
            this.spinner = false;
            $.notify({
              icon: "add_alert", message: "el password no coincide!"
              },{
                type: 'warning', timer: 4000,
                placement: {
                from: 'top',
                align: 'right'
              }
            });
          }

       }else{
        this.spinner = false;
        $.notify({
          icon: "add_alert", message: "por favor ingrese todos los campos!"
          },{
            type: 'warning', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
       }
  }

  saveCompany(){

    const company = {
      'name' : this.data.companyName,
      'nit'  : this.data.companyNit,
      'description'  : this.data.companyDescription
    }

    this.companysService.createCompany(company)
    .then(response => this.saveRol(response))
    .catch(error => {
      this.spinner = false;
      this.errorUser();
      console.log(error);
    });

  }

  saveRol(response){
 
    const rol = {
      'name' : 'Administrador',
      'description'  : 'n/a',
      'company' : response.company._id
    }

    this.rolsService.createRol(rol)
    .then(response => this.saveUser(response))
    .catch(error => {
      this.spinner = false;
      this.errorUser();
      console.log(error);
    });
    
  }


  saveUser(response){

      //estado 0 inactivo
      //estado 1 activo
      //estado 2 vencimiento plan

      var m = moment(new Date(), 'ddd MMM D YYYY HH:mm');

      const user = {
        'name' : this.data.userName,
        'photo' : '',
        'identification' : this.data.userIdentification,
        'email' : this.data.userEmail,
        'city' : this.data.userCity,
        'address' : this.data.userAddress,
        'phone' : this.data.userPhone,
        'rol' : response.rol._id,
        'company' : response.rol.company,
        'state' : 0,
        'password' : this.data.userPassword,
        'imei' : '',
        'paw' : '',
        'session' : 0,
        'super' : 2,
        'codeVerify': Math.round(Math.random()*1800),
        'dateCreation':  m.format('x')
      }

      this.usersService.addUser(user)
      .then(response => this.successUser(response))
      .catch(error => {
        this.spinner = false;
        this.errorUser();
        console.log(error);
      });

  }


  successUser(response){

    $.notify({
      icon: "add_alert", message: "Registro exitoso"
      },{
        type: 'success', timer: 4000,
        placement: {
        from: 'top',
        align: 'right'
      }
    });

    this.spinner = false;
    this.data = {companyName:null, 
      companyNit:null,
      companyDescription:null,
      userName:null, 
      userIdentification:null, 
      userEmail:null, 
      userCity : null,
      userAddress : null,
      userPhone : null,
      userPassword:null,
      userAfterPassword:null
    };

  }

  errorUser(){
    $.notify({
      icon: "add_alert", message: "Error al crear el usuario"
      },{
        type: 'warning', timer: 4000,
        placement: {
        from: 'top',
        align: 'right'
      }
    });

    this.spinner = false;
    this.data = {companyName:null, 
      companyNit:null,
      companyDescription:null,
      userName:null, 
      userIdentification:null, 
      userEmail:null, 
      userCity : null,
      userAddress : null,
      userPhone : null,
      userPassword:null,
      userAfterPassword:null
    };

  }

}
