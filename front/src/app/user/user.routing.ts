import { Routes } from '@angular/router';
import { AuthGuard } from '../providers/guard/auth.guard'

import { LoginComponent } from '../user/login/login.component';
import { RegisterComponent } from '../user/register/register.component';
import { ProfileComponent } from '../user/profile/profile.component';
import { CodeverifyComponent } from './codeverify/codeverify.component';


export const UserRoutes: Routes = [
    { path: 'login',               component: LoginComponent },
    { path: 'register',            component: RegisterComponent, canActivate:[AuthGuard] },
    { path: 'profile',             component: ProfileComponent, canActivate:[AuthGuard] },
    { path: 'codeverify',          component: CodeverifyComponent, canActivate:[AuthGuard] },
];
