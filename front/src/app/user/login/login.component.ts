import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../providers/auth/auth.service';
import {FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../../models/user'
import { CodeverifyComponent } from '../codeverify/codeverify.component';
import {MatDialog} from '@angular/material';
declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [ AuthService ]
})
export class LoginComponent implements OnInit {

  data = { email:null, password:null };
  spinner:boolean = false;

  constructor(
      private Auth:AuthService, 
      private router:Router,
      public dialog: MatDialog
      ) { }

  private user: User;

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  ngOnInit(){
    
  }

  onLogin(){

    this.spinner = true;

    this.Auth.loginuser(this.data.email, this.data.password)
    .then( response => this.evaluateResponse(response) )
    .catch(error => {
      var json = JSON.parse(error._body);
      let status = error.status;
      this.spinner = false;
      this.evaluateStatus(status, json.message);
    })

  }

  

  evaluateStatus(status, messageRest){

    if(status==400){
      $.notify({
        icon: "add_alert", message: "Datos invalidos!"
        },{
          type: 'error', timer: 4000,
          placement: {
          from: 'top',
          align: 'right'
        }
      });
    }

    
  }

  evaluateResponse(response){
    
    this.spinner = false;
    

    if(response.state === 0){
      this.validCodeVerify(response)
    }else{
      
      this.Auth.setUser(response)
      this.Auth.setToken(response._id);
      this.router.navigate(['dashboard'])

      $.notify({
        icon: "add_alert", message: "Bienvenido a start connect!"
        },{
          type: 'success', timer: 4000,
          placement: {
          from: 'top',
          align: 'right'
        }
      });
    }

    

  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Campo requerido' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  validCodeVerify(response){

    const dialogRef = this.dialog.open(CodeverifyComponent, {
      width: '650px',
      disableClose: true,
      data: {user: response}
    });

  }

  submit(){
    //emppty stuff
  }


  // public loginUser(): void {
    
  //   const access:boolean = true;
  //   if(access){
  //     this.Auth.setLoggedIn()
  //     // this.router.navigate(['dashboard'])
  //   }else{
  //     window.alert('Invalid')
  //   }
    
  // }

}
