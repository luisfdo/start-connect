import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
declare var $: any;
import { CardsService } from '../../providers/cards/cards.service';
import { PaymentsService } from '../../providers/payment/payments.service';
import { UsersService } from '../../providers/users/users.service';
import { AuthService } from '../../providers/auth/auth.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-codeverify',
  templateUrl: './codeverify.component.html',
  styleUrls: ['./codeverify.component.scss'],
  providers: [ CardsService, PaymentsService, UsersService, AuthService ]
})
export class CodeverifyComponent implements OnInit {

  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  spinner:boolean = false;

  card = {number:null, exp_year:null, exp_month:null, cvc:null, user:null}
  codeVerify:string = null;

  constructor(
    public cardService: CardsService,
    public paymentService: PaymentsService,
    public userService: UsersService,
    private Auth:AuthService,
    private _formBuilder: FormBuilder,
    private router:Router,
    public dialogRef: MatDialogRef<CodeverifyComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {


    
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });

  }


  sendActiveUser(){

    if(!this.codeVerify){
      $.notify({
        icon: "add_alert", message: "Debes ingresar el codigo que te llego a tu correo"
        },{
          type: 'warning', timer: 4000,
          placement: {
          from: 'top',
          align: 'right'
        }
      });
    }else{
      
      if(this.card.number && this.card.exp_year && this.card.exp_month && this.card.cvc){
        this.validCodeVerify()
      }else{
        $.notify({
          icon: "add_alert", message: "Debes ingresar los datos de tu tarjeta de credito"
          },{
            type: 'warning', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
      }

    }
  }

  validCodeVerify(){

    if(this.data.user.codeVerify == this.codeVerify){
      this.resexistCard();
    }else{
      $.notify({
        icon: "add_alert", message: "El codigo que estas ingresando es nulo, por favor revisa el codigo que enviamos a tu correo"
        },{
          type: 'warning', timer: 4000,
          placement: {
          from: 'top',
          align: 'right'
        }
      });
    }

  }


  resexistCard(){

      this.spinner = true;
      this.card.user = this.data.user._id
      this.cardService.addCard(this.card)
      .then(response => this.createCard(response))
      .catch(error => {
        console.log(error);
      });

  }


  createCard(data){

    console.log('....---....');
    console.log(data);
    console.log('....---....');

    if(data.subscription){
     
      const userActive = {
        '_id' : this.data.user._id,
        'state' : 1
      }

      this.userService.editUser(userActive)
        .then(response => this.accessSuccess(response))
        .catch(error => {
          this.card = {number:null, exp_year:null, exp_month:null, cvc:null, user:null};
          console.log(error);
          this.spinner = false;
      });
    }else{

      console.log(data);

      if(data.message.message.statusCode == 404){
        this.card = {number:null, exp_year:null, exp_month:null, cvc:null, user:null};
          $.notify({
            icon: "add_alert", message: "Problemas al validar el pago con esta tarjeta"
            },{
              type: 'warning', timer: 4000,
              placement: {
              from: 'top',
              align: 'right'
            }
          });

          this.spinner = false;
      }
    }
    
  }


  accessSuccess(response){

    this.Auth.setUser(response.user)
    this.Auth.setToken(response.user._id);
    this.router.navigate(['dashboard'])

    this.card = {number:null, exp_year:null, exp_month:null, cvc:null, user:null};
    this.spinner = false;
    this.dialogRef.close();
    $.notify({
      icon: "add_alert", message: "Excelente ahora podras crear contratos con tus clientes!!"
      },{
        type: 'success', timer: 7000,
        placement: {
        from: 'top',
        align: 'right'
      }
    });

  }


}
