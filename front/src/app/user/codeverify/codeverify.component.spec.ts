import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CodeverifyComponent } from './codeverify.component';

describe('CodeverifyComponent', () => {
  let component: CodeverifyComponent;
  let fixture: ComponentFixture<CodeverifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodeverifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeverifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
