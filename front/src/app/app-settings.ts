import { environment } from '../environments/environment';

export class AppSettings {

    public static get API_ENDPOINT(): string {
        return environment.backendUrl;
    }

    public static get API_TOKEN(): string {
        return environment.apiToken;
    }

    public static get SOCKET_URL(): string {
      return environment.socks;
    }
}
