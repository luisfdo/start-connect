import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';

import { CompanysComponent } from '../../companys/companys.component';
import { AddDialogComponent } from '../../companys/dialogs/add-dialog/add-dialog.component';
import { DeleteDialogComponent } from '../../companys/dialogs/delete-dialog/delete-dialog.component';
import { EditDialogComponent } from '../../companys/dialogs/edit-dialog/edit-dialog.component';

import { CategoriesComponent } from '../../categories/categories.component';
import { AddDialogCategoryComponent } from '../../categories/dialogs/add-category/add-dialog.component';
import { DeleteDialogCategoryComponent } from '../../categories/dialogs/delete-category/delete-dialog.component';
import { EditDialogCategoryComponent } from '../../categories/dialogs/edit-category/edit-dialog.component';


import { BanksComponent } from '../../banks/banks.component';
import { AddDialogBankComponent } from '../../banks/dialogs/add-bank/add-dialog.component';
import { DeleteDialogBankComponent } from '../../banks/dialogs/delete-bank/delete-dialog.component';
import { EditDialogBankComponent } from '../../banks/dialogs/edit-bank/edit-dialog.component';

import { RolsComponent } from '../../rols/rols.component';
import { AddDialogRolComponent } from '../../rols/dialogs/add-rol/add-dialog.component';
import { DeleteDialogRolComponent } from '../../rols/dialogs/delete-rol/delete-dialog.component';
import { EditDialogRolComponent } from '../../rols/dialogs/edit-rol/edit-dialog.component';

import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';

import { BellsComponent } from '../../bells/bells.component';
import { AddDialogBellComponent } from '../../bells/bells-admin/dialogs/add-bell/add-dialog.component';
import { DeleteDialogBellComponent } from '../../bells/bells-admin/dialogs/delete-bell/delete-dialog.component';
import { EditDialogBellComponent } from '../../bells/bells-admin/dialogs/edit-bell/edit-dialog.component';
import { SmsDialogBellComponent } from '../../bells/bells-admin/dialogs/sms-bell/sms-dialog.component';
import { VoiceDialogBellComponent } from '../../bells/bells-admin/dialogs/voice-bell/voice-dialog.component';


import { CustomersComponent } from '../../customers/customers.component';
import { AddDialogCustomerComponent } from '../../customers/dialogs/add-customer/add-dialog.component';
import { DeleteDialogCustomerComponent } from '../../customers/dialogs/delete-customer/delete-dialog.component';
import { EditDialogCustomerComponent } from '../../customers/dialogs/edit-customer/edit-dialog.component';

import { BellsAdminComponent } from '../../bells/bells-admin/bells-admin.component';

import { QuoteComponent } from '../../customers/imports/quote/quote.component';
import { BillingComponent } from '../../customers/imports/billing/billing.component';
import { ListComponent } from '../../customers/data/list/list.component';

import { GridModule } from '@progress/kendo-angular-grid';

import { CompanysService } from '../../providers/companys/companys.service';
import { AuthService } from '../../providers/auth/auth.service';
import { AuthGuard } from '../../providers/guard/auth.guard';
import { AmazingTimePickerModule } from 'amazing-time-picker';

import {BillAffiliatesModule} from '../../bill-affiliates/bill-affiliates.module';
import {UserModule} from '../../user/user.module';
import {MikrotikModule} from '../../mikrotik/mikrotik.module';


import {PlanModule} from '../../plans/plan.module';
import {ContractModule} from '../../contracts/contract.module';


import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSelectModule,
  MatToolbarModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatDialogModule,
  MatCardModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatGridListModule,
  MatTabsModule,
  MatCheckboxModule
} from '@angular/material';



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    GridModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    MatCardModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatGridListModule,
    MatTabsModule,
    MatCheckboxModule,
    AmazingTimePickerModule,
    BillAffiliatesModule,
    UserModule,
    MikrotikModule,
    PlanModule,
    ContractModule
  ],
  providers: [
    CompanysService,
    AuthService,
    AuthGuard
  ],
  declarations: [
    DashboardComponent,
    CompanysComponent,
    AddDialogComponent,
    DeleteDialogComponent,
    EditDialogComponent,
    RolsComponent,
    AddDialogRolComponent,
    DeleteDialogRolComponent,
    EditDialogRolComponent,
    CategoriesComponent,
    AddDialogCategoryComponent,
    DeleteDialogCategoryComponent,
    EditDialogCategoryComponent,
    BanksComponent,
    AddDialogBankComponent,
    DeleteDialogBankComponent,
    EditDialogBankComponent,
    MapsComponent,
    NotificationsComponent,
    BellsComponent,
    BellsAdminComponent,
    QuoteComponent,
    BillingComponent,
    ListComponent,
    AddDialogBellComponent,
    DeleteDialogBellComponent,
    EditDialogBellComponent,
    SmsDialogBellComponent,
    VoiceDialogBellComponent,
    CustomersComponent,
    AddDialogCustomerComponent,
    DeleteDialogCustomerComponent,
    EditDialogCustomerComponent
  ]
})

export class AdminLayoutModule {}
