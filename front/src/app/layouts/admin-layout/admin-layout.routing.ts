import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';

import { CompanysComponent } from '../../companys/companys.component';
import { AddDialogComponent } from '../../companys/dialogs/add-dialog/add-dialog.component';
import { DeleteDialogComponent } from '../../companys/dialogs/delete-dialog/delete-dialog.component';
import { EditDialogComponent } from '../../companys/dialogs/edit-dialog/edit-dialog.component';


import { RolsComponent } from '../../rols/rols.component';
import { AddDialogRolComponent } from '../../rols/dialogs/add-rol/add-dialog.component';
import { DeleteDialogRolComponent } from '../../rols/dialogs/delete-rol/delete-dialog.component';
import { EditDialogRolComponent } from '../../rols/dialogs/edit-rol/edit-dialog.component';


import { CategoriesComponent } from '../../categories/categories.component';
import { AddDialogCategoryComponent } from '../../categories/dialogs/add-category/add-dialog.component';
import { DeleteDialogCategoryComponent } from '../../categories/dialogs/delete-category/delete-dialog.component';
import { EditDialogCategoryComponent } from '../../categories/dialogs/edit-category/edit-dialog.component';

import { BanksComponent } from '../../banks/banks.component';
import { AddDialogBankComponent } from '../../banks/dialogs/add-bank/add-dialog.component';
import { DeleteDialogBankComponent } from '../../banks/dialogs/delete-bank/delete-dialog.component';
import { EditDialogBankComponent } from '../../banks/dialogs/edit-bank/edit-dialog.component';



import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';

import { BellsComponent } from '../../bells/bells.component';
import { AddDialogBellComponent } from '../../bells/bells-admin/dialogs/add-bell/add-dialog.component';
import { DeleteDialogBellComponent } from '../../bells/bells-admin/dialogs/delete-bell/delete-dialog.component';
import { EditDialogBellComponent } from '../../bells/bells-admin/dialogs/edit-bell/edit-dialog.component';
import { SmsDialogBellComponent } from '../../bells/bells-admin/dialogs/sms-bell/sms-dialog.component';
import { VoiceDialogBellComponent } from '../../bells/bells-admin/dialogs/voice-bell/voice-dialog.component';

import { CustomersComponent } from '../../customers/customers.component';
import { AddDialogCustomerComponent } from '../../customers/dialogs/add-customer/add-dialog.component';
import { DeleteDialogCustomerComponent } from '../../customers/dialogs/delete-customer/delete-dialog.component';
import { EditDialogCustomerComponent } from '../../customers/dialogs/edit-customer/edit-dialog.component';

import { QuoteComponent } from '../../customers/imports/quote/quote.component';
import { BillingComponent } from '../../customers/imports/billing/billing.component';
import { ListComponent } from '../../customers/data/list/list.component';


import { AuthGuard } from '../../providers/guard/auth.guard'

export const AdminLayoutRoutes: Routes = [
    
    { path: 'dashboard',           component: DashboardComponent, canActivate:[AuthGuard] },
    
    { path: 'companys',            component: CompanysComponent, canActivate:[AuthGuard] },
    { path: 'add-company',         component: AddDialogComponent, canActivate:[AuthGuard] },
    { path: 'delete-company',      component: DeleteDialogComponent, canActivate:[AuthGuard] },
    { path: 'edit-company',        component: EditDialogComponent, canActivate:[AuthGuard] },

    { path: 'rols',                component: RolsComponent, canActivate:[AuthGuard] },
    { path: 'add-rol',             component: AddDialogRolComponent, canActivate:[AuthGuard] },
    { path: 'delete-rol',          component: DeleteDialogRolComponent, canActivate:[AuthGuard] },
    { path: 'edit-rol',            component: EditDialogRolComponent, canActivate:[AuthGuard] },

    { path: 'categories',          component: CategoriesComponent, canActivate:[AuthGuard] },
    { path: 'add-category',        component: AddDialogCategoryComponent, canActivate:[AuthGuard] },
    { path: 'delete-category',     component: DeleteDialogCategoryComponent, canActivate:[AuthGuard] },
    { path: 'edit-category',       component: EditDialogCategoryComponent, canActivate:[AuthGuard] },

    { path: 'banks',               component: BanksComponent, canActivate:[AuthGuard] },
    { path: 'add-bank',            component: AddDialogBankComponent, canActivate:[AuthGuard] },
    { path: 'delete-bank',         component: DeleteDialogBankComponent, canActivate:[AuthGuard] },
    { path: 'edit-bank',           component: EditDialogBankComponent, canActivate:[AuthGuard] },

    { path: 'maps',                component: MapsComponent, canActivate:[AuthGuard] },
    { path: 'notifications',       component: NotificationsComponent, canActivate:[AuthGuard] },

    { path: 'bells',               component: BellsComponent, canActivate:[AuthGuard] },
    { path: 'add-bell',            component: AddDialogBellComponent, canActivate:[AuthGuard] },
    { path: 'delete-bell',         component: DeleteDialogBellComponent, canActivate:[AuthGuard] },
    { path: 'edit-bell',           component: EditDialogBellComponent, canActivate:[AuthGuard] },
    { path: 'sms-bell',            component: SmsDialogBellComponent, canActivate:[AuthGuard] },
    { path: 'voice-bell',          component: VoiceDialogBellComponent, canActivate:[AuthGuard] },


    { path: 'customers',           component: CustomersComponent, canActivate:[AuthGuard] },
    { path: 'add-customer',        component: AddDialogCustomerComponent, canActivate:[AuthGuard] },
    { path: 'delete-customer',     component: DeleteDialogCustomerComponent, canActivate:[AuthGuard] },
    { path: 'edit-customer',       component: EditDialogCustomerComponent, canActivate:[AuthGuard] },

    { path: 'customer-quote',      component: QuoteComponent, canActivate:[AuthGuard] },
    { path: 'customer-billing',    component: BillingComponent, canActivate:[AuthGuard] },
    { path: 'customer-list',       component: ListComponent, canActivate:[AuthGuard] },

    // { path: 'bill-affiliate',       component: BillAffiliatesComponent, canActivate:[AuthGuard] },



];
