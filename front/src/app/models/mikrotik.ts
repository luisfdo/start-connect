export class Mikrotik {
    _id: string;
    name: string;
    host: string;
    user: string;
    password: string;
    port: string;
    status : string;
    company: string;
  }