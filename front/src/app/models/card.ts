export class Card {
     _id: string;
     number: string;
     exp_year: string;
     exp_month: string;
     cvc: string;
     token: string;
     dateCreation: string;
     user: string;
}
