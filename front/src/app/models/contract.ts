export class Contract {
    _id: string;
    customer: string;
    plan: string;
    state: string;
    mikrotik: string;
    interface: string;
    addressIp: string;
    networkMask: string;
    addressMac: string;
    observations: string;
    dateStart: string;
    price: string;
    company: string;
  }