export class Customers {
    _id: string;
    name: string;
    identification: string;
    country: string;
    state: string;
    city: string;
    address : string;
    phone: string;
    email: string;
    company: string;
  }