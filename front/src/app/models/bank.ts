export class Bank {
     _id: string;
     typeAccount: string;
     nameAccount: string;
     numberAccount: number;
     beginningBalance: number;
     date: string;
     description: string;
     company: string;
}
