export class Payment {
     _id: string;
     nameProduct: string;
     paidValue: string;
     transaction_id: string;
     user: string;
}
