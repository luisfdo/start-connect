export class Plan {
    _id: string;
    name: string;
    maxRise: string;
    uploadUnit: string;
    maxDesc: string;
    descentUnit: string;
    frequency: string;
    company: string;
  }