export class Voice {
     _id: string;
     callId: string;
     from: string;
     to: string;
     body: number;
     cost: string;
     createdAt: string;
     bells:string;
     company: string;
}
