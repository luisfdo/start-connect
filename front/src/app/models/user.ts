export class User {
  _id: string;
  name: string;
  photo:string;
  identification: number;
  email: string;
  rol: string;
  company: string;
  state:number
  password:string;
  imei:string;
  paw:string;
  session:number;
  super:number;
  codeVerify:string;
  dateCreation:string;
  city: string;
  address: string;
  phone: string;

}
