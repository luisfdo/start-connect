export class Sms {
     _id: string;
     messageId: string;
     from: string;
     to: string;
     body: number;
     cost: string;
     createdAt: string;
     bells:string;
     company: string;
}
