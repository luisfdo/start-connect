
import { RolsService } from '../providers/rols/rols.service';

import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {Rol} from '../models/rol';
import {DataSource} from '@angular/cdk/collections';
import {AddDialogRolComponent} from './dialogs/add-rol/add-dialog.component';
import {EditDialogRolComponent} from './dialogs/edit-rol/edit-dialog.component';
import {DeleteDialogRolComponent} from './dialogs/delete-rol/delete-dialog.component';
import {BehaviorSubject, fromEvent, merge, Observable} from 'rxjs';
import {map} from 'rxjs/operators';


@Component({
  selector: 'app-rols',
  templateUrl: './rols.component.html',
  styleUrls: ['./rols.component.scss'],
  providers: [ RolsService ]
})

export class RolsComponent implements OnInit {

  displayedColumns = ['name', 'description', 'actions'];
  exampleDatabase: RolsService | null;
  dataSource: ExampleDataSource | null;
  index: number;
  _id: string;


  constructor(
    private rolsService: RolsService,
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public dataService: RolsService
  ){}

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;

  ngOnInit(){
    this.loadData();
  }

  public loadData() {
    this.exampleDatabase = new RolsService(this.httpClient);
    this.dataSource = new ExampleDataSource(this.exampleDatabase, this.paginator, this.sort);
    fromEvent(this.filter.nativeElement, 'keyup')
      // .debounceTime(150)
      // .distinctUntilChanged()
      .subscribe(() => {
        console.log('okleyyyy');
        console.log(this.dataSource);
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }

  addNew(rol: Rol) {

    const dialogRef = this.dialog.open(AddDialogRolComponent, {
      data: {rol: rol }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.loadData();
      }
    });

  }

  startEdit(i: number, _id: string, name: string, description: string, company: number) {
    this._id = _id;
    this.index = i;
    // console.log(this.index);
    const dialogRef = this.dialog.open(EditDialogRolComponent, {
      data: {_id: _id, name: name, description: description, company: company}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.loadData();
        // const foundIndex = this.exampleDatabase.dataChange.value.findIndex(x => x._id === this._id);
        // this.exampleDatabase.dataChange.value[foundIndex] = this.dataService.getDialogData();
        // this.refreshTable();
      }
    });
  }

  deleteItem(i: number, _id: string, name: string, description: string, company: string) {
    this.index = i;
    this._id = _id;
    const dialogRef = this.dialog.open(DeleteDialogRolComponent, {
      data: {_id: _id, name: name, description: description, company: company}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.loadData();
      }
    });
  }


  private refreshTable() {
    // Refreshing table using paginator
    // Thanks yeager-j for tips
    // https://github.com/marinantonio/angular-mat-table-crud/issues/12
    this.paginator._changePageSize(this.paginator.pageSize);
  }


}


export class ExampleDataSource extends DataSource<Rol> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Rol[] = [];
  renderedData: Rol[] = [];

  constructor(public _exampleDatabase: RolsService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Rol[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._exampleDatabase.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    this._exampleDatabase.getAllRols();


    return merge(...displayDataChanges).pipe(map( () => {
        // Filter data
        this.filteredData = this._exampleDatabase.data.slice().filter((rol: Rol) => {
          const searchStr = (rol._id + rol.name + rol.description + rol.company).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        });

        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());

        // Grab the page's slice of the filtered sorted data.
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
        return this.renderedData;
      }
    ));
  }

  disconnect() {}


  /** Returns a sorted copy of the database data. */
  sortData(data: Rol[]): Rol[] {

    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case '_id': [propertyA, propertyB] = [a._id, b._id]; break;
        case 'name': [propertyA, propertyB] = [a.name, b.name]; break;
        case 'description': [propertyA, propertyB] = [a.description, b.description]; break;
        case 'company': [propertyA, propertyB] = [a.company, b.company]; break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}
