import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDialogRolComponent } from './add-dialog.component';

describe('AddDialogRolComponent', () => {
  let component: AddDialogRolComponent;
  let fixture: ComponentFixture<AddDialogRolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDialogRolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDialogRolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
