import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import { RolsService } from '../../../providers/rols/rols.service';

import {FormControl, Validators} from '@angular/forms';
import {Rol} from '../../../models/rol';

@Component({
  selector: 'app-add-dialog-rol',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.scss'],
  providers: [ RolsService ]
})
export class AddDialogRolComponent {

  constructor(
    public dialogRef: MatDialogRef<AddDialogRolComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Rol,
    public dataService: RolsService
  ) { }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('name') ? 'Not a valid name' :
        '';
  }

  // submit() {
  // // emppty stuff
  // }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.dataService.addRol(this.data);
  }

}
