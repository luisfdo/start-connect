import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
// import { CompanysService } from '../../../providers/companys/companys.service';

import { RolsService } from '../../../providers/rols/rols.service';

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.scss'],
  providers: [ RolsService ]
})
export class EditDialogRolComponent {

  constructor(public dialogRef: MatDialogRef<EditDialogRolComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public dataService: RolsService) { }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
    this.formControl.hasError('email') ? 'Not a valid email' :
    '';
  }

  submit() {
  // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    this.dataService.updateRol(this.data);
  }

}
