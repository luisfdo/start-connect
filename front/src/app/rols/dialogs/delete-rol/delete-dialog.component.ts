import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import { RolsService } from '../../../providers/rols/rols.service';

@Component({
  selector: 'app-delete-dialog-rol',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss'],
  providers: [ RolsService ]
})
export class DeleteDialogRolComponent  {

  constructor(
    public dialogRef: MatDialogRef<DeleteDialogRolComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public dataService: RolsService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.dataService.deleteRol(this.data._id);
  }

}
