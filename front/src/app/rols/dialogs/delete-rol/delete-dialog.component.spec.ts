import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteDialogRolComponent } from './delete-dialog.component';

describe('DeleteDialogRolComponent', () => {
  let component: DeleteDialogRolComponent;
  let fixture: ComponentFixture<DeleteDialogRolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteDialogRolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteDialogRolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
