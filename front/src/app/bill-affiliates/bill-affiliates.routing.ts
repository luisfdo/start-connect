import { Routes } from '@angular/router';
import { AuthGuard } from '../providers/guard/auth.guard'

import { BillAffiliatesComponent } from '../bill-affiliates/bill-affiliates.component';
import { PlansComponent } from '../bill-affiliates/plans/plans.component';
import { PopupPayComponent } from './popup-pay/popup-pay.component';



export const BillAffiliatesRoutes: Routes = [
    
    { path: 'bill-affiliate',  component: BillAffiliatesComponent, canActivate:[AuthGuard] },
    { path: 'plans',           component: PlansComponent, canActivate:[AuthGuard] },
    { path: 'popup-pay',           component: PopupPayComponent, canActivate:[AuthGuard] }

];
