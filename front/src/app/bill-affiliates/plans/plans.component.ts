import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { PopupPayComponent } from '../popup-pay/popup-pay.component';

@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.scss']
})
export class PlansComponent implements OnInit {
  user:any;
  planSelected = { name: null, cost:null };
  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem("currentUser"));
  }

  openDialog(plan, cost): void {

    this.planSelected = { name:plan, cost:cost }

    const dialogRef = this.dialog.open(PopupPayComponent, {
      width: '650px',
      data: {user: this.user, plan: this.planSelected}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });

  }

}
