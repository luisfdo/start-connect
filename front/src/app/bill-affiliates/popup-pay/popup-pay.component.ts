import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import { CardsService } from '../../providers/cards/cards.service';
import { PaymentsService } from '../../providers/payment/payments.service';
declare var $: any;

@Component({
  selector: 'app-popup-pay',
  templateUrl: './popup-pay.component.html',
  styleUrls: ['./popup-pay.component.scss'],
  providers: [ CardsService, PaymentsService ]
})
export class PopupPayComponent implements OnInit {

  spinner:boolean = false;
  card = {number:null, exp_year:null, exp_month:null, cvc:null, user:null}
  planSelected = { name: null, cost:null };
  card_token:string

  constructor(
    public cardService: CardsService,
    public paymentService: PaymentsService,
    @Inject(MAT_DIALOG_DATA) public data: any
    ) {}

  ngOnInit() {
    this.planSelected = this.data.plan
  }

  addCard(){

      this.spinner = true
      this.card.user = this.data.user._id

      this.cardService.addCard(this.card)
      .then(response => this.successCard(response))
      .catch(error => {
        this.errorCard(error)
      });

  }

  errorCard(error){
      
      this.spinner = false

      $.notify({
        icon: "add_alert", message: "Los datos son erroneos o son requeridos, por favor compruebe"
        },{
          type: 'warning', timer: 4000,
          placement: {
          from: 'top',
          align: 'right'
        }
      });
  }

  successCard(response){

    var customer_info = {
      token_card: response.card.token,
      name: this.data.user.name,
      email: this.data.user.email,
      default: true,
      city: this.data.user.city,
      address: this.data.user.address,
      phone: this.data.user.phone,
      cell_phone: this.data.user.phone,
      _id:this.data.user._id
    }

    this.card_token = response.card.token

    this.cardService.addCustomerPay(customer_info)
    .then(response => this.successCustomer(response))
    .catch(error => {
      this.errorCustomer(error)
    });

    
  }

  errorCustomer(error){
      
    this.spinner = false

    $.notify({
      icon: "add_alert", message: "Ocurrio un problema al crear el cliente"
      },{
        type: 'warning', timer: 4000,
        placement: {
        from: 'top',
        align: 'right'
      }
    });
  }


  successCustomer(response){

    // console.log('--------testing---------');
    // console.log(this.data.user)
    // console.log('--------testing---------');
    // console.log(response);

    var payment_info = {
      token_card: this.card_token,
      customer_id: response.user.id_customer,
      doc_type: "CC",
      doc_number: response.user.identification,
      name: response.user.name,
      last_name: response.user.name,
      email: response.user.email,
      bill: "n/a",
      description: this.planSelected.name,
      value: "116000",
      tax: "16000",
      tax_base: "100000",
      currency: "COP",
      dues: "12",
      id_user: this.data.user._id
  }

    this.paymentService.addPayment(payment_info)
    .then(response => this.successPayment(response))
    .catch(error => {
      this.errorPayment(error)
    });

      // console.log('----ESTO ES RETURN-----');
      // console.log(data);
      // console.log('----ESTO ES RETURN-----');
  }


  errorPayment(error){
      
    this.spinner = false

    $.notify({
      icon: "add_alert", message: "Ocurrio un problema al generar el pago"
      },{
        type: 'warning', timer: 4000,
        placement: {
        from: 'top',
        align: 'right'
      }
    });
  }


  successPayment(response){
    console.log('------------PAYMENT------------');
    console.log(response);
    console.log('------------PAYMENT------------');

  }


}
