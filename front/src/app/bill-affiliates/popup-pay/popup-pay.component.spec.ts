import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupPayComponent } from './popup-pay.component';

describe('PopupPayComponent', () => {
  let component: PopupPayComponent;
  let fixture: ComponentFixture<PopupPayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupPayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupPayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
