import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillAffiliatesComponent } from './bill-affiliates.component';

describe('BillAffiliatesComponent', () => {
  let component: BillAffiliatesComponent;
  let fixture: ComponentFixture<BillAffiliatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillAffiliatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillAffiliatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
