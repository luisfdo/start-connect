import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {MikrotikService} from '../../providers/mikrotik/mikrotik.service';

@Component({
  selector: 'app-delete-dialog-mikrotik',
  templateUrl: './delete-mikrotik.component.html',
  styleUrls: ['./delete-mikrotik.component.scss'],
  providers: [ MikrotikService ]
})
export class DeleteDialogMikrotikComponent  {

  constructor(
    public dialogRef: MatDialogRef<DeleteDialogMikrotikComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public dataService: MikrotikService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.dataService.deleteMikrotikAll(this.data._id);
  }

}
