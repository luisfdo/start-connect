import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteDialogMikrotikComponent } from './delete-mikrotik.component';

describe('DeleteDialogMikrotikComponent', () => {
  let component: DeleteDialogMikrotikComponent;
  let fixture: ComponentFixture<DeleteDialogMikrotikComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteDialogMikrotikComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteDialogMikrotikComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
