import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {MikrotikService} from '../../providers/mikrotik/mikrotik.service';
import {FormControl, Validators} from '@angular/forms';
import {Mikrotik} from '../../models/mikrotik';
declare var $: any;

@Component({
  selector: 'app-add-mikrotik',
  templateUrl: './add-mikrotik.component.html',
  styleUrls: ['./add-mikrotik.component.scss'],
  providers: [ MikrotikService ]
})

export class AddMikrotikComponent implements OnInit {

  data = {
    name: null,
    host: null,
    user: null,
    password: null,
    port: null,
    status:true
  }

  spinner:boolean = false

  constructor(
    private mikrService:MikrotikService,
    public dialogRef: MatDialogRef<AddMikrotikComponent>,
  ){}

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage(){
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('name') ? 'Not a valid' :
        '';
  }

  ngOnInit(){
  }


  private onNoClick(): void {
    this.dialogRef.close();
  }


  private saveMikrotik(){

      this.spinner = true;
      this.mikrService.addMikrotik(this.data)
      .then(response => {
        $.notify({
          icon: "add_alert", message: "Servidor guardado con exíto"
          },{
            type: 'successs', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
        this.spinner = false;
        this.dialogRef.close();
      })
      .catch(error => {
        console.log(error);
      });

  }



}
