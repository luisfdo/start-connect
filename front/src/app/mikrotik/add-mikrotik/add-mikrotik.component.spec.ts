import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMikrotikComponent } from './add-mikrotik.component';

describe('AddMikrotikComponent', () => {
  let component: AddMikrotikComponent;
  let fixture: ComponentFixture<AddMikrotikComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMikrotikComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMikrotikComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
