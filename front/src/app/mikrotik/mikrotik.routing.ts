import { Routes } from '@angular/router';
import { AuthGuard } from '../providers/guard/auth.guard'

import { MikrotikComponent } from '../mikrotik/mikrotik.component';
import { AddMikrotikComponent } from '../mikrotik/add-mikrotik/add-mikrotik.component';
import { EditDialogMikrotikComponent } from '../mikrotik/edit-mikrotik/edit-mikrotik.component';
import { DeleteDialogMikrotikComponent } from '../mikrotik/delete-mikrotik/delete-mikrotik.component';


export const MikrotikRoutes: Routes = [
    
    { path: 'mikrotik',         component: MikrotikComponent, canActivate:[AuthGuard] },
    { path: 'add-mikrotik',     component: AddMikrotikComponent, canActivate:[AuthGuard] },
    { path: 'edit-mikrotik',    component: EditDialogMikrotikComponent, canActivate:[AuthGuard] },
    { path: 'delete-mikrotik',  component: DeleteDialogMikrotikComponent, canActivate:[AuthGuard] },

];
