import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MikrotikComponent } from './mikrotik.component';

describe('MikrotikComponent', () => {
  let component: MikrotikComponent;
  let fixture: ComponentFixture<MikrotikComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MikrotikComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MikrotikComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
