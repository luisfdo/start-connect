import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDialogMikrotikComponent } from './edit-mikrotik.component';

describe('EditDialogMikrotikComponent', () => {
  let component: EditDialogMikrotikComponent;
  let fixture: ComponentFixture<EditDialogMikrotikComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDialogMikrotikComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDialogMikrotikComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
