import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MikrotikService} from '../../providers/mikrotik/mikrotik.service';

@Component({
  selector: 'app-edit-dialog-mikrotik',
  templateUrl: './edit-mikrotik.component.html',
  styleUrls: ['./edit-mikrotik.component.scss'],
  providers: [ MikrotikService ]
})
export class EditDialogMikrotikComponent {

  constructor(public dialogRef: MatDialogRef<EditDialogMikrotikComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public dataService: MikrotikService) {
      console.log(this.data);
    }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
    this.formControl.hasError('name') ? 'Not a valid name' :
    '';
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  startEdit(): void {
    this.dataService.updateMikrotik(this.data);
  }

}
