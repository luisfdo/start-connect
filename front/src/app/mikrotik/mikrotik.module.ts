import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MikrotikRoutes } from './mikrotik.routing';

import { MikrotikComponent } from '../mikrotik/mikrotik.component';
import { AddMikrotikComponent } from './add-mikrotik/add-mikrotik.component';
import { EditDialogMikrotikComponent } from '../mikrotik/edit-mikrotik/edit-mikrotik.component';
import { DeleteDialogMikrotikComponent } from '../mikrotik/delete-mikrotik/delete-mikrotik.component';



 import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSelectModule,
  MatToolbarModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatDialogModule,
  MatCardModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatGridListModule,
  MatTabsModule,
  MatCheckboxModule
} from '@angular/material';



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MikrotikRoutes),
    FormsModule,  
    MatCardModule,
    MatGridListModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatRippleModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatSelectModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTabsModule,
    MatCheckboxModule
  ],
  providers: [
    // CompanysService,
  ],
  declarations: [
    MikrotikComponent,
    AddMikrotikComponent,
    EditDialogMikrotikComponent,
    DeleteDialogMikrotikComponent
  ]
})

export class MikrotikModule {}
