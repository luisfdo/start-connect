
import { BanksService } from '../providers/banks/banks.service';
import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {Bank} from '../models/bank';
import {DataSource} from '@angular/cdk/collections';
import {AddDialogBankComponent} from './dialogs/add-bank/add-dialog.component';
import {EditDialogBankComponent} from './dialogs/edit-bank/edit-dialog.component';
import {DeleteDialogBankComponent} from './dialogs/delete-bank/delete-dialog.component';
import {BehaviorSubject, fromEvent, merge, Observable} from 'rxjs';
import {map} from 'rxjs/operators';


@Component({
  selector: 'app-banks',
  templateUrl: './banks.component.html',
  styleUrls: ['./banks.component.scss'],
  providers: [ BanksService ]
})

export class BanksComponent implements OnInit {

  displayedColumns = ['typeAccount', 'nameAccount', 'actions'];
  exampleDatabase: BanksService | null;
  dataSource: ExampleDataSource | null;
  index: number;
  _id: string;

  constructor(
    private banksService: BanksService,
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public dataService: BanksService
  ){}

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;

  ngOnInit(){
    this.loadData();
  }

  public loadData() {
    this.exampleDatabase = new BanksService(this.httpClient);
    this.dataSource = new ExampleDataSource(this.exampleDatabase, this.paginator, this.sort);
    fromEvent(this.filter.nativeElement, 'keyup')
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }

  addNew(bank: Bank) {

    const dialogRef = this.dialog.open(AddDialogBankComponent, {
      data: {bank: bank}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.loadData();
      }
    });

  }

  startEdit(i: number, 
            _id: string, 
            typeAccount: string, 
            nameAccount: string, 
            numberAccount:number, 
            beginningBalance:number, 
            date:string, 
            description:string, 
            company: string){

              this._id = _id;
              this.index = i;
              // console.log(this.index);
              const dialogRef = this.dialog.open(EditDialogBankComponent, {
                data: {_id: _id, typeAccount: typeAccount, nameAccount:nameAccount, numberAccount:numberAccount, beginningBalance:beginningBalance, date:date, description: description, company: company}
              });

              dialogRef.afterClosed().subscribe(result => {
                if (result === 1) {
                  this.loadData();
                }
              });
  }

  deleteItem(i: number, 
             _id: string, 
             typeAccount: string, 
             nameAccount: string, 
             numberAccount:number, 
             beginningBalance:number, 
             date:string, 
             description:string, 
             company: string) {

              this.index = i;
              this._id = _id;
              const dialogRef = this.dialog.open(DeleteDialogBankComponent, {
                data: {_id: _id, typeAccount: typeAccount, nameAccount:nameAccount, numberAccount:numberAccount, beginningBalance:beginningBalance, date:date, description: description, company: company}
              });

              dialogRef.afterClosed().subscribe(result => {
                if (result === 1) {
                  this.loadData();
                }
              });
  }


  private refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }

}


export class ExampleDataSource extends DataSource<Bank> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Bank[] = [];
  renderedData: Bank[] = [];

  constructor(public _exampleDatabase: BanksService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Bank[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._exampleDatabase.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    this._exampleDatabase.getAllBank();


    return merge(...displayDataChanges).pipe(map( () => {
        // Filter data
        this.filteredData = this._exampleDatabase.data.slice().filter((bank: Bank) => {
          const searchStr = (bank._id + bank.typeAccount +  bank.nameAccount + bank.numberAccount + bank.beginningBalance + bank.date + bank.description + bank.company).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        });

        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());

        // Grab the page's slice of the filtered sorted data.
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
        return this.renderedData;
      }
    ));
  }

  disconnect() {}


  /** Returns a sorted copy of the database data. */
  sortData(data: Bank[]): Bank[] {

    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case '_id': [propertyA, propertyB] = [a._id, b._id]; break;
        case 'typeAccount': [propertyA, propertyB] = [a.typeAccount, b.typeAccount]; break;
        case 'nameAccount': [propertyA, propertyB] = [a.nameAccount, b.nameAccount]; break;
        case 'numberAccount': [propertyA, propertyB] = [a.numberAccount, b.numberAccount]; break;
        case 'beginningBalance': [propertyA, propertyB] = [a.beginningBalance, b.beginningBalance]; break;
        case 'date': [propertyA, propertyB] = [a.date, b.date]; break;
        case 'description': [propertyA, propertyB] = [a.description, b.description]; break;
        case 'company': [propertyA, propertyB] = [a.company, b.company]; break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}
