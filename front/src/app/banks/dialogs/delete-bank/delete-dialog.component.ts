import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {BanksService} from '../../../providers/banks/banks.service';

@Component({
  selector: 'app-delete-dialog-bank',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss'],
  providers: [ BanksService ]
})
export class DeleteDialogBankComponent  {

  constructor(
    public dialogRef: MatDialogRef<DeleteDialogBankComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public dataService: BanksService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.dataService.deleteBank(this.data._id);
  }

}
