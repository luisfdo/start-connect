import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDialogBankComponent } from './add-dialog.component';

describe('AddDialogBankComponent', () => {
  let component: AddDialogBankComponent;
  let fixture: ComponentFixture<AddDialogBankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDialogBankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDialogBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
