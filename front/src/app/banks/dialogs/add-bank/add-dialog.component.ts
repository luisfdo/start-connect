import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {BanksService} from '../../../providers/banks/banks.service';
import {FormControl, Validators} from '@angular/forms';
import {Bank} from '../../../models/bank';


@Component({
  selector: 'app-add-dialog-bank',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.scss'],
  providers: [ BanksService ]
})
export class AddDialogBankComponent {
  
  constructor(
    public dialogRef: MatDialogRef<AddDialogBankComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Bank,
    public dataService: BanksService
  ) { 
  }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('name') ? 'Not a valid name' :
        '';
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.dataService.addBank(this.data);
  }

}
