import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDialogBankComponent } from './edit-dialog.component';

describe('EditDialogBankComponent', () => {
  let component: EditDialogBankComponent;
  let fixture: ComponentFixture<EditDialogBankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDialogBankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDialogBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
