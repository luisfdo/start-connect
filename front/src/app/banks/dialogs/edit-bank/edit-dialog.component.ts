import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {BanksService} from '../../../providers/banks/banks.service';

@Component({
  selector: 'app-edit-dialog-bank',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.scss'],
  providers: [ BanksService ]
})
export class EditDialogBankComponent {

  constructor(public dialogRef: MatDialogRef<EditDialogBankComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public dataService: BanksService) {
    }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
    this.formControl.hasError('name') ? 'Not a valid name' :
    '';
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    this.dataService.updateBank(this.data);
  }

}
