
import { CategoriesService } from '../providers/categories/categories.service';
import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {Category} from '../models/category';
import {DataSource} from '@angular/cdk/collections';
import {AddDialogCategoryComponent} from './dialogs/add-category/add-dialog.component';
import {EditDialogCategoryComponent} from './dialogs/edit-category/edit-dialog.component';
import {DeleteDialogCategoryComponent} from './dialogs/delete-category/delete-dialog.component';
import {BehaviorSubject, fromEvent, merge, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
// import { Category } from 'app/models/category';


@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
  providers: [ CategoriesService ]
})

export class CategoriesComponent implements OnInit {

  displayedColumns = ['title', 'description', 'actions'];
  exampleDatabase: CategoriesService | null;
  dataSource: ExampleDataSource | null;
  index: number;
  _id: string;


  constructor(
    private cateogriesService: CategoriesService,
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public dataService: CategoriesService
  ){}

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;

  ngOnInit(){
    this.loadData();
  }

  public loadData() {
    this.exampleDatabase = new CategoriesService(this.httpClient);
    this.dataSource = new ExampleDataSource(this.exampleDatabase, this.paginator, this.sort);
    fromEvent(this.filter.nativeElement, 'keyup')
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }

  addNew(category: Category) {

    const dialogRef = this.dialog.open(AddDialogCategoryComponent, {
      data: {category: category }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.loadData();
      }
    });

  }

  startEdit(i: number, _id: string, title: string, description: string, company: number) {
    this._id = _id;
    this.index = i;
    // console.log(this.index);
    const dialogRef = this.dialog.open(EditDialogCategoryComponent, {
      data: {_id: _id, title: title, description: description, company: company}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.loadData();
      }
    });
  }

  deleteItem(i: number, _id: string, title: string, description: string, company: string) {
    this.index = i;
    this._id = _id;
    const dialogRef = this.dialog.open(DeleteDialogCategoryComponent, {
      data: {_id: _id, title: title, description: description, company: company}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.loadData();
      }
    });
  }


  private refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }

}


export class ExampleDataSource extends DataSource<Category> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Category[] = [];
  renderedData: Category[] = [];

  constructor(public _exampleDatabase: CategoriesService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Category[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._exampleDatabase.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    this._exampleDatabase.getAllCategories();


    return merge(...displayDataChanges).pipe(map( () => {
        // Filter data
        this.filteredData = this._exampleDatabase.data.slice().filter((category: Category) => {
          const searchStr = (category._id + category.title + category.description + category.company).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        });

        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());

        // Grab the page's slice of the filtered sorted data.
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
        return this.renderedData;
      }
    ));
  }

  disconnect() {}


  /** Returns a sorted copy of the database data. */
  sortData(data: Category[]): Category[] {

    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case '_id': [propertyA, propertyB] = [a._id, b._id]; break;
        case 'title': [propertyA, propertyB] = [a.title, b.title]; break;
        case 'description': [propertyA, propertyB] = [a.description, b.description]; break;
        case 'company': [propertyA, propertyB] = [a.company, b.company]; break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}
