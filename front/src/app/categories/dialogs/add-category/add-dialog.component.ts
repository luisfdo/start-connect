import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import { CategoriesService } from '../../../providers/categories/categories.service';

import {FormControl, Validators} from '@angular/forms';
import {Category} from '../../../models/category';

@Component({
  selector: 'app-add-dialog-category',
  templateUrl: './add-dialog.component.html',
  styleUrls: ['./add-dialog.component.scss'],
  providers: [ CategoriesService ]
})
export class AddDialogCategoryComponent {

  constructor(
    public dialogRef: MatDialogRef<AddDialogCategoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Category,
    public dataService: CategoriesService
  ) { }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('name') ? 'Not a valid name' :
        '';
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.dataService.addCategory(this.data);
  }

}
