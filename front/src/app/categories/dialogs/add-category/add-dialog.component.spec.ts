import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDialogCategoryComponent } from './add-dialog.component';

describe('AddDialogCategoryComponent', () => {
  let component: AddDialogCategoryComponent;
  let fixture: ComponentFixture<AddDialogCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDialogCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDialogCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
