import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDialogRolComponent } from './edit-dialog.component';

describe('EditDialogRolComponent', () => {
  let component: EditDialogRolComponent;
  let fixture: ComponentFixture<EditDialogRolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDialogRolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDialogRolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
