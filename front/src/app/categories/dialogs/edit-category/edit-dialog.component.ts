import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
// import { CompanysService } from '../../../providers/companys/companys.service';

import { CategoriesService } from '../../../providers/categories/categories.service';

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.scss'],
  providers: [ CategoriesService ]
})
export class EditDialogCategoryComponent {

  constructor(public dialogRef: MatDialogRef<EditDialogCategoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public dataService: CategoriesService) { }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
    this.formControl.hasError('email') ? 'Not a valid email' :
    '';
  }

  submit() {
  // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {

    console.log('aca vamos para edit');
    console.log(this.data);
    this.dataService.updateCategory(this.data);
  }

}
