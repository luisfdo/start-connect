import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteDialogCategoryComponent } from './delete-dialog.component';

describe('DeleteDialogCategoryComponent', () => {
  let component: DeleteDialogCategoryComponent;
  let fixture: ComponentFixture<DeleteDialogCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteDialogCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteDialogCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
