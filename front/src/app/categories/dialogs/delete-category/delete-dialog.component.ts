import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import { CategoriesService } from '../../../providers/categories/categories.service';

@Component({
  selector: 'app-delete-dialog-category',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss'],
  providers: [ CategoriesService ]
})
export class DeleteDialogCategoryComponent  {

  constructor(
    public dialogRef: MatDialogRef<DeleteDialogCategoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public dataService: CategoriesService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.dataService.deleteCategory(this.data._id);
  }

}
