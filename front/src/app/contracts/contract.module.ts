import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { ContratRoutes } from './contract.routing';

import { ContractComponent } from '../contracts/contract.component';
import { AddContractComponent } from './add-contract/add-contract.component';
import { EditContractComponent } from './edit-contract/edit-contract.component';
import { DeleteContractComponent } from './delete-contract/delete-contract.component';

 import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSelectModule,
  MatToolbarModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatDialogModule,
  MatCardModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatGridListModule,
  MatTabsModule,
  MatCheckboxModule,
  MatAutocompleteModule
} from '@angular/material';



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ContratRoutes),
    FormsModule,  
    MatCardModule,
    MatGridListModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatRippleModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatSelectModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTabsModule,
    MatCheckboxModule,
    AutocompleteLibModule,
    MatAutocompleteModule
  ],
  providers: [
    // CompanysService,
  ],
  declarations: [
    ContractComponent,
    AddContractComponent,
    EditContractComponent,
    DeleteContractComponent
  ]
})

export class ContractModule {}
