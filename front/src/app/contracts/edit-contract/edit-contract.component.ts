import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {ContractService} from '../../providers/contracts/contracts.service';
import {FormControl, Validators} from '@angular/forms';
// import {Contract} from '../../models/contract';
import {CustomersService} from '../../providers/customers/customers.service';
import {PlanService} from '../../providers/plans/plan.service';
import {MikrotikService} from '../../providers/mikrotik/mikrotik.service';

declare var $: any;

@Component({
  selector: 'app-edit-contract',
  templateUrl: './edit-contract.component.html',
  styleUrls: ['./edit-contract.component.scss'],
  providers: [ ContractService, CustomersService, PlanService, MikrotikService ]
})

export class EditContractComponent implements OnInit {

  dataCustomers = [];
  dataPlans = [];
  dataMkt = [];

  selectedOne:string;
  selectedTwo:string;
  selectedThree:string;


  spinner:boolean = false
  constructor(
    public dialogRef: MatDialogRef<EditContractComponent>,
    private customersService:CustomersService,
    private planService:PlanService,
    private mikrotikService:MikrotikService,
    @Inject(MAT_DIALOG_DATA) public data: any, public dataService: ContractService) {
      this.selectedThree = data.state;
      data.addressIpId = data.addressIp
      console.log(data);

    }

    ngOnInit() {
      this.getCustomers()
      this.getPlans()
      this.getMikrotiks()
    }

    formControl = new FormControl('', [
      Validators.required
    ]);

    getErrorMessage() {
      return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('name') ? 'Not a valid name' :
      '';
    }

    onNoClick(): void {
      this.dialogRef.close();
    }

    getCustomers(){
        
      this.spinner = true;
      this.customersService.getCustomers()
      .then(response => {
        const cust:any = response
        cust.forEach(cust => {

          if(cust._id == this.data.customer[0]._id){
            this.selectedOne = cust._id
          }
          this.dataCustomers.push(cust)
        });
        
        this.spinner = false;
      })
      .catch(error => {
        console.log(error);
      });   

  }


  getPlans(){
    this.spinner = true;
      this.planService.getPlans()
      .then(response => {
        const plns:any = response
        plns.forEach(plns => {
          if(plns._id == this.data.plan[0]._id){
            this.selectedTwo = plns._id
          }
          this.dataPlans.push(plns)
        });
        
        this.spinner = false;
      })
      .catch(error => {
        console.log(error);
      });
  }


  getMikrotiks(){
    this.spinner = true;
      this.mikrotikService.getMikrotiks()
      .then(response => {
        const mkt:any = response
        mkt.forEach(mkt => {
          this.dataMkt.push(mkt)
        });
        this.spinner = false;
      })
      .catch(error => {
        console.log(error);
      });
  }

  startEdit(): void {

    this.spinner = true;
    this.data.customer = this.data.customer[0]._id
    this.data.plan = this.data.plan[0]._id

      this.dataService.updateContract(this.data)
      .then(response => {
        $.notify({
          icon: "add_alert", message: "Contrato Editado con exíto"
          },{
            type: 'successs', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
        this.spinner = false;
        this.dialogRef.close();
      })
      .catch(error => {
        console.log(error);
      });

  }



}
