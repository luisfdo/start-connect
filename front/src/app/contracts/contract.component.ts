import {SelectionModel} from '@angular/cdk/collections';
import { ContractService } from '../providers/contracts/contracts.service';
import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {Contract} from '../models/contract';
import {DataSource} from '@angular/cdk/collections';
import {AddContractComponent} from './add-contract/add-contract.component';
import {EditContractComponent} from './edit-contract/edit-contract.component';
import {DeleteContractComponent} from './delete-contract/delete-contract.component'
import {BehaviorSubject, fromEvent, merge, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
declare var $: any;

export interface PeriodicElement {
    customer: string;
    plan: string;
    state: string;
    mikrotik: string;
    interface: string;
    addressIp: string;
    networkMask: string;
    addressMac: string;
    observations: string;
    dateStart: string;
    price: string;
}

@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.scss'],
  providers: [ ContractService ]
})
export class ContractComponent implements OnInit {

  displayedColumns = [
    'select', 
    'customer', 
    'plan', 
    'state', 
    // 'mikrotik', 
    // 'interface', 
    'addressIp', 
    // 'networkMask', 
    // 'addressMac', 
    'observations', 
    'dateStart', 
    'price', 
    'actions'
  ];

  exampleDatabase: ContractService | null;
  dataSource: any;
  selection :any;
  index: number;
  _id: string;
  cont:number = 0
  succesData = []
  errorData = []
  showView:boolean = false;

  constructor(
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public dataService: ContractService
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;

  ngOnInit() {
    this.loadData();
  }

  public loadData(){

    this.exampleDatabase = new ContractService(this.httpClient);
    this.dataSource = new ExampleDataSource(this.exampleDatabase, this.paginator, this.sort);
    this.selection = new SelectionModel<PeriodicElement>(true, []);
    fromEvent(this.filter.nativeElement, 'keyup')
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });

  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected(){
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.filteredData.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle(){

    if(this.selection.selected.length > 0){
      // this.showDelete = false;
      this.selection.clear()
    }else{
      // this.showDelete = true;
      this.dataSource.filteredData.forEach(row => this.selection.select(row));
    }

  }

  deleteSelectedContract(){
    if(this.selection.selected.length > 0){
      this.showView = true
      this.iteraDelete();
    }else{
      this.notifications('top','right', 'Usted no a seleccionado los registros a eliminar', 'warning');
    }

    
  }

  iteraDelete(){
    if(this.cont  < this.selection.selected.length){
        
      this.deleteContract(this.selection.selected[this.cont]._id)
        .then(() => {
            this.succesData.push(this.selection.selected[this.cont])
            this.cont++;
            this.iteraDelete();
        })
        .catch(error => console.log(error))


    }else{
      this.showView = false;
      this.loadData();
      this.notifications('top','right', 'Los registros seleccionados fueron eliminados', 'success');
      this.cont = 0;
    }
  }


  deleteContract(_id){   
    return this.dataService.deleteContractAll(_id)
      .then(response => {        
        return true;
      })
      .catch(error => console.log(error))
  }


  notifications(from, align, message, type_){

    const type = [type_];

    $.notify({
        icon: "notifications",
        message: message
    },{
        type: type,
        timer: 4000,
        placement: {
            from: from,
            align: align
        },
        template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });

  }

  addContract(contract: Contract) {

    const dialogRef = this.dialog.open(AddContractComponent, {
      data: { contract: contract }
    });

    dialogRef.afterClosed().subscribe(result => {
        console.log('testing 123');
        this.loadData();
    });

  }

  startEdit(
        i: number, 
        _id: string, 
        customer: string,
        plan: string,
        state: string,
        mikrotik: string,
        interfaces: string,
        addressIp: string,
        networkMask: string,
        addressMac: string,
        observations: string,
        dateStart: string,
        price: string
      ){
    
      this._id = _id;
      this.index = i;

      const dialogRef = this.dialog.open(EditContractComponent, {
        data: {_id: _id, customer: customer, plan: plan, state:state, mikrotik: mikrotik, interfaces:interfaces, addressIp:addressIp, networkMask:networkMask, addressMac:addressMac, observations:observations, dateStart:dateStart, price:price}
      });

      dialogRef.afterClosed().subscribe(result => {

        console.log('1234');
        console.log(result);
        console.log('1234');
        //if (result === 1) {
          this.loadData();
        //}
      });
  }

  deleteItem(
    i: number, 
    _id: string, 
    customer: string,
    plan: string,
    state: string,
    mikrotik: string,
    interfaces: string,
    addressIp: string,
    networkMask: string,
    addressMac: string,
    observations: string,
    dateStart: string,
    price: string
    ){

    this.index = i;
    this._id = _id;
    const dialogRef = this.dialog.open(DeleteContractComponent, {
      data: {_id: _id, customer: customer, plan: plan, state:state, mikrotik: mikrotik, interfaces:interfaces, addressIp:addressIp, networkMask:networkMask, addressMac:addressMac, observations:observations, dateStart:dateStart, price:price}
    });

    dialogRef.afterClosed().subscribe(result => {
      //if (result === 1) {
        this.loadData();
      //}
    });
  }


}



export class ExampleDataSource extends DataSource<Contract> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Contract[] = [];
  renderedData: Contract[] = [];

  constructor(public _exampleDatabase: ContractService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Contract[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._exampleDatabase.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    this._exampleDatabase.getAllContracts();


    return merge(...displayDataChanges).pipe(map( () => {
        // Filter data
        this.filteredData = this._exampleDatabase.data.slice().filter((contract: Contract) => {
          const searchStr = (contract._id + contract.customer + contract.plan + contract.state + contract.mikrotik + contract.interface + contract.addressIp + contract.networkMask + contract.addressMac + contract.observations + contract.dateStart + contract.price + contract.company).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        });

        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());

        // Grab the page's slice of the filtered sorted data.
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
        return this.renderedData;
      }
    ));
  }

  disconnect() {}


  /** Returns a sorted copy of the database data. */
  sortData(data: Contract[]): Contract[] {

    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA:  Date | number | string = '';
      let propertyB:  Date | number | string = '';

      switch (this._sort.active) {
        case '_id': [propertyA, propertyB] = [a._id, b._id]; break;
        case 'customer': [propertyA, propertyB] = [a.customer, b.customer]; break;
        case 'plan': [propertyA, propertyB] = [a.plan, b.plan]; break;
        case 'state': [propertyA, propertyB] = [a.state, b.state]; break;
        case 'mikrotik': [propertyA, propertyB] = [a.mikrotik, b.mikrotik]; break;
        case 'interface': [propertyA, propertyB] = [a.interface, b.interface]; break;
        case 'addressIp': [propertyA, propertyB] = [a.addressIp, b.addressIp]; break;
        case 'addressIp': [propertyA, propertyB] = [a.addressIp, b.addressIp]; break;
        case 'networkMask': [propertyA, propertyB] = [a.networkMask, b.networkMask]; break;
        case 'addressMac': [propertyA, propertyB] = [a.addressMac, b.addressMac]; break;
        case 'observations': [propertyA, propertyB] = [a.observations, b.observations]; break;
        case 'dateStart': [propertyA, propertyB] = [a.dateStart, b.dateStart]; break;
        case 'price': [propertyA, propertyB] = [a.price, b.price]; break;
        case 'company' : [propertyA, propertyB] = [a.company, b.company]; break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}
