import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject} from '@angular/core';
import {ContractService} from '../../providers/contracts/contracts.service';
declare var $: any;

@Component({
  selector: 'app-delete-contract',
  templateUrl: './delete-contract.component.html',
  styleUrls: ['./delete-contract.component.scss'],
  providers: [ContractService]
})
export class DeleteContractComponent  {

  spinner:boolean = false

  constructor(
    public dialogRef: MatDialogRef<DeleteContractComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public dataService: ContractService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    //this.dataService.deleteBank(this.data._id);
    this.spinner = true;
      this.dataService.deleteContractAll(this.data._id)
      .then(response => {
        $.notify({
          icon: "add_alert", message: "Contrato Eliminado con exíto"
          },{
            type: 'successs', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
        this.spinner = false;
        this.dialogRef.close();
      })
      .catch(error => {
        console.log(error);
      });

  }

}
