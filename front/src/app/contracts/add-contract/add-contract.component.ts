import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {ContractService} from '../../providers/contracts/contracts.service';
import {CustomersService} from '../../providers/customers/customers.service';
import {PlanService} from '../../providers/plans/plan.service';
import {MikrotikService} from '../../providers/mikrotik/mikrotik.service';

import {FormControl, Validators} from '@angular/forms';
import {Contract} from '../../models/contract';
declare var $: any;

@Component({
  selector: 'app-add-contract',
  templateUrl: './add-contract.component.html',
  styleUrls: ['./add-contract.component.scss'],
  providers: [ ContractService, CustomersService, PlanService, MikrotikService ]
})

export class AddContractComponent implements OnInit {

  dataCustomers = [];
  dataPlans = [];
  dataMkt = [];

  data:Contract = {
    _id: null,
    customer: null,
    plan: null,
    state: null,
    mikrotik: null,
    interface: null,
    addressIp: null,
    networkMask: null,
    addressMac: null,
    observations: null,
    dateStart: null,
    price: null,
    company: null
  }

  spinner:boolean = false

  constructor(
    private contractService:ContractService,
    private customersService:CustomersService,
    private planService:PlanService,
    private mikrotikService:MikrotikService,
    public  dialogRef: MatDialogRef<AddContractComponent>,
  ){}

  ngOnInit() {
    this.getCustomers()
    this.getPlans()
    this.getMikrotiks()
  }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage(){
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('name') ? 'Not a valid' :
        '';
  }


  private onNoClick(): void {
    this.dialogRef.close();
  }


  private getCustomers(){
      
      this.spinner = true;
      this.customersService.getCustomers()
      .then(response => {
        const cust:any = response
        cust.forEach(cust => {
          this.dataCustomers.push(cust)
        });
        
        this.spinner = false;
      })
      .catch(error => {
        console.log(error);
      });   

  }


  private getPlans(){
    this.spinner = true;
      this.planService.getPlans()
      .then(response => {
        const plns:any = response
        plns.forEach(plns => {
          this.dataPlans.push(plns)
        });
        
        this.spinner = false;
      })
      .catch(error => {
        console.log(error);
      });
  }


  private getMikrotiks(){
    this.spinner = true;
      this.mikrotikService.getMikrotiks()
      .then(response => {
        const mkt:any = response
        mkt.forEach(mkt => {
          this.dataMkt.push(mkt)
        });
        this.spinner = false;
      })
      .catch(error => {
        console.log(error);
      });
  }


  private saveContract(){

    console.log('---here---');
    console.table(this.data);
    console.log('---here---');


      this.spinner = true;
      this.contractService.addContract(this.data)
      .then(response => {
        $.notify({
          icon: "add_alert", message: "Contracto guardado con exíto"
          },{
            type: 'successs', timer: 4000,
            placement: {
            from: 'top',
            align: 'right'
          }
        });
        this.spinner = false;
        this.dialogRef.close();
      })
      .catch(error => {
        console.log(error);
      });
  }



}
