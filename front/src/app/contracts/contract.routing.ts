import { Routes } from '@angular/router';
import { AuthGuard } from '../providers/guard/auth.guard'

import { ContractComponent } from '../contracts/contract.component';
import { AddContractComponent } from '../contracts/add-contract/add-contract.component';
import { EditContractComponent } from '../contracts/edit-contract/edit-contract.component';
import { DeleteContractComponent } from '../contracts/delete-contract/delete-contract.component';

export const ContratRoutes: Routes = [
    { path: 'contrats',      component: ContractComponent, canActivate:[AuthGuard] },
    { path: 'add-contrat',   component: AddContractComponent, canActivate:[AuthGuard] },
    { path: 'edit-contrat',  component: EditContractComponent, canActivate:[AuthGuard] },
    { path: 'delete-contrat',component: DeleteContractComponent, canActivate:[AuthGuard] }
];
