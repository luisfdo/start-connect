import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GoogleMaps, GoogleMap, Marker, Environment, GoogleMapOptions, GoogleMapsEvent } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  map: GoogleMap;
  latitude:any;
  longitude:any;
  addMarkerBan:boolean = false;

  constructor(public navCtrl: NavController, private geolocation: Geolocation) {
  }

  ionViewDidLoad(){
    // this.loadMap();
    // this.updateMarker();
  }

  loadMap(){

    this.geolocation.getCurrentPosition().then((resp) => {

      this.latitude  = resp.coords.latitude;
      this.longitude = resp.coords.longitude;

      let mapOptions: GoogleMapOptions = {
        camera: {
           target: {
            lat: this.latitude,
            lng: this.longitude
           },
           zoom: 18,
           tilt: 30
         }
      };
  
      this.map = GoogleMaps.create('map_canvas', mapOptions);
      this.addMarkerBan = true;
      this.addMarker();
      
    }).catch((error) => {
       console.log('Error getting location', error);
    });
  
  }

  addMarker(){
    if(this.addMarkerBan){

       
        let marker: Marker = this.map.addMarkerSync({
          title: 'Ionic',
          icon: 'blue',
          animation: 'DROP',
          position: {
            lat: this.latitude,
            lng: this.longitude
          }
        });
        marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
          alert('clicked');
        });
    }
  }

  // updateMarker(){
  //   let watch = this.geolocation.watchPosition();
  //   watch.subscribe((data) => {
  //     this.latitude  = data.coords.latitude
  //     this.longitude = data.coords.longitude
  //     //this.map.clear()
  //     this.addMarker()
  //   });
  // }

}
